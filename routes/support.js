/**
 * Created by Sundeep on 2/11/2015.
 */
var express = require('express');
var router = express.Router();
var getData =  require('../library/GetData4View');
var passport = require('../library/routesLib').passport;
var commonUserFunction = require('./helpers/commonUserFunction');
var supportHelper = require('./helpers/supportRoutesHelper');


// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function isOwner(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        if(req.user.clickdaily.userType){
            return next()
        }
    }
    res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}

router.get('/',isLoggedIn, function(req, res) {
    res.render('support'
        ,{
            type:"Support"
            ,title : "Support",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)
            ,err : req.flash('err')
            ,msg : req.flash('msg')
        });
});

router.post('/email',isLoggedIn,function(req, res){
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    supportHelper.email(lastLocationName + " : " + req.body.subject,req.body.message,function(err){
        if(err){
            req.flash('err', 'Failed!');
            return res.redirect('/support/');
        }
        req.flash('msg', 'Success!');
        return res.redirect('/support/');
    });
});

router.get('/forgotPassword',function (req,res) {
    res.render('forgotPassword' ,{message : req.flash('msg')});
});

router.post('/forgotPassword',function (req,res) {
    var email = req.body.username;
    supportHelper.forgetPass(email,function (err,username,newPass) {
        if(err){
            return setTimeout(function () {
                req.flash('msg', 'An email with new password has been sent to your email!');
                return res.redirect('/support/forgotPassword/');
            })
        }
        supportHelper.emailUser(
            email,
            "Reset Password Notification"
            ,"Hello," +
            "<br><br>Your Login informations is :" +
            "<br><br>" +
            "Username - <b>" + username +  "</b>" +
            "<br>Password - <b>"+ newPass + "</b>." +
            "<br><br>Thank you for contacting Clickdaily support!",
            function (err) {
                console.log("Email Sent : ",err);
                req.flash('msg', 'An email with new password has been sent to your email!');
                return res.redirect('/support/forgotPassword/');
            }
        )
    });
});

module.exports = router;
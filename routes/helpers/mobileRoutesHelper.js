/**
 * Created by Sundeep on 3/23/2015.
 */

var vars = require('../../library/socialHelpers/Vars/socialVars.js');
var QRcode  = require('../../models/socialMedia').QRcode;
var Social = require('../../models/socialMedia').Social;

var getLoyaltyRecordFromCache = function(cachedRecordId){
    cachedRecordId = cachedRecordId.split('.')[0];
    if((vars.loyaltyIdTranslation[cachedRecordId])&&(vars.loyaltyIdTranslation[cachedRecordId].record_id)){
        return vars.loyaltyIdTranslation[cachedRecordId];
    }
    return null;
};

//----------- /Qr -----------------------

function redeemQR(qr,locationId,done){
    var cachedRecordId = qr.substr(0,qr.length-10);
    var cachedRecord= getLoyaltyRecordFromCache(cachedRecordId);
    var loyalty_record_id = cachedRecord.record_id;
    var userId = cachedRecord.userId;
    var locId = cachedRecord.locId;
    var document = vars.users[locId];
    var imageName = cachedRecordId+"."+cachedRecord.type;
    if(locationIds!==locId){
        return done('Not from this location');
    }
    QRcode.findById(qr, function (err, doc) {
        if (err) return done("Database did not respond Try Again");
        if(!doc) return done("Doesn't Exist");
        if (doc.redeemed){
            return done("Already Redeemed", {
                redeem_time              : doc.redeemTime,
                profile_name             : doc.profileName,
                email                    : doc.email,
                picture                  : doc.pic_large,
                promo_pic                : "https://merchant.clickdaily.com/loyalty_image/"+imageName
            });
        }
        doc.redeemed = true;
        doc.redeemTime = new Date();
        doc.save(function (err, doc) {
            if (err) console.log("Database did not respond Try Again");
            document.loyalty[0].loyalty_record.id(loyalty_record_id).redeemed += 1;
            document.save(function(err,loyaltyDoc){
                var redeemDoc = {
                    discount                : loyaltyDoc.loyalty[0].loyalty_record.id(loyalty_record_id).discount,
                    user                    : loyaltyDoc.loyalty[0].loyalty_record.id(loyalty_record_id).user,
                    redeemed                : loyaltyDoc.loyalty[0].loyalty_record.id(loyalty_record_id).redeemed,
                    created_at              : loyaltyDoc.loyalty[0].loyalty_record.id(loyalty_record_id).created_at,
                    redeem_time             : doc.redeemTime,
                    profile_name            : doc.profileName,
                    email                   : doc.email,
                    picture                 : doc.pic_large,
                    promo_pic               : "https://merchant.clickdaily.com/loyalty_image/"+imageName
                };
                if(doc.email){
                    var mailOptions = {
                        from: 'no-reply@clickdaily.com',
                        to: doc.email,
                        subject: 'Thanks for being a valued customer',
                        text: 'Thanks',
                        html : '<h1>Thanks</h1><br/>&copy;Clickdaily 2015'
                    };
                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            console.log(error);
                            done('Could Not Send Email',redeemDoc);
                        }else{
                            console.log('Message sent: ' + info.response);
                            done(null,redeemDoc);
                        }
                    });
                }else{

                    done(null,redeemDoc);
                }
            });
            //callback(null, doc);


        });
    });
}

function cacheLocation(userId,locId,done){
    if(!vars.users[locId]){
    }
    Social.findById(locId, function(err, social) {
        // if there are any errors, return the error
        if (err)
            return done(err);

        // check to see if theres already a cache for that userId
        if (social) {
            vars.users[locId] = social;
            done(true);
        }else{
            done(false);
        }

    })
}

exports.redeemQR                  = redeemQR;
exports.cacheLocation             = cacheLocation;
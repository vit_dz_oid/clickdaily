/**
 * Created by Sundeep on 5/28/2015.
 */

var MongoClient = require('mongodb').MongoClient;
var configDB = require('../../config/database.js');

var insertDocument = function(db, doc,callback) {
    db.collection('emails').insertOne( doc, function(err, result) {
        if(err){
            console.log("Error inserting to mongo : ",err);
            return callback(err)
        }
        console.log("Inserted a document into the restaurants collection.");
        callback(null,result);
    });
};

var findMails = function(db, callback) {
    var rs = [];
    var cursor =db.collection('emails').find( );
    cursor.each(function(err, doc) {
        if(err){
            console.log("Error fetching from mongo : ",err);
            return callback(err)
        }
        if (doc != null) {
            console.dir(doc);
            rs.push(doc);
        } else {
            callback(null,rs);
        }
    });
};

function saveMail(mail,callback){
    MongoClient.connect(configDB.url, function(err, db) {
        if(err){
            console.log("Error connecting to mongo : ",err);
            return callback(err);
        }
        insertDocument(db,mail,function(err,result){
            if(err){
                return callback(err);
            }
            console.log("Inserted with ID : ", result.id);
            db.close();
            callback(null,result);
        });
    });
}

function listMail(callback){
    MongoClient.connect(configDB.url, function(err, db) {
        if(err){
            console.log("Error connecting to mongo : ",err);
            return callback(err);
        }
        findMails(db,function(err,result){
            if(err){
                return callback(err);
            }
            db.close();
            callback(null,result);
        });
    });
}

exports.saveMail = saveMail;
exports.listMail = listMail;

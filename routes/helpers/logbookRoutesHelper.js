/**
 * Created by Sundeep on 3/13/2015.
 */

var Chance = require('chance');
var numeral = require('numeral');
var chance = new Chance();
var generateData = function(){
    var sale =  {
        
        gross:0,
        net:0,
        coverCount:chance.integer({min:200,max:500}),
        averageCheck:chance.floating({min:15,max:40,fixed: 2}),
        promo:chance.integer({min:0,max:20}),
        void:chance.integer({min:0,max:20}),
        labor:chance.integer({min:0,max:20})

    };

    sale.net = numeral((sale.coverCount * sale.averageCheck)).format('$ 0,0.00');
    sale.gross = numeral((sale.coverCount * sale.averageCheck * 1.8875)).format('$ 0,0.00');
    sale.coverCount = numeral(sale.coverCount ).format('0,0');
    sale.averageCheck = numeral(sale.averageCheck ).format('$ 0,0.00');
    sale.promo = numeral(sale.promo/100 ).format('0,0%');
    sale.void = numeral(sale.void ).format('0,0');
    sale.labor = numeral(sale.labor/100 ).format('0,0%');
    return sale;
};

exports.genrateData= generateData;

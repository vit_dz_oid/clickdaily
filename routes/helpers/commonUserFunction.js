/**
 * Created by Sundeep on 4/3/2015.
 */
var fs = require('fs');

var LocationName        = require("../../models/user").LocationName;
var async               = require("async");

function createString(user,path){
    var now = new Date();
    var str = now + ',';
    if(user){
        str = str + user.username + ',' + path;
    }else{
        str = str + path;
    }
    return str + '\n';
}

function logUser(user,path){
    var log_string;
    var filename = 'logs/users/';
    if((user)&&(user.clickdaily)){
        filename = filename + user.clickdaily.username+ '.csv';
        log_string = createString(user.clickdaily,path);
    }else{
        filename = filename + 'noUser.csv';
        log_string = createString(null,path);
    }
    fs.appendFile(filename,log_string,function(err){
       if(err)  {
           return console.log("error : ", err);
       }
        console.log("logged : ",log_string)
    });
}

function getLocationName(id,done){
    LocationName.find({ SQLid : id},function(err,location){
        if(err){
            return done(err);
        }
        if((location)&&(location.length)){
            location = location[0].toObject();
            var loc = {
                id : location.SQLid
                ,name :location.name
                ,lat : location.lat
                ,lon : location.lon
                ,address : location.address
            };
            return done(null, loc);
        }
        return done(new Error("No Location found"));
    });
}

function viewObject(user,lastLoc,done){
   
    if((user)&&(user.clickdaily)){
        var ids = user.clickdaily.locations.map(function(d,i){
            return d._doc._id;
        });
        async.map(ids, getLocationName, function(err, locations){
            // results is now an array of stats for each file

            var lastLocationId;
            if(lastLoc){
                lastLocationId = lastLoc;
            } else {
                lastLocationId = user.clickdaily.lastLocationId;
            }
            var lastLocation = user.clickdaily.locations.id(lastLocationId);
            var modules = lastLocation.modules.toObject();
            locations = locations.filter(function(d){
               return (d?(d.id && d.name):false);
            });
            locations = locations.sort(function(a,b){
                return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
            });
            if(typeof (done)=="function") {
                return done(null, {
                    location: {
                        all: locations
                        , selected: lastLocationId
                    },
                    name: user.clickdaily.firstname,
                    modules: modules,
                    isTwitter : user.clickdaily.username == "twitterdemo"
                });
            }
        });

    }else{
        if(typeof (done)=="function") {
            return done(null, {
                location: {
                    all: []
                    , selected: ''
                },
                name: '',
                modules: {}
            });
        } else {
            return {
                location: {
                    all: []
                    , selected: ''
                },
                name: '',
                modules: {}
            };
        }
    }

}

exports.log = logUser;
exports.viewObject = viewObject;
/**
 * Created by Sundeep on 3/23/2015.
 */
var User             = require('../../models/user').User;

var resetPass = function(username,  body,done){
    User.findOne({ 'clickdaily.username' :  username }, function(err, user) {
        // if there are any errors, return the error
        if (err)
            return done(err);

        // check to see if theres already a user with that email
        if (user) {
            if(body.newpass !== body.confirmpass){
                return done(true, 'Password do not match.')
            }
            if(user.validPassword(body.oldpass)){
                user.clickdaily.password = user.generateHash(body.newpass)
                user.clickdaily.firstLogin = false;
                user.save(function(err){
                    if(err)  return done (err,'Could not save try again')
                    return done(null);
                });

            }else{
                return done(true, 'Wrong Password.!');
            }
        } else {
            return done(true, 'Cannot change password, user doesnt exist');
        }
    });
};



exports.resetPass = resetPass;
/**
 * Created by Sundeep on 5/5/2015.
 */

var nodemailer       = require('nodemailer');
var User             = require('../../models/user').User;
var generatePassword = require('password-generator');

var transporter = nodemailer.createTransport({
    host: "smtpout.secureserver.net",
    name: "merchant.clickdaily.com",
    port: 465,
    secure: true,
    auth: {
        user: 'no-reply@clickdaily.com',
        pass: '3615clickdaily'
    },
    tls: {rejectUnauthorized: false}
});

var email = function(subject,msg,done){
    var mailOptions = {
        from    : 'no-reply@clickdaily.com',
        to      : ['sundeep@clickdaily.com', 'g.denoue@clickdaily.com', 'mike@clickdaily.com'],
        subject : 'Clickdaily Support : ' + subject ,
        text    : msg
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error);
            done(true);
        }else{
            console.log('Message sent: ' + info.response);
            done(null);
        }
    });
};

var emailUser = function(email,subject,msg,done){
    var mailOptions = {
        from    : 'no-reply@clickdaily.com',
        to      : email,
        bcc     : ['sundeep@clickdaily.com','mike@clickdaily.com'],
        subject : 'Clickdaily Support : ' + subject ,
        html    : msg
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error);
            done(true);
        }else{
            console.log('Message sent: ' + info.response);
            done(null);
        }
    });
};

var forgetPass = function(email,done){
    User.findOne({ 'clickdaily.email' :  email }, function(err, user) {
        // if there are any errors, return the error
        if (err) {
            return done(err);
        }
        // check to see if theres already a user with that email

        if (user) {
            var newPass = generatePassword(10, false);
            user.clickdaily.password = user.generateHash(newPass);
            user.save(function(err){
                if(err)  return done (err,'Could not save try again');
                return done(null,user.clickdaily.username,newPass);
            });
        } else {
            return done(true);
        }
    });
};

exports.email       = email;
exports.emailUser   = emailUser;
exports.forgetPass  = forgetPass;
/**
 * Created by Sundeep on 3/4/2015.
 */

var path = require('path');
var vars = require('../../library/socialHelpers/Vars/socialVars.js');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialStatusRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var QRcode  = require('../../models/socialMedia').QRcode;
var socialVars = require('./socialRoutesVars');

var status = function(err, locId,info,result, responseId){
    if((result.fb)||(result.tw)||(result.fs)||(result.it)){
        try{
            socialVars.responses[responseId].redirect("/Social/Dashboard")
        } catch(e){
            errorLog.log("Response Expired",e);
        }

    }else{
        try{
            socialVars.responses[responseId].redirect("/Social/First")
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
    delete socialVars.responses[responseId];
};

var list = function(userId, locId,done){
    var document = vars.users[locId];
    var list = null;
    if(document.loyalty.length){
        list = document.loyalty[0].loyalty_record.toObject({ getters: true });
        for(var i=0;i<list.length;i++){
            list[i].id = list[i]._id.toString();
            delete list[i]._id;
        }
    }
    done(list);
};

var detail = function(userId, locId,id,done){
    var document = vars.users[locId];
    var loyalty_record = document.loyalty[0].loyalty_record.id(id).toObject({ getters: true });
    loyalty_record.id = loyalty_record._id.toString();
    delete loyalty_record._id;
    QRcode.find({loyalty_record_id:id},function(err,results){
        var codes = results.map(function(d){
            return d.toObject({ getters: true })
       });
        
        done(loyalty_record,codes);
    });
};

var deleteL = function(userId, locId,id,done){
    var document = vars.users[locId];
    document.loyalty[0].loyalty_record.id(id).remove();
    document.save(function(err){
        if (err) return console.log(err);
    });
    QRcode.remove({loyalty_record_id:id},function(err){
        if (err) console.log(err);
        done(id);
    });
};

exports.list    = list;
exports.detail  = detail;
exports.deleteL = deleteL;
exports.status  = status;



/**
 * Created by Sundeep on 6/22/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialITRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.create = function(err,type,url, responseId){
    if(type == "redirect"){
        try{
            socialVars.responses[responseId].redirect(url);
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    } else {
        try{
            socialVars.responses[responseId].redirect("/Social/Dashboard");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
    delete socialVars.responses[responseId];
};

exports.delNetwork =  function(err, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].redirect("/Social/First");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};
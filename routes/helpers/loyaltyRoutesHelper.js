/**
 * Created by Sundeep on 3/5/2015.
 */

var qr = require('qr-image');
var async = require('async');
var nodemailer = require('nodemailer');

var vars = require('../../library/socialHelpers/Vars/socialVars.js');
var QRcode  = require('../../models/socialMedia').QRcode;
var FBRecord = require('../../models/loyaltyRecords').FBRecord;
var TWRecord = require('../../models/loyaltyRecords').TWRecord;
var random  = require('../../library/random');
var Facebook = require('../../library/socialHelpers/fb-mod');
var Twitter = require('../../library/socialHelpers/tw-mod');

var requestInfoTw = {};

var transporter = nodemailer.createTransport({
    host: "smtpout.secureserver.net",
    name: "merchant.clickdaily.com",
    port: 465,
    secure: true,
    auth: {
        user: 'no-reply@clickdaily.com',
        pass: '3615clickdaily'
    },
    tls: {rejectUnauthorized: false}
});


var email = function(code,emailId,done){
    var qr_png = qr.image(code, {ec_level: 'H', type: 'png' });
    //qr_svg.pipe(require('fs').createWriteStream(doc.qrcode+'.png'));
    var mailOptions = {
        from: 'no-reply@clickdaily.com',
        to: emailId,
        subject: 'Your Discount Code from Clickdaily',
        text: 'Your QR Code is - '+code,
        html : '<h1>Enjoy Your Discount!!</h1><br/><br/><img src="cid:"'+code+'"><br/>&copy;Clickdaily 2015',
        attachments: [
            {   // utf-8 string as an attachment
                filename: 'qrcode.png',
                content: qr_png,
                cid : code
            }
        ]
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error);
            done(true);
        }else{
            console.log('Message sent: ' + info.response);
            done(null);
        }
    });
};


//----------- /fb -----------------------

var getFBRedirectUrl = function(cachedRecord){
    var fb = new Facebook();
    var perimissions = "public_profile,email,user_birthday,user_likes";
    var loginUrl = fb.getLoginUrl(
        {
            client_id : "752078144824356",

            redirect_uri : "https://merchant.clickdaily.com/redeem/code/",

            response_type : "code",

            scope : perimissions,

            state : cachedRecord

        }
    );
    return loginUrl;
};

//----------- /tw -----------------------

var getTWRedirectUrl = function(cachedRecord,done){
    var config = {
        consumerKey : "lofJrNEpTBmGngqGNqaK5DmHP"
        , consumerSecret : "WJBMAPmSDveEBgpPBQLoCEekk6srHyOJNDG9aa4ziV7S2eCqeY"
        , callBackUrl : "https://merchant.clickdaily.com/redeem/twitter/"+cachedRecord
    };
    var tw = new Twitter(config);
    if(!requestInfoTw[cachedRecord]){
        requestInfoTw[cachedRecord] = [];
    }
    tw.oauth.getOAuthRequestToken(function(err,oauth_token,oauth_token_secret){
        if(err){
            console.log(err);
            done(err);
        }
        requestInfoTw[cachedRecord].push(
            {
                request_token : oauth_token,
                request_token_secret : oauth_token_secret
            });
        done(null,"https://api.twitter.com/oauth/authorize?oauth_token="+oauth_token);
    });

};


//--------------------- code --------------------------

var getLoyaltyRecordFromCache = function(cachedRecordId){
    cachedRecordId = cachedRecordId.split('.')[0];
    if((vars.loyaltyIdTranslation[cachedRecordId])&&(vars.loyaltyIdTranslation[cachedRecordId].record_id)){
        return vars.loyaltyIdTranslation[cachedRecordId];
    }
    return null;
};


var getTokenFromCode = function(code,cachedRecord,callback){
    var fb = new Facebook();
    fb.api('oauth/access_token', {
        client_id: "752078144824356",
        client_secret: "421973cc7e13cdf70bda0177a217f934",
        redirect_uri: "https://merchant.clickdaily.com/redeem/code/",
        code: code
    }, function (res) {
        if(!res || res.error) {
            console.log(!res ? 'error occurred' : res.error);
            return callback(!res ? 'Facebook replied with an error, Try Again!' : res.error);
        }
        var accessToken = res.access_token;
        callback(null,accessToken,cachedRecord);
    });
};

var getProfile = function(token,cachedRecord,callback){
    var fb = new Facebook();
    fb.setAccessToken(token);
    var pagedId = cachedRecord.pageID;
    fb.api('me', {
        fields:'picture.type(large).redirect(0),id,name,birthday,email,link,location,likes.target_id('+pagedId+'),first_name,last_name,middle_name'
    } , function (result) {
        if (!result || result.error) {
            return callback(!result ? 'Facebook replied with an error, Try Again!' : result.error);
        }
        if((result.likes)&&(result.likes.data)&&(result.likes.data.length)&&(result.likes.data[0])&&(result.likes.data[0].id==pagedId)){
            return callback(null,token,cachedRecord,result);
        }
        FBRecord.findById(result.id,function(err,record) {
            if (record) {
                return callback('Please Like this Page and try again','redirect',"https://www.facebook.com/"+ pagedId);
            } else {
                result._id = result.id;
                record = new FBRecord(result);
                record.token = token;
                return callback('Please Like this Page and try again','redirect',"https://www.facebook.com/"+ pagedId);
            }
        });


    });
};

var checkDuplicate = function(token,cachedRecord,result,callback){
    FBRecord.findById(result.id,function(err,record){
        if(record){
            if(record.QRcodes.id(cachedRecord.record_id)){
                if(record.QRcodes.id(cachedRecord.record_id).code){
                    return callback("You have Already redeemed the following QRCode",'show',record.QRcodes.id(cachedRecord.record_id).code);
                }
                return callback(null,record,cachedRecord)
            }
            record.QRcodes.push({ _id: cachedRecord.record_id });
            record.token = token;
            return callback(null,record,cachedRecord)
        }else{
            result._id = result.id;
            record = new FBRecord(result);
            record.QRcodes.push({ _id: cachedRecord.record_id });
            record.token = token;
            return callback(null,record,cachedRecord)
        }
    });
};

var generate = function(loyaltyRecord,cachedRecord,callback){
    var loyalty_record_id = cachedRecord.record_id;
    var userId = cachedRecord.userId;
    var locId = cachedRecord.locId;
    var document = vars.users[locId];
    if (document.loyalty[0].loyalty_record.id(loyalty_record_id).user > document.loyalty[0].loyalty_record.id(loyalty_record_id).issued) {
        document.loyalty[0].loyalty_record.id(loyalty_record_id).issued += 1;
        document.save(function (err) {
            //if ((err)) return callback(err);
            qrcode_id = document.loyalty[0].loyalty_record.id(loyalty_record_id).QRcodes[document.loyalty[0].loyalty_record.id(loyalty_record_id).issued - 1];
            QRcode.findById(qrcode_id, function (err, doc) {
                if (err) return callback(err);
                if (doc.issued) return callback("Offer Expired");
                doc.issued = true;
                if(loyaltyRecord.type  == "FB"){
                    doc.network = "FB";
                    doc.profileName = loyaltyRecord.name;
                    doc.profileId = loyaltyRecord.id;
                    doc.pic_large = loyaltyRecord.profilePicture;
                    doc.email = loyaltyRecord.email;
                }else if(loyaltyRecord.type  == "TW"){
                    doc.network = "TW";
                    doc.profileName = loyaltyRecord.name;
                    doc.profileId = loyaltyRecord.id;
                    doc.pic_large = loyaltyRecord.picture;
                }

                doc.qrcode = document.loyalty[0].loyalty_record.id(loyalty_record_id).cachedRecordId + random(10);
                loyaltyRecord.QRcodes.id(loyalty_record_id).code = doc.qrcode;
                doc.save(function (err, doc) {
                    if (err) callback(err);
                    callback(null, doc.qrcode);
                });
                loyaltyRecord.markModified('QRcodes');
                loyaltyRecord.save(function (err) {
                    if (err) console.log(err);
                });

            });
        })

    } else {
        callback("Offer Expired");
    }
};

function sendQR(code, cachedRecord, done){
        async.waterfall([
            function(callback){
                callback(null,code,cachedRecord);
            },
            getTokenFromCode,
            getProfile,
            checkDuplicate,
            generate
        ],done)

}

//--------------------- tw/:id --------------------------

var getTokenFromCodeTW = function(reqToken,code,cachedRecord,cachedRecordId,callback){
    var config = {
        consumerKey : "lofJrNEpTBmGngqGNqaK5DmHP"
        , consumerSecret : "WJBMAPmSDveEBgpPBQLoCEekk6srHyOJNDG9aa4ziV7S2eCqeY"
        , callBackUrl : "https://merchant.clickdaily.com/redeem/twitter/"+cachedRecord
    };
    var tw = new Twitter(config);
    if(!requestInfoTw[cachedRecordId]){
        return callback('Twitter replied with an error, Try Again!')
    }
    var requestObject = requestInfoTw[cachedRecordId].filter(function(d,i){
       if(d.request_token==reqToken) return d;
    });
    if(requestObject.length==1){
        requestObject = requestObject[0];
    }else{
        return callback('Twitter replied with an error, Try Again!')
    }
    tw.oauth.getOAuthAccessToken(
        requestObject.request_token
        , requestObject.request_token_secret
        , code
        ,function(err, oauth_access_token, oauth_access_token_secret, results ) {

            if (err) {
                console.log(err);
                return callback('Twitter replied with an error, Try Again!')
            }
            var TWinfo = {};
            TWinfo.token = oauth_access_token;
            TWinfo.token_secret = oauth_access_token_secret;
            TWinfo._id = results.user_id;
            TWinfo.screen_name = results.screen_name;
            callback(null, TWinfo, cachedRecord);
        });
};

var getProfileTW = function(TWinfo,cachedRecord,callback){
    var config = {
        consumerKey : "lofJrNEpTBmGngqGNqaK5DmHP"
        , consumerSecret : "WJBMAPmSDveEBgpPBQLoCEekk6srHyOJNDG9aa4ziV7S2eCqeY"
        , accessToken : TWinfo.token
        , accessTokenSecret : TWinfo.token_secret
        , callBackUrl : "https://merchant.clickdaily.com/redeem/twitter/"+cachedRecord
    };
    var tw = new Twitter(config);
    tw.getTweet({ id: cachedRecord.twPostID}
        ,function(err){
            console.log(err);
            var TWRecordDB = new TWRecord(TWinfo);
            TWRecordDB.save(function(err){
               console.log("saved with error - ",err);
            });
            return callback('Twitter replied with an error, Try Again!')
        },
        function(res){
            res = JSON.parse(res);
            if(res.retweeted){
                return callback(null,TWinfo,cachedRecord);
            }
            TWRecord.findById(TWinfo._id,function(err,record) {
                if (record) {
                    return callback('Please Like this Page and try again','redirect',"https://www.facebook.com/"+ pagedId);
                } else {
                    record = new TWRecord(TWinfo);
                    record.save(function (err) {
                        return callback('Please Retweet and try again this Page and try again');
                    });
                }
            });
        });
};

var checkDuplicateTW = function(TWinfo,cachedRecord,callback){
    TWRecord.findById(TWinfo._id,function(err,record){
        if(record){
            if(record.QRcodes.id(cachedRecord.record_id)){
                if(record.QRcodes.id(cachedRecord.record_id).code){
                    return callback("You have Already redeemed the following QRCode",'show',record.QRcodes.id(cachedRecord.record_id).code);
                }
                return callback(null,record,cachedRecord)
            }
            record.QRcodes.push({ _id: cachedRecord.record_id });
            return callback(null,record,cachedRecord)
        }else{
            var config = {
                consumerKey : "lofJrNEpTBmGngqGNqaK5DmHP"
                , consumerSecret : "WJBMAPmSDveEBgpPBQLoCEekk6srHyOJNDG9aa4ziV7S2eCqeY"
                , accessToken : TWinfo.token
                , accessTokenSecret : TWinfo.token_secret
                , callBackUrl : "https://merchant.clickdaily.com/redeem/twitter/"+cachedRecord
            };
            var tw = new Twitter(config);
            tw.getUser({ user_id: TWinfo._id}
                ,function(err){
                    console.log(err);
                    var TWRecordDB = new TWRecord(TWinfo);
                    TWRecordDB.save(function(err){
                        console.log("saved with error - ",err);
                    });
                    return callback('Twitter replied with an error, Try Again!')
                },
                function(res){
                    res = JSON.parse(res);
                    TWinfo.screen_name = res.screen_name;
                    TWinfo.picture = res.profile_image_url_https;
                    TWinfo.name = res.name;
                    TWinfo.location = res.location;
                    record = new TWRecord(TWinfo);
                    record.QRcodes.push({ _id: cachedRecord.record_id });
                    record.token = token;
                    return callback(null,record,cachedRecord)
                });
        }
    });
};

function sendQRTW(reqToken, code, cachedRecord,id, done){
    async.waterfall([
        function(callback){
            callback(null,reqToken, code,cachedRecord,id);
        },
        getTokenFromCodeTW,
        getProfileTW,
        checkDuplicateTW,
        generate
    ],done)

}

//----------- /Qr -----------------------

function redeemQR(qr,locationIds,locId,userId,done){
    var cachedRecordId = qr.substr(0,qr.length-10);
    var cachedRecord= getLoyaltyRecordFromCache(cachedRecordId);
    if(cachedRecord.locId!=locId){
        if(locationIds.indexOf(locId)==-1){
            return done('Not Authorised');
        }
        locId = cachedRecord.locId;
    }
    QRcode.findById(qr, function (err, doc) {
        if (err) return callback(err);
        if(!doc) return callback("Doesn't Exist");
        if (doc.redeemed) return callback("Already Redeemed", doc);
        doc.redeemed = true;
        doc.redeemTime = new Date();
        doc.save(function (err, doc) {
            if (err) console.log(err);
            //callback(null, doc);
        });
        if(doc.email){
            var mailOptions = {
                from: 'no-reply@clickdaily.com',
                to: doc.email,
                subject: 'Thanks for being a valued customer',
                text: 'Thanks',
                html : '<h1>Thanks</h1><br/>&copy;Clickdaily 2015'
            };
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                    console.log(error);
                    done(true);
                }else{
                    console.log('Message sent: ' + info.response);
                    done(null);
                }
            });
        }
        done(null);
    });
}

exports.sendQR                    = sendQR;
exports.redeemQR                  = redeemQR;
exports.email                     = email;
exports.getFBRedirectUrl          = getFBRedirectUrl;
exports.getLoyaltyRecordFromCache = getLoyaltyRecordFromCache;
exports.getTWRedirectUrl          = getTWRedirectUrl;
exports.sendQRTW                  = sendQRTW;







/**
 * Created by Sundeep on 1/26/2015.
 */
var _ = require('lodash');
var async = require('async');
var uuid = require('node-uuid');
var express = require('express');
var util    = require('util');
var redisRPCLib         = require("redis-rpc");

var router = express.Router();
var passport = require('../library/routesLib').passport;
var socialVars = require('./helpers/socialRoutesVars');
var commonUserFunction = require('./helpers/commonUserFunction');

var socialStatusHelper = require('./helpers/socialRoutesHelper');
var socialFBHelper = require('./helpers/socialFBRoutesHelper');
var socialTWHelper = require('./helpers/socialTWRoutesHelper');
var socialFSHelper = require('./helpers/socialFSRoutesHelper');
var socialITHelper = require('./helpers/socialITRoutesHelper');

var moduleString = 'routesDispatcher';

var itRPC = new redisRPCLib({
    subModule : moduleString + "it",
    pubModule : "it" + moduleString,
    tasks     : socialITHelper
});

var fsRPC = new redisRPCLib({
    subModule : moduleString + "fs",
    pubModule : "fs" + moduleString,
    tasks     : socialFSHelper
});

var twRPC = new redisRPCLib({
    subModule : moduleString + "tw",
    pubModule : "tw" + moduleString,
    tasks     : socialTWHelper
});


var fbRPC = new redisRPCLib({
    subModule : moduleString + "fb",
    pubModule : "fb" + moduleString,
    tasks     : socialFBHelper
});

var statusRPC = new redisRPCLib({
    subModule : moduleString + "status",
    pubModule : "status" + moduleString ,
    tasks     : socialStatusHelper
});

function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    if (req.isAuthenticated()){
        if((req.user.clickdaily.lastLocation)
            &&(req.user.clickdaily.lastLocation.modules)
            &&(req.user.clickdaily.lastLocation.modules.Social)){
            return next();
        }else{
            res.status(404).render('error404');
        }
    }
    res.redirect('/');
}

function isNotLoggedIn(req, res, next) {
    if (!req.isAuthenticated())
        return next();
    res.redirect('/');
}

function getLastLocation(req){
    return req.session.lastLoc;
}

router.get('/', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locId  = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    statusRPC.sendCall({
        task        : "status",
        args        : [locId,{}],
        sessionId   : responseId
    });
});

// Dashboard Section =========================
router.get('/split', isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('dashboard', {
            type: "Social",
            subtitle: "Split",
            title: "Timelines",
            layoutObject: viewObject
        });
    });
});

router.get('/merged', isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('merged', {
            type: "Social",
            subtitle: "Merged",
            title: "Timelines",
            layoutObject: viewObject
        });
    });
});

router.get('/first', isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('first', {
            type: "Social",
            title: "Timelines",
            layoutObject: viewObject
        });
    });
});
/*
router.get('/Loyalty/Create', isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,function(err,viewObject) {
        ifreq.session.lastLoc,(err){
            return res.sendStatus(500);
        }
        res.render('loyalty_create', {
            type: "Social",
            subtitle: "Create",
            title: "Loyalty",
            layoutObject: viewObject
        });
    });
});

router.get('/Loyalty/List', isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,function(err,viewObject) {
        ifreq.session.lastLoc,(err){
            return res.sendStatus(500);
        }
        socialStatusHelper.list(userId,req.user.clickdaily.lastLocationId,function(list){
            res.render('loyalty_list', {
                type: "Social",
                subtitle: "List",
                title: "Loyalty",
                list:list,
                layoutObject: viewObject
            });
        });
    });
});

router.get('/Loyalty/Posts', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    socialStatusHelper.detail(userId,req.user.clickdaily.lastLocationId,req.query.id,function(list,codes){
        res.render('loyalty_detail',{type:"Social",title:"Loyalty",subtitle:"List",list:list,code:codes,layoutObject:commonUserFunction.viewObject(req.user)});
    });
});

router.get('/Loyaltyreq.session.lastLoc,/Delete', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    socialHelper.deleteL(userId,req.user.clickdaily.lastLocationId,req.query.id,function(id){
        res.redirect('/Loyalty/List');
    });
});
*/
// Social Section =========================================

// Facebook

router.get('/Facebook', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locId  = getLastLocation(req);

    var perimissions = "public_profile,user_friends,email,user_about_me,user_activities";
    perimissions = perimissions + ",user_birthday,user_education_history,user_events";
    perimissions = perimissions + ",user_groups,user_hometown,user_interests,user_likes";
    perimissions = perimissions + ",user_location,user_photos,user_relationships,user_relationship_details";
    perimissions = perimissions + ",user_religion_politics,user_status,user_tagged_places,user_videos";
    perimissions = perimissions + ",user_website,user_work_history,read_friendlists,read_insights";
    perimissions = perimissions + ",read_mailbox,read_stream,manage_notifications,publish_actions,rsvp_event";
    perimissions = perimissions + ",publish_actions,user_actions.books,user_actions.fitness,user_actions.music";
    perimissions = perimissions + ",user_actions.news,user_actions.video,manage_pages,read_page_mailboxes";

    var loginUrl = 'https://www.facebook.com/dialog/oauth'
        + '?response_type=code'
        +  '&scope=' + encodeURIComponent(perimissions)
        + '&redirect_uri=' + encodeURIComponent("https://merchant.clickdaily.com/Social/FBAuth")
        + '&client_id=' + "752078144824356";

    res.redirect(loginUrl);
});

router.get('/FBAuth', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locId  = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "create",
        args        : [locId,req.query.code,req.query.error],
        sessionId   : responseId
    });
});

router.get('/FB/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});

// Twitter

router.get('/Twitter',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "create",
        args        : [locId,req.query.oauth_verifier],
        sessionId   : responseId
    });

});

router.get('/TW/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/Instagram',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "create",
        args        : [locId,req.url,!!(req.query.code)],
        sessionId   : responseId
    });

});

router.get('/IT/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});


router.get('/Foursquare',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req);
    if(typeof(vars.users[locId])==="undefined"){
        res.redirect('/');
    }
    var done = function(){
        res.redirect('/Social/Dashboard');
    };
    var error = function(){
        res.redirect('/error');
    };

    var document = vars.users[locId];
    if(!document.FS[0]){
        var newFS = new FS();
        document.FS.push(newFS);
    }
    if((typeof(document.FS[0].token)==="undefined")&&(typeof(req.query.code)==="undefined")) {
        url = document.FS[0].helper.getAuthClientRedirectUrl();
        res.redirect(url);
    }else if(req.query.code){
        document.FS[0].helper.getAccessToken({
            code: req.query.code
        }, function (err, accessToken) {
            if(err){
                console.log(err);
                return error();
            }
            document.FS[0].token = accessToken;
            document.FS[0].helper.Users.getUser('self', accessToken, function (err,result) {
                if (err) {
                    console.log('error occurred' , err);
                    return;
                }

                result = result.user;
                if(result.type=="venuePage"){
                    //document.FS[0].remove();

                }
                res.redirect('/Social/Dashboard');
                document.save(function(err,doc){
                    if(err) console.log("error : ",err);
                });
            });
            //res.redirect('/Social/Dashboard');
        });
    } else{
        res.redirect('/Social/Dashboard');
    }

});

router.get('/Connections',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('connections', {
            type: "Social",
            title: "Connections",
            layoutObject: viewObject
        });
    });
});

router.get('/Dashboard',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('feedback', {
            type: "Social",
            title: "Dashboard",
            layoutObject: viewObject
        });
    });
});

router.get('/Suggestions',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        if(viewObject.isTwitter){
            return res.sendStatus(404);
        }
        res.render('suggestions', {
            type: "Social",
            title: "Potential Customers",
            layoutObject: viewObject
        });
    });
});

router.get('/schedule',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('socialSchedule', {
            type: "Social",
            title: "Schedule",
            skipButtons:true,
            layoutObject: viewObject
        });
    });
});

router.get('/insights',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('insights', {
            type: "Social",
            title: "Insights",
            skipButtons:true,
            layoutObject: viewObject
        });
    });
});

router.get('/keywords',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('keywords', {
            type: "Social",
            title: "Keywords",
            skipButtons:true,
            layoutObject: viewObject
        });
    });
});

router.get('/ShortUrls',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('shortURLs', {
            type: "Social",
            title: "Short URLs",
            skipButtons:true,
            layoutObject: viewObject
        });
    });
});

router.get('/changeLocation',isLoggedIn, function(req, res) {
    var locations =req.user.clickdaily.locations.map(function (d) {
        return d._id;
    });
    var newLoc = req.query.id;
    if(locations.indexOf(newLoc)==-1){
        return res.send("false");
    }
    req.session.lastLoc = newLoc;
   // req.session.save();
    res.send("true");
});


router.get('/Statistics',isLoggedIn, function(req, res) {
    var blocks = [
        {name:"Likes",class:"fa-thumbs-up",graphId:"likes-graph",numberId:"likes-number"},
        {name:"Comments",class:"fa-comments",graphId:"comments-graph",numberId:"comments-number"},
        {name:"Posts",class:"fa-picture-o",graphId:"posts-graph",numberId:"posts-number"},
        {name:"Followers", class:"fa-arrow-right",graphId:"followers-graph",numberId:"followers-number"},
        {name:"Following",class:"fa-arrow-left",graphId:"following-graph",numberId:"following-number"},
        {name:"Growth Followers",class:"fa-arrow-circle-right",graphId:"growth-graph",numberId:"growth-number"}
    ];
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('statistics', {
            type: "Social",
            title: "Statistics",
            skipButtons:true,
            layoutObject: viewObject,
            blocks : blocks
        });
    });
});

module.exports = router;





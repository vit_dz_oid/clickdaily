/**
 * Created by Sundeep on 5/28/2015.
 */

var express = require('express');
var router = express.Router();
var passport = require('../library/routesLib').passport;
var commonUserFunction = require('./helpers/commonUserFunction');
var mailHelper = require('./helpers/mailRoutesHelper');
var multiparty = require('multiparty');
var util = require('util');
var fs = require('fs');

function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

function isSuperAdmin(req, res, next) {
    if (req.user.clickdaily.username == 'sundeepnarang'){
        return next();
    }
    res.redirect('/');
}

router.post('/', function(req, res) {
    if(req.get('X-Real-IP')=="127.0.0.1") {
        var form = new multiparty.Form({
            maxFieldsSize: 70000000
        });

        form.parse(req, function (err, fields) {
            if (err) {
                console.log(err.stack);
                return res.send(500, 'Unable to read form');
            }
            console.log(util.inspect(fields.mailinMsg, {
                depth: 5
            }));
            console.log('Parsed fields: ' + Object.keys(fields));
            mailHelper.saveMail(fields, function (err, result) {
                if (err) {
                    console.log(err.stack);
                    return res.send(500, 'Unable save form');
                }
                res.send(true);
            });
        });
    }else {
        var data = util.inspect(req)
            +"\n\n--------------------------------------------\n\n";
        fs.appendFile('MailsRouteHackAttempts.txt',data,function(err){
           return res.sendStatus(404);
        });
    }
});

router.get('/list',isLoggedIn, isSuperAdmin, function(req, res) {
    mailHelper.listMail(function(err,result){
        res.send(result);
    });
});

router.head('/', function(req, res) {
      res.sendStatus(200);
});

router.get('/', function(req, res) {
      res.sendStatus(200);
});

router.options('/', function(req, res) {
      res.sendStatus(200);
});

module.exports = router;
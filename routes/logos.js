/**
 * Created by Sundeep on 2/11/2015.
 */
var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var commonUserFunction = require('./helpers/commonUserFunction');

// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}


router.get('/:id',isLoggedIn, function(req, res) {
    var logoFileName = path.basename(req.params.id);
    if(logoFileName!=req.params.id){
        var log = {
            user : req.user,
            path : req.params.id
        };
        var logStr = "[\nTime : " + (new Date()).toISOString()
            + "\n\nlogObject:" + JSON.stringify(log) + "\n\n]\n\n";
        return fs.writeFile("pathHack.log",logStr,function(err){
            res.sendStatus(404);
        })
    }
    if(logoFileName==req.user.clickdaily.lastLocationId){
        var logoPath = path.normalize(__dirname+"/../public/logos/"+logoFileName+".png");
        fs.stat(logoPath,function(err,stat){
           if(err){
               return res.sendStatus(404);
           }
            return res.sendFile(logoPath)
        });
    } else {
        return res.sendStatus(403);
    }
});

module.exports = router;
/**
 * Created by Sundeep on 2/10/2015.
 */

var express = require('express');
var router = express.Router();
var getData =  require('../library/GetData4View');
var passport = require('../library/routesLib').passport;
var commonUserFunction = require('./helpers/commonUserFunction');

// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }
    // if they aren't redirect them to the home page
    res.redirect('/');
}

function isOwner(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        if(req.use.userType===1){
            return next()
        }
    }
    res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}

router.get('/',isLoggedIn, function(req, res) {
    res.redirect('/SalesSummaryReport');
});

router.get('/SalesSummaryReport',isLoggedIn, function(req, res) {
    //9e527d10e9064aa3a6c2aa8774b43178
    //d769129267cd4dd7936e1419334bb81f

    //dc0987c11d6042ecb72d7eae3dd08ff7

    var FilterType = 1;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    var time = new Date();
    if(time.getHours()>12) FilterType = 2;
    getData.getSalesSummaryReport(req.user.clickdaily.lastLocationId,FilterType,"2014-12-10 00:00:00.000","2014-12-10 00:00:00.000",function(SalesPaymentReport,SalesRevenueReport,SalesTaxReport){
        res.render('sales',{type:"Reports",title : "Sales Report",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc),SalesPaymentReport:SalesPaymentReport,SalesRevenueReport:SalesRevenueReport,SalesTaxReport:SalesTaxReport,FilterType:FilterType});
    });

});

router.get('/ItemsReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Items Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/SalesCategoryReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Sales Category Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});
router.get('/WaiterReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Waiter/Tips Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/LaborReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Labor Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/TranctionsReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Tranctions Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/SalesTypeStatisticsReports',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Reports",title : "Sales Type Statistics Reports",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

module.exports = router;
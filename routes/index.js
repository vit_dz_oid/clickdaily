var express = require('express');
var router = express.Router();
var getData =  require('../library/GetData4View');
var passport = require('../library/routesLib').passport;
var util    = require('util');
var commonUserFunction = require('./helpers/commonUserFunction');

// =====================================
// MIDDLEWARE TO CHECK USER ============
// =====================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
    return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function isOwner(req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated()){
    if(req.user.clickdaily.userType){
      return next()
    }
  }
  res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}
// =====================================
// HOME PAGE (with login links) ========
// =====================================
router.get('/', function(req, res) {
  res.redirect('/login'); // load the index.ejs file
});

// =====================================
// LOGIN ===============================
// =====================================
// show the login form
router.get('/login', function(req, res) {

  // render the page and pass in any flash data if it exists
  res.render('login.jade', { message: req.flash('loginMessage') });
});

// process the login form
router.post('/login', passport.authenticate('local-login', {

  failureRedirect : '/login', // redirect back to the signup page if there is an error
  failureFlash : true // allow flash messages
}),function(req,res){
    if(req.user.clickdaily.locations.length==0) {
        return res.render("noLocation")
    }
    if(!req.user.clickdaily.lastLocationId){
        req.user.clickdaily.lastLocationId = req.user.clickdaily.locations[0]._id;
        return req.user.save(function(){
            if(req.user.clickdaily.firstLogin){
                return res.redirect('/Settings/ResetPassword/');
            }
            return res.redirect('/Social/')
        })
    }
    if(req.user.clickdaily.firstLogin){
        return res.redirect('/Settings/ResetPassword/');
    }
    return res.redirect('/Social/')
});

router.get('/noLocation', isLoggedIn, function(req,res){
    res.render("noLocation");
});


// =====================================
// SIGNUP ==============================
// =====================================
// show the signup form
/*router.get('/signup', function(req, res) {

  // render the page and pass in any flash data if it exists
  res.render('signup.jade', { message: req.flash('signupMessage') });
});

// process the signup form
router.post('/signup', passport.authenticate('local-signup', {
  successRedirect : '/profile', // redirect to the secure profile section
  failureRedirect : '/signup', // redirect back to the signup page if there is an error
  failureFlash : true // allow flash messages
}));*/


// =====================================
// PROFILE SECTION =====================
// =====================================
// we will want this protected so you have to be logged in to visit
// we will use route middleware to verify this (the isLoggedIn function)
/*router.get('/profile', isLoggedIn, function(req, res) {
  res.render('profile.jade', {
    user : req.user // get the user out of session and pass to template
  });
});
*/
// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

/* GET SalesSummaryReport */

module.exports = router;


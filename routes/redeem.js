/**
 * Created by Sundeep on 2/27/2015.
 */
var express = require('express');
var router = express.Router();
var Step = require('step');
var loyaltyHelper = require('./helpers/loyaltyRoutesHelper');
var Facebook = require('../library/socialHelpers/fb-mod');
var commonUserFunction = require('./helpers/commonUserFunction');

function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    if (req.isAuthenticated()){
        return next();
    }
    return res.status(404).end();
}


router.get('/', function(req,res){
    var id = req.query.id;
    if(id){
        id = id.split('.')[0];
        if(req.headers.referer.indexOf("://t.co/")!=-1){
            return res.render('redeemRedirect',{code:id,network:'tw'});
        }
        if(req.headers.referer.indexOf("://www.facebook.com/")!=-1){
            return res.render('redeemRedirect',{code:id,network:'fb'});
        }
        return res.render('redeemRedirect',{code:id});
    }
    return res.status(404).end();
});

router.get('/qr/:code', isLoggedIn,function(req,res){
    var qr = req.param.code;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var locationIds = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.id;
    });
    var userId = req.user.id;
    var locId  = req.user.clickdaily.lastLocationId;
    var lastLocation = req.user.clickdaily.locations.id(req.user.clickdaily.lastLocationId).name;
    if(qr){
        loyaltyHelper.redeemQR(qr,locationIds,locId,userId,function(err,res){
            res.render('logbook',{type:"Logbook",title : "Logbook",location : {all : locations, selected : lastLocation}});
        });
    }

    return res.status(404).end();
});

router.get('/fb', function(req,res){
    var id = req.query.id;
    if(id){
        res.redirect(loyaltyHelper.getFBRedirectUrl(id));
    }else{
        return res.status(404).end();
    }
});

router.get('/tw', function(req,res){
    var id = req.query.id;
    if(id){
        loyaltyHelper.getTWRedirectUrl(id,function(err,url){
            if(err){
                return res.status(404).end();
            }
            return res.redirect(url);
        });
    } else{
        return res.status(404).end();
    }
});

router.get('/code', function(req,res){
    var src = "/images/blank.png";
    var id = req.query.state;
    var code = req.query.code;
    if(id){
        var cachedRecord = loyaltyHelper.getLoyaltyRecordFromCache(id);
        if(!cachedRecord){
            return res.render('redeem',{src:src,err:"Offer Expired"});
        }
        src = "/loyalty_image/"+id+"."+cachedRecord.type;
        loyaltyHelper.sendQR(code,cachedRecord,function(err,code,arg1,arg2){
            if(code==='show'){
                code=arg1;
            }
            if(code=="redirect"){
                return res.render('redeem',{src:src,err:err,link:arg1});
            }
            if((err)||(!code)){
                if(typeof(err)=="string") return res.render('redeem',{src:src,err:err,code:code});
                return res.render('redeem',{src:src,err:"Offer Expired",code:code});
            }
            return res.render('redeem',{src:src,code:code});
        });
    }else{
        return res.status(404).end();
    }
});

router.get('/twitter/:id', function(req,res){
    var src = "/images/blank.png";
    var id = req.params.id;
    var code = req.query.oauth_verifier;
    var reqToken = req.query.oauth_token;
    if(id){
        var cachedRecord = loyaltyHelper.getLoyaltyRecordFromCache(id);
        if(!cachedRecord){
            return res.render('redeem',{src:src,err:"Offer Expired"});
        }
        src = "/loyalty_image/"+id+"."+cachedRecord.type;
        loyaltyHelper.sendQRTW(reqToken,code,cachedRecord,id,function(err,code,arg1,arg2){
            if(code==='show'){
                code=arg1;
            }
            if(code=="redirect"){
                return res.render('redeem',{src:src,err:err,link:arg1});
            }
            if((err)||(!code)){
                if(typeof(err)=="string") return res.render('redeem',{src:src,err:err,code:code});
                return res.render('redeem',{src:src,err:"Offer Expired",code:code});
            }
            return res.render('redeem',{src:src,code:code});
        });
    }else{
        return res.status(404).end();
    }
});


router.post('/email',function(req,res){
    var email = null;
    var code = null;
    if(req.body){
        if(req.body.email){
            email = req.body.email;
        }
        if(req.body.code){
            code = req.body.code;
        }
    }
    if((code)&&(email)){
        loyaltyHelper.email(code,email,function(err){
            if(err) return res.send(false);
            return res.send(true);
        });
    }else{
        res.send(false);
    }
});

module.exports = router;





/**
 * Created by Sundeep on 2/23/2015.
 */

var express = require('express');
var router = express.Router();
var passport = require('../library/routesLib').passport;
var util    = require('util');
var FB = require('../library/socialHelpers/FBHelperFunctions');
var TW = require('../library/socialHelpers/TWHelperFunctions');
var IT = require('../library/socialHelpers/ITHelperFunctions');
var FS = require('../library/socialHelpers/FSHelperFunctions');
var mobileHelper = require('./helpers/mobileRoutesHelper');
var commonUserFunction = require('./helpers/commonUserFunction');

function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.send('false');
}

router.get('/fail',function(req, res) {
    return res.send('false');
});

router.post('/Login' , passport.authenticate('local-login',{
    failureRedirect : '/mobile/fail' // redirect back to the signup page if there is an error
}),function(req, res) {
    var userId = req.user.id;
    var locId  = req.user.clickdaily.lastLocationId;
    mobileHelper.cacheLocation(userId,locId,function(result){
        return res.send(result);
    });
});

router.get('/GetMerchantLocations',isLoggedIn,function(req,res){
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return {id : d._doc._id,
            name : d._doc.name};
    });
    res.send([locations[0]]);
});

router.post('/Logoff',isLoggedIn,function(req,res){
    req.logout();
    res.send('true');
});


router.get('/RedeemPromotion',isLoggedIn,function(req,res){
    console.log(req.query);
    mobileHelper.redeemQR(req.query.id,req.user.clickdaily.lastLocationId,function(err,data){
        if(err) return res.send({err:err});
        return res.send(data);
    });

});

router.get('/GetFacebookTimeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = req.user.clickdaily.lastLocationId;
    FB.getMeFeed(userId,locId, function (feed) {
        res.send(feed);
    },true);
});

router.get('/GetTwitterTimeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = req.user.clickdaily.lastLocationId;
    TW.getMeFeed(userId,locId, function (feed) {
        res.send(feed);
    },true);
});

router.get('/GetInstagramTimeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = req.user.clickdaily.lastLocationId;
    IT.getMeFeed(userId,locId, function (feed) {
        res.send(feed);
    },true);
});

router.post('/social/:network/:edge',isLoggedIn,function(req,res){
    res.send(true);
});

router.get('/social/:network/:edge',isLoggedIn,function(req,res){
    res.send(true);
});


module.exports = router;


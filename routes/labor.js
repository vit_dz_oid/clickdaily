/**
 * Created by Sundeep on 2/11/2015.
 */
var express = require('express');
var router = express.Router();
var getData =  require('../library/GetData4View');
var passport = require('../library/routesLib').passport;
var commonUserFunction = require('./helpers/commonUserFunction');
// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function isOwner(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        if(req.use.userType===1){
            return next()
        }
    }
    res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}

router.get('/',isLoggedIn, function(req, res) {
    res.redirect('/Employees');
});

router.get('/Employees',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Labor",title : "Employees",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/TimeClocking',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Labor",title : "Time Clocking",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});
router.get('/LaborScheduling',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Labor",title : "Labor Scheduling",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

router.get('/EmployeesSchedules',isLoggedIn, function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('comingsoon',{type:"Labor",title : "Employees Schedules",layoutObject:commonUserFunction.viewObject(req.user)});
});

module.exports = router;
/**
 * Created by Sundeep on 4/2/2015.
 */

var express = require('express');
var router = express.Router();
var passport = require('../library/routesLib').passport;
var commonUserFunction = require('./helpers/commonUserFunction');

function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

function isNotLoggedIn(req, res, next) {
    if (!req.isAuthenticated())
        return next();
    res.redirect('/');
}

function getLastLocation(user){
    return user.clickdaily.lastLocationId;
}

router.get('/', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('debug',{type:"Debug",title:"Debug",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)});
});

module.exports = router;

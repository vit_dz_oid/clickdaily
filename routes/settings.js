/**
 * Created by Sundeep on 2/11/2015.
 */
var express = require('express');
var fs = require('fs');
var router = express.Router();
var getData =  require('../library/GetData4View');
var passport = require('../library/routesLib').passport;
var settingsHelper = require('./helpers/settingsRoutesHelper');
var commonUserFunction = require('./helpers/commonUserFunction');

// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function isOwner(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        if(req.user.clickdaily.userType){
            return next()
        }
    }
    res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}

router.get('/',isLoggedIn, function(req, res) {
    res.redirect('/MainDashboard');
});

router.get('/MainDashboard',isLoggedIn, function(req, res) {
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('comingsoon', {type: "Settings", title: "Main Dashboard", layoutObject: viewObject});
    });
});

router.get('/ResetPassword/',isLoggedIn,function(req,res){
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('setPassword',{
            type:"Settings"
            ,title:"Reset Password"
            ,layoutObject:viewObject
            ,first:req.user.clickdaily.firstLogin});
    });
});

router.get('/addLogo/',isLoggedIn,function(req,res){
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
        if(err){
            return res.sendStatus(500);
        }
        res.render('setLogo',{
            type:"Settings"
            ,title:"Upload Logo"
            ,layoutObject:viewObject
            ,first:req.user.clickdaily.firstLogin});
    });
});

router.post('/addLogo/',isLoggedIn,function(req,res){
    var fileName = req.user.clickdaily.lastLocationId + ".png";

    if (!req.busboy) { //Nothing to parse..
        return res.sendStatus(404);
    }
    req.busboy.on('file', function(name, file, filename, encoding, mimetype) {
        if(mimetype.substring(0,6).toLowerCase()!="image/"){
            return res.sendStatus(403);
        }
        var fstream = fs.createWriteStream(__dirname+"/../public/logos/"+fileName);
        fstream.on("close",function(){
            return res.redirect("/Settings/addLogo");
        });
        fstream.on("error",function(err){
            return res.sendStatus(500);
        });
        file.pipe(fstream);
    });
    req.pipe(req.busboy);
});

router.post('/removeLogo/',isLoggedIn,function(req,res){
    var fileName = req.user.clickdaily.lastLocationId + ".png";
    fs.stat(__dirname+"/../public/logos/"+fileName,function(err,stat){
        if(err){
            return res.sendStatus(404);
        }
        fs.unlink(__dirname+"/../public/logos/"+fileName,function(err,stat) {
            if (err) {
                return res.sendStatus(404);
            }
            return res.redirect("/Settings/addLogo");
        });
    });

});

router.post('/ResetPassword/',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;

    settingsHelper.resetPass(req.user.clickdaily.username,req.body,function(err){
        if(err) {
            return commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
                if(err){
                    return res.sendStatus(500);
                }
                res.render('setPassword', {
                    type: "Settings",
                    title: "Reset Password",
                    layoutObject: viewObject,
                    first: req.user.clickdaily.firstLogin,
                    err: err
                });
            });
        }
        return commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,viewObject) {
            if(err){
                return res.sendStatus(500);
            }
            res.render('setPassword',{type:"Settings",title:"Reset Password",layoutObject:viewObject,first:req.user.clickdaily.firstLogin,done:true});
        });
    });
});

module.exports = router;
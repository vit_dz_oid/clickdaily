var express = require('express');
var router = express.Router();
var userManagement = require("../library/createUsers.js");
var userSessionCache = {};
var commonUserFunction = require('./helpers/commonUserFunction');
// =====================================
// MIDDLEWARE TO CHECK USER ============
//======================================

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
    return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function canCreateUser(req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated()){

    if(req.user.clickdaily.lastLocation.permissions.userManagement.create){
      return next()
    }
  }
  res.sendStatus(404); // equivalent to res.status(404).send('Not Found')
}


// =====================================
// USER CREATION =======================
//======================================
/* Get Step 1 */
router.get('/create/1', canCreateUser,function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('createUser1',{title : "Create User Step 1",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc),message:req.flash("step1")});
});

/* Post Step 1 */
router.post('/create/1',canCreateUser, function(req, res) {
    if(userSessionCache[req.sessionID]){
        userSessionCache[req.sessionID].create2 = {};
    }else{
        userSessionCache[req.sessionID] = {create2 : {}};
    }

    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    userManagement.findUser(locations,req.body,function(err,auth,msg){
        if((err)){
            req.flash("step1",msg);
          return res.redirect('1')
        }
        if(auth){
            if(userSessionCache[req.sessionID]){
                userSessionCache[req.sessionID].create2 = {};
                userSessionCache[req.sessionID].create3 = {probableUsers:msg};

            }else{
                userSessionCache[req.sessionID] = {create3 : {probableUsers:msg}};
            }
            delete userSessionCache[req.sessionID].create3.user;
            return res.render('createUser3',{title : "Create User Step 3",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc),probableUsers:msg});
        }else{
          userSessionCache[req.sessionID].create2 = { user : req.body };
          return res.redirect('2');
        }
    });
});

/* Get Step 2 */
router.get('/create/2', canCreateUser,function(req, res) {
    var locations = req.user.clickdaily.locations.map(function(d,i){
        return d._doc.name;
    });
    var lastLocation = req.user.clickdaily.lastLocation;
    var lastLocationName = lastLocation.name;
    var modules = lastLocation.modules;
    res.render('createUser2',{title : "Create User Step 2",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc),cache:userSessionCache[req.sessionID]});
});

/* Post Step 2 */
router.post('/create/2', canCreateUser,function(req, res) {
  userManagement.createUser(req.user.id,req.user.clickdaily.lastLocationId,req.body,function(err,auth,msg){
    if((err)||(!auth)){
      return res.render('',{message:msg});
    }
    if(userSessionCache[req.sessionID]){
        userSessionCache[req.sessionID].create2 = {};
        userSessionCache[req.sessionID].create3 = {user:msg};
    }else{
        userSessionCache[req.sessionID] = {create3 : {user:msg}};
    }

      delete userSessionCache[req.sessionID].create3.probableUsers;
    return res.render('createUser3',{title : "Create User Step 3",layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc),users:msg});

  });

});

router.post('/create/3', canCreateUser,function(req, res) {
    if(userSessionCache[req.sessionID].create3.user){

    }else if(userSessionCache[req.sessionID].create3.probableUsers) {

    }
});

/* GET Step 4 */
router.get('/create/4', canCreateUser,function(req, res) {
  res.render('respond with a resource');
});

/* Post Step 4 */
router.post('/create/4', canCreateUser,function(req, res) {
  userManagement.addPermission(req.user.clickdaily.lastLocation.permissions,req.body,function(err,auth,msg){
    if((err)||(!auth)){
      return res.render('',{message:msg})
    }
    return res.redirect('/create/5');
  });
});

/* GET Step 4 */
router.get('/create/4', canCreateUser,function(req, res) {
  res.render('respond with a resource');
});

/* Post Step 4 */
router.post('/create/4', canCreateUser,function(req, res) {
  userManagement.addPermission(req.user.clickdaily.lastLocation.permissions,req.body,function(err,auth,msg){
    if((err)||(!auth)){
      return res.render('',{message:msg})
    }
    return res.redirect('/create/5');
  });
});

module.exports = router;

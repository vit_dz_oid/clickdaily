var http = require('http');
//var https = require('https');
var socket = require('socket.io');
var fs = require('fs');

var busboy = require('connect-busboy');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');

var mongoose = require('mongoose');
var flash    = require('connect-flash');

var logger = require('morgan');
var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');
var passport = require('./library/routesLib').passport;
var MongoStore = require('connect-mongo')(session);

var sessionStore = new MongoStore({
    db : 'merchant-sessions'
});

//startup

require('./library/startup').loadSocialDbModel();

var routes      = require('./routes/index');
var users       = require('./routes/users');
var mobile       = require('./routes/mobile');
var redeem       = require('./routes/redeem');
var social_r    = require('./routes/social');
var reports     = require('./routes/reports');
var pos         = require('./routes/pos');
var labor       = require('./routes/labor');
var support     = require('./routes/support');
var logbook     = require('./routes/logbook');
var settings    = require('./routes/settings');
var debug_r     = require('./routes/debug');
var mails       = require('./routes/mails');
var logos       = require('./routes/logos');

var configDB = require('./config/database.js');
var socketLib = require('./library/socketLib');
var socialSocketLib = require('./library/socialSocketLib');
var debugSocketLib = require('./library/debugSocketLib');
var errLogger = require('./library/logger');
var commonUserFunction = require('./routes/helpers/commonUserFunction');

// configuration ===============================================================
// connect to our database
var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var app = express();

require('./config/passport')(passport);

// view engine setup
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(busboy({
    limits: {
        fileSize: 50 * 1024 * 1024
    }
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/favicon.ico'));


//app.use(require('node-compass')({mode: 'expanded'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/lib',express.static(__dirname + '/bower_components'));
app.use('/loyalty_image',express.static(__dirname + '/data/loyalty'));
app.use(session({
    secret: 'e51e4b5b5e3b28180a6482e76fb0d8ae057569b9c1d2033aaeef3766f3a2ed28'
    , name : 'merchant'
    , store : sessionStore
    , resave : true
    , saveUninitialized : true
//    , domain : '.clickdaily.com'
    , cookie : {
        maxAge : 1*60*60*1000,
        domain : '.clickdaily.com'
    }
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());

app.use('/', routes);
app.use('/Mobile', mobile);
app.use('/redeem', redeem);
app.use('/users', users);
app.use('/social', social_r);
app.use('/reports', reports);
app.use('/pos', pos);
app.use('/labor', labor);
app.use('/support', support);
app.use('/logbook', logbook);
app.use('/settings', settings);
app.use('/debug', debug_r);
app.use('/mails', mails);
app.use('/logos', logos);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;

    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    app.use(function(err, req, res, next) {
        errLogger.routeErr(req.user,req.path,err);
        console.error(err.stack);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    errLogger.routeErr(req.user,req.path,err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        layoutObject:commonUserFunction.viewObject(req.user,req.session.lastLoc)
    });
});


/**
 * Get port from environment and store in Express.
 */

var ports = parseInt(process.env.PORTS, 10) || 3000;
app.set('port', ports);

var servers = http.createServer(app);
servers.on('error', onError);
servers.on('listening', onListening);
servers.listen(ports);

/**
 * Socket Connections
 */

io = new socket(servers);

var socketAuth = function(socket, next) {
    try {
        var data = socket.handshake || socket.request;
        if (! data.headers.cookie) {
            return next(new Error('Missing cookie headers'));
        }
        console.log('cookie header ( %s )', JSON.stringify(data.headers.cookie));
        var cookies = cookie.parse(data.headers.cookie);
        console.log('cookies parsed ( %s )', JSON.stringify(cookies));
        if (! cookies['merchant']) {
            return next(new Error('Missing cookie ' + 'merchant'));
        }
        var sid = cookieParser.signedCookie(cookies['merchant'], 'e51e4b5b5e3b28180a6482e76fb0d8ae057569b9c1d2033aaeef3766f3a2ed28');
        if (! sid) {
            return next(new Error('Cookie signature is not valid'));
        }
        console.log('session ID ( %s )', sid);
        data.sid = sid;
        sessionStore.get(sid, function(err, session) {

            if (err) return next(err);
            if (! session) return next(new Error('session not found'));
            data.session = session;
            next();
        });
    } catch (err) {
        console.error(err.stack);
        next(new Error('Internal server error'));
    }
};

io.use(socketAuth);

io.on('connection', function (socket) {

    socket.on('update', function (FilterType,StartDate,EndDate) {
        console.log(arguments);
        socketLib.update(socket,FilterType,StartDate,EndDate);
    });
});

var social = io.of('/social');
social.use(socketAuth);

social.on('connection',socialSocketLib);

var debugging = io.of('/debug');
debugging.use(socketAuth);

debugging.on('connection',debugSocketLib);

//module.exports = servers;
var debug = require('debug')('Clickdaily:server');



/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error('Port ' + ports + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error('Port ' + ports + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    debug('Listening on port ' + servers.address().port);
}
/**
 * Created by Sundeep on 2/18/2015.
 */
var util = require('util');

exports.SocialAPILog = function(network,edge,error){
  console.log(network," API Error at ",edge," : \n",util.inspect(error));
};

exports.SocialDatabaseLog = function(network,edge,error){
    console.log(network," Database Save Error at ",edge," : \n",util.inspect(error));
};
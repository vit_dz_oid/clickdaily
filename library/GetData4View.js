/**
 * Created by Sundeep on 1/8/2015.
 */
var Step = require('step');
var SQLCaller = require('./SqlConnection').SQLCaller;
var numeral = require('numeral');
var SQL = new SQLCaller();


// ***************************************
// ********* Sales Summary Report ********
// ***************************************

function MakeSalesPaymentReport(SalesPayment,SalesPaymentCashInOut,SalesRevenue){

    function format(a){
        if(a.QTY) a.QTY = numeral(a.QTY).format('0,0');
        if(a.Amount) a.Amount = numeral(a.Amount).format('$ 0,0.00');
        if(a.Tips) a.Tips = numeral(a.Tips).format('$ 0,0.00');
        if(a.Total) a.Total = numeral(a.Total).format('$ 0,0.00');
        if(a.GrossP) a.GrossP = numeral(a.GrossP).format('0,0.00');
        return a;
    }

    var SalesPaymentReport = {};
    var TenderType = {
        ACRC: "Accounts Receivable",
        AMMN: "Amex Machine",
        ARCN: "Airmiles Conversion",
        CASH: "Cash",
        CCMN: "Credit Card Machine",
        CHCK: "Check",
        CHCR: "Check Card",
        CHNG: "Change",
        COUP: "Coupon",
        CPAY: "CoPay",
        CPBN: "Capital Bond",
        CRDB: ["Credit Card","Visa and MasterCard","American Express"],
        CSAC: "Customer Account",
        ELTC: "Electronic Toll Collection",
        FDST: "Food Stamps",
        GFCR: "Gift Certificate",
        HSAC: "House Account",
        INMS: "International Maestro",
        LLTY: "Loyalty",
        MNCP: "Manufacturer Coupon",
        PROR: "Purchase Order",
        SDAL: "Staff Dress Allowance",
        STVL: "Stored Value",
        TRCH: "Travelers Check",
        UKMS: "UKMaestro",
        VCHR: "Voucher",
        WCCH: "WICCheck",
        TCCP : "Total Credit Card Payments",
        TOTP : "Total Other Payments",
        TOTL : "Total Payments",
        CASI : "Cash In",
        CASO : "Cash Out",
        CASD : "Cash Due"

    };

    SalesPaymentReport.TOTL = {};
    SalesPaymentReport.TOTL.QTY = 0;
    SalesPaymentReport.TOTL.Amount = 0;
    SalesPaymentReport.TOTL.Total = 0;
    SalesPaymentReport.TOTL.Tips = 0;
    SalesPaymentReport.TOTL.GrossP = 100;
    SalesPaymentReport.TOTL.Name = TenderType.TOTL;

    SalesPaymentReport.TCCP = {};
    SalesPaymentReport.TCCP.QTY = 0;
    SalesPaymentReport.TCCP.Amount = 0;
    SalesPaymentReport.TCCP.Total = 0;
    SalesPaymentReport.TCCP.Tips = 0;
    SalesPaymentReport.TCCP.GrossP = 0;
    SalesPaymentReport.TCCP.Name = TenderType.TCCP;

    SalesPaymentReport.TOTP = {};
    SalesPaymentReport.TOTP.QTY = 0;
    SalesPaymentReport.TOTP.Amount = 0;
    SalesPaymentReport.TOTP.Total = 0;
    SalesPaymentReport.TOTP.Tips = 0;
    SalesPaymentReport.TOTP.GrossP = 0;
    SalesPaymentReport.TOTP.Name = TenderType.TOTP;

    SalesPaymentReport.CASI = {};
    SalesPaymentReport.CASI.QTY = 0;
    SalesPaymentReport.CASI.Amount = 0;
    SalesPaymentReport.CASI.Total = 0;
    SalesPaymentReport.CASI.Tips = 0;
    SalesPaymentReport.CASI.GrossP = 0;
    SalesPaymentReport.CASI.Name = TenderType.CASI;

    SalesPaymentReport.CASO = {};
    SalesPaymentReport.CASO.QTY = 0;
    SalesPaymentReport.CASO.Amount = 0;
    SalesPaymentReport.CASO.Total = 0;
    SalesPaymentReport.CASO.Tips = 0;
    SalesPaymentReport.CASO.GrossP = 0;
    SalesPaymentReport.CASO.Name = TenderType.CASO;

    SalesPaymentReport.CASD = {};
    SalesPaymentReport.CASD.QTY = 0;
    SalesPaymentReport.CASD.Amount = 0;
    SalesPaymentReport.CASD.Total = 0;
    SalesPaymentReport.CASD.Tips = 0;
    SalesPaymentReport.CASD.GrossP = 0;
    SalesPaymentReport.CASD.Name = TenderType.CASD;

    var CCtypes = ["AMMN","CCMN","CRDB"];
    var Specific = ["GFCR","CASH"]

    if(SalesPayment.length){
        SalesPayment.forEach(function(d,i) {
            SalesPaymentReport.TOTL.Amount = SalesPaymentReport.TOTL.Amount + d.Amount;
            SalesPaymentReport.TOTL.QTY = SalesPaymentReport.TOTL.QTY + d.TransactionsCount;
            SalesPaymentReport.TOTL.Tips = SalesPaymentReport.TOTL.Tips + d.Tips;
            SalesPaymentReport.TOTL.Total = SalesPaymentReport.TOTL.Total + d.Total;
        });

        SalesPayment.forEach(function(d,i){

            SalesPaymentReport[d.TenderType] = {};
            SalesPaymentReport[d.TenderType].QTY = d.TransactionsCount;
            SalesPaymentReport[d.TenderType].Amount = d.Amount;
            SalesPaymentReport[d.TenderType].Tips =d.Tips;
            SalesPaymentReport[d.TenderType].Total =d.Total;
            SalesPaymentReport[d.TenderType].Name = TenderType[d.TenderType];
            SalesPaymentReport[d.TenderType].GrossP = d.Total * 100 / SalesPaymentReport.TOTL.Total;

            SalesPaymentReport[d.TenderType] = format(SalesPaymentReport[d.TenderType]);


            if(CCtypes.indexOf(d.TenderType) !=-1 ){

                SalesPaymentReport.TCCP.Amount = SalesPaymentReport.TCCP.Amount + d.Amount;
                SalesPaymentReport.TCCP.QTY = SalesPaymentReport.TCCP.QTY + d.TransactionsCount;
                SalesPaymentReport.TCCP.Tips = SalesPaymentReport.TCCP.Tips + d.Tips;
                SalesPaymentReport.TCCP.Total = SalesPaymentReport.TCCP.Total + d.Total;

            } else if(Specific.indexOf(d.TenderType) !=-1 ){

                SalesPaymentReport.TOTP.Amount = SalesPaymentReport.TOTP.Amount + d.Amount;
                SalesPaymentReport.TOTP.QTY = SalesPaymentReport.TOTP.QTY + d.TransactionsCount;
                SalesPaymentReport.TOTP.Tips = SalesPaymentReport.TOTP.Tips + d.Tips;
                SalesPaymentReport.TOTP.Total = SalesPaymentReport.TOTP.Total + d.Total;

            }

        });

        SalesPaymentReport.TCCP.GrossP = SalesPaymentReport.TCCP.Total * 100 / SalesPaymentReport.TOTL.Total;
        SalesPaymentReport.TOTP.GrossP = SalesPaymentReport.TOTP.Total * 100 / SalesPaymentReport.TOTL.Total;

        SalesPaymentReport.TCCP = format(SalesPaymentReport.TCCP);
        SalesPaymentReport.TOTP = format(SalesPaymentReport.TOTP);
        SalesPaymentReport.TOTL = format(SalesPaymentReport.TOTL);
    }

    if(SalesPaymentCashInOut.length){
        SalesPaymentCashInOut.forEach(function(d,i){
            if(d.CashInOut === 'CashIn'){
                SalesPaymentReport.CASI.QTY = SalesPaymentReport.CASI.QTY+1;
                SalesPaymentReport.CASI.Total =  SalesPaymentReport.CASI.Total + d.Value;
                SalesPaymentReport.CASI.Amount =  SalesPaymentReport.CASI.Amount + d.Value;
            }else if(d.CashInOut === 'PaidOut'){
                SalesPaymentReport.CASO.QTY = SalesPaymentReport.CASO.QTY+1;
                SalesPaymentReport.CASO.Total =  SalesPaymentReport.CASO.Total + d.Value;
                SalesPaymentReport.CASO.Amount =  SalesPaymentReport.CASO.Amount + d.Value;
            }
        });

        var CashBal = SalesPaymentReport.CASI.Amount - SalesPaymentReport.CASO.Amount;
        var ServiceCharge  = 0;
        var Cash = 0;
        if((SalesPaymentReport.CASH)&&(SalesPaymentReport.CASH.Total)){
            Cash = SalesPaymentReport.CASH.Total
        }
        if((SalesRevenue.length)&&(SalesRevenue[0])&&(SalesRevenue[0].ServiceCharge)){
            ServiceCharge = SalesRevenue[0].ServiceCharge;
        }
        SalesPaymentReport.CASD.Total = Cash + CashBal - ServiceCharge;
        SalesPaymentReport.CASD.Amount = Cash + CashBal - ServiceCharge;
        SalesPaymentReport.CASD.QTY = 1;
    }

    SalesPaymentReport.CASD = format(SalesPaymentReport.CASD);
    SalesPaymentReport.CASI = format(SalesPaymentReport.CASI);
    SalesPaymentReport.CASO = format(SalesPaymentReport.CASO);

    return SalesPaymentReport;

}

function MakeSalesTaxReport(SalesTax) {
    var SalesTaxReport = {};

    function format(a){
        if(a.QTY) a.QTY = numeral(a.QTY).format('0,0');
        if(a.Gross) a.Gross = numeral(a.Gross).format('$ 0,0.00');
        if(a.Tax) a.Tax = numeral(a.Tax).format('$ 0,0.00');
        if(a.Net) a.Net = numeral(a.Net).format('$ 0,0.00');
        if(a.GrossPercent) a.GrossPercent = numeral(a.GrossPercent).format('0,0.00');
        return a;
    }

    SalesTaxReport.Total = {};
    SalesTaxReport.Total.Name = "Total";
    SalesTaxReport.Total.QTY = 0;
    SalesTaxReport.Total.Gross = 0;
    SalesTaxReport.Total.Tax = 0;
    SalesTaxReport.Total.Net = 0;
    SalesTaxReport.Total.GrossPercent = 0;

    if (SalesTax.length) {
        SalesTax.forEach(function (d, i) {

            if (d.IsTaxIncluded == null) {
                SalesTaxReport.Exempt = {};
                SalesTaxReport.Exempt.Name = "Exempt";
                SalesTaxReport.Exempt.QTY = d.QTY;
                SalesTaxReport.Exempt.Gross = d.Gross;
                SalesTaxReport.Exempt.Tax = d.Tax;
                SalesTaxReport.Exempt.Net = d.Net;
                SalesTaxReport.Exempt.GrossPercent = d.GrossPercent;

                SalesTaxReport.Total.QTY = SalesTaxReport.Total.QTY + SalesTaxReport.Exempt.QTY;
                SalesTaxReport.Total.Gross = SalesTaxReport.Total.Gross + SalesTaxReport.Exempt.Gross;
                SalesTaxReport.Total.Tax = SalesTaxReport.Total.Tax + SalesTaxReport.Exempt.Tax;
                SalesTaxReport.Total.Net = SalesTaxReport.Total.Net + SalesTaxReport.Exempt.Net;
                SalesTaxReport.Total.GrossPercent = SalesTaxReport.Total.GrossPercent + SalesTaxReport.Exempt.GrossPercent;

                SalesTaxReport.Exempt = format(SalesTaxReport.Exempt);

            } else if (d.IsTaxIncluded) {
                SalesTaxReport.Included = {};
                SalesTaxReport.Included.Name = "Included";
                SalesTaxReport.Included.QTY = d.QTY;
                SalesTaxReport.Included.Gross = d.Gross;
                SalesTaxReport.Included.Tax = d.Tax;
                SalesTaxReport.Included.Net = d.Net;
                SalesTaxReport.Included.GrossPercent = d.GrossPercent;

                SalesTaxReport.Total.QTY = SalesTaxReport.Total.QTY + SalesTaxReport.Included.QTY;
                SalesTaxReport.Total.Gross = SalesTaxReport.Total.Gross + SalesTaxReport.Included.Gross;
                SalesTaxReport.Total.Tax = SalesTaxReport.Total.Tax + SalesTaxReport.Included.Tax;
                SalesTaxReport.Total.Net = SalesTaxReport.Total.Net + SalesTaxReport.Included.Net;
                SalesTaxReport.Total.GrossPercent = SalesTaxReport.Total.GrossPercent + SalesTaxReport.Included.GrossPercent;

                SalesTaxReport.Included = format(SalesTaxReport.Included);

            } else {
                SalesTaxReport.Excluded = {};
                SalesTaxReport.Excluded.Name = "Excluded";
                SalesTaxReport.Excluded.QTY = d.QTY;
                SalesTaxReport.Excluded.Gross = d.Gross;
                SalesTaxReport.Excluded.Tax = d.Tax;
                SalesTaxReport.Excluded.Net = d.Net;
                SalesTaxReport.Excluded.GrossPercent = d.GrossPercent;

                SalesTaxReport.Total.QTY = SalesTaxReport.Total.QTY + SalesTaxReport.Excluded.QTY;
                SalesTaxReport.Total.Gross = SalesTaxReport.Total.Gross + SalesTaxReport.Excluded.Gross;
                SalesTaxReport.Total.Tax = SalesTaxReport.Total.Tax + SalesTaxReport.Excluded.Tax;
                SalesTaxReport.Total.Net = SalesTaxReport.Total.Net + SalesTaxReport.Excluded.Net;
                SalesTaxReport.Total.GrossPercent = SalesTaxReport.Total.GrossPercent + SalesTaxReport.Excluded.GrossPercent;

                SalesTaxReport.Excluded = format(SalesTaxReport.Excluded);
            }
        });

    }

    SalesTaxReport.Total = format(SalesTaxReport.Total);

    return SalesTaxReport;
}

function MakeSalesRevenueReport(SalesRevenue){
    function format(a){
        if(a.NetSales) a.NetSales = numeral(a.NetSales).format('$ 0,0.00');
        if(a.TaxCollected) a.TaxCollected = numeral(a.TaxCollected).format('$ 0,0.00');
        if(a.CreditCardRefund) a.CreditCardRefund = numeral(a.CreditCardRefund).format('$ 0,0.00');
        if(a.TotalRevenueGross) a.TotalRevenueGross = numeral(a.TotalRevenueGross).format('$ 0,0.00');
        if(a.ServiceCharge) a.ServiceCharge = numeral(a.ServiceCharge).format('$ 0,0.00');
        if(a.TotalAmount) a.TotalAmount = numeral(a.TotalAmount).format('$ 0,0.00');
        return a;
    }
    var SalesRevenueReport = [format(SalesRevenue[0])];
    return SalesRevenueReport;
}

exports.getSalesSummaryReport = function (BusinessUnitId, FilterType, StartDate, EndDate,done) {
    Step(
        // Loads two files in parallel
        function loadStuff() {
            console.time("dbCall");
            SQL.GetSalesPaymentCashInOutReport(BusinessUnitId, FilterType, StartDate, EndDate, this.parallel());
            SQL.GetSalesPaymentReport(BusinessUnitId, FilterType, StartDate, EndDate, this.parallel());
            SQL.GetSalesRevenueReport(BusinessUnitId, FilterType, StartDate, EndDate, this.parallel());
            SQL.GetSalesTaxReport(BusinessUnitId, FilterType, StartDate, EndDate, this.parallel())
        },
        // Show the result when done
        function showStuff(err, one, two, three, four) {
            if (err) console.log(err);
            console.timeEnd("dbCall");
            var SalesPaymentCashInOut = one[0];
            var SalesPayment = two[0];
            var SalesRevenue = three[0];
            var SalesTax = four[0];
            var SalesPaymentReport = MakeSalesPaymentReport(SalesPayment,SalesPaymentCashInOut,SalesRevenue);
            var SalesTaxReport = MakeSalesTaxReport(SalesTax);
            var SalesRevenueReport = MakeSalesRevenueReport(SalesRevenue);
            console.dir(SalesRevenue);
            console.dir(SalesTax);
            done(SalesPaymentReport, SalesRevenueReport, SalesTaxReport);
        }
    )
};
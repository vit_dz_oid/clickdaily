/**
 * Created by Sundeep on 6/11/2015.
 */
var User                = require("../models/user").User;
var LocationName        = require("../models/user").LocationName;
exports.socketsSessions = {};

exports.setLocation = function(userId,locId,done) {
    User.findById(userId,function(err,user){
        if(err){
            return done(err);
        }
        if(!user){
            return done(new Error("No User in System with given ID"));
        }
        if( user.clickdaily.locations.id(locId)) {
            user.clickdaily.lastLocationId = locId;
            user.save(function(err) {
                if (err) {
                    return done(err);
                }
                return done(null, {id: locId});
            });
            return
        }
        return done(null,false);
    });
};
/**
 * Created by Sundeep on 1/29/2015.
 */
var redisRPCLib         = require("redis-rpc");
var path                = require("path");
var sockets             = require("./sockets");
var FBCBs               = require('./socialHelpers/FBHelperFunctions');
var TWCBs               = require('./socialHelpers/TWHelperFunctions');
var ITCBs               = require('./socialHelpers/ITHelperFunctions');
var FSCBs               = require('./socialHelpers/FSHelperFunctions');
var keywordCBs          = require('./socialHelpers/KeywordsHelperFunctions');
var post                = require('./socialHelpers/postHelperFunctions');
var urlShorten          = require('./socialHelpers/urlShortenHelperFunctions');
var postReadNotification= require('./socialHelpers/postReadNotificationHelperFunctions');
var Loyalty             = require('./socialHelpers/LoyaltyHelperFunctions');
var Schedule            = require('./socialHelpers/ScheduleHelperFunctions');
var insightCBs          = require('./socialHelpers/insightHelperFunctions');
var ss                  = require('socket.io-stream');
var redis = require('redis');
var redisWStream = require('redis-wstream'); // factory
var client = redis.createClient(null, null, {detect_buffers: true});


var moduleString = "socialDispatcher";

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};

var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);


var keywordsRPC = new redisRPCLib({
    subModule : moduleString + "keywords",
    pubModule : "keywords" + moduleString,
    tasks     : keywordCBs
});

var itRPC = new redisRPCLib({
    subModule : moduleString + "it",
    pubModule : "it" + moduleString,
    tasks     : ITCBs
});

var fsRPC = new redisRPCLib({
    subModule : moduleString + "fs",
    pubModule : "fs" + moduleString,
    tasks     : FSCBs
});

var twRPC = new redisRPCLib({
    subModule : moduleString + "tw",
    pubModule : "tw" + moduleString,
    tasks     : TWCBs
});


var fbRPC = new redisRPCLib({
    subModule : moduleString + "fb",
    pubModule : "fb" + moduleString,
    tasks     : FBCBs
});

var postRPC = new redisRPCLib({
    subModule : moduleString + "post",
    pubModule : "post" + moduleString,
    tasks     : post
});

var urlShortenRPC = new redisRPCLib({
    subModule : moduleString + "urlShorten",
    pubModule : "urlShorten" + moduleString,
    tasks     : urlShorten
});

var postReadNotificationRPC = new redisRPCLib({
    subModule : moduleString + "postReadNotification",
    pubModule : "postReadNotification" + moduleString,
    tasks     : postReadNotification
});

var insightsRPC = new redisRPCLib({
    subModule : moduleString + "insights",
    pubModule : "insights" + moduleString,
    tasks     : insightCBs
});

var lib = require('./socialHelpers/socialLib')(keywordsRPC,itRPC,fbRPC,twRPC,fsRPC,postRPC);

var statusRPC = new redisRPCLib({
    subModule : moduleString + "status",
    pubModule : "status" + moduleString,
    tasks     : lib
});
// Keywords

var LocationCache = {};

function getLocationId(socket){
    return socket.handshake.session.lastLoc;
}

module.exports = function (socket) {
    console.log("Socket Connection Established");
    sockets.socketsSessions[socket.id] = socket;

    var userId = socket.handshake.session.passport.user._id;

    socket.on('Ready',function(info){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        statusRPC.sendCall({
            task        : "status",
            args        : [locId,info],
            sessionId   : socket.id
        });
    });

    /*socket.on("changeLocation",function(newLoc){
        var userId = socket.handshake.session.passport.user._id;
        LocationCache[userId] = newLoc;
        return socket.emit("locationChange",{id: newLoc});
        sockets.setLocation(userId,newLoc, function (err,result) {
            if((err)||(!result)){
                return socket.emit("locationChange","false");
            }
            return socket.emit("locationChange",result);
        });
    })*/;


    // ------------- Facebook Edges ---------------

    socket.on("FB_AddPage",function(data){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "addPage",
            args        : [locId,data],
            sessionId   : socket.id
        });
    });

    socket.on("FB_SendUnselectedPages",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "unselectedPages",
            args        : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("FB_Comment",function(id,text){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "commentPost",
            args        : [locId,id,text],
            sessionId   : socket.id
        });
    });

    socket.on("FB_Like",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "likePost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    ss(socket).on('FB_Post', function (stream,data,approve) {
        console.log("FB_post Social Socket");
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var task = "create";
        if(data.dateTime){
            task = "schedule";
        }
        if(approve) {
            var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
                return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
            });
            var userType = lastLocation[0].modules.Social;
            data.postingUserType = userType;
            if(userType==1) {
                data.approvingUser = [2];
            } else if(userType==2) {
                data.approvingUser = [1];
            }
        }
        data.creatingUserID = userId;
        if ((typeof(stream) == "object") && (stream.readable)){
            stream._events.end.push(function () {
                    data.stream = socket.id;
                if(approve) {
                    postRPC.sendCall({
                        task        : task,
                        args        : [locId,0,3,data],
                        sessionId   : socket.id
                    });
                } else {
                    postRPC.sendCall({
                        task: task,
                        args: [locId, 0, 2, data],
                        sessionId: socket.id
                    });
                }
            });
             stream._events.error = [stream._events.error];
             stream._events.error.push(function (err) {
                    errorLog.log('FB_Post error writing to redis',err);
                    return socket.emit('FB_PostUnsuccessful',id,data.id);
                });
             if (!stream.path)
                stream.path = true;
            if (!stream.mode)
                stream.mode = true;
            stream.pipe(redisWStream(client, "FB"+socket.id));
        } else if(approve) {
            postRPC.sendCall({
                task        : task,
                args        : [locId,0,4,data],
                sessionId   : socket.id
            });
        } else {
            postRPC.sendCall({
                task        : task,
                args        : [locId,0,1,data],
                sessionId   : socket.id
            });
        }
    });

    socket.on('FB_RePost', function (id,datetime) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postRPC.sendCall({
            task : 'duplicatePost',
            args : [locId,id,datetime],
            sessionId   : socket.id
        });
    });

    socket.on('FB_ChangePost', function (id,datetime,text,stream) {

        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postRPC.sendCall({
            task        : "reschedulePost",
            args        : [locId,id,datetime],
            sessionId   : socket.id
        });
        /*var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        Schedule.rescheduleFB(userId,locId,text,stream,datetime,id,function(err,id){
            if(err) return socket.emit('FB_ChangePostUnsuccessful',id);
            socket.emit('FB_ChangePostSuccessful',id);
        });*/
    });

    socket.on('FB_Insights', function () {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket)
        fbRPC.sendCall({
            task        : "getInsights",
            args        : [locId],
            sessionId   : socket.id
        });
    });

    socket.on('FB_Posted', function () {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "getPageYouFeed",
            args        : [locId,false],
            sessionId   : socket.id
        });
    });


    // ------------- Twitter Edges ---------------

    socket.on("TW_Reply",function(id,text){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "replyPost",
            args        : [locId,id,text],
            sessionId   : socket.id
        });
    });

    socket.on("TW_Favorite",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "favoritePost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("TW_UnFavorite",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "unfavoritePost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("TW_follow",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "follow",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("TW_unfollow",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "unfollow",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("TW_Retweet",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "retweetPost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    ss(socket).on('TW_Post', function (stream,data,approve) {
        console.log("TW_Post Social Socket");
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var task = "create";
        if(data.dateTime){
            task = "schedule";
        }
        if(approve) {
            var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
               return d._id = locId;
            });
            var userType = lastLocation[0].modules.Social;
            data.postingUserType = userType;
            if(userType==1) {
                data.approvingUser = [2];
            } else if(userType==2) {
                data.approvingUser = [1];
            }
        }
        data.creatingUserID = userId;
        if ((typeof(stream) == "object") && (stream.readable)) {
            //stream.setEncoding();
            //var buff = new Buffer(data.size);
            stream._events.end.push(function () {
                data.stream = socket.id;
                if(approve) {
                    postRPC.sendCall({
                        task        : task,
                        args        : [locId,1,3,data],
                        sessionId   : socket.id
                    });
                } else {
                    postRPC.sendCall({
                        task        : task,
                        args        : [locId,1,2,data],
                        sessionId   : socket.id
                    });
                }
            });
            stream._events.error = [stream._events.error];
            stream._events.error.push(function (err) {
                errorLog.log('TW_Post error writing to redis',err);
                return socket.emit('TW_PostUnsuccessful',id,data.id);
            });
            if (!stream.path)
                stream.path = true;
            if (!stream.mode)
                stream.mode = true;
            stream.pipe(redisWStream(client, "TW"+socket.id));
        } else if(approve) {
            postRPC.sendCall({
                task        : task,
                args        : [locId,1,4,data],
                sessionId   : socket.id
            });
        } else {
            postRPC.sendCall({
                task        : task,
                args        : [locId,1,1,data],
                sessionId   : socket.id
            });
        }
    });

    socket.on('TW_RePost', function (id,datetime) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postRPC.sendCall({
            task : 'duplicatePost',
            args : [locId,id,datetime],
            sessionId   : socket.id
        });
    });

    socket.on('TW_ChangePost', function (id,datetime,text,stream) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postRPC.sendCall({
            task        : "reschedulePost",
            args        : [locId,id,datetime],
            sessionId   : socket.id
        });
        /*var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        Schedule.rescheduleTW(userId,locId,text,stream,datetime,id,function(err,id){
            if(err) return socket.emit('FB_ChangePostUnsuccessful',id);
            socket.emit('FB_ChangePostSuccessful',id);
        });*/
    });

    socket.on('TW_GetMentions', function () {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "getMentionsFeed",
            args        : [locId,false],
            sessionId   : socket.id
        });
    });

    socket.on('TW_Posted', function (id,datetime,text,stream) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "getYouFeed",
            args        : [locId,false],
            sessionId   : socket.id
        });
    });

    socket.on('TW_Delete', function (id) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        console.log("deleting id : ",id);
        twRPC.sendCall({
            task        : "delPost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on('TW_getreplies', function (id) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "getInReplyTweets",
            args        : [locId,id,5,null],
            sessionId   : socket.id
        });
    });

    socket.on('getRetweets', function (id,maxID) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "getRetweets",
            args        : [locId,id,maxID],
            sessionId   : socket.id
        });
    });

    socket.on('searchTweets', function (q,lat,long,rad,next) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        if(!lat) {
            lat = "40.758680"
        }
        if(!long) {
            long = "-73.979723"
        }
        if(!rad) {
            rad = "10mi"
        }
        var geocode = lat+","+long+","+rad;
        twRPC.sendCall({
            task        : "Search",
            args        : [locId,q,geocode,next],
            sessionId   : socket.id
        });
    });



    // ------------- Instagram Edges ---------------

    socket.on("IT_Comment",function(id,text){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        itRPC.sendCall({
            task        : "commentPost",
            args        : [locId,id,text],
            sessionId   : socket.id
        });
    });

    socket.on("IT_Like",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        itRPC.sendCall({
            task        : "likePost",
            args        : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on('IT_Posted', function (id,datetime,text,stream) {
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        itRPC.sendCall({
            task        : "getYouFeed",
            args        : [locId,false],
            sessionId   : socket.id
        });
    });

    ss(socket).on('IT_Post', function (stream,data,approve) {
        console.log("IT_Post Social Socket");
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var task = "schedule";
        if(approve) {
            var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
                return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
            });
            var userType = lastLocation[0].modules.Social;
            data.postingUserType = userType;
            if(userType==1) {
                data.approvingUser = [2];
            } else if(userType==2) {
                data.approvingUser = [1];
            }
        }
        data.creatingUserID = userId;
        if ((typeof(stream) == "object") && (stream.readable)) {
            stream._events.end.push(function () {
                data.stream = socket.id;
                if(approve) {
                    postRPC.sendCall({
                        task        : task,
                        args        : [locId,2,3,data],
                        sessionId   : socket.id
                    });
                } else {
                    postRPC.sendCall({
                        task: task,
                        args: [locId, 2, 2, data],
                        sessionId: socket.id
                    });
                }
            });
            stream._events.error = [stream._events.error];
            stream._events.error.push(function (err) {
                errorLog.log('IT_Post error writing to redis',err);
                return socket.emit('IT_PostUnsuccessful',id,data.id);
            });
            if (!stream.path)
                stream.path = true;
            if (!stream.mode)
                stream.mode = true;
            stream.pipe(redisWStream(client, "IT"+socket.id));
        } else if(approve) {
            postRPC.sendCall({
                task        : task,
                args        : [locId,2,4,data],
                sessionId   : socket.id
            });
        } else {
            postRPC.sendCall({
                task        : task,
                args        : [locId,2,1,data],
                sessionId   : socket.id
            });
        }
    });


    // ------------- Loyalty Edges ---------------

    ss(socket).on('loyalty_post', function (stream,data) {
        console.log("loyalty_post Social Socket");
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var bufs = [];
        if(!data.type){
            data.type = 'image/png';
        }
        if(data.size){
            stream.on('data',function(chunck){
                bufs.push(chunck);
            });
            stream.on('end',function(){
                //console.log('buffer',buff);
                var buff = Buffer.concat(bufs);
                Loyalty.post(userId, locId, data.form, buff, data.type.split('/')[1],function (err,type) {
                    if(err){
                        if(type=='tw') socket.emit('TW_PostUnsuccessful');
                        if(type=='fb') socket.emit('FB_PostUnsuccessful');
                        return;
                    }
                    if(type=='tw') socket.emit('TW_PostSuccessful');
                    if(type=='fb') socket.emit('FB_PostSuccessful');
                });
            });
        }
    });


    // ------------- Friends ---------------

    socket.on("GetFriends",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "getFriends",
            args        : [locId,false],
            sessionId   : socket.id
        });
        twRPC.sendCall({
            task        : "getFollwers",
            args        : [locId,false],
            sessionId   : socket.id
        });
        itRPC.sendCall({
            task        : "getFriends",
            args        : [locId, false],
            sessionId   : socket.id
        });
        itRPC.sendCall({
            task        : "getFollwers",
            args        : [locId, false],
            sessionId   : socket.id
        });
    });

    // -------------- Posts ----------------

    socket.on("getPost",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'getPost',
            args : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("getHistoryPost",function(id,network){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'getHistoryPost',
            args : [locId,id,network],
            sessionId   : socket.id
        });
    });

    socket.on("delPost",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'delPost',
            args : [locId,id],
            sessionId   : socket.id
        });
    });

    socket.on("approvePost",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
            return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
        });
        var userType = lastLocation[0].modules.Social;

        postRPC.sendCall({
            task : 'approvePost',
            args : [locId,id,userType],
            sessionId   : socket.id
        });
    });

    socket.on("rejectPost",function(id){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
            return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
        });
        var userType = lastLocation[0].modules.Social;

        postRPC.sendCall({
            task : 'rejectPost',
            args : [locId,id,userType],
            sessionId   : socket.id
        });
    });

    socket.on("getPendingApprovalPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
            return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
        });
        var userType = lastLocation[0].modules.Social;

        postRPC.sendCall({
            task : 'getPendingApprovalPosts',
            args : [locId,userType],
            sessionId   : socket.id
        });
    });

    socket.on("getNeedingApprovalPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        var lastLocation = socket.handshake.session.passport.user.clickdaily.locations.filter(function(d){
            return d._id =  socket.handshake.session.passport.user.clickdaily.lastLocationId;
        });
        var userType = lastLocation[0].modules.Social;

        postRPC.sendCall({
            task : 'getNeedingApprovalPosts',
            args : [locId,userType],
            sessionId   : socket.id
        });
    });

    socket.on("getScheduledPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'getScheduledPosts',
            args : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("getRejectedPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'getRejectedPosts',
            args : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("getHistoryPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        console.log("Fetching History");
        postRPC.sendCall({
            task : 'getHistoryPosts',
            args : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("getExpiredPosts",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'getExpiredPosts',
            args : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("editPostText",function(postId,text){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        postRPC.sendCall({
            task : 'editPostText',
            args : [locId,postId,text],
            sessionId   : socket.id
        });
    });

    // -------------- Insights ------------

    socket.on("dailyStats",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        insightsRPC.sendCall({
            task : 'dailyStats',
            args : [locId],
            sessionId   : socket.id
        });
    });

    socket.on("monthlyStats",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);

        insightsRPC.sendCall({
            task : 'monthlyStats',
            args : [locId],
            sessionId   : socket.id
        });
    });

    // -------------- Load More ------------

    socket.on("FB_LoadMore",function(next){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        fbRPC.sendCall({
            task        : "loadMoreFeed",
            args        : [locId,next],
            sessionId   : socket.id
        });
    });

    socket.on("TW_LoadMore",function(next){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        twRPC.sendCall({
            task        : "loadMoreFeed",
            args        : [locId,next],
            sessionId   : socket.id
        });
    });

    socket.on("IT_LoadMore",function(next){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        itRPC.sendCall({
            task        : "loadMoreFeed",
            args        : [locId, next],
            sessionId   : socket.id
        });
    });

    /*socket.on("FS_LoadMore",function(network,next){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        FSCBs.loadMore(userId,locId,next,function(feed,next){
            socket.emit("FS_LoadMore",feed,next)
        });
    });*/

    // ------------- Keywords ---------------

    socket.on("addKeywords",function(list){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        keywordsRPC.sendCall({
            task        : "addKeywords",
            args        : [locId, list],
            sessionId   : socket.id
        });
    });

    socket.on("delKeyword",function(keyword){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        keywordsRPC.sendCall({
            task        : "delKeyword",
            args        : [locId, keyword],
            sessionId   : socket.id
        });
    });

    // ------------- Self Updates ---------------

    socket.on("update_feeds",function(){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        statusRPC.sendCall({
            task        : "update_feeds",
            args        : [locId],
            sessionId   : socket.id
        });
    });

    // ----------- URL Shorten ---------------

    socket.on("create_url",function(url){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        urlShortenRPC.sendCall({
            task        : "addUrl",
            args        : [locId,url,userId],
            sessionId   : socket.id
        });
    });

    socket.on("getAllUrls",function(url){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        urlShortenRPC.sendCall({
            task        : "getAll",
            args        : [locId],
            sessionId   : socket.id
        });
    });

    // ------------- Dashboard Notification -----------

    socket.on("markRead",function(id,network){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postReadNotificationRPC.sendCall({
            task        : "save",
            args        : [locId,id,network,true],
            sessionId   : socket.id
        });
    });

    socket.on("isRead",function(id,network){
        var userId = socket.handshake.session.passport.user._id;
        var locId  = getLocationId(socket);
        postReadNotificationRPC.sendCall({
            task        : "getN",
            args        : [locId,id,network],
            sessionId   : socket.id
        });
    });


    socket.on('disconnect',function(){
        if(sockets.socketsSessions[socket.id])
        delete sockets.socketsSessions[socket.id];
    })

};





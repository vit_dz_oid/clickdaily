/**
 * Created by Sundeep on 4/3/2015.
 */
var fs = require('fs');

function createErrString(err){
    var errstr = "[";
    if(typeof(err)=='object'){
        try{
            errstr = errstr + JSON.stringify(err);
        }catch (e){
            errstr = errstr + 'JSON of err Coverstion Failed'
        }
    } else if(typeof(err)=='string'){
        errstr = errstr + err;
    } else if(typeof(err)=='number'){
        errstr = errstr + err;
    } else {
        errstr = errstr + "Undefined err type";
    }
    return errstr + "]";
}

function createEnvString(env){
    var envstr = "[";
    if(typeof(env)=='object'){
        try{
            envstr = envstr + JSON.stringify(env);
        }catch (e){
            envstr = envstr + 'JSON of environment Coverstion Failed'
        }
    } else if(typeof(env)=='string'){
        envstr = envstr + env;
    } else if(typeof(env)=='number'){
        envstr = envstr + env;
    } else {
        envstr = envstr + "Undefined environment type";
    }
    return envstr + "]";
}

function createRouteString(user,path,err){
    var now = new Date();
    var str = now.toString() + '|';
    var errstr = createErrString(err);
    if(user){
        str = str + user.username + '|' + path;
    }else{
        str = str + '|' + path;
    }
    str = str + '|' + errstr;
    return str + '\n';
}

function routeErr(user,path,err){
    var log_string;
    var filename = 'logs/errors/routeErrors/';
    if((user)&&(user.clickdaily)){
        filename = filename + user.clickdaily.username+ '.csv';
        log_string = createRouteString(user.clickdaily,path,err);
    }else{
        filename = filename + 'noUser.csv';
        log_string = createRouteString(null,path,err);
    }
    fs.appendFile(filename,log_string,function(err){
        if(err)  {
            return console.log("error : ", err);
        }
        console.log("logged : ",log_string)
    });
}

function createOtherString(file,err,environment){
    var now = new Date();
    var str = now.toString() + '|';
    var errstr = createErrString(err);
    var envstr = createEnvString(environment);
    str = str + file + '|' +
        errstr + '|' + envstr;
    return str + '\n';
}

function otherErr(file,err,environment){
    var now = new Date();
    var log_string;
    var filename = 'logs/errors/other/' + now.toDateString().substr(4).replace(/ /g,'_');
    log_string = createOtherString(file,err,environment);
    fs.appendFile(filename,log_string,function(err){
        if(err) {
            return console.log("error : ", err);
        }
        console.log("logged : ",log_string)
    });
}

exports.routeErr = routeErr;
exports.otherErr = otherErr;
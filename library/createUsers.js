/**
 * Created by Sundeep on 1/16/2015.
 */
require("string_score");
var generatePassword = require('password-generator');
var User            = require('../models/user').User;
var Location        = require('../models/user').Location;

exports.findUser = function(locations,body,done){
    if(body.phonenumber){
        User.find({ 'clickdaily.phonenumber' :  body.phonenumber },function(err,results){

            if(err){
                done(err,false,"Ops something went wrong! We are trying to fix it.");
            }

            if(results.length){
                var probableUsers = results.filter(function(d,i){
                    return ((d.firstname.score(body.firstname)>0.7)&(d.lastname.score(body.lastname)>0.7))
                });
                if(probableUsers.length){
                    probableUsers = probableUsers.map(function(d,i){
                       return {
                           username     : d.clickdaily.username
                           ,phonenumber : d.clickdaily.phonenumber
                           ,firstname   : d.clickdaily.firstname
                           ,lastname    : d.clickdaily.lastname
                           ,middlename  : d.clickdaily.middlename
                           ,gender      : d.clickdaily.gender

                       }
                    });
                    done(null,true,probableUsers);
                }else{
                    done(null,false);
                }
            }else{
                done(null,false);
            }
        });
    } else if(body.email){
        User.find({ 'clickdaily.email' :  body.email },function(err,results){

            if(err){
                done(err,false,"Ops something went wrong! We are trying to fix it.");
            }

            if(results.length){
                var probableUsers = results.filter(function(d,i){
                    return ((d.clickdaily.firstname.score(body.firstname)+d.clickdaily.lastname.score(body.lastname)>1.5))
                });
                if(probableUsers.length){
                    probableUsers = probableUsers.map(function(d,i){
                        return {
                            username     : d.clickdaily.username
                            ,email       : d.clickdaily.email
                            ,firstname   : d.clickdaily.firstname
                            ,lastname    : d.clickdaily.lastname
                            ,middlename  : d.clickdaily.middlename
                            ,gender      : d.clickdaily.gender

                        }
                    });
                    done(null,true,probableUsers);
                }else{
                    done(null,false);
                }
            }else{
                done(null,false);
            }
        });
    } else if(body.dob){
        User.find({ 'clickdaily.dob' :  body.dob },function(err,results){

            if(err){
                done(err,false,"Ops something went wrong! We are trying to fix it.");
            }

            if(results.length){
                var probableUsers = results.filter(function(d,i){
                    return ((d.firstname.score(body.firstname)>0.7)&(d.lastname.score(body.lastname)>0.7))
                });
                if(probableUsers.length){
                    probableUsers = probableUsers.map(function(d,i){
                        return {
                            username      : d.clickdaily.username
                            , dob         : d.clickdaily.dob
                            , firstname   : d.clickdaily.firstname
                            , lastname    : d.clickdaily.lastname
                            , middlename  : d.clickdaily.middlename
                            , gender      : d.clickdaily.gender

                        }
                    });
                    done(null,true,probableUsers);
                }else{
                    done(null,false);
                }
            }else{
                done(null,false);
            }
        });
    }else{
        done(true,true,"Please enter atleast one out of Email, Phone, Date of Birth.");
    }
};

exports.createUser = function(creator,location,body,done){
    User.findOne({ 'clickdaily.username' :  body.username }, function(err, user) {
        // if there are any errors, return the error
        if (err)
            return done(err,false,"Ops something went wrong! We are trying to fix it.");

        // check to see if theres already a user with that email

        if (user) {
            return done(null, false, "That username is already taken.");
        } else {

            // if there is no user with that email
            // create the user
            var tempPass               = generatePassword(8, false);
            var newUser                = new User();

            newUser.clickdaily.username                         = body.username;
            newUser.clickdaily.email                            = body.email;
            newUser.clickdaily.password                         = newUser.generateHash(tempPass);
            newUser.clickdaily.phonenumber                      = body.phonenumber;
            newUser.clickdaily.dob                              = body.dob;
            newUser.clickdaily.firstname                        = body.firstname;
            newUser.clickdaily.lastname                         = body.lastname;
            newUser.clickdaily.middlename                       = body.middlename;
            newUser.clickdaily.gender                           = body.gender;
            newUser.clickdaily.martialstatus                    = body.martialstatus;
            newUser.clickdaily.address.line1                    = body.addressline1;
            newUser.clickdaily.address.line2                    = body.addressline2;
            newUser.clickdaily.address.line3                    = body.addressline3;
            newUser.clickdaily.address.city                     = body.addressscity;
            newUser.clickdaily.address.state                    = body.addressstate;
            newUser.clickdaily.address.zip                      = body.addresszip;
            newUser.clickdaily.address.country                  = body.addresscountry;
            newUser.clickdaily.homephone                        = body.homephone;
            newUser.clickdaily.emergencycontact.name            = body.emergencyname;
            newUser.clickdaily.emergencycontact.relationship    = body.emergencyrelation;
            newUser.clickdaily.emergencycontact.phone           = body.emergencyphone;
            newUser.clickdaily.creator                          = creator;
            newUser.clickdaily.userType                         = 2;

            var newLoc 			                = new Location();
            newLoc._id                          = location;
            newUser.clickdaily.lastLocationId   = location;
            newUser.clickdaily.locations.push(newLoc);

            // save the user
            newUser.save(function(err) {
                if (err)
                    return done(err,false,"Ops something went wrong! We are trying to fix it.");
                var user = newUser.toObject({show:["username","email","phonenumber"], transform: true });
                user = user.clickdaily;
                user.tempPass = tempPass;
                return done(null,true, user);
            });
        }

    });
};

exports.addPermission = function(permissions, body, done){

    User.findOne({ 'clickdaily.username' :  body.username }, function(err, user) {

        // if there are any errors, return the error

        if (err)
            return done(err,false,"Ops something went wrong! We are trying to fix it.");

        // check to see if theres already a user with that email

        if (user) {
            var newLoc 			   = user.clickdaily.locations.id(body.locid);

            newLoc.permissions.salesReport.read   		= body.salesreportR & permissions.salesReport.read;
            newLoc.permissions.Inventory.read        	= body.inventorymanagementR & permissions.Inventory.read;
            newLoc.permissions.POSManagement.read       = body.posmanagementR & permissions.POSManagement.read;
            newLoc.permissions.userManagement.readYours = body.usermanagementRY & permissions.userManagement.readYours;
            newLoc.permissions.userManagement.readAll   = body.usermanagementRY & permissions.userManagement.readAll;
            newLoc.permissions.userManagement.create    = body.usermanagementC & permissions.userManagement.create;
            newLoc.permissions.userManagement.edit      = body.usermanagementE & permissions.userManagement.edit;
            newLoc.permissions.schedules.create      	= body.scheduleC & permissions.schedules.create;
            newLoc.permissions.schedules.edit      		= body.scheduleC & permissions.schedules.edit;
            newLoc.permissions.schedules.readAll   		= body.scheduleRA & permissions.schedules.readAll;
            newLoc.permissions.schedules.readYours      = body.scheduleRY & permissions.schedules.readYours;
            newLoc.permissions.timeClocking.createYours = body.timeclockingCY & permissions.timeClocking.createYours;
            newLoc.permissions.timeClocking.createAll 	= body.timeClockingCA & permissions.timeClocking.createAll;
            newLoc.permissions.timeClocking.edit      	= body.timeClockingE & permissions.timeClocking.edit;
            newLoc.permissions.timeClocking.readAll   	= body.timeClockingRA & permissions.timeClocking.readAll;
            newLoc.permissions.timeClocking.readYours 	= body.timeClockingRY & permissions.timeClocking.readYours;
            newLoc.permissions.logbook.create      		= body.logbookC & permissions.logbook.create;
            newLoc.permissions.logbook.edit      		= body.logbookE & permissions.logbook.edit;
            newLoc.permissions.logbook.read      		= body.logbookR & permissions.logbook.read;
            newLoc.permissions.socialMedia.read        	= body.socialMediaR & permissions.socialMedia.read;
            newLoc.permissions.reservation.create      	= body.reservationC & permissions.reservation.create;
            newLoc.permissions.reservation.edit      	= body.reservationE & permissions.reservation.edit;
            newLoc.permissions.reservation.read      	= body.reservationR & permissions.reservation.read;

            // save the user
            user.save(function(err) {
                if (err)
                    done(err,false,"Ops something went wrong! We are trying to fix it.");
                return done(null,true, newUser);
            });

        } else {
            return done(null, false, "The username was modified after creation");
        }
    });
};

/**
 * Created by Sundeep on 5/4/2015.
 */

var vars = require('./socialHelpers/Vars/socialVars.js');
var TW = require('../library/socialHelpers/TWHelperFunctions');
var Social = require('../models/socialMedia').Social;

function loadSocialDbModel(){
    Social.find({},function(err, results) {
        // if there are any errors, return the error
        if (err)
            return done(err);

        // check to see if theres already a cache for that userId
        results.forEach(function (social, i) {
            if (social) {
                var locId = social.id;
                vars.users[locId] = social;
                if (vars.users[locId].loyalty[0]) {
                    if (vars.users[locId].loyalty[0].loyalty_record) {
                        vars.users[locId].loyalty[0].loyalty_record.forEach(function (d, i) {
                            vars.loyaltyIdTranslation[d._doc.cachedRecordId] = d._doc._id.toString();
                        });
                    }
                }

                if(vars.users[locId].posts.length){
                    var document = vars.users[locId];
                    for(var ij = 0;ij<document.posts.length;ij++){
                        var Post = document.posts[ij];
                        if(!Post){
                            return;
                        }
                        var dateTime = Post.post_time;

                        if(dateTime > new Date()){
                            var cb = function(document,newPostId) {
                                return function(err,body){
                                    document.posts.id(newPostId).post_id = body.id_str;
                                    document.posts.id(newPostId).data.stream =
                                        (((body)&&(body.entities)&&(body.entities.media)&&(body.entities.media.length))?
                                            (body.entities.media
                                                .filter(function(d){return (d.type=="photo")})
                                                .map(function(d){return d.id_str})
                                            )
                                            :false);
                                    document.markModified('posts');
                                    document.save(function (err) {
                                        console.log(err);
                                    });
                                };
                            }(document,Post.id);
                            var text   = Post.data.text;
                            var stream = Post.data.stream;
                            var oldId  = Post.oldId;
                            var method = TW.post.bind(null,"",locId,text,stream,oldId,cb);
                            var oldPost = vars.schedule.scheduledJobs[Post.id];
                            if(oldPost){
                                oldPost.cancel();
                            }
                            var j = vars.schedule.scheduleJob(Post.id,dateTime,method);
                        }
                    }
                }
            }
        });
    });
}

//loadSocialDbModel();

exports.loadSocialDbModel = loadSocialDbModel;
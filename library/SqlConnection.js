/**
 * Created by Sundeep on 1/8/2015.
 */

var sql = require('mssql');


function  SQLCaller() {
    this.config = {
        user: 'Clickdaily@kvvl91cppj',
        password: 'ARTSDatabase77',
        server: 'kvvl91cppj.database.windows.net', // You can use 'localhost\\instance' to connect to named instance
        database: 'ARTSDemo',

        options: {
            encrypt: true // Use this if you're on Windows Azure
        }
    }
}

SQLCaller.prototype.GetSalesTaxReport = function(BusinessUnitId,FilterType,StartDate,EndDate,done){
    var connection = new sql.Connection(this.config, function(err) {
        if(err){
            console.log("error : ",err);
        }
       // Stored Procedure

        var request = new sql.Request(connection);
        request.input('BusinessUnitId', sql.NVarChar, BusinessUnitId);
        request.input('FilterType', sql.Int,FilterType);
        request.input('StartDate', sql.DateTime2,new Date(StartDate));
        request.input('EndDate', sql.DateTime2,new Date(EndDate));
        request.execute('[dbo].[GetSalesTaxReport]', function(err, recordsets) {

            if(err){
                console.log("error : ",err);
            }
            done(err,recordsets);
           // console.dir(recordsets);
        });

    });
};

SQLCaller.prototype.GetSalesRevenueReport = function(BusinessUnitId,FilterType,StartDate,EndDate,done) {
    var connection = new sql.Connection(this.config, function(err) {
        if(err){
            console.log("error : ",err);
        }
        // Stored Procedure

        var request = new sql.Request(connection);
        request.input('BusinessUnitId', sql.NVarChar, BusinessUnitId);
        request.input('FilterType', sql.Int,FilterType);
        request.input('StartDate', sql.DateTime2,new Date(StartDate));
        request.input('EndDate', sql.DateTime2,new Date(EndDate));
        request.execute('[dbo].[GetSalesRevenueReport]', function(err, recordsets) {

            if(err){
                console.log("error : ",err);
            }
            done(err,recordsets);
            // console.dir(recordsets);
        });

    });
};

SQLCaller.prototype.GetSalesPaymentReport = function(BusinessUnitId,FilterType,StartDate,EndDate,done) {
    var connection = new sql.Connection(this.config, function(err) {
        if(err){
            console.log("error : ",err);
        }
        // Stored Procedure

        var request = new sql.Request(connection);
        request.input('BusinessUnitId', sql.NVarChar, BusinessUnitId);
        request.input('FilterType', sql.Int,FilterType);
        request.input('StartDate', sql.DateTime2,new Date(StartDate));
        request.input('EndDate', sql.DateTime2,new Date(EndDate));
        request.execute('[dbo].[GetSalesPaymentReport]', function(err, recordsets) {

            if(err){
                console.log("error : ",err);
            }
            done(err,recordsets);
            // console.dir(recordsets);
        });

    });
};

SQLCaller.prototype.GetSalesPaymentCashInOutReport = function(BusinessUnitId,FilterType,StartDate,EndDate,done) {
    var connection = new sql.Connection(this.config, function(err) {
        if(err){
            console.log("error : ",err);
        }
        // Stored Procedure

        var request = new sql.Request(connection);
        request.input('BusinessUnitId', sql.NVarChar, BusinessUnitId);
        request.input('FilterType', sql.Int,FilterType);
        request.input('StartDate', sql.DateTime2,new Date(StartDate));
        request.input('EndDate', sql.DateTime2,new Date(EndDate));
        request.execute('[dbo].[GetSalesPaymentCashInOutReport]', function(err, recordsets) {

            if(err){
                console.log("error : ",err);
            }
            done(err,recordsets);
        });

    });
};

exports.SQLCaller = SQLCaller;

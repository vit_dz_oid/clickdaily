/**
 * Created by Sundeep on 3/6/2015.
 */
var crypto = require('crypto');

function random (howMany, chars) {
    chars = chars
    || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    var rnd = crypto.randomBytes(howMany)
        , value = new Array(howMany)
        , len = chars.length;

    for (var i = 0; i < howMany; i++) {
        value[i] = chars[rnd[i] % len];
    }

    return value.join('');
}

module.exports = random;
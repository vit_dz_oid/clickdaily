/**
 * Created by Sundeep on 4/2/2015.
 */
var FB = require('./socialHelpers/FBHelperFunctions');
var TW = require('./socialHelpers/TWHelperFunctions');
var IT = require('./socialHelpers/ITHelperFunctions');
var FS = require('./socialHelpers/FSHelperFunctions');

module.exports = function (socket) {
    console.log("Socket Connection Established");

    socket.on("FB_ClearFeed",function(data){
        console.log('Got command to clear');
        var userId = socket.handshake.session.passport.user._id;
        var locId  = socket.handshake.session.lastLoc;
        FB.clearFeed(userId,locId,function(pic,feed){
            console.log('Command to clear finished - \n\n ',pic,feed);
            socket.emit("FB_FeedCleared",pic,feed);
        });
    });
    socket.on("TW_ClearFeed",function(data){
        console.log('Got command to clear');
        var userId = socket.handshake.session.passport.user._id;
        var locId  = socket.handshake.session.lastLoc;
        TW.clearFeed(userId,locId,function(pic,feed){
            console.log('Command to clear finished - \n\n ',pic,feed);
            socket.emit("TW_FeedCleared",pic,feed);
        });
    });
    socket.on("IT_ClearFeed",function(data){
        console.log('Got command to clear');
        var userId = socket.handshake.session.passport.user._id;
        var locId  = socket.handshake.session.lastLoc;
        IT.clearFeed(userId,locId,function(pic,feed){
            console.log('Command to clear finished - \n\n ',pic,feed);
            socket.emit("IT_FeedCleared",pic,feed);
        });
    });
    socket.on("FS_ClearFeed",function(data){
        console.log('Got command to clear');
        var userId = socket.handshake.session.passport.user._id;
        var locId  = socket.handshake.session.lastLoc;
        FS.clearFeed(userId,locId,function(pic,feed){
            console.log('Command to clear finished - \n\n ',pic,feed);
            socket.emit("FSs_FeedCleared",pic,feed);
        });
    });
};
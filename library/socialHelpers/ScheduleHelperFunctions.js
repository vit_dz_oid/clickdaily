/**
 * Created by Sundeep on 4/9/2015.
 */

var FB = require('./FBHelperFunctions');
var TW = require('./TWHelperFunctions');
var post = require('../../models/socialMedia').post;
var moment = require('moment');
var vars = require('./Vars/socialVars.js');
var mongoose = require('mongoose');

var scheduleFB = function(userId,locId,text,dateTime,stream,oldId,done){
    var document = vars.users[locId];
    var newPost = new post({
        network          : 0
        , create_time    : new Date()
        , post_time      : dateTime
        , num_likes      : 0
        , num_comments   : 0
        , num_reached    : 0
        , data           : {
            text         : text
            , stream     : (stream?true:false)
        }
        , locID          : locId
        , oldId          : oldId
    });
    dateTime = moment(dateTime).format("X");
    if(parseInt(dateTime)-parseInt(moment().format("X"))<600){
        return done("time less tha 10 minutes");
    }
    FB.postSchedule(userId,locId,text,stream,dateTime,function(body){
        if(body.error){
            return done(body.error);
        }

        newPost.post_id = body.id;
        document.posts.push(newPost);
        document.markModified('posts');
        document.save(function(err,doc){
            done(err,newPost.id);
        });
    });
};

var duplicateFB = function(userId,locId,post_id,dateTime,done){
    var document = vars.users[locId];
    var Post = document.posts.id(post_id);
    if(!Post){
        return done("NoPost");
    }
    var newPost = post(Post);
    newPost.create_time   = new Date();
    newPost.post_time     = dateTime;
    newPost._id = mongoose.Types.ObjectId();
    dateTime = moment(dateTime).format("X");
    if(parseInt(dateTime)-parseInt(moment().format("X"))<600){
        return done("time less tha 10 minutes");
    }
    var text = Post.data.text;
    var stream = (Post.data.stream?Post.post_id:false);
    var oldId  = Post.oldId;
    FB.postSchedule(userId,locId,text,stream,dateTime,function(body){
        if(body.error){
            return done(body.error);
        }

        newPost.post_id = body.id;
        document.posts.push(newPost);
        document.markModified('posts');
        document.save(function(err,doc){
            done(err,newPost.id,oldId);
        });
    });
};

var rescheduleFB = function(userId,locId,text,stream,dateTime,post_id,done){
    var document = vars.users[locId];
    var Post = document.posts.id(post_id);
    if(!Post){
        return done("NoPost");
    }
    var originTime = Post.post_time;
    document.posts.id(post_id).post_time = dateTime;
    dateTime = moment(dateTime).format("X");
    if(parseInt(dateTime)-parseInt(moment().format("X"))<600){
        return done("time less tha 10 minutes");
    }
    FB.postReSchedule(userId,locId,text,stream,dateTime,Post.post_id,function(body){
        if(body.error){
            return done(body.error);
            document.posts.id(post_id).post_time = originTime
        }
        document.markModified('posts');
        document.save(function(err,doc){
            done(err,doc.id);
        });
    });
};


var scheduleTW = function(userId,locId,text,dateTime,stream,oldId,done){
    var document = vars.users[locId];
    dateTime = new Date(dateTime);
    var newPost = new post({
        network          : 1
        , create_time    : new Date()
        , post_time      : dateTime
        , num_likes      : 0
        , num_comments   : 0
        , num_reached    : 0
        , data           : {
            text         : text,
            stream       : (stream?stream:false)
        }
        , locId          : locId
        , oldId          : oldId
    });
    var cb = function(document,newPostId) {
        return function(err,body,oldId){
            document.posts.id(newPostId).post_id = body.id;
            document.posts.id(newPostId).data.stream =
                (((body)&&(body.entities)&&(body.entities.media)&&(body.entities.media.length))?
                    (body.entities.media
                        .filter(function(d){return (d.type=="photo")})
                        .map(function(d){return d.id_str})
                    )
                :false);
            document.markModified('posts');
            document.save(function (err) {
                console.log(err);
            });
        };
    }(document,newPost.id);
    var method = TW.postSchedule.bind(null,userId,locId,text,stream,oldId,cb);
    var j = vars.schedule.scheduleJob(newPost.id,dateTime,method);
    document.posts.push(newPost);
    document.markModified('posts');
    document.save(function (err,doc) {
        console.log(err);
        done(err,newPost.id);
    });
};

var duplicateTW = function(userId,locId,post_id,dateTime,done){
    var document = vars.users[locId];
    var Post = document.posts.id(post_id);
     if(!Post){
        return done("NoPost");
    }
    var newPost = post(Post);
    dateTime = new Date(dateTime);
    newPost._id = mongoose.Types.ObjectId();
    newPost.create_time   = new Date();
    newPost.post_time     = dateTime;
    var cb = function(document,newPostId) {
        return function(err,body){
            document.posts.id(newPostId).post_id = body.id;
            document.posts.id(newPostId).data.stream =
                (((body)&&(body.entities)&&(body.entities.media)&&(body.entities.media.length))?
                    (body.entities.media
                        .filter(function(d){return (d.type=="photo")})
                        .map(function(d){return d.id_str})
                    )
                    :false);
            document.markModified('posts');
            document.save(function (err) {
                console.log(err);
            });
        };
    }(document,newPost.id);
    var text   = Post.data.text;
    var stream = Post.data.stream;
    var oldId  = Post.oldId;
    var method = TW.post.bind(null,userId,locId,text,stream,oldId,cb);
    var j = vars.schedule.scheduleJob(newPost.id,dateTime,method);
    document.posts.push(newPost);
    document.markModified('posts');
    document.save(function (err,doc) {
        console.log(err);
        done(err,doc.id,oldId);
    });
};


var rescheduleTW = function(userId,locId,text,stream,dateTime,post_id,done){
    var document = vars.users[locId];
    dateTime = new Date(dateTime);
    var Post = document.posts.id(post_id);
    if(!Post){
        return done("NoPost");
    }
    document.posts.id(post_id).post_time     = dateTime;

    var cb = function(document,newPostId) {
        return function(err,body){
            document.posts.id(newPostId).post_id = body.id_str;
            document.posts.id(newPostId).data.stream =
                (((body)&&(body.entities)&&(body.entities.media)&&(body.entities.media.length))?
                    (body.entities.media
                        .filter(function(d){return (d.type=="photo")})
                        .map(function(d){return d.id_str})
                    )
                    :false);
            document.markModified('posts');
            document.save(function (err) {
                console.log(err);
            });
        };
    }(document,Post.id);
    var text   = Post.data.text;
    var stream = Post.data.stream;
    var oldId  = Post.oldId;
    var method = TW.post.bind(null,userId,locId,text,stream,oldId,cb);
    var oldPost = vars.schedule.scheduledJobs[Post.id];
    if(oldPost){
        oldPost.cancel();
    }
    var j = vars.schedule.scheduleJob(Post.id,dateTime,method);
    document.markModified('posts');
    document.save(function (err,doc) {
        console.log(err);
        done(err,doc.id);
    });

};

var getSchedules = function(userId,locId){
    var document = vars.users[locId];
    if( document.posts){
        return document.posts.toObject();
    }
    return [];
};

exports.scheduleFB      = scheduleFB;
exports.rescheduleFB    = rescheduleFB;
exports.duplicateFB     = duplicateFB;
exports.scheduleTW      = scheduleTW;
exports.rescheduleTW    = rescheduleTW;
exports.duplicateTW     = duplicateTW;
exports.getSchedules    = getSchedules;
exports.scheduleObject  = vars.schedule;






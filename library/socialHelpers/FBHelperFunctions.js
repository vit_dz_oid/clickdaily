/**
 * Created by Sundeep on 2/4/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "tw";

function FB_status(userId,locId){
    /*var document = vars.users[locId];
    return ((document.FB[0])&&(document.FB[0].token));*/
}

function FB_getUserInfo(err,user_info, page_info, selected_page,socketid) {
    if(err){
        return console.log("Error getting user info for facebook : ",err);
    }
    console.log('FB_doneUserInfo : ', arguments);

    if ((selected_page == "") && (page_info.length)) {

        sockets.socketsSessions[socketid].emit('FB_SelectPage', user_info, page_info);
    } else {
        page_info = page_info.filter(function(d){
            return d.id == selected_page;
        });
        sockets.socketsSessions[socketid].emit('FB_PageInfo', page_info[0]);
    }
    console.log("Facebook UserInfo Sequence Completed");
}

function FB_getPageFeed(err,pageInfo, feed,next,socketid) {
    if(err){
        return console.log("Error getting page feed for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit("FB_PageFeed", feed,next);
    console.log("Facebook PageFeed Sequence Completed");
}

function FB_getPageYouFeed(err,data,socketid){
    if(err){
        return console.log("Error GETTING you feed for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit('FB_Posted',data)
}

function FB_getPromotablePosts(userId,locId,done,mobile) {

}

function FB_loadMoreFeed(err,feed,next,socketid) {
    if(err){
        return console.log("Error GETTING more feed for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit("FB_LoadMore",feed,next)
}

function FB_getProfilePic(userId, locId,id,done) {

}

function FB_addPage(err,page_info,feed,next,socketid) {
    if(err){
        return console.log("Error adding Page for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit('FB_PageInfo',page_info);
    sockets.socketsSessions[socketid].emit("FB_Posted",feed,next);
}

function  FB_unselectedPages(err,user_info,unselectedPages,socketid) {
    if(err){
        return console.log("Error adding Page for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit("FB_SelectPage", user_info, unselectedPages);
}

function FB_likePost(err,res,id,socketid) {
    if(err){
        return console.log("Error liking for facebook : ",err);
    }
    if(res.success==true) sockets.socketsSessions[socketid].emit("FB_LikeSuccesful", id);
}

function FB_commentPost(err,id,socketid) {
    if(err) return sockets.socketsSessions[socketid].emit("FB_CommentUnsuccesful", id);
    return sockets.socketsSessions[socketid].emit("FB_CommentSuccesful", id);
}

function FB_getPost(userId,locId,id,done) {

}

function FB_delPost(userId,locId,id,done) {

}

function FB_post(userId,locId,text,stream,oldId,done) {

}

function FB_postLink(userId,locId,url,name,caption,picture,done) {

}

function FB_postSchedule(userId,locId,text,stream,timestamp,done) {

}

function FB_postReSchedule(userId,locId,text,stream,timestamp,post_id,done) {

}

function FB_postLinkSchedule(userId,locId,url,name,caption,picture,timestamp,done) {

}

function FB_getInsights(err,data,socketid) {
    if(err){
        return console.log("Error GETTING INSIGTHS for facebook : ",err);
    }
    sockets.socketsSessions[socketid].emit('FB_Insights',err,data)
}

var FB_clearFeed = function(userId,locId,done) {

};


exports.clearFeed           = FB_clearFeed;
exports.status              = FB_status;
exports.getUserInfo         = FB_getUserInfo;
exports.getPageFeed         = FB_getPageFeed;
exports.getProfilePic       = FB_getProfilePic;
exports.unselectedPages     = FB_unselectedPages;
exports.likePost            = FB_likePost;
exports.commentPost         = FB_commentPost;
exports.post                = FB_post;
exports.getPost             = FB_getPost;
exports.delPost             = FB_delPost;
exports.postSchedule        = FB_postSchedule;
exports.postReSchedule      = FB_postReSchedule;
exports.postLinkSchedule    = FB_postLinkSchedule;
exports.postLink            = FB_postLink;
exports.addPage             = FB_addPage;
exports.loadMoreFeed        = FB_loadMoreFeed;
exports.getInsights         = FB_getInsights;
exports.getPageYouFeed      = FB_getPageYouFeed;







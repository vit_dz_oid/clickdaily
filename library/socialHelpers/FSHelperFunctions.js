/**
 * Created by Sundeep on 2/9/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "fs";

function FS_status(userId,locId){
    var document = vars.users[locId];
    return ((document.FS[0])&&(document.FS[0].token));
}

function FS_getUserInfo(err, user_info, venue_info, selected_venue, socketid) {
    if(err){
        if ((!user_info) && (err.type = "User type venue")) {
            return sockets.socketsSessions[socketid].emit('FS_UserTypeWrong');
        }
        return console.log("Error getting User Info on foursquare : ",err);
    }
    //sockets.socketsSessions[socketid].emit('FS_UserInfo',user_info);
    if ((!selected_venue) && (venue_info.length)) {
        sockets.socketsSessions[socketid].emit('FS_SelectVenue', user_info, venue_info);
    } else {
        if(venue_info[selected_venue[0]]){
            sockets.socketsSessions[socketid].emit('FS_VenueInfo', venue_info[selected_venue[0]]);
        }else{
            sockets.socketsSessions[socketid].emit('FS_VenueInfo', null);
        }
    }
    console.log("Foursquare UserInfo Sequence Completed");
}

function FS_getVenuesFeed(err,venueInfo, feed,socketid){
    if(err){
        return console.log("Error getting User Info on foursquare : ",err);
    }
    sockets.socketsSessions[socketid].emit("FS_VenueFeed", feed);
    console.log("Foursquare Feed Sequence Completed");
}

/*function FS_getVenuesFeed(userId,locId,done) {
    var document = vars.users[locId];
    if(document.FS[0].selected_venue.length){
        document.FS[0].selected_venue.forEach(function (d,i) {
            var flag = 0;
            var diff;
            if((document.FS[0].cache.feeds)&&(document.FS[0].cache.feeds[d])){
                done(document.FS[0].cache.venue_info.id(d),document.FS[0].cache.feeds[d]);
                diff = (new Date()) - document.FS[0].updateTime.feeds[d];
                if(diff>vars.updateTimes.FS.feed){
                    flag=1;
                }
            }else{
                flag = 1;
            }

            if(flag) {
                FS_getVenueFeedFromId(userId,locId,d,done);
            }
        });
    }
}*/

function  FS_addVenues(err,venueInfo,feed,socketid) {
    if(err){
        return console.log("Error adding venue on foursquare : ",err);
    }
    sockets.socketsSessions[socketid].emit("FS_VenueInfo",venueInfo);
    sockets.socketsSessions[socketid].emit("FS_VenueFeed",feed);
}

function  FS_addAllVenues(userId,locId,done) {
    var document = vars.users[locId];
    var data = document.FS[0].venue;
    FS_addVenues(userId,locId,data,done);
}

function FS_clearFeed(userId,locId,done) {
    var document = vars.users[locId];
    if(!document.FS[0].cache.feeds){
        document.FS[0].cache.feeds = {};
    }
    document.FS[0].updateTime.feeds = {};
    document.FS[0].cache.feeds = {};
    document.FS[0].markModified('cache.feeds');
    document.save(function (err, doc) {
        done(document.FS[0].cache.feeds.me);
        if(err) return console.log(err);
        console.log("Doc Saved");
    });

}

exports.status           = FS_status;
exports.getUserInfo      = FS_getUserInfo;
exports.getVenuesFeed    = FS_getVenuesFeed;
exports.addVenues        = FS_addVenues;
exports.addAllVenues     = FS_addAllVenues;
exports.clearFeed        = FS_clearFeed;
exports.moduleString     = moduleString;


/**
 * Created by Sundeep on 5/11/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "keywords";

function dailyStats(err, stats, socketid) {
    if (err) {
        return console.log("dailyStats err : ", err);
    }
    sockets.socketsSessions[socketid].emit("dailyStats", stats);
}

function monthlyStats(err, stats, socketid) {
    if (err) {
        return console.log("monthlyStats err : ", err);
    }
    sockets.socketsSessions[socketid].emit("monthlyStats", stats);
}


exports.moduleString  = moduleString;
exports.dailyStats    = dailyStats;
exports.monthlyStats  = monthlyStats;
/**
 * Created by Sundeep on 1/26/2015.
 */
var sockets             = require("../sockets");
var moduleString        = "status";

module.exports = function(keywordsRPC,itRPC,fbRPC,twRPC,fsRPC,postRPC){
    return {
        status : function(err, locId,info,result, socketid){
            // -------------------------------------------
            // ------------- Keywords --------------------
            // -------------------------------------------
            var networks = [];
            keywordsRPC.sendCall({
                task        : "getKeyWords",
                args        : [locId],
                sessionId   : socketid
            });

            // -------------------------------------------
            // --------- Facebook self emits -------------
            // -------------------------------------------

            if(result.fb){
                networks.push("Facebook");
                console.log("Facebook Self Emits Sequence Initiationed");
                // ----------------- User Info --------------------

                if(info.userInfo) {
                    fbRPC.sendCall({
                        task        : "getUserInfo",
                        args        : [locId],
                        sessionId   : socketid
                    });
                }

                // ----------------- Page Feeds --------------------
                if(info.feed) {
                    fbRPC.sendCall({
                        task        : "getPageFeed",
                        args        : [locId,false],
                        sessionId   : socketid
                    });
                }
                /*

                 ME FEED SHUT DOWN

                 // ----------------- Home Feed --------------------

                 var FB_doneMeFeed = function(feed){
                 socket.emit("FB_MeFeed",feed);
                 console.log("Facebook MeFeed Sequence Completed");
                 };

                 FBCBs.getMeFeed(userId,locId,FB_doneMeFeed);
                 */
                console.log("Facebook Self Emits Sequence Calls Completed");
            }

            // -------------------------------------------
            // --------- Twitter self emits -------------
            // -------------------------------------------

            if(result.tw){
                networks.push("Twitter");
                console.log("Twitter Self Emits Sequence Initiationed");
                // ----------------- User Info --------------------
                if(info.userInfo) {
                    twRPC.sendCall({
                        task        : "getUserInfo",
                        args        : [locId],
                        sessionId   : socketid
                    });
                }


                // ----------------- Home Feed --------------------
                if(info.feed){
                    twRPC.sendCall({
                        task        : "getMeFeed",
                        args        : [locId,false],
                        sessionId   : socketid
                    });
                }
                console.log("Twitter Self Emits Sequence Calls Completed");
            }

            // -------------------------------------------
            // --------- Instagram self emits -------------
            // -------------------------------------------

            if(result.it){
                networks.push("Instagram");
                console.log("Instagram Self Emits Sequence Initiationed");
                // ----------------- User Info --------------------
                if(info.userInfo) {

                    itRPC.sendCall({
                        task        : "getUserInfo",
                        args        : [locId,false],
                        sessionId   : socketid
                    });

                }


                // ----------------- Home Feed --------------------
                if(info.feed) {
                    itRPC.sendCall({
                        task        : "getMeFeed",
                        args        : [locId,false],
                        sessionId   : socketid
                    });
                }
                console.log("Instagram Self Emits Sequence Calls Completed");
            }

            // -------------------------------------------
            // --------- Foursquare self emits -----------
            // -------------------------------------------

            if(result.fs){
                networks.push("FourSquare");
                console.log("Foursquare Self Emits Sequence Initiationed");
                // ----------------- User Info --------------------
                if(info.userInfo) {

                    fsRPC.sendCall({
                        task        : "getUserInfo",
                        args        : [locId],
                        sessionId   : socketid
                    });
                }

                // ----------------- Home Feed --------------------
                if(info.feed) {
                    fsRPC.sendCall({
                        task        : "getVenuesFeed",
                        args        : [locId],
                        sessionId   : socketid
                    });
                }
                console.log("Foursquare Self Emits Sequence Calls Completed");
            }
            console.log("Socket Self Emits Sequence Calls Completed");
            if(sockets.socketsSessions[socketid]){
                sockets.socketsSessions[socketid].emit('status',networks);
            }

            if(info.schedule){
                console.log('sent for posts');
                postRPC.sendCall({
                    task        : "getScheduledPosts",
                    args        : [locId],
                    sessionId   : socketid
                });
                /*postRPC.sendCall({
                    task        : "getHistoryPosts",
                    args        : [locId],
                    sessionId   : socketid
                });*/
                if(sockets.socketsSessions[socketid]) {
                    var lastLocation = sockets.socketsSessions[socketid].handshake.session.clickdaily.locations.filter(function (d) {
                        return d._id = sockets.socketsSessions[socketid].handshake.session.clickdaily.lastLocationId;
                    });
                    var userType = lastLocation[0].modules.Social;
                    postRPC.sendCall({
                        task : 'getNeedingApprovalPosts',
                        args : [locId,userType],
                        sessionId   : socketid
                    });
                    postRPC.sendCall({
                        task : 'getPendingApprovalPosts',
                        args : [locId,userType],
                        sessionId   : socketid
                    });
                }
                //sockets.socketsSessions[socketid].emit('schedules',Schedule.getSchedules(userId,locId));
            }
        },
        updateFeeds : function(err, locId,result, socketid){

            // -------------------------------------------
            // --------- Facebook self emits -------------
            // -------------------------------------------

            if(result.fb){
                // ----------------- Page Feeds --------------------
                fbRPC.sendCall({
                    task        : "getPageFeed",
                    args        : [locId,false],
                    sessionId   : socketid
                });
            }

            // -------------------------------------------
            // --------- Twitter self emits -------------
            // -------------------------------------------

            if(result.tw){
                // ----------------- Home Feed --------------------
                twRPC.sendCall({
                    task        : "getMeFeed",
                    args        : [locId,false],
                    sessionId   : socketid
                });
            }

            // -------------------------------------------
            // --------- Instagram self emits -------------
            // -------------------------------------------

            if(result.it){
                // ----------------- Home Feed --------------------
                itRPC.sendCall({
                    task        : "getMeFeed",
                    args        : [locId,false],
                    sessionId   : socketid
                });
            }

            // -------------------------------------------
            // --------- Foursquare self emits -----------
            // -------------------------------------------

            if(result.fs){
                // ----------------- Home Feed --------------------
                fsRPC.sendCall({
                    task        : "getVenuesFeed",
                    args        : [locId],
                    sessionId   : socketid
                });
            }
        }
    };
};
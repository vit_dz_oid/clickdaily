/**
 * Created by Sundeep on 5/11/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "keywords";

module.exports          = {
    moduleString        : moduleString,
    getKeyWords         : function (err, list, socketid) {
                            if (err) {
                                return console.log("Keywords Fetch err : ", err);
                            }
                            sockets.socketsSessions[socketid].emit("keywords", list);
                        },
    addKeywords         : function (err, newList, socketid) {
                            if (err) {
                                console.log(err);
                                return sockets.socketsSessions[socketid].emit("addKeywordsFailed");
                            }
                            sockets.socketsSessions[socketid].emit("updateKeywords", newList);
                        },
    delKeyword          : function (err, newList, socketid) {
                            if (err) {
                                console.log(err);
                                return socket.emit("delKeywordFailed");
                            }
                            sockets.socketsSessions[socketid].emit("updateKeywords", newList);
                        }
};


/**
 * Created by Sundeep on 1/26/2015.
 */
var tenminutes = 10*60*1000;
var schedule = require('node-schedule');
exports.objects = {};
exports.users = {};
exports.profilePicsCache = {};
exports.loyaltyIdTranslation = {};
exports.updateTimes = {
    FB : {
        userInfo : tenminutes,
        feed     : tenminutes
    },
    TW : {
        userInfo : tenminutes,
        feed     : tenminutes,
        friends  : tenminutes,
        follwers : tenminutes
    },
    IT : {
        userInfo : tenminutes,
        feed     : tenminutes,
        friends  : tenminutes,
        follwers : tenminutes
    },
    FS : {
        userInfo : tenminutes,
        feed     : tenminutes
    }
};

exports.schedule = schedule;
/**
 * Created by Sundeep on 8/30/2015.
 */
var sockets     = require("../sockets");

function save(err,id,network,res,socketid){
    if(err) {
        console.log("Error creating url : ",err);
        return sockets.socketsSessions[socketid].emit('saveNotificationFailed',id,network);
    }
    sockets.socketsSessions[socketid].emit('savedNotification',id,network,res);
}

function getN(err,id,network,res,socketid){
    if(err) {
        console.log("Error creating url : ",err);
        return sockets.socketsSessions[socketid].emit('notificationType',id,network,false);
    }
    sockets.socketsSessions[socketid].emit('notificationType',id,network,res);
}

exports.save = save;
exports.getN = getN;
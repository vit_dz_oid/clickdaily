/**
 * Created by Sundeep on 2/5/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "tw";

function TW_status(userId,locId){
    /*var document = vars.users[locId];
    return ((document.TW[0])&&(document.TW[0].token));*/
}

function TW_getUserInfo(err,user_info,socketid) {
    if(err){
        return console.log("Error getting user info for twitter : ",err);
    }
    if (user_info) {
        sockets.socketsSessions[socketid].emit('TW_UserInfo', user_info);
    }
}

function TW_getMeFeed(err,feed,socketid) {
    if(err){
        return console.log("Error getting feed for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TW_MeFeed", feed);
}


function TW_getMentionsFeed(err,feed,socketid) {
    if(err){
        return console.log("Error getting mentions feed for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TW_Mentions", feed);
}

function TW_getYouFeed(err,feed,socketid) {
    if(err){
        return console.log("Error getting your feed for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TW_Posted", feed);
}

function TW_loadMoreFeed(err,feed,socketid) {
    if(err){
        return console.log("Error getting more feed for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TW_LoadMore", feed);
}

function TW_post(userId,locId,text,stream,oldId,done) {

}

function TW_postSchedule(userId,locId,text,stream,oldId,done) {

}

function TW_retweetPost(err,res,id,socketid) {
    if(res==true) {
        return sockets.socketsSessions[socketid].emit("TW_RetweetSuccesful", id);
    }
}

function TW_getPost(userId,locId,id,done) {

}

function TW_delPost(err,res,socketid) {
    if(err){
        return console.log("Error deleting post for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TW_Deleted", res);
}

function TW_favoritePost(err,fav,id,socketid) {
    if(fav==true) {
        sockets.socketsSessions[socketid].emit("TW_LikeSuccesful", id);
    }
}

function TW_unfavoritePost(err,fav,id,socketid) {
    if(fav==false) {
        sockets.socketsSessions[socketid].emit("TW_UnLikeSuccesful", id);
    }
}

function TW_replyPost(err,res,id,socketid) {
    if(err) return sockets.socketsSessions[socketid].emit("TW_CommentUnsuccesful", null,id);
    return sockets.socketsSessions[socketid].emit("TW_CommentSuccesful", res.in_reply_to_status_id_str,id);

}

function TW_getFriends(err,res,socketid) {
    if(err){
        return console.log("Error getting Friends for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("tw_friends", res);
}

function TW_getFollwers(err,res,socketid) {
    if(err){
        return console.log("Error getting Follwers for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("tw_follwers", res);
}

function TW_getRetweets(err,id,Retweet,socketid) {
    if(err){
        return console.log("Error getting retweets for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("Retweets", id,Retweet);
}

function TW_Follow(err,res,id,socketid) {
    if(err){
        return console.log("Error TW_Follow for twitter : ",err);
    }
    if(res==true) {
        return sockets.socketsSessions[socketid].emit("TW_FollowSuccesful", id);
    }
}

function TW_UnFollow(err,res,id,socketid) {
    if(err){
        return console.log("Error TW_UnFollow for twitter : ",err);
    }
    if(res==true) {
        return sockets.socketsSessions[socketid].emit("TW_UnFollowSuccesful", id);
    }
}

function TW_Search(err,Retweets,q,socketid) {
    if(err){
        return console.log("Error getting retweets for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("TweetsNearYou", Retweets,q);
}


function TW_getInReplyTweets(err,tweetObject,socketid){
    if(err){
        return console.log("Error getting retweets for twitter : ",err);
    }
    sockets.socketsSessions[socketid].emit("InReplyTweets", tweetObject);
}

function TW_clearFeed(userId,locId,done) {

}

exports.status           = TW_status;
exports.getUserInfo      = TW_getUserInfo;
exports.getMeFeed        = TW_getMeFeed;
exports.post             = TW_post;
exports.getPost          = TW_getPost;
exports.delPost          = TW_delPost;
exports.postSchedule     = TW_postSchedule;
exports.getFriends       = TW_getFriends;
exports.getFollwers      = TW_getFollwers;
exports.clearFeed        = TW_clearFeed;
exports.retweetPost      = TW_retweetPost;
exports.favoritePost     = TW_favoritePost;
exports.unfavoritePost   = TW_unfavoritePost;
exports.replyPost        = TW_replyPost;
exports.loadMoreFeed     = TW_loadMoreFeed;
exports.getMentionsFeed  = TW_getMentionsFeed;
exports.getYouFeed       = TW_getYouFeed;
exports.getRetweets      = TW_getRetweets;
exports.follow           = TW_Follow;
exports.unfollow         = TW_UnFollow;
exports.getInReplyTweets = TW_getInReplyTweets;
exports.Search           = TW_Search;




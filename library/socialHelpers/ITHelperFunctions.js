/**
 * Created by Sundeep on 2/6/2015.
 */

var sockets             = require("../sockets");
var moduleString        = "it";


function IT_status(err,status){
    if(err){
        return !!err;
    }
    return status;
}

function IT_getUserInfo(err,IT_UserInfo, socketid){
    if(err){
        return console.log("Error getting user info for instagram : ",err);
    }
    if (IT_UserInfo) {
        sockets.socketsSessions[socketid].emit('IT_UserInfo', IT_UserInfo);
    }
}

function IT_getMeFeed(err,feed,next,socketid) {
    if(err){
        return console.log("Error getting feed for instagram : ",err);
    }
    sockets.socketsSessions[socketid].emit("IT_MeFeed", feed,next);
    console.log("Instagram MeFeed Sequence Completed");
}


function IT_getYouFeed(err,feed,next,socketid) {
    if(err){
        return console.log("Error getting feed for instagram : ",err);
    }
    sockets.socketsSessions[socketid].emit("IT_YouFeed", feed,next);
    console.log("Instagram MeFeed Sequence Completed");
}


function IT_loadMoreFeed(errr,feed,next){
    if(err){
        return console.log("Error returning more feed on instagram : ",err);
    }
    socket.emit("IT_LoadMore",feed,next)
}

function IT_likePost(err,res,id,socketid) {
    if(err){
        return console.log("Error liking on instagram : ",err);
    }
    if(res==true) {
        sockets.socketsSessions[socketid].emit("IT_LikeSuccesful", id);
    }
}

function IT_commentPost(err,res,id,socketid) {
    if(err){
        console.log("Error liking on instagram : ",err);
        return sockets.socketsSessions[socketid].emit("IT_CommentUnsuccesful", id);
    }
    return sockets.socketsSessions[socketid].emit("IT_CommentSuccesful", id);
}

function IT_getFriends(err,list,paging,socketid){
    if(err){
        return console.log("Error getting Friends on instagram : ",err);
    }
    sockets.socketsSessions[socketid].emit("it_friends",list);
}

function IT_getFollwers(err,list,paging,socketid){
    if(err){
        return console.log("Error getting Follwers on instagram : ",err);
    }
    sockets.socketsSessions[socketid].emit("it_follwers",list);
}


function IT_clearFeed(userId,locId,done) {
    var document = vars.users[locId];
    if(!document.IT[0].cache.feeds){
        document.IT[0].cache.feeds = {};
    }
    document.IT[0].updateTime.feeds = new Date(1427988307103);
    document.IT[0].cache.feeds.me = [];
    document.IT[0].markModified('cache.feeds');
    document.save(function (err, doc) {
        done(document.IT[0].cache.feeds.me);
        if(err) return console.log(err);
        console.log("Doc Saved");
    });
}

exports.status           = IT_status;
exports.getUserInfo      = IT_getUserInfo;
exports.getMeFeed        = IT_getMeFeed;
exports.getYouFeed       = IT_getYouFeed;
exports.clearFeed        = IT_clearFeed;
exports.likePost         = IT_likePost;
exports.commentPost      = IT_commentPost;
exports.loadMoreFeed     = IT_loadMoreFeed;
exports.getFriends       = IT_getFriends;
exports.getFollwers      = IT_getFollwers;
exports.moduleString     = moduleString;


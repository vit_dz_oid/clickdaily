/**
 * Created by Sundeep on 4/20/2015.
 */

var vars        = require('./Vars/socialVars.js');
var FB          = require('./FBHelperFunctions');
var TW          = require('./TWHelperFunctions');
var Schedule    = require('./ScheduleHelperFunctions');
var post        = require('../../models/socialMedia').post;
var sockets     = require("../sockets");
var redis       = require('redis');
var client      = redis.createClient(null, null, {detect_buffers: true});

function create(err, body, network,postdata,socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        console.log("Error posting on fb : ",err);
        if(network==0) {
            return sockets.socketsSessions[socketid].emit('FB_PostUnsuccessful');
        } else {
            return sockets.socketsSessions[socketid].emit('TW_PostUnsuccessful');
        }
    }
    if(network==0) {
        sockets.socketsSessions[socketid].emit('FB_PostSuccessful');
    } else {
        sockets.socketsSessions[socketid].emit('TW_PostSuccessful');
    }
}

function schedule(err, body, network,postdata,socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",error.stack);
    }
    if(err) {
        console.log("Error posting on fb : ",err);
        if(network==0) {
            return sockets.socketsSessions[socketid].emit('FB_PostUnsuccessful',postdata.id,postdata.oldId);
        } else if(network==1){
            return sockets.socketsSessions[socketid].emit('TW_PostUnsuccessful',postdata.id,postdata.oldId);
        } else {
            return sockets.socketsSessions[socketid].emit('IT_PostUnsuccessful',postdata.id,postdata.oldId);
        }
    }
    if(network==0) {
        sockets.socketsSessions[socketid].emit('FB_PostSuccessful',postdata.id,postdata.oldId);
    } else if(network==1) {
        sockets.socketsSessions[socketid].emit('TW_PostSuccessful',postdata.id,postdata.oldId);
    } else {
        sockets.socketsSessions[socketid].emit('IT_PostSuccessful',postdata.id,postdata.oldId);
    }
}

function getPosts(err, post, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('schedules', post);
}

function getPost(err, post, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        console.log("Error getting post : ", err);
        return sockets.socketsSessions[socketid].emit("getPostFailed");
    }

    var lastLocation = sockets.socketsSessions[socketid].handshake.session.clickdaily.locations.filter(function(d){
        return d._id =  sockets.socketsSessions[socketid].handshake.session.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    if(post && post.post_data && post.post_data.stream){
        console.log("post.post_data.stream : ",post.post_data.stream);
        client.get(post.post_data.stream, function (err,data) {
            client.del(post.post_data.stream);
            console.log("client.get : ",err);
            post.post_data.stream = data;
            sockets.socketsSessions[socketid].emit('postData', post,userType)
        });
    }else {
        sockets.socketsSessions[socketid].emit('postData', post,userType)
    }
}

function getHistoryPost(err, post, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        console.log("Error getting post : ", err);
        return sockets.socketsSessions[socketid].emit("getPostFailed");
    }

    var lastLocation = sockets.socketsSessions[socketid].handshake.session.clickdaily.locations.filter(function(d){
        return d._id =  sockets.socketsSessions[socketid].handshake.session.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    sockets.socketsSessions[socketid].emit('postData', post,userType);
}

function delPost(err, post, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return sockets.socketsSessions[socketid]
            .emit("delPostFailed");
    }
    return sockets.socketsSessions[socketid]
        .emit("postDeleted",post);
}

function duplicatePost(err, body, network,postdata,socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        console.log("Error posting on fb : ",err);
        if(network==0) {
            return sockets.socketsSessions[socketid].emit('FB_PostUnsuccessful',postdata.id,postdata.oldId);
        } else {
            return sockets.socketsSessions[socketid].emit('TW_PostUnsuccessful',postdata.id,postdata.oldId);
        }
    }
    if(network==0) {
        sockets.socketsSessions[socketid].emit('FB_PostSuccessful',postdata.id,postdata.oldId);
    } else {
        sockets.socketsSessions[socketid].emit('TW_PostSuccessful',postdata.id,postdata.oldId);
    }
}

function getPendingApprovalPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('pendingApprovalPosts', posts);
}


function getNeedingApprovalPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('needingApprovalPosts', posts);
}


function getScheduledPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('scheduledPosts', posts);
}


function getRejectedPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('rejectedPosts', posts);
}

function getHistoryPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('historyPosts', posts);
}

function getExpiredPosts(err, posts, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        return console.log("Error getting posts : ", err);
    }
    sockets.socketsSessions[socketid].emit('expiredPosts', posts);
}

function approvePost(err, id, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        sockets.socketsSessions[socketid].emit('approvedPostFailed', id);
        return console.log("Error approvedPost : ", err);
    }
    sockets.socketsSessions[socketid].emit('approvedPost', id);
}

function rejectPost(err, id, socketid){
    if((typeof sockets.socketsSessions[socketid] != "object")||(typeof sockets.socketsSessions[socketid].emit != "function")){
        var error = new Error("Socket Lost");
        return console.log(error.message," : \n",JSON.stringify(error));
    }
    if(err) {
        sockets.socketsSessions[socketid].emit('rejectedPostFailed', id);
        return console.log("Error rejectedPost : ", err);
    }
    sockets.socketsSessions[socketid].emit('rejectedPost', id);
}

exports.create                        = create;
exports.schedule                      = schedule;
exports.getPosts                      = getPosts;
exports.getPost                       = getPost;
exports.getHistoryPost                = getHistoryPost;
exports.delPost                       = delPost;
exports.duplicatePost                 = duplicatePost;
exports.approvePost                   = approvePost;
exports.rejectPost                    = rejectPost;
exports.getNeedingApprovalPosts       = getNeedingApprovalPosts;
exports.getScheduledPosts             = getScheduledPosts;
exports.getRejectedPosts              = getRejectedPosts;
exports.getPendingApprovalPosts       = getPendingApprovalPosts;
exports.getExpiredPosts               = getExpiredPosts;
exports.getHistoryPosts               = getHistoryPosts;

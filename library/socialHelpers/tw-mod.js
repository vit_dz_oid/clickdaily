/**
 * Created by Sundeep on 3/9/2015.
 */
var tw = require('twitter-js-client');
var request = require('request');
tw.Twitter.prototype.postMedia = function (params, error, success) {
    if((typeof (params.text)!= 'string')||(params.text=='')){
        params.text = 'Image';
    }
    var url = 'https://upload.twitter.com/1.1/media/upload.json';
    var auth =
    { consumer_key: this.consumerKey
        , consumer_secret: this.consumerSecret
        , token: this.accessToken
        , token_secret: this.accessTokenSecret
    };
    console.log('auth -',auth);
    request.post({url:url
            ,oauth:auth
            ,preambleCRLF: true
            ,postambleCRLF: true
            ,formData:{'media':new Buffer(params.media)}}
        ,function (e,response,body) {
            body = JSON.parse(body);
            //console.log('\nerror - ',e,'\nbody - ',body.media_id_string);
            url = 'https://api.twitter.com/1.1/statuses/update.json';
            request.post({url:url
                    ,oauth:auth
                    ,preambleCRLF: true
                    ,postambleCRLF: true
                    ,qs:{status:params.text,media_ids:body.media_id_string}}
                ,function(e,response,body) {
                    body = JSON.parse(body);
                    success(body);
                }
            );
        }
    );
};

tw.Twitter.prototype.doPost = function (url, error, success) {
    var auth =
    { consumer_key: this.consumerKey
        , consumer_secret: this.consumerSecret
        , token: this.accessToken
        , token_secret: this.accessTokenSecret
    };
    request.post({url:url
            ,oauth:auth
            ,preambleCRLF: true
            ,postambleCRLF: true
            // ,qs:{status:params.text}
        }
        ,function(err,response,body) {
            body = JSON.parse(body);
            if (!err && response.statusCode == 200) {
                success(body);
            } else {
                error(err, response, body);
            }
        }
    );
};

tw.Twitter.prototype.tweet = function (params, error, success) {
    var path = '/statuses/update.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doPost(url, error, success);
};

tw.Twitter.prototype.retweet = function (params, error, success) {
    if(params.id) {
        var path = '/statuses/retweet/' + params.id + '.json';
        var url = this.baseUrl + path;
        this.doPost(url, error, success);
    } else {
        error("tweet id missing");
    }
};

tw.Twitter.prototype.getReTweets = function (params, error, success) {
    if(params.id) {
        var path = '/statuses/retweets/' + params.id + '.json'  + this.buildQS(params);
        var url = this.baseUrl + path;
        this.doRequest(url, error, success);
    } else {
        error("tweet id missing");
    }
};

tw.Twitter.prototype.favorite = function (params, error, success) {
    var path = '/favorites/create.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doPost(url, error, success);
};

tw.Twitter.prototype.unfavorite = function (params, error, success) {
    var path = '/favorites/destroy.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doPost(url, error, success);
};

tw.Twitter.prototype.follow = function (params, error, success) {
    var path = '/friendships/create.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doPost(url, error, success);
};

tw.Twitter.prototype.unfollow = function (params, error, success) {
    var path = '/friendships/destroy.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doPost(url, error, success);
};

tw.Twitter.prototype.getFriends = function (params, error, success) {
    var path = '/friends/list.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doRequest(url, error, success);
};

tw.Twitter.prototype.getFollowers = function (params, error, success) {
    var path = '/followers/list.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doRequest(url, error, success);
};


tw.Twitter.prototype.delTweet = function (params, error, success) {
    if(params.id) {
        var path = '/statuses/destroy/' + params.id + '.json';
        var url = this.baseUrl + path;
        this.doPost(url, error, success);
    } else {
        error("tweet id missing");
    }
};

tw.Twitter.prototype.search = function (params, error, success) {
    var path = '/search/tweets.json' + this.buildQS(params);
    var url = this.baseUrl + path;
    this.doRequest(url, error, success);
};
module.exports = tw.Twitter;

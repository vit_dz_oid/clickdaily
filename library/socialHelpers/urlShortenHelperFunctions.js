/**
 * Created by Sundeep on 8/30/2015.
 */
var sockets     = require("../sockets");

function addUrl(err, hash,url,socketid){
    if(err) {
        console.log("Error creating url : ",err);
        return sockets.socketsSessions[socketid].emit('URL_CreateFailed',url);
    }
    sockets.socketsSessions[socketid].emit('URL_Created',hash,url);
}

function getAll(err, list,socketid){
    if(err) {
        console.log("Error creating url : ",err);
        return sockets.socketsSessions[socketid].emit('URL_listFailed');
    }
    sockets.socketsSessions[socketid].emit('URL_list',list);
}

exports.addUrl = addUrl;
exports.getAll = getAll;
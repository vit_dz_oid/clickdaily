/**
 * Created by Sundeep on 2/20/2015.
 */
var FB = require('./FBHelperFunctions');
var TW = require('./TWHelperFunctions');
var vars = require('./Vars/socialVars.js');
var random  = require('../../library/random');
var fs = require('fs');

var size = 10;
var Loyalty = require('../../models/socialMedia').loyalty;
var QRcode  = require('../../models/socialMedia').QRcode;

var createLoyaltyQRCodes = function(userId,locId,loyalty_record_id,user,document){
    var codes = [];
    if(! document.loyalty[0].loyalty_record.id(loyalty_record_id).QRcodes){
         document.loyalty[0].loyalty_record.id(loyalty_record_id).QRcodes = [];
    }
    for(var i =0;i<user;i++){
        codes[i] = new QRcode({
                created_at              : new Date(),
                loyalty_record_id       : loyalty_record_id,
                sequenceNumber          : i+1
            });
        document.loyalty[0].loyalty_record.id(loyalty_record_id).QRcodes.push(codes[i].id);
        codes[i].save(function(err){
            if(err) console.log("error : ",err);
        });
    }   
    document.markModified('loyalty');
    document.save(function(err){
        if(err) console.log("error : ",err);
    });

};

var post = function(userId, locId, form, buff,type,done){
    var document = vars.users[locId];
    if(!document.loyalty[0]) {
        var newLoyalty = new Loyalty();
        newLoyalty.created_at = new Date();
        document.loyalty.push(newLoyalty);
    }
    var cachedRecordId = random(size);
    var count = 0;
    while(vars.loyaltyIdTranslation[cachedRecordId]){
        size += Math.floor(count/size);
        count +=1;
        cachedRecordId = random(size);
    }
    var loyalty_record = document.loyalty[0].loyalty_record.create({
        discount:form.discount,
        user:form.user,
        created_at:(new Date()),
        cachedRecordId : cachedRecordId
    });

    var loyalty_record_id = loyalty_record.id;
    vars.loyaltyIdTranslation[cachedRecordId] = {
        record_id:loyalty_record_id,
        locId:locId,
        userId:userId,
        pageID:document.FB[0].selected_page,
        type: type
    };
    document.loyalty[0].loyalty_record.push(loyalty_record);
    document.markModified('loyalty');
    document.save(function(err,doc){
        if(err) console.log("error : ",err);
    });
    createLoyaltyQRCodes(userId,locId,loyalty_record_id,form.user,document);
    var imageName = cachedRecordId+"."+type;
    var FB_text = "First "
        + form.user
        + " users to like this will get "
        + form.discount
        + "% discount. Click link to claim.";
    var TW_text = "First "
        + form.user
        + " users to retweet this will get "
        + form.discount
        + "% discount. Click link to claim."
        +"https://merchant.clickdaily.com/redeem?id="+imageName;
    if (TW.status(userId, locId)) {
        TW.post(userId, locId, TW_text, buff, null, function (err,body) {
            vars.loyaltyIdTranslation[cachedRecordId].twPostID = body.id_str;
            document.loyalty[0].loyalty_record.id(loyalty_record_id).twPostID = body.id_str;
            done('tw');
            document.markModified('loyalty');
            document.save(function(err,doc){
                if(err) {
                    console.log("error : ",err);
                    done(err,'tw');
                }
                done(null,'tw');
            });
        });
    }
    
    fs.writeFile("data/loyalty/"+imageName,buff, function(err) {
        if(err) {
            console.log(err);
        } else {
            if (FB.status(userId, locId)) {
                FB.postLink(userId, locId, "https://merchant.clickdaily.com/redeem?id="+imageName, FB_text,"CLICKDAILY.COM", "https://merchant.clickdaily.com/loyalty_image/"+imageName, function (body) {
                    vars.loyaltyIdTranslation[cachedRecordId].fbPostID = body.id;
                    document.loyalty[0].loyalty_record.id(loyalty_record_id).fbPostID = body.id;
                    document.markModified('loyalty');
                    document.save(function(err,doc){
                        if(err) {
                            console.log("error : ",err);
                            done(err,'fb');
                        }
                        done(null,'fb');
                    });
                });
            }
        }
    }); 
    
};

exports.post = post;









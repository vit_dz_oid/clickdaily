/**
 * Created by Sundeep on 1/9/2015.
 */

var getData =  require('../library/GetData4View');
var jade = require('jade');



exports.update = function(socket,FilterType,StartDate,EndDate){
    getData.getSalesSummaryReport(socket.handshake.session.clickdaily.lastLocationId,FilterType,StartDate,EndDate,function(SalesPaymentReport,SalesRevenueReport,SalesTaxReport){

        var html = jade.renderFile('./views/sales_update.jade',{title : "Sales Report",SalesPaymentReport:SalesPaymentReport,SalesRevenueReport:SalesRevenueReport,SalesTaxReport:SalesTaxReport,FilterType:FilterType});
        socket.emit("update",html);
    });
};
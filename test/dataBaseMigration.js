/**
 * Created by Sundeep on 6/25/2015.
 */

var Social = require('../models/socialMedia').Social;
var fb = require('../Microservices/fbMicroService/Model.js').fb;
var fs = require('../Microservices/fsMicroService/Model.js').fs;
var tw = require('../Microservices/twMicroService/Model.js').tw;
var it = require('../Microservices/itMicroService/Model.js').it;
var keywords = require('../Microservices/keywordsMicroApp/Model.js').keywords;
var post = require('../Microservices/postMicroService/Model.js').post;

Social.find({},function(err,r){
    r.forEach(function(d){

        /*Facebook*/
        if(d.FB[0]){
            var FB = new fb(d.FB[0].toObject());
            FB._id = d._id;
            FB.save(function(err){
                console.log(err);
            });
        }


        /*Twitter*/
        if(d.TW[0]) {
            var TW = new tw(d.TW[0].toObject());
            TW._id = d._id;
            TW.save(function (err) {
                console.log(err);
            });
        }

        /*Facebook*/
        if(d.IT[0]) {
            var IT = new it(d.IT[0].toObject());
            IT._id = d._id;
            IT.save(function (err) {
                console.log(err);
            });
        }

        /*Facebook*/
        if(d.FS[0]) {
            var FS = new fs(d.FS[0].toObject());
            FS._id = d._id;
            FS.save(function (err) {
                console.log(err);
            });
        }

        if(d.keywords) {
            var Keywords = new keywords;
            d.keywords.forEach(function(e){
                Keywords.list.push({value:e});
            });
            Keywords._id = d._id;
            Keywords.save(function (err) {
                console.log(err);
            });
        }

        if(d.posts) {
            var newPost = new post;
            d.posts.forEach(function(e){
                e = e.toObject();
                e.network_data = {
                    reached   : e.num_reached,
                    likes     : e.num_likes,
                    comments  : e.num_comments,
                    post_id   : e.post_id
                };
                e.post_data = {
                    stream : e.data.stream,
                    text   : e.data.text,
                    oldId  : e.oldId
                };
                newPost.posts.push(e);

            });
            newPost._id = d._id;
            newPost.save(function (err) {
                console.log(err);
            });

        }
    });
});
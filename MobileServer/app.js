var http = require('http');
//var https = require('https');
var fs = require('fs');

var express = require('express');
var path = require('path');

var mongoose = require('mongoose');

var logger = require('morgan');
var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');
var passport = require('./library/passport');
var MongoStore = require('connect-mongo')(session);

var sessionStore = new MongoStore({
    db : 'merchant-sessions'
});

var social_r    = require('./routes/social');

var configDB = require('../config/database.js');

var commonUserFunction = require('../routes/helpers/commonUserFunction');

// configuration ===============================================================
// connect to our database
mongoose.connect(configDB.url);

var app = express();

require('./config/passport')(passport);

// view engine setup
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.raw());

//app.use(require('node-compass')({mode: 'expanded'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'fbbb68de03dd586fe796e56e7b614d6a793e517e4f5e6e1a18a53921dc4ad933'
    , name : 'mobile'
    , store : sessionStore
    , resave : false
    , saveUninitialized : false
//    , domain : '.clickdaily.com'
    , cookie : {
        maxAge : 1*60*60*1000,
        domain : '.clickdaily.com'
    }
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use('/social', social_r);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;

    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    app.use(function(err, req, res, next) {
        //errLogger.routeErr(req.user,req.path,err);
        console.error(err.stack);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            layoutObject:commonUserFunction.viewObject(req.user)
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    //errLogger.routeErr(req.user,req.path,err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        layoutObject:commonUserFunction.viewObject(req.user)
    });
});


/**
 * Get port from environment and store in Express.
 */

var ports = parseInt(process.env.PORTS, 10) || 4000;
app.set('port', ports);

var servers = http.createServer(app);
servers.on('error', onError);
servers.on('listening', onListening);
servers.listen(ports);

/**
 * Socket Connections
 */

//module.exports = servers;
var debug = require('debug')('Clickdaily:server');

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error('Port ' + ports + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error('Port ' + ports + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    debug('Listening on port ' + servers.address().port);
}
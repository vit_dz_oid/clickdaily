/**
 * Created by Sundeep on 6/24/2015.
 */


var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialFBRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.getKeyWords = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.addKeywords = function (err, newList, responseId) {
    if(err){
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        delete socialVars.responses[responseId];
        return console.log("Error adding keyword : ",err);
    }
    try {
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.delKeyword = function (err, newList, responseId) {
    if(err){
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        delete socialVars.responses[responseId];
        return console.log("Error adding keyword : ",err);
    }
    try {
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};
/**
 * Created by Sundeep on 3/4/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialStatusRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

var status = function(err, locId,info,result, responseId){

    try{
        socialVars.responses[responseId].send(result)
    } catch(e){
        errorLog.log("Response Expired",e);
    }

    delete socialVars.responses[responseId];
};

exports.status  = status;



/**
 * Created by Sundeep on 6/24/2015.
 */


var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialFBRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.statsLikes = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.statsComments = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.statsMedia = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.statsFollowers = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.statsFollowing = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.statsGrowth = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

/**
 * Created by Sundeep on 6/22/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialFSRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.create = function(err,result, responseId){
    try{
        socialVars.responses[responseId].redirect("/Social/Dashboard")
    } catch(e){
        errorLog.log("Response Expired",e);
    }
};

exports.delNetwork = function(err, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

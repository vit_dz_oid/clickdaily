/**
 * Created by Sundeep on 6/22/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialFBRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.getPostViews = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getPostInteractions = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getUniquePostViews = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getUniquePostInteractions = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getPageYouFeed = function(err,result, responseId){
    try {
        socialVars.responses[responseId].send(result);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.delNetwork = function(err, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getPageFeed = function(err,pageInfo, feed,next, responseId){
    try {
        socialVars.responses[responseId].send(feed);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.likePost = function(err,res,id,responseId) {
    if(err){
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return console.log("Error liking for facebook : ",err);
    }
    if(res.success==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.commentPost = function(err,res,id,responseId) {
    if(err){
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return console.log("Error liking for facebook : ",err);
    }
    if(res.success==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.getUserInfo = function(err,user_info, page_info, selected_page,responseId) {
    if(err){
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return console.log("Error liking for facebook : ",err);
    }
    console.log('FB_doneUserInfo : ', arguments);

    if ((selected_page == "") && (page_info.length)) {
        try {
            socialVars.responses[responseId].send({
                user_info : user_info,
                page_info : page_info
            });
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    } else {
        page_info = page_info.filter(function(d){
            return d.id == selected_page;
        });
        page_info[0].picture = page_info[0].profile_pic;
        delete page_info[0].profile_pic;
        try {
            socialVars.responses[responseId].send(page_info[0]);
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
    console.log("Facebook UserInfo Sequence Completed");
};

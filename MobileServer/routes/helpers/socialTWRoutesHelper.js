/**
 * Created by Sundeep on 6/22/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialTWRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.delNetwork = function(err, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};


exports.getMeFeed = function(err,feed, responseId){
    try {
        socialVars.responses[responseId].send(feed);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getMentionsFeed = function(err,feed, responseId){
    try {
        socialVars.responses[responseId].send(feed);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getYouFeed = function(err,feed, responseId){
    try {
        socialVars.responses[responseId].send(feed);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getFriends = function(err,data, responseId){
    try {
        var friends = [];
        Object.keys(data).forEach(function(d){
            friends.push(data[d]);
        });
        socialVars.responses[responseId].send(friends);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getFollwers = function(err,data, responseId){
    try {
        var follwers = [];
        Object.keys(data).forEach(function(d){
            follwers.push(data[d]);
        });
        socialVars.responses[responseId].send(follwers);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.Search = function(err,Retweets,q,responseId){
    try {
        socialVars.responses[responseId].send(Retweets.statuses);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.follow = function(err,res,id,responseId) {
    if(err){
        console.log("Error TW_Follow for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return;
    }

    if(res==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.unfollow = function(err,res,id,responseId) {
    if(err){
        console.log("Error TW_unfollow for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return;
    }

    if(res==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.retweetPost = function(err,res,id,responseId) {
    if(err){
        console.log("Error TW_unfollow for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return;
    }

    if(res==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.favoritePost = function(err,res,id,responseId) {
    if(err){
        console.log("Error TW_Follow for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return;
    }

    if(res==true) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.unfavoritePost = function(err,res,id,responseId) {
    if(err){
        console.log("Error TW_Follow for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
        return;
    }

    if(res==false) {
        try {
            socialVars.responses[responseId].send("true");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

exports.getUserInfo = function(err,user_info,responseId) {
    if(err){
        console.log("Error getUserInfo for twitter : ",err);
        try {
            socialVars.responses[responseId].send("false");
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
    if (user_info) {
        user_info.friends_count = user_info.friends;
        delete user_info.friends;
        user_info.followers_count = user_info.followers;
        delete user_info.followers;
        try {
            socialVars.responses[responseId].send(user_info);
        } catch(e){
            errorLog.log("Response Expired",e);
        }
    }
};

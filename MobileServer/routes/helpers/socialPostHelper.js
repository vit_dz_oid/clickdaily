/**
 * Created by Sundeep on 7/19/2015.
 */


var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialFBRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');
var redis       = require('redis');
var streamLib       = require('stream')
var client      = redis.createClient(null, null, {detect_buffers: true});

exports.create = function(err,body, network,postdata, responseId){
    try {
        if(err){
            return socialVars.responses[postdata].send("false");
        }
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.schedule = function(err,body, network,postdata, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getPendingApprovalPosts = function(err, posts, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        if(posts && posts.length){
            posts = posts.map(function(d){
                return {
                    create_time : d.create_time,
                    post_time : d.post_time,
                    text : d.post_data.text,
                    id : d._id,
                    stream : d.post_data.stream,
                    network : d.network
                }
            });
        }
        socialVars.responses[responseId].send(posts);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getNeedingApprovalPosts = function(err, posts, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        if(posts && posts.length){
            posts = posts.map(function(d){
                return {
                    create_time : d.create_time,
                    post_time : d.post_time,
                    text : d.post_data.text,
                    id : d._id,
                    stream : d.post_data.stream,
                    network : d.network
                }
            });
        }
        socialVars.responses[responseId].send(posts);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getScheduledPosts = function(err, posts, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        if(posts && posts.length){
            posts = posts.map(function(d){
                return {
                    create_time : d.create_time,
                    post_time : d.post_time,
                    text : d.post_data.text,
                    id : d._id,
                    stream : d.post_data.stream,
                    network : d.network
                }
            });
        }
        socialVars.responses[responseId].send(posts);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getRejectedPosts = function(err, posts, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        if(posts && posts.length){
            posts = posts.map(function(d){
                return {
                    create_time : d.create_time,
                    post_time : d.post_time,
                    text : d.post_data.text,
                    id : d._id,
                    stream : d.post_data.stream,
                    network : d.network
                }
            });
        }
        socialVars.responses[responseId].send(posts);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.approvePost = function(err, id, responseId){
    console.log("Get approve post.");
    console.log("err : ",err);
    console.log("id : ",id);
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.rejectPost = function(err, id, responseId){
    try {
        if(err){
            socialVars.responses[responseId].send("false");
        }
        socialVars.responses[responseId].send("true");
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getPostPicture = function(err,picString, responseId){
    try {
        if(err){
            console.log("Error Getting Picture.",err);
            return socialVars.responses[responseId].sendStatus(404);
        }
        console.log("Getting Picture.");
        client.get(picString,function(err,data) {
            if (err) {
                return socialVars.responses[responseId].sendStatus(404);
            }
            console.log("Got Picture.");
            var binString = new Buffer(data, 'base64');
            var upStream = new streamLib.Readable();
            upStream._read = function noop() {};
            upStream.push(binString);
            upStream.push(null);
            client.del(picString);

            socialVars.responses[responseId].set({
                'Content-Type': 'image/png',
                'Content-Length': binString.length,
                'Cache-Control': 'public, max-age=0',
                'Accept-Ranges': 'bytes'
            });
            upStream.pipe(socialVars.responses[responseId]);
            delete socialVars.responses[responseId];
        });
    } catch(e){
        errorLog.log("Response Expired",e);
    }

};
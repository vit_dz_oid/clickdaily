/**
 * Created by Sundeep on 6/22/2015.
 */

var path = require('path');
var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : 'socialITRoutes'
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);
var socialVars = require('./socialRoutesVars');

exports.getFriends = function(err,data,paging, responseId){
    try {
        socialVars.responses[responseId].send(data);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getFollwers = function(err,data,paging, responseId){
    try {
        socialVars.responses[responseId].send(data);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getMeFeed = function(err,data,paging, responseId){
    try {
        socialVars.responses[responseId].send(data);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getYouFeed = function(err,data,paging, responseId){
    try {
        socialVars.responses[responseId].send(data);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.delNetwork = function(err, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.likePost = function(err,like, id, responseId){
    var reply = "true";
    if(err){
        reply = err;
    }
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};

exports.getUserInfo = function(err,IT_UserInfo, responseId){
    console.log("It got user info : ",arguments);
    var reply = IT_UserInfo;
    if(err){
        reply = "false";
    }
    reply.profile_picture = reply.profile_pic;
    delete reply.profile_pic;
    try {
        socialVars.responses[responseId].send(reply);
    } catch(e){
        errorLog.log("Response Expired",e);
    }
    delete socialVars.responses[responseId];
};
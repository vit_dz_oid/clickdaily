/**
 * Created by Sundeep on 6/23/2015.
 */
var express = require('express');
var router = express.Router();
var uuid = require('node-uuid');
var commonUserFunction = require('../../routes/helpers/commonUserFunction');
var library = require('./helpers/library');
var passport = require('../library/passport');
var socialVars = require('./helpers/socialRoutesVars');
var redisRPCLib         = require("redis-rpc");


var socialStatusHelper      = require('./helpers/socialRoutesHelper');
var socialFBHelper          = require('./helpers/socialFBRoutesHelper');
var socialTWHelper          = require('./helpers/socialTWRoutesHelper');
var socialFSHelper          = require('./helpers/socialFSRoutesHelper');
var socialITHelper          = require('./helpers/socialITRoutesHelper');
var socialKeywordsHelper    = require('./helpers/socialKeywordsHelper');
var socialPostHelper        = require('./helpers/socialPostHelper');
var insightCBs              = require('./helpers/insightCBs');

var moduleString = 'mobileDispatcher';

var itRPC = new redisRPCLib({
    subModule : moduleString + "it",
    pubModule : "it" + moduleString,
    tasks     : socialITHelper
});

var fsRPC = new redisRPCLib({
    subModule : moduleString + "fs",
    pubModule : "fs" + moduleString,
    tasks     : socialFSHelper
});

var twRPC = new redisRPCLib({
    subModule : moduleString + "tw",
    pubModule : "tw" + moduleString,
    tasks     : socialTWHelper
});


var fbRPC = new redisRPCLib({
    subModule : moduleString + "fb",
    pubModule : "fb" + moduleString,
    tasks     : socialFBHelper
});

var statusRPC = new redisRPCLib({
    subModule : moduleString + "status",
    pubModule : "status" + moduleString ,
    tasks     : socialStatusHelper
});

var keywordsRPC = new redisRPCLib({
    subModule : moduleString + "keywords",
    pubModule : "keywords" + moduleString ,
    tasks     : socialKeywordsHelper
});

var postRPC = new redisRPCLib({
    subModule : moduleString + "post",
    pubModule : "post" + moduleString,
    tasks     : socialPostHelper
});

var insightsRPC = new redisRPCLib({
    subModule : moduleString + "insights",
    pubModule : "insights" + moduleString,
    tasks     : insightCBs
});


function isLoggedIn(req, res, next) {
    commonUserFunction.log(req.user,req.baseUrl + req.path);
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

function getLastLocation(user,session){
    if(session.lastLoc){
        return session.lastLoc;
    }
   return user.clickdaily.lastLocationId;
}

router.get('/fail',function(req, res) {
    return res.send('false');
});

router.post('/Login'
    ,function(req, res, next) {
        next();
    }
    ,passport.authenticate('local-login',{
        failureRedirect : '/social/fail' // redirect back to the signup page if there is an error
    })
    ,function(req, res) {
        res.send('true');
    }
);

router.get('/GetMerchantLocations',isLoggedIn,function(req,res){
    commonUserFunction.viewObject(req.user,req.session.lastLoc,function(err,info){
        if(err){
            res.sendStatus(404);
        }
        if(info&&info.location) {
            res.send({
                lastLocationId: info.location.selected
                , locations: info.location.all.map(function(d){
                    d.logo = '/logos/'+d.id;
                    return d;
                })
            });
        }else{
            res.sendStatus(404);
        }
    });
});

router.post('/Logoff',isLoggedIn,function(req,res){
    req.logout();
    res.send('true');
});

/*Status*/

router.get('/Status',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    statusRPC.sendCall({
        task        : "status",
        args        : [locId,{}],
        sessionId   : responseId
    });
});


/*Facebook*/

router.get('/FB/GetUserInfo',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getUserInfo",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/FB/GetPostViews',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getPostViews",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/FB/getPostInteractions',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getPostInteractions",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/FB/getUniquePostViews',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getUniquePostViews",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/FB/getUniquePostInteractions',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getUniquePostInteractions",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/FB/getPostsCreated',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getPageYouFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/FB/Timeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "getPageFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/FB/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    fbRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});

router.post('/FB/comment',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var text = req.query.text;
    var id   = req.query.id;

    fbRPC.sendCall({
        task        : "commentPost",
        args        : [locId,id,text],
        sessionId   : responseId
    });
});

router.post('/FB/like',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id   = req.query.id;

    fbRPC.sendCall({
        task        : "likePost",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/FB/post',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var task = "create";
    var data = {
        text : req.query.text
    };
    if(req.query.schedule ){
        data.dateTime = req.query.schedule;
        task = "schedule";
    }
    postRPC.sendCall({
        task        : task,
        args        : [locId,0,1,data],
        sessionId   : responseId
    });
});

/*Twitter*/

router.get('/TW/GetUserInfo',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getUserInfo",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/TW/Timeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getMeFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/TW/GetMentions',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getMentionsFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/TW/getPostsCreated',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getYouFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/TW/Followers',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getFollwers",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/TW/Following',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "getFriends",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/TW/getPotentialCustomers',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var lat  = req.query.lat;
    var long = req.query.long;
    var rad  = req.query.rad;
    var q    = req.query.keyword;
    var next = req.query.next;
    if(!next){
        next = null;
    }
    if(!lat) {
        lat = "40.758680";
    }
    if(!long) {
        long = "-73.979723";
    }
    if(!rad) {
        rad = "10mi";
    }
    var geocode = lat+","+long+","+rad;
    twRPC.sendCall({
        task        : "Search",
        args        : [locId,q,geocode,next],
        sessionId   : responseId
    });
});

router.get('/TW/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    twRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});

router.post('/TW/follow',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id  = req.query.id;

    twRPC.sendCall({
        task        : "follow",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/TW/unfollow',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id  = req.query.id;

    twRPC.sendCall({
        task        : "unfollow",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/TW/retweet',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id  = req.query.id;

    twRPC.sendCall({
        task        : "retweetPost",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/TW/favorite',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id  = req.query.id;

    twRPC.sendCall({
        task        : "favoritePost",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/TW/unfavorite',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var id  = req.query.id;

    twRPC.sendCall({
        task        : "unfavoritePost",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/TW/post',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var task = "create";
    var data = {
        text : req.query.text
    };
    if(req.query.schedule ){
        data.dateTime = req.query.schedule;
        task = "schedule";
    }
    postRPC.sendCall({
        task        : task,
        args        : [locId,1,1,data],
        sessionId   : responseId
    });
});

router.post('/TW/reply',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var task = "create";
    var data = {
        text : req.query.text,
        inReplyto : req.query.id
    };
    if(req.query.schedule ){
        data.dateTime = req.query.schedule;
        task = "schedule";
    }
    postRPC.sendCall({
        task        : task,
        args        : [locId,1,1,data],
        sessionId   : responseId
    });
});

router.post('/TW/statsLike',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsLikes",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});

router.post('/TW/statsComments',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsComments",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});

router.post('/TW/statsMedia',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsMedia",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});

router.post('/TW/statsFollowers',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsFollowers",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});

router.post('/TW/statsFollowing',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsFollowing",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});

router.post('/TW/statsGrowth',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsGrowth",
        args        : [locId,1,t],
        sessionId   : responseId
    });
});


/*Instagram*/

router.get('/IT/GetUserInfo',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "getUserInfo",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/IT/Timeline',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "getMeFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/IT/GetPostsCreated',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "getYouFeed",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/IT/Followers',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "getFollwers",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/IT/Following',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "getFriends",
        args        : [locId,true],
        sessionId   : responseId
    });
});

router.get('/IT/delNetwork',isLoggedIn,function(req,res) {
    var userId = req.user.id;
    var locId = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "delNetwork",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/IT/Like',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var id = req.query.id;
    socialVars.responses[responseId] = res;
    itRPC.sendCall({
        task        : "likePost",
        args        : [locId,id],
        sessionId   : responseId
    });
});

router.post('/IT/statsLike',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsLikes",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});

router.post('/IT/statsComments',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsComments",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});

router.post('/IT/statsMedia',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsMedia",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});

router.post('/IT/statsFollowers',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsFollowers",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});

router.post('/IT/statsFollowing',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsFollowing",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});

router.post('/IT/statsGrowth',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    var t   = req.query.t;
    insightsRPC.sendCall({
        task        : "statsGrowth",
        args        : [locId,2,t],
        sessionId   : responseId
    });
});


/*Keywords*/

router.get('/posts/getPendingApprovalPosts', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var lastLocation = req.user.clickdaily.locations.filter(function(d){
        return d._id =  req.user.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    console.log("getPendingApprovalPosts Logs for User Type : \n");
    console.log("\n-- VARIABLES --\n\n");
    console.log("userId : "+ userId +"\n");
    console.log("locId : "+ locId +"\n");
    console.log("lastLocation : "+ JSON.stringify(lastLocation) +"\n");
    console.log("userType : "+ userType +"\n");
    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "getPendingApprovalPosts",
        args        : [locId,userType],
        sessionId   : responseId
    });
});

router.get('/posts/getNeedingApprovalPosts', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var lastLocation = req.user.clickdaily.locations.filter(function(d){
        return d._id =  req.user.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    console.log("getNeedingApprovalPosts Logs for User Type : \n");
    console.log("\n-- VARIABLES --\n\n");
    console.log("userId : "+ userId +"\n");
    console.log("locId : "+ locId +"\n");
    console.log("lastLocation : "+ JSON.stringify(lastLocation) +"\n");
    console.log("userType : "+ userType +"\n");
    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "getNeedingApprovalPosts",
        args        : [locId,userType],
        sessionId   : responseId
    });
});

router.get('/posts/getScheduledPosts', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "getScheduledPosts",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/posts/getRejectedPosts', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }

    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "getRejectedPosts",
        args        : [locId],
        sessionId   : responseId
    });
});

router.get('/posts/approvePost', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var lastLocation = req.user.clickdaily.locations.filter(function(d){
        return d._id =  req.user.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    var id = req.query.id;
    socialVars.responses[responseId] = res;
    console.log("Sent to approve post.");
    console.log("locId : ",locId);
    console.log("userType : ",userType);
    console.log("id : ",id);

    postRPC.sendCall({
        task        : "approvePost",
        args        : [locId,id,userType],
        sessionId   : responseId
    });
});

router.get('/posts/rejectPost', isLoggedIn, function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var lastLocation = req.user.clickdaily.locations.filter(function(d){
        return d._id =  req.user.clickdaily.lastLocationId;
    });
    var userType = lastLocation[0].modules.Social;
    var id = req.query.id;
    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "rejectPost",
        args        : [locId,id,userType],
        sessionId   : responseId
    });
});

router.get('/posts/getPostPicture/:id', function(req,res){
    var locId  = req.query.locId;
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    var id = req.params.id;
    id = id.split(".")[0];
    socialVars.responses[responseId] = res;
    postRPC.sendCall({
        task        : "getPostPicture",
        args        : [locId,id],
        sessionId   : responseId
    });
});


router.get('/Keywords/GetAll',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    var responseId = uuid.v1();
    while(socialVars.responses[responseId]){
        responseId = uuid.v1();
    }
    socialVars.responses[responseId] = res;
    keywordsRPC.sendCall({
        task        : "getKeyWords",
        args        : [locId],
        sessionId   : responseId
    });
});

router.post('/Keywords/add',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    if(req.query.text) {
        var responseId = uuid.v1();
        while (socialVars.responses[responseId]) {
            responseId = uuid.v1();
        }
        socialVars.responses[responseId] = res;

        var list = req.query.text;
        if(list[0]=='['){
            try {
                list = JSON.parse(list);
            }catch(e){
                console.log("Error parsing list as array passing as is.");
                list = req.query.text;
            }
        }

        keywordsRPC.sendCall({
            task: "addKeywords",
            args: [locId, list],
            sessionId: responseId
        });
    } else {
        res.send('false');
    }

});

router.post('/Keywords/del',isLoggedIn,function(req,res){
    var userId = req.user.id;
    var locId  = getLastLocation(req.user,req.session);
    if(req.query.text) {
        var responseId = uuid.v1();
        while (socialVars.responses[responseId]) {
            responseId = uuid.v1();
        }
        socialVars.responses[responseId] = res;
        var list = req.query.text;
        keywordsRPC.sendCall({
            task: "delKeyword",
            args: [locId, list],
            sessionId: responseId
        });
    } else {
        res.send('false');
    }

});

router.get('/changeLocation',isLoggedIn, function(req, res) {
    var locations =req.user.clickdaily.locations.map(function (d) {
        return d._id;
    });
    var newLoc = req.query.id;
    if(locations.indexOf(newLoc)==-1){
        return res.send("false");
    }
    req.session.lastLoc = newLoc;
    res.send("true");
});


module.exports = router;
/**
 * Created by Sundeep on 1/12/2015.
 */
// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var conn2 = mongoose.createConnection('mongodb://localhost:27017/merchants');

var schemaOptions = {
    toObject: {
        virtuals: true
    }
    ,toJSON: {
        virtuals: true
    }
};

var locationSchema = mongoose.Schema({
    _id                  : String,
    //name                : String,
    permissions         : {
        salesReport     : {
            read        : { type: Boolean, default: false}
        },
        Inventory       : {
            read        : { type: Boolean, default: false}
        },
        POSManagement   : {
            read        : { type: Boolean, default: false}
        },
        userManagement  : {
            readYours   : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , create    : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
        },
        schedules       : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , readYours : { type: Boolean, default: false}
        },
        timeClocking    : {
            createYours : { type: Boolean, default: false}
            , createAll : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , readYours : { type: Boolean, default: false}
        },
        logbook         : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , read      : { type: Boolean, default: false}
        },
        socialMedia     : {
            read        : { type: Boolean, default: false}
        },
        reservation     : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , read      : { type: Boolean, default: false}
        }
    },
    modules : {
        "Dashboard"     :{type: Boolean, default: false}
        ,"Reporting"    :{type: Boolean, default: false}
        ,"POS"          :{type: Boolean, default: false}
        ,"Labor"        :{type: Boolean, default: false}
        ,"Logbook"      :{type: Boolean, default: false}
        ,"Social"       :{type: Number, default: 0}
        ,"Scheduling"   :{type: Boolean, default: false}
        ,"Reservations" :{type: Boolean, default: false}
        ,"Debug"        :{type: Boolean, default: false}
    }
}, { _id: false });

var locationNSchema = mongoose.Schema({
    SQLid               : String,
    name                : String,
    lat                 : String,
    lon                 : String,
    address             : String
});

// define the schema for our user model
var userSchema = mongoose.Schema({

    clickdaily              : {
        username            : {type: String, required: true},
        email               : String,
        password            : {type: String, required: true},
        phonenumber         : String,
        firstname           : {type: String, required: true},
        lastname            : {type: String, required: true},
        middlename          : String,
        address             : {
            line1           : String,
            line2           : String,
            line3           : String,
            city            : String,
            state           : String,
            zip             : Number,
            country         : String
        },
        homephone           : String,
        creator             : String,
        jobs                : [String],
        posUser             : String,
        lastLocationId      : String,
        locations           : [locationSchema],
        userType            : { type: Boolean, default: false},
        firstLogin          : {type: Boolean, default: true}
    }
},schemaOptions);

userSchema.virtual('clickdaily.lastLocation').get(function () {
   return this.clickdaily.locations.id(this.clickdaily.lastLocationId);
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.clickdaily.password);
};

if (!userSchema.options.toObject) userSchema.options.toObject = {};
userSchema.options.toObject.hide = 'password';
userSchema.options.toObject.transform = function (doc, ret, options) {
    if (options.hide) {
        options.hide.split(' ').forEach(function (prop) {
            delete ret[prop];
        });
    }
    if (options.show) {
        for(key in ret.clickdaily){
            if(options.show.indexOf(key)===-1){
                delete ret.clickdaily[key];
            }
        }
    }
};


// create the model for users and expose it to our app
exports.User            = conn2.model('User', userSchema);
exports.Location        = conn2.model('Loc', locationSchema);
exports.LocationName    = conn2.model('Location', locationNSchema);
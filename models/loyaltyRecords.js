/**
 * Created by Sundeep on 3/11/2015.
 */
var mongoose = require('mongoose');

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var qrcode = mongoose.Schema({
    _id                     : String,
    code                    : String
},schemaOptions);

var FBRecord_Schema = mongoose.Schema({
    _id                     : String,  // FB user Id
    token                   : String,
    first_name              : String,
    last_name               : String,
    middle_name             : String,
    name                    : String,
    birthday                : String,
    email                   : String,
    picture                 : Object,
    location                : Object,
    type                    : {default:"FB", type:String},
    QRcodes                 : [qrcode]
},schemaOptions);

FBRecord_Schema.virtual('profilePicture').get(function () {
    if((this.picture)&&(this.picture.data)&&(this.picture.data.url)) return this.picture.data.url;
    return "";
});

var TWRecord_Schema = mongoose.Schema({
    _id                     : String,  // FB user Id
    token                   : String,
    token_secret            : String,
    name                    : String,
    screen_name             : String,
    picture                 : String,
    location                : String,
    type                    : {default:"TW", type:String},
    QRcodes                 : [qrcode]
},schemaOptions);

exports.FBRecord   = mongoose.model('FBRecord', FBRecord_Schema);
exports.TWRecord   = mongoose.model('TWRecord', TWRecord_Schema);
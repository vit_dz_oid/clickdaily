/**
 * Created by Sundeep on 1/29/2015.
 */
var mongoose = require('mongoose');
var request = require('request');
var Facebook = require('../library/socialHelpers/fb-mod');
var Twitter = require('../library/socialHelpers/tw-mod');
var Instagram = require('../library/socialHelpers/it-mod');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var schemaOptionsID = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
};


var FB_PageInfoSchema = mongoose.Schema({
    name            : String
    , _id           : String
    , profile_pic   : String
},schemaOptions);

FB_PageInfoSchema.virtual('link').get(function () {
    return "https://www.facebook.com/"+this.id;
});

var FB_Schema = mongoose.Schema({
    userId                  : String,  // FB user Id
    token                   : String,
    expires                 : String,
    private_user_info       : String,
    pages                   : [String],
    page_access_token       : [String],
    selected_page           : {type : String, default : ""},
    feeds                   : Object, //{me:feed,pageId:feed}
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        pages_info          : Date,
        feeds               : Object //{me:Date,pageId:Date}
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache                   : {
        user_info           : {
            name            : String
            , id            : String
            , profile_pic   : String
        }
        , page_info         : [FB_PageInfoSchema]
        , feeds             : Object
        , messages          : Object
        , friends           : Object
    }
},schemaOptions);

FB_Schema.virtual('helper').get(function () {
    var fb = new Facebook();
    fb.setAccessToken(this.token);
    return fb;
});

FB_Schema.virtual('selected_page_info').get(function () {
    return this.cache.page_info.id(this.selected_page);
});

FB_Schema.virtual('selected_page_feed').get(function () {
    if(this.cache.feeds) return this.cache.feeds[this.selected_page];
    return [];
});

FB_Schema.virtual('selected_page_feed_paging').get(function () {
    if((this.paging.feeds)&&(this.paging.feeds[this.selected_page])) return this.paging.feeds[this.selected_page];
    return {};
});

FB_Schema.virtual('selected_page_pic').get(function () {
    if (this.cache.page_info.id(this.selected_page))  return this.cache.page_info.id(this.selected_page).profile_pic;
    return "";
});

FB_Schema.virtual('selected_page_token').get(function () {
    var index = this.pages.indexOf(this.selected_page);
    if((index>-1)&&(index<this.page_access_token.length))  return this.page_access_token[index];
    return "";
});

FB_Schema.methods.AddPages = function(page_info) {
    if(this.pages.indexOf(page_info._id)==-1){
        this.cache.page_info.push(page_info);
        this.pages.push(page_info._id);
        this.page_access_token.push(page_info.access_token);

    }
};

FB_Schema.methods.SelectPage = function(page_id) {
    if(this.pages.indexOf(page_id)!==-1){
        this.selected_page = page_id;
    }
};

FB_Schema.methods.AddPagePic = function(id,url) {
    this.cache.page_info.id(id).profile_pic=url;
};

var TW_Schema = mongoose.Schema({
    token                   : String,
    token_secret            : String,
    userId                  : String,
    request_token           : String,
    request_token_secret    : String,
    private_user_info       : Object,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        feeds               : Date,
        friends             : Date,
        followers           : Date,
        mentions            : Date,
        you                 : Date
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache:{
        user_info           : {
            screen_name     : String
            , profile_pic   : String
        }
        , feeds             : Object
        , messages          : Object
        , friends           : Object
        , followers         : Object
    }
},schemaOptions);

TW_Schema.virtual('config').get(function () {
    return {
        consumerKey : "lofJrNEpTBmGngqGNqaK5DmHP"
        , consumerSecret : "WJBMAPmSDveEBgpPBQLoCEekk6srHyOJNDG9aa4ziV7S2eCqeY"
        , accessToken : this.token
        , accessTokenSecret : this.token_secret
        , callBackUrl : "https://merchant.clickdaily.com/social/Twitter"
    };
});

TW_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
});

TW_Schema.virtual('helper').get(function () {
    return new Twitter(this.config);
});

var IT_Schema = mongoose.Schema({
    token                   : String,
    userId                  : String,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        feeds               : Date,
        friends             : Date,
        followers           : Date
    },
    paging                  : {
        feeds               : Object,
        messages            : Object,
        friends             : Object,
        followers           : Object
    },
    cache:{
        user_info           : {
            username        : String
            , profile_pic   : String
            , full_name     : String
            , bio           : String
            , website       : String
        }
        , feeds             : Object
        , messages          : Object
        , friends           : Object
        , followers         : Object
    }
},schemaOptions);

IT_Schema.virtual('config').get(function () {
    return {
        client_id : "b670d31ac1ff415eab666305ae3ff352"
        , client_secret : "867f62af17f34d23a91585833d550eb7"
        , accessToken : this.token
        , callBackUrl : "https://merchant.clickdaily.com/social/Instagram"
    };
});

IT_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
});

IT_Schema.virtual('helper').get(function () {
    var It = new Instagram();
    It.set('client_id', this.config.client_id);
    It.set('client_secret', this.config.client_secret);
    It.set('callback_url', this.config.callBackUrl);
    It.set('redirect_uri', this.config.callBackUrl);
    It.set('maxSockets', 10);
    if(this.config.accessToken){
        It.set('access_token', this.config.accessToken);
    }
    return It;
});

var FS_VenueInfoSchema = mongoose.Schema({
    name            : String
    , _id           : String
    , profile_pic   : String
    , location      : Object
    , stats         : Object
    , contact       : Object
    , venuePage     : Object
},schemaOptions);

var FS_Schema = mongoose.Schema({
    userId                  : String,  // FB user Id
    token                   : String,
    expires                 : String,
    venue                   : [String],
    selected_venue          : [String],
    feeds                   : Object, //{me:feed,pageId:feed}
    private_user_info       : Object,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        venue_info          : Date,
        feeds               : Object //{me:Date,pageId:Date}
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache                   : {
        user_info           : {
            firstName       : String
            ,lastName       : String
            ,profile_pic    : String
        }
        , venue_info        : [FS_VenueInfoSchema]
        , feeds             : Object
        , messages          : Object
        , friends           : Object
    }

},schemaOptions);


FS_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
}).set(function (id) {
    this.set('userId', id);
});

FS_Schema.virtual('cache.user_info.name').get(function () {
    return this.cache.user_info.firstName + ' ' + this.cache.user_info.lastName;
}).set(function (setFullNameTo) {
    var split = setFullNameTo.split(' ')
        , firstName = split[0]
        , lastName = split[1];

    this.set('cache.user_info.firstName', firstName);
    this.set('cache.user_info.lastName', lastName);
});

FS_Schema.virtual('config').get(function () {
    return {
        secrets:{
            clientId : "LIKUTRY2RNA1I4CZUKKQ0JO4MTLUTFVWVMAEJVDCWZBCVQFR"
            , clientSecret : "CZK11Z0FGSMPY5OMFZX0AVP0H23MW1GZCBBHEKQ3VVOJUEOM"
            , redirectUrl : "https://merchant.clickdaily.com/Social/Foursquare"
        }
    
    };
});

FS_Schema.virtual('helper').get(function () {
    return require('node-foursquare')(this.config);
});

FS_Schema.methods.AddVenue = function(venue_info) {
    if(this.venue.indexOf(venue_info._id)===-1){
        this.cache.venue_info.push(venue_info);
        this.venue.push(venue_info._id);
    }
};

FS_Schema.methods.AddVenuePic = function(id,url) {
    this.cache.venue_info.id(id).profile_pic=url;
};

var QRcodeSchema = mongoose.Schema({
    created_at              : Date,
    loyalty_record_id       : String,
    sequenceNumber          : {type : Number, default:1},
    issued                  : {type : Boolean, default:false},
    redeemed                : {type : Boolean, default:false},
    redeemTime              : Date,
    network                 : String,
    profileName             : String,
    profileId               : String,
    email                   : String,
    pic_large               : String,
    qrcode                  : String
});

var loyalty_record_schema = mongoose.Schema({
    created_at          : Date,
    cachedRecordId      : String,
    fbPostID            : String,
    twPostID            : String,
    discount            : Number,
    user                : Number,
    redeemed            :{type : Number, default:0},
    issued              :{type : Number, default:0},
    QRcodes             :{type : [String], default:[]}
});

var loyalty_Schema = mongoose.Schema({
    loyalty_record          : [loyalty_record_schema]
},schemaOptions);

var post_Schema = mongoose.Schema({
    network          : Number //0:Facebook 1:Twitter
    , post_id        : String
    , create_time    : Date
    , post_time      : Date
    , num_likes      : Number
    , num_comments   : Number
    , num_reached    : Number
    , locId          : String
    , data           : mongoose.Schema.Types.Mixed
    , oldId          : String
},schemaOptionsID);

var SocialSchema = mongoose.Schema({
    _id                 : String  //Location Id
    , FB                : [FB_Schema]
    , TW                : [TW_Schema]
    , IT                : [IT_Schema]
    , FS                : [FS_Schema]
    , posts             : [post_Schema]
    , loyalty           : [loyalty_Schema]
    , keywords          : [String]
},schemaOptionsID);

exports.FB_PageInfoSchema   = mongoose.model('FB_PageInfoSchema', FB_PageInfoSchema);
exports.FB                  = mongoose.model('FB', FB_Schema);
exports.TW                  = mongoose.model('TW', TW_Schema);
exports.IT                  = mongoose.model('IT', IT_Schema);
exports.FS                  = mongoose.model('FS', FS_Schema);
exports.loyalty             = mongoose.model('loyalty', loyalty_Schema);
exports.Social              = mongoose.model('Social', SocialSchema);
exports.QRcode              = mongoose.model('QRcode', QRcodeSchema);
//exports.post                = mongoose.model('post', post_Schema);





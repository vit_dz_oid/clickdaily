/**
 * Created by Sundeep on 1/12/2015.
 */

// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User            = require('../models/user').User;
var Location        = require('../models/user').Location;

// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        var u = user.toObject({hide:"password", transform: true });
        delete u.password;
        done(null, u);
    });

    // used to deserialize the user
    passport.deserializeUser(function(user, done) {
        User.findById(user._id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    // Add User Info
    // ==============

    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function() {

                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({ 'clickdaily.username' :  username }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
                    } else {

                        var newUser                = new User();

                        newUser.clickdaily.username                         = username;
                        newUser.clickdaily.email                            = req.body.email;
                        newUser.clickdaily.password                         = newUser.generateHash(password);
                        newUser.clickdaily.phonenumber                      = req.body.phonenumber;
                        newUser.clickdaily.dob                              = req.body.dob;
                        newUser.clickdaily.firstname                        = req.body.firstname;
                        newUser.clickdaily.lastname                         = req.body.lastname;
                        newUser.clickdaily.middlename                       = req.body.middlename;
                        newUser.clickdaily.gender                           = req.body.gender;
                        newUser.clickdaily.martialstatus                    = req.body.martialstatus;
                        newUser.clickdaily.address.line1                    = req.body.addressline1;
                        newUser.clickdaily.address.line2                    = req.body.addressline2;
                        newUser.clickdaily.address.line3                    = req.body.addressline3;
                        newUser.clickdaily.address.state                    = req.body.addressstate;
                        newUser.clickdaily.address.zip                      = req.body.addresszip;
                        newUser.clickdaily.address.country                  = req.body.addresscountry;
                        newUser.clickdaily.homephone                        = req.body.homephone;
                        newUser.clickdaily.emergencycontact.name            = req.body.emergencyname;
                        newUser.clickdaily.emergencycontact.relationship    = req.body.emergencyrelation;
                        newUser.clickdaily.emergencycontact.phone           = req.body.emergencyphone;
                        newUser.clickdaily.lastLocation                     = req.body.locid;
                        newUser.clickdaily.locations.push(newLoc);
                        newUser.clickdaily.userType                         = 2;

                        // save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }));

    // Add User Permissions
    // ==============

    passport.use('local-permission', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function() {

                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({ 'clickdaily.username' :  username }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
                    } else {

                        // if there is no user with that email
                        // create the user
                        var newLoc 			   = new Location();

                        newLoc.id                             		= req.body.locid;
                        newLoc.permissions.salesReport.read   		= req.body.salesreportR;
                        newLoc.permissions.Inventory.read        	= req.body.inventorymanagementR;
                        newLoc.permissions.POSManagement.read       = req.body.posmanagementR;
                        newLoc.permissions.userManagement.readYours = req.body.usermanagementRY;
                        newLoc.permissions.userManagement.readAll   = req.body.usermanagementRA;
                        newLoc.permissions.schedules.create      	= req.body.scheduleC;
                        newLoc.permissions.schedules.edit      		= req.body.scheduleC;
                        newLoc.permissions.schedules.readAll   		= req.body.scheduleRA;
                        newLoc.permissions.schedules.readYours      = req.body.scheduleRY;
                        newLoc.permissions.timeClocking.createYours = req.body.timeclockingCY;
                        newLoc.permissions.timeClocking.createAll 	= req.body.timeClockingCA;
                        newLoc.permissions.timeClocking.edit      	= req.body.timeClockingE;
                        newLoc.permissions.timeClocking.readAll   	= req.body.timeClockingRA;
                        newLoc.permissions.timeClocking.readYours 	= req.body.timeClockingRY;
                        newLoc.permissions.logbook.create      		= req.body.logbookC;
                        newLoc.permissions.logbook.edit      		= req.body.logbookE;
                        newLoc.permissions.logbook.read      		= req.body.logbookR;
                        newLoc.permissions.socialMedia.read        	= req.body.socialMediaR;
                        newLoc.permissions.reservation.create      	= req.body.reservationC;
                        newLoc.permissions.reservation.edit      	= req.body.reservationE;
                        newLoc.permissions.reservation.read      	= req.body.reservationR;


                        var newUser                = new User();

                        newUser.clickdaily.username                         = username;
                        newUser.clickdaily.email                            = req.body.email;
                        newUser.clickdaily.password                         = newUser.generateHash(password);
                        newUser.clickdaily.phonenumber                      = req.body.phonenumber;
                        newUser.clickdaily.dob                              = req.body.dob;
                        newUser.clickdaily.firstname                        = req.body.firstname;
                        newUser.clickdaily.lastname                         = req.body.lastname;
                        newUser.clickdaily.middlename                       = req.body.middlename;
                        newUser.clickdaily.gender                           = req.body.gender;
                        newUser.clickdaily.martialstatus                    = req.body.martialstatus;
                        newUser.clickdaily.address.line1                    = req.body.addressline1;
                        newUser.clickdaily.address.line2                    = req.body.addressline2;
                        newUser.clickdaily.address.line3                    = req.body.addressline3;
                        newUser.clickdaily.address.state                    = req.body.addressstate;
                        newUser.clickdaily.address.zip                      = req.body.addresszip;
                        newUser.clickdaily.address.country                  = req.body.addresscountry;
                        newUser.clickdaily.homephone                        = req.body.homephone;
                        newUser.clickdaily.emergencycontact.name            = req.body.emergencyname;
                        newUser.clickdaily.emergencycontact.relationship    = req.body.emergencyrelation;
                        newUser.clickdaily.emergencycontact.phone           = req.body.emergencyphone;
                        newUser.clickdaily.lastLocation                     = req.body.locid;
                        newUser.clickdaily.locations.push(newLoc);
                        newUser.clickdaily.userType                         = 2;

                        // save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form

            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({ 'clickdaily.username' :  username }, function(err, user) {
                // if there are any errors, return the error before anything else
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

                // if the user is found but the password is wrong
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

                // all is well, return successful user
                req.session.lastLoc = user.clickdaily.lastLocationId;
                return done(null, user);
            });

        }));

};



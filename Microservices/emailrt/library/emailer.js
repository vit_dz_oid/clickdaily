/**
 * Created by Sundeep on 9/14/2015.
 */


var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    host: "smtpout.secureserver.net",
    name: "merchant.clickdaily.com",
    port: 465,
    secure: true,
    auth: {
        user: 'no-reply@clickdaily.com',
        pass: '3615clickdaily'
    },
    tls: {
        rejectUnauthorized: false
    }
});


exports.save_to_db = function(ip,body,done){
    console.log("IP - ",ip);
    this.send_email(body,done)
};

exports.send_email = function(body,done){
    var mailOptions = {
        from: 'no-reply@clickdaily.com',
        to: "info@clickdaily.com",
        subject: 'Contact Us Form Submission!',
        text: 'Following Info was sent : \n' +
        '\n\nFirstname -' + body.firstname +
        '\nLastname - ' + body.lastname +
        '\nEmail - ' + body.email +
        '\nPhone - ' + body.phone +
        '\nPromo - ' + body.promo +
        '\nBusiness Name - ' + body.businessname +
        '\nBusiness Site - ' + body.businesssite +
        '\nComments - ' + body.comments +
        '\nCopyright, Clickdaily 2015',
        html : '<h1>Following Info was sent :</h1>' +
        '<br/><br/><p>Firstname - ' + body.firstname + '</p>' +
        '<br/><p>Lastname - ' + body.lastname + '</p>' +
        '<br/><p>Email - ' + body.email + '</p>' +
        '<br/><p>Phone - ' + body.phone + '</p>' +
        '<br/><p>Promo - ' + body.promo + '</p>' +
        '<br/><p>Business Name - ' + body.businessname + '</p>' +
        '<br/><p>Business Site - ' + body.businesssite + '</p>' +
        '<br/><p>Comments - ' + body.comments + '</p>' +
        '<br/>&copy;Clickdaily 2015'
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error);
            done("Failed");
        }else{
            console.log('Message sent: ' + info.response);
            done("Success");
        }
    });
};
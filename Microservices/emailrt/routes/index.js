var express = require('express');
var router = express.Router();

var library = require('../library/emailer');



/* GET home page. */
router.post('/', function(req, res) {
    var ip = "NULL";
    if(req.headers){
        ip = req.headers['x-forwarded-for'];
    }
    library.save_to_db(ip,req.body,function(msg){
        res.send(msg);
    });
});

module.exports = router;

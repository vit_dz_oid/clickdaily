/**
 * Created by Sundeep on 9/21/2015.
 */
var path    = require("path");
var model   = require('../itMicroService/Model').it;
var moment = require('moment-timezone');

var yesterday = 366; //Program how many days data to get. Default is 1 i.e. yesterday(only 1 day).

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "it_insights"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

// Indefinite length addition
function addAsString(x, y) { // x, y strings
    var s = '';
    if (y.length > x.length) { // always have x longer
        s = x;
        x = y;
        y = s;
    }
    s = (parseInt(x.slice(-9),10) + parseInt(y.slice(-9),10)).toString(); // add last 9 digits
    x = x.slice(0,-9); // cut off last 9 digits
    y = y.slice(0,-9);
    if (s.length > 9) { // if >= 10, add in the 1
        if (x === '') return s; // special case (e.g. 9+9=18)
        x = addAsString(x, '1');
        s = s.slice(1);
    } else if (x.length) { // if more recursions to go
        while (s.length < 9) { // make sure to pad with 0s
            s = '0' + s;
        }
    }
    if (y === '') return x + s; // if no more chars then done, return
    return addAsString(x, y) + s; // else recurse, next digit
}

// Indefinite length subtraction (x - y, |x| >= |y|)
function subtractAsString(x, y) {
    var s;
    s = (parseInt('1'+x.slice(-9),10) - parseInt(y.slice(-9),10)).toString(); // subtract last 9 digits
    x = x.slice(0,-9); // cut off last 9 digits
    y = y.slice(0,-9);
    if (s.length === 10 || x === '') { // didn't need to go mod 1000000000
        s = s.slice(1);
    } else { // went mod 1000000000, inc y
        if (y.length) { // only add if makes sense
            y = addAsString(y, '1');
        } else { // else set
            y = '1';
        }
        if (x.length) {
            while (s.length < 9) { // pad s
                s = '0' + s;
            }
        }
    }
    if (y === '') { // finished
        s = (x + s).replace(/^0+/,''); // dont return all 0s
        return s;
    }
    return subtractAsString(x, y) + s;
}

// Indefinite length addition or subtraction (via above)
function addORsub(x, y) {
    var s = '';
    x = x.replace(/^(-)?0+/,'$1').replace(/^-?$/,'0'); // -000001 = -1
    y = y.replace(/^(-)?0+/,'$1').replace(/^-?$/,'0'); // -000000 =  0
    if (x[0] === '-') { // x negative
        if (y[0] === '-') { // if y negative too
            return '-' + addAsString(x.slice(1), y.slice(1)); // return -(|x|+|y|)
        }
        return addORsub(y, x); // else swap
    }
    if (y[0] === '-') { // x positive, y negative
        s = y.slice(1);
        if (s.length < x.length || (s.length === x.length && s < x)) return subtractAsString(x, s) || '0'; // if |x|>|y|, return x-y
        if (s === x) return '0'; // equal then 0
        s = subtractAsString(s, x); // else |x|<|y|
        s = (s && '-' + s) || '0';
        return s; // return -(|y|-x)
    }
    return addAsString(x, y); // x, y positive, return x+y
}

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function IT_getYouFeed(locId,max_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            count: '200',
            user_id: "self", //document.cache.user_info.id,
            error: function (err, value, caller) {
                console.log("Insta Error : ",err,value,caller);
                errorLog.log("getUserTimeline", err, {value :value,caller : caller});
                return done(err);
            },
            complete: function (data, pagination) {

                console.log("Called Insta");
                var feed = data;
                feed = feed.map(function (d) {
                    return {
                        created_at: moment(d.created_time,"X"),
                        created_at_str: moment(d.created_time,"X").tz('UTC').format("YYYY-MM-DD HH:mm:ss"),
                        id: d.id.split("_")[0],
                        text: (d.caption?(d.caption.text?d.caption.text:""):""),
                        network: 2,
                        likes : (d.likes?(d.likes.count?d.likes.count:0):0),
                        comments : (d.comments?(d.comments.count?d.comments.count:0):0)
                    }
                });
                done(null, feed);
            }
        };
        if(max_id){
            reqObj.max_id = max_id;
        }
        document.helper.users.recent(reqObj);
    });
}

function IT_getYouFeedSince(locId,since_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            count: '200',
            user_id: "self", //document.cache.user_info.id,
            error: function (err, value, caller) {
                errorLog.log("getUserTimeline", err, {value :value,caller : caller});
                return done(err);
            },
            complete: function (data, pagination) {
                console.log("Called Insta");
                var feed = data;
                feed = feed.map(function(d){
                    return {
                        created_at: moment(d.created_time,"X"),
                        created_at_str: moment(d.created_time,"X").tz('UTC').format("YYYY-MM-DD HH:mm:ss"),
                        id: d.id.split("_")[0],
                        text: (d.caption?(d.caption.text?d.caption.text:""):""),
                        network: 2
                    }
                });
                done(null,feed);
            }
        };
        if(since_id){
            reqObj.min_id = since_id;
        }
        document.helper.users.recent(reqObj);
    });
}

function IT_getFollowers(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            user_id: "self", //document.cache.user_info.id,
            error: function (err, value, caller) {
                errorLog.log("getUserFollowers", err, {value :value,caller : caller});
                return done(err);
            },
            complete: function (data, pagination) {
                console.log("Called Insta");
                if(data&&data.counts){
                    done(null,data.counts);
                }else {
                    done(null,{
                        "media": 0,
                        "followed_by": 0,
                        "follows": 0
                    });
                }
            }
        };
        document.helper.users.info(reqObj);
    });
}

function getYesterdaysPosts(locId,maxId,feed,done){
    if(!feed){
        feed=[];
    }
    IT_getYouFeed(locId,maxId,function(err,res){
        if(err){
            return done((err?err:new Error("no results")));
        }
        if(!res.length){
            return done(null,feed);
        }
        var yesterdaysDate = moment().tz("GMT").subtract(yesterday,"days").startOf("day");
        var dayBeforeDate = yesterdaysDate.clone().subtract(1,"days").startOf("day");

        var yesterdaysPosts = res.filter(function(d){
           return yesterdaysDate.isBefore(d.created_at.startOf("day"))
        });
        var hasDayBefore = res.filter(function(d){
            return dayBeforeDate.isSame(d.created_at.startOf("day")) || dayBeforeDate.isAfter(d.created_at.startOf("day"));
        }).length;


        Array.prototype.push.apply(feed, res);

        if(hasDayBefore){
            done(null,feed);
        }else {
            var maxId = addORsub(res[res.length-1].id, '-1');
            getYesterdaysPosts(locId,maxId,feed,done)
        }

    });
}

function getYesterdaysPostsSince(locId,since_id,feed,done){
    if(!feed){
        feed=[];
    }
    IT_getYouFeedSince(locId,since_id,function(err,res){
        if(err){
            return done((err?err:new Error("no results")));
        }
        if(!res.length){
            return done(null,feed);
        }

        Array.prototype.unshift.apply(feed, res);

        var sinceId = addORsub(res[0].id, '1');
        getYesterdaysPostsSince(locId,sinceId,feed,done)

    });
}

exports.getYesterdaysPosts      = getYesterdaysPosts;
exports.getYesterdaysPostsSince = getYesterdaysPostsSince;
exports.getFollowers            = IT_getFollowers;
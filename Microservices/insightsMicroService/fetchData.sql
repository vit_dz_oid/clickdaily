#tweets

SELECT userid, `date`, count(*) as count
FROM twitterinsights.tweets
group by userid, `date`
order by userid, date;


# Mentions

SELECT mentioneduserid, `date`, count(*) as count
FROM twitterinsights.mentions
group by mentioneduserid, `date`
order by mentioneduserid, date;

#Retweets impressions

SELECT a.tweetid, a.userid, sum(b.followercount) as impressions
FROM twitterinsights.retweets a
join twitterinsights.followercount b
on a.retweetuserid=b.userid
group by a.tweetid;

#Mentions impressions
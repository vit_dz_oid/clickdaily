/**
 * Created by Sundeep on 2/28/2016.
 */

var dbconfig    = require("./dbconfig");
var helper      = require("./ITHelpers");

var utils       = require("util");
var moment      = require("moment");
var mysql       = require('mysql');

var pool = mysql.createPool(dbconfig.dbConfig);

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

function insertToDb(records,callback){
    var query = "INSERT  INTO `youposts` " +
        "(`id`, `text`, `loc_id`, `created_at`, `network`,likes,comments) VALUES ";
    var values = [];
    var ids = [];
    records.forEach(function(d){
        if(ids.indexOf(d.id)!=-1){
            return;
        }
        query = query + "(?,?,?,?,?,?,?)\n,";
        ids.push(d.id);
        values.push(d.id);
        values.push(d.text);
        values.push(d.loc_id);
        values.push(d.created_at_str);
        values.push(d.network);
        values.push(d.likes);
        values.push(d.comments);
    });
    query=query.substr(0,query.length-1);
    query=query + " ON DUPLICATE KEY UPDATE likes=VALUES(likes), comments=VALUES(comments);";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("Error saving to DB : ",err);
            return callback(err);
        }
        connection.query(query,values, function(err, rows, fields) {
            connection.release();
            if (err) {
                console.log(this.sql,this.values);
                return callback(err);
            }
            return callback(null);
        });
    });
}

function insertFollowersToDb(locId,followers,following,media,callback){
    var query = "INSERT IGNORE INTO `history`.`followers` " +
        "(`loc_id`, `media_count`, `followers_count`" +
        ", `following_count`, `network`) VALUES (?,?,?,?,?);";

    pool.getConnection(function(err, connection) {
        if(err){
            console.log("Error saving to DB : ",err);
            return callback(err);
        }
        connection.query(query,[locId,media,followers,following,2], function(err, rows, fields) {
            connection.release();
            if (err) {
                console.log(this.sql,this.values);
                return callback(err);
            }
            return callback(null);
        });
    });
}

function wrapperYouPost(locId,done){
    console.log("--------- Started Gathering YouPost for location("+locId+") ---------");
    helper.getYesterdaysPosts(locId,false,[],function(err,youposts){
        if(err){
            return done(null,{error:err});
        }
        done(null,youposts)
    });
}

function createDbRecords(cur_loc,results,done){
    var records = [];
    if(results && results.length && results instanceof Array){
        results.forEach(function(d,j) {
            var record = d;
            delete record.created_at;
            record.loc_id = cur_loc;
            records.push(record);
        });
    }
    done(null,records);
}


function getHistoryForALocation(locId,done){
    console.log("--------- Started Gathering History for location("+locId+") ---------");
    wrapperYouPost(locId,function(err,results){
        if(err){
            console.log("--------- Error Gathering History for location("+locId+") ---------",err);
            return done(err)
        }
        console.log("--------- Gathered History for location("+locId+") ---------");
        createDbRecords(locId,results,function(err,records){
            if(!records.length){
                console.log("No records to write to DB for Location [",locId,"]");
                return done(null);
            }

            insertToDb(records,function(err){
                if(err){
                    console.log("Error Writting to DB for Location [",locId,"]: ",err);
                }
                console.log("Written to DB for Location [",locId,"]");
                done(null);
            });
        });
    });
}

function getFollowersForALocation(locId,done){
    console.log("--------- Started Gathering Followers for location("+locId+") ---------");
    helper.getFollowers(locId,function(err,counts){
        if(err){
            console.log("Error Gathering Followers for Location [",locId,"]: ",err);
            return done(null,{error:err});
        }
        console.log("--------- Gathered Followers for location("+locId+") ---------");
        insertFollowersToDb(locId,counts.followed_by,counts.follows,counts.media,function(err){
            if(err){
                console.log("Error Writting Followers to DB for Location [",locId,"]: ",err);
            }
            console.log("Written Followers to DB for Location [",locId,"]");
            done(null);
        });
    });
}

exports.getHistoryForALocation      = getHistoryForALocation;
exports.getFollowersForALocation    = getFollowersForALocation;
/**
 * Created by Sundeep on 1/18/2016.
 */

var locations = require("../../models/user").LocationName;
var instaHelper = require("./dailyInstaHelper");

var async = require("async");
var fs = require("fs");

locations.find(function(err,results){
    if(err){
        console.log("--------- Error Gathering locations ---------",err);
        return process.exit(1);
    }
    var locs = results.map(function(d){return d.SQLid});
    async.series([
        function(callback) {
            console.log("--------- Started Gathering History ---------");
            async.mapSeries(locs,instaHelper.getHistoryForALocation,function(err){
                if(err){
                    console.log("--------- Error Gathering History for locations ---------",err);
                    return callback(null,{error:err});
                }
                console.log("--------- Gathered History for locations ---------");
                callback(null);
            });
        },
        function(callback) {
            console.log("--------- Started Gathering Followers ---------");
            async.mapSeries(locs,instaHelper.getFollowersForALocation,function(err){
                if(err){
                    console.log("--------- Error Gathering Followers for locations ---------",err);
                    return callback(null,{error:err});
                }
                console.log("--------- Gathered Followers for locations ---------");
                callback(null);
            });
        }]
        ,function(err,results){
            console.log("All Done!!");
            process.exit(0);
        });
});
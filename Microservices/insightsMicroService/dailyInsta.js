/**
 * Created by Sundeep on 1/23/2016.
 */

var locations = require("../../models/user").LocationName;
var helper = require("./ITHelpers");
var async = require("async");
var utils = require("util");
var moment = require("moment");
var fs = require("fs");
//var dbconfig = require("./dbconfig");

var mysql      = require('mysql');
var pool = mysql.createPool(dbconfig.dbConfig);

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

function insertToDb(records,callback){
    var query = "INSERT IGNORE INTO `youposts` " +
        "(`id`, `text`, `loc_id`, `created_at`, `network`) VALUES ";
    var values = [];
    var ids = [];
    records.forEach(function(d){
        if(ids.indexOf(d.id)!=-1){
            return;
        }
        query = query + "(?,?,?,?,?)\n,";
        ids.push(d.id);
        values.push(d.id);
        values.push(d.text);
        values.push(d.loc_id);
        values.push(d.created_at_str);
        values.push(d.network);
    });
    query=query.substr(0,query.length-1);
    query=query + ";";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("Error saving to DB : ",err);
            return callback(err);
        }
        connection.query(query,values, function(err, rows, fields) {
            connection.release();
            if (err) {
                return callback(err);
            }
            return callback(null);
        });
    });
}

function wrapperYouPost(locObject,done){
    console.log("--------- Started Gathering YouPost for location("+locObject.loc_id+") ---------");
    helper.getYesterdaysPostsSince(locObject.loc_id,locObject.since_id,[],function(err,youposts){
        if(err){
            return done(null,{error:err});
        }
        done(null,youposts)
    });
}

function getFutureForALocation(locObject,done){
    console.log("--------- Started Gathering History for location("+locObject.loc_id+") ---------");
    wrapperYouPost(locObject,function(err,results){
        if(err){
            console.log("--------- Error Gathering History for location("+locObject.loc_id+") ---------",err);
            return done(err)
        }
        console.log("--------- Gathered History for location("+locObject.loc_id+") ---------");
        done(null,results);
    });
}
function main() {
    var query =
        "SELECT " +
        "CAST((CAST( MAX(`id`) AS UNSIGNED) + 1 )AS CHAR)  AS since_id " +
        ",loc_id  " +
        "FROM `youposts` " +
        "WHERE network=2 " +
        "GROUP BY loc_id;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("Error Connecting to pool : ",err);
            return process.exit(1);
        }
        connection.query(query, function(err, rows) {
            if (err) {
                console.log("Error Connecting to DB : ",err);
                return process.exit(1);
            }
            async.map(rows, getFutureForALocation, function (err, results) {
                if(err){
                    console.log("--------- Error Gathering History for locations ---------",err);
                    return process.exit(2);
                }
                console.log("--------- Gathered History for locations ---------", err);
                var records = [];
                for (var i = 0; i < results.length; i++) {
                    var cur_loc = rows[i].loc_id;
                    if (results[i] && results[i].length && results[i] instanceof Array) {
                        results[i].forEach(function (d, j) {
                            var record = d;
                            delete record.created_at;
                            record.loc_id = cur_loc;
                            records.push(record)
                        });
                    }
                }
                if(records.length) {
                    insertToDb(records, function (err) {
                        if(err){
                            console.log("Error Writting to DB : ",err);
                            return process.exit(3);
                        }
                        console.log("Written to DB");
                        process.exit(0);
                    });
                } else {
                    console.log("Nothing to write to DB");
                    process.exit(0);
                }
            });
        });
    });
}

main();
/**
 * Created by Sundeep on 9/21/2015.
 */
var path    = require("path");
var model   = require('../twMicroService/Model').tw;
var moment = require('moment-timezone');

var yesterday = 365; //Program how many days data to get. Default is 1 i.e. yesterday(only 1 day).

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "tw_insights"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

// Indefinite length addition
function addAsString(x, y) { // x, y strings
    var s = '';
    if (y.length > x.length) { // always have x longer
        s = x;
        x = y;
        y = s;
    }
    s = (parseInt(x.slice(-9),10) + parseInt(y.slice(-9),10)).toString(); // add last 9 digits
    x = x.slice(0,-9); // cut off last 9 digits
    y = y.slice(0,-9);
    if (s.length > 9) { // if >= 10, add in the 1
        if (x === '') return s; // special case (e.g. 9+9=18)
        x = addAsString(x, '1');
        s = s.slice(1);
    } else if (x.length) { // if more recursions to go
        while (s.length < 9) { // make sure to pad with 0s
            s = '0' + s;
        }
    }
    if (y === '') return x + s; // if no more chars then done, return
    return addAsString(x, y) + s; // else recurse, next digit
}

// Indefinite length subtraction (x - y, |x| >= |y|)
function subtractAsString(x, y) {
    var s;
    s = (parseInt('1'+x.slice(-9),10) - parseInt(y.slice(-9),10)).toString(); // subtract last 9 digits
    x = x.slice(0,-9); // cut off last 9 digits
    y = y.slice(0,-9);
    if (s.length === 10 || x === '') { // didn't need to go mod 1000000000
        s = s.slice(1);
    } else { // went mod 1000000000, inc y
        if (y.length) { // only add if makes sense
            y = addAsString(y, '1');
        } else { // else set
            y = '1';
        }
        if (x.length) {
            while (s.length < 9) { // pad s
                s = '0' + s;
            }
        }
    }
    if (y === '') { // finished
        s = (x + s).replace(/^0+/,''); // dont return all 0s
        return s;
    }
    return subtractAsString(x, y) + s;
}

// Indefinite length addition or subtraction (via above)
function addORsub(x, y) {
    var s = '';
    x = x.replace(/^(-)?0+/,'$1').replace(/^-?$/,'0'); // -000001 = -1
    y = y.replace(/^(-)?0+/,'$1').replace(/^-?$/,'0'); // -000000 =  0
    if (x[0] === '-') { // x negative
        if (y[0] === '-') { // if y negative too
            return '-' + addAsString(x.slice(1), y.slice(1)); // return -(|x|+|y|)
        }
        return addORsub(y, x); // else swap
    }
    if (y[0] === '-') { // x positive, y negative
        s = y.slice(1);
        if (s.length < x.length || (s.length === x.length && s < x)) return subtractAsString(x, s) || '0'; // if |x|>|y|, return x-y
        if (s === x) return '0'; // equal then 0
        s = subtractAsString(s, x); // else |x|<|y|
        s = (s && '-' + s) || '0';
        return s; // return -(|y|-x)
    }
    return addAsString(x, y); // x, y positive, return x+y
}

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function TW_getUserInfo(locId,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }

        document.helper.getUser({user_id: document.cache.user_info.id}
            , function (err) {
                errorLog.log(func_name+" | Twitter Reply Error",err,arguments);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                try{
                    res = JSON.parse(res);
                }catch(e){
                    errorLog.log(func_name+" | JSON parse error",e,res);
                    return done(e);
                }

                var userInfo = {
                    id :  res.id_str,
                    followed_by : res.followers_count,
                    follows : res.friends_count,
                    media : res.statuses_count
                };
                return done(null, userInfo);
            });

    });
}

function TW_getRetweetsOfTweet(locId,id,max_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            id:id,
            count: '100'
        };
        if(max_id){
            reqObj.max_id = max_id;
        }
        document.helper.getReTweets(reqObj, function (err) {
                errorLog.log("getReTweets", err);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log("getReTweets",!res ? new Error('error occurred') : res.error, res);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                var feed = JSON.parse(res);
                feed = feed.map(function(d){
                    return {
                        created_at              : moment(new Date(d.created_at)).tz("GMT"),
                        id                      : d.id_str,
                        favorite_count          : d.favorite_count,
                        user_followers_count    : d.user.followers_count,
                        user_id                 : d.user.id_str

                    }
                });
                done(null,feed);
            }
        );
    });
}

function TW_getMentionsFeed(locId,max_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            count: '100',
            include_rts : 'true'
        };
        if(max_id){
            reqObj.max_id = max_id;
        }
        document.helper.getMentionsTimeline(reqObj, function (err) {
                errorLog.log("getMentionsTimeline", err);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log("getMentionsTimeline",!res ? new Error('error occurred') : res.error, res);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                var feed = JSON.parse(res);
                feed = feed.map(function(d){
                    d.created_at =  moment(new Date(d.created_at)).tz("GMT");
                    d.id = d.id_str;
                    return d;
                    /*return {
                        created_at              : moment(new Date(d.created_at)).tz("GMT"),
                        id                      : d.id_str,
                        favorite_count          : d.favorite_count,
                        retweet_count           : d.retweet_count,
                        in_reply        : {
                            tweet               : d.in_reply_to_status_id_str
                            , user              : d.in_reply_to_user_id_str
                        },
                        retweet                 : {
                            tweet               : (d.retweeted_status? d.retweeted_status.id_str:null),
                            user                    : (d.retweeted_status? d.retweeted_status.user.id_str:null)
                        },
                        user_followers_count    : d.user.followers_count,
                        user_id                 : d.user.id_str

                    }*/
                });
                done(null,feed);
            }
        );
    });
}

function TW_getYouFeed(locId,max_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            count: '100'
        };
        if(max_id){
            reqObj.max_id = max_id;
        }
        document.helper.getUserTimeline(reqObj, function (err) {
                errorLog.log("getUserTimeline", err);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log("getUserTimeline",!res ? new Error('error occurred') : res.error);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                var feed = JSON.parse(res);
                feed = feed.map(function(d){
                    return {
                        created_at              : moment(d.created_at).tz("UTC"),
                        created_at_str          : moment(d.created_at).tz("UTC").format("YYYY-MM-DD HH:mm:ss"),
                        id                      : d.id_str,
                        text                    : d.text,
                        network                 : 1,
                        likes                   : (d.favorite_count?d.favorite_count:0),
                        comments                : (d.retweet_count?d.retweet_count:0)
                    }
                });
                done(null,feed);
            }
        );
    });
}

function TW_getYouFeedSince(locId,since_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            count: '200'
        };
        if(since_id){
            reqObj.since_id = since_id;
        }
        document.helper.getUserTimeline(reqObj, function (err) {
                errorLog.log("getUserTimeline", err);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log("getUserTimeline",!res ? new Error('error occurred') : res.error);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                var feed = JSON.parse(res);
                feed = feed.map(function(d){
                    return {
                        created_at              : moment(d.created_at).tz("UTC"),
                        created_at_str          : moment(d.created_at).tz("UTC").format("YYYY-MM-DD HH:mm:ss"),
                        id                      : d.id_str,
                        text                    : d.text,
                        network                 : 1
                    }
                });
                done(null,feed);
            }
        );
    });
}

function TW_getFollowerId(locId,cursor,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var reqObj = {
            user_id: document.userId,
            stringify_ids:"true"
        };
        if(cursor){
            reqObj.cursor = cursor;
        }
        document.helper.getFollowersIds(reqObj, function (err) {
                errorLog.log("getFollowersIds", err);
                return done(err);
            },
            function (res) {
                if(!res ) {
                    errorLog.log("getFollowersIds",!res ? new Error('error occurred') : res.error);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                try {
                    var feed = JSON.parse(res);
                }catch(e){
                    var err = new Error("Json Parse Error")
                    errorLog.log("getFollowersIds",err);
                    done(err);
                }
                done(null,feed);
            }
        );
    });
}

function getAllFollowerIds(locId,followers,cursor,done){
    if(!followers){
        followers=[];
    }
    TW_getFollowerId(locId,cursor,function(err,res){
        if(err){
            return done(err);
        }
        Array.prototype.push.apply(followers, res.ids);
        if(res.next_cursor_str!="0"){
            getAllFollowerIds(locId,followers,cursor,done);
        }else {
            done(null,followers)
        }
    });
}

function getYesterdaysPosts(locId,maxId,feed,done){
    if(!feed){
        feed=[];
    }
    TW_getYouFeed(locId,maxId,function(err,res){
        if((err)||(!res.length)){
            return done((err?err:new Error("no results")));
        }
        var yesterdaysDate = moment().tz("GMT").subtract(yesterday,"days").startOf("day");
        var dayBeforeDate = yesterdaysDate.clone().subtract(1,"days").startOf("day");

        var yesterdaysPosts = res.filter(function(d){
           return yesterdaysDate.isBefore(d.created_at.startOf("day"))
        });
        var hasDayBefore = res.filter(function(d){
            return dayBeforeDate.isSame(d.created_at.startOf("day")) || dayBeforeDate.isAfter(d.created_at.startOf("day"));
        }).length;


        Array.prototype.push.apply(feed, res);

        if(hasDayBefore){
            done(null,feed);
        }else {
            var maxId = addORsub(res[res.length-1].id, '-1');
            getYesterdaysPosts(locId,maxId,feed,done)
        }

    });
}

function getYesterdaysPostsSince(locId,since_id,feed,done){
    if(!feed){
        feed=[];
    }
    TW_getYouFeedSince(locId,since_id,function(err,res){
        if(err){
            return done((err?err:new Error("no results")));
        }
        if(!res.length){
            return done(null,feed);
        }

        Array.prototype.unshift.apply(feed, res);

        var sinceId = addORsub(res[0].id, '1');
        getYesterdaysPostsSince(locId,sinceId,feed,done)

    });
}

function getYesterdaysMentions(locId,maxId,feed,done){
    if(!feed){
        feed=[];
    }
    TW_getMentionsFeed(locId,maxId,function(err,res){
        if((err)||(!res.length)){
            return done((err?err:new Error("no results")));
        }
        var todaysDate =  moment().tz("GMT").startOf("day");
        var yesterdaysDate = todaysDate.clone().subtract(yesterday,"days").startOf("day");
        var dayBeforeDate = yesterdaysDate.clone().subtract(1,"days").startOf("day");

        var yesterdaysPosts = res.filter(function(d){
            return yesterdaysDate.isBefore(d.created_at.startOf("day")) && todaysDate.isAfter(d.created_at.startOf("day"))
        });
        var hasDayBefore = res.filter(function(d){
            return dayBeforeDate.isSame(d.created_at.startOf("day")) || dayBeforeDate.isAfter(d.created_at.startOf("day"));
        }).length;


        Array.prototype.push.apply(feed, yesterdaysPosts);

        if(hasDayBefore){
            done(null,feed);
        }else {
            var maxId = addORsub(res[res.length-1].id, '-1');
            getYesterdaysMentions(locId,maxId,feed,done)
        }

    })

}

function getAllRetweetsOfATweet(locId,id,maxId,feed,done){
    if(!feed){
        feed=[];
    }
    TW_getRetweetsOfTweet(locId,id,maxId,function(err,res){
        if(err){
            return done(err);
        }

        if(res.length){
            Array.prototype.push.apply(feed, res);
            var maxId = addORsub(res[res.length-1].id, '-1');
            getAllRetweetsOfATweet(locId,id,maxId,feed,done)

        }else {
            done(null,feed);
        }

    })

}


exports.getYesterdaysPosts      = getYesterdaysPosts;
exports.getYesterdaysPostsSince = getYesterdaysPostsSince;
exports.getYesterdaysMentions   = getYesterdaysMentions;
exports.getAllRetweetsOfATweet  = getAllRetweetsOfATweet;
exports.getAllFollowerIds       = getAllFollowerIds;
exports.getUserInfo             = TW_getUserInfo;
/**
 * Created by Sundeep on 3/13/2016.
 */

var moduleString = "insights";

var path  = require('path');
var async = require("async");
var mysql = require('mysql');

var dbconfig    = require("./dbconfig");

var pool = mysql.createPool(dbconfig.dbConfig);

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

function dailyStats(locId,done){
    var query = "SELECT count(id) AS total_media, sum(likes) AS total_likes" +
        ", sum(comments) AS total_comments,  date(created_at) AS `create_date` " +
        ", network " +
        "FROM history.youposts " +
        "WHERE loc_id = ? " +
        "AND created_at BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() " +
        "GROUP BY create_date, network;" +


        "SELECT  followers_count, following_count "+
        ", network, modified "+
        "FROM history.followers "+
        "WHERE loc_id = ? "+
        "AND modified BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() "+
        "ORDER BY network asc, modified asc; " +


        " SELECT a.network, a.id, b.id, a.followers_count-b.followers_count as diff, a.modified FROM "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified "+
        "  FROM history.followers "+
        " WHERE loc_id = ? "+
        " AND modified BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) a join "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified + INTERVAL 1 DAY as modified "+
        " FROM history.followers "+
        " WHERE loc_id = ? "+
        " AND modified BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) b " +
        " ON date(a.modified)= date(b.modified) and a.network = b.network;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("dailyStats getConnection : ",err);
            errorLog.log("dailyStats getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,locId,locId,locId], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("dailyStats query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function monthlyStats(locId,done){
    var query = "SELECT count(id) AS total_media, sum(likes) AS total_likes" +
        ", sum(comments) AS total_comments,  date(created_at) AS `create_date` " +
        ", network " +
        "FROM history.youposts " +
        "WHERE loc_id = ? " +
        "AND created_at BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() " +
        "GROUP BY create_date, network;" +


        "SELECT  followers_count, following_count "+
        ", network, modified "+
        "FROM history.followers "+
        "WHERE loc_id = ? "+
        "AND modified BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() "+
        "ORDER BY network asc, modified asc; " +


        " SELECT a.network, a.id, b.id, a.followers_count-b.followers_count as diff, a.modified FROM "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified "+
        "  FROM history.followers "+
        " WHERE loc_id = ? "+
        " AND modified BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) a join "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified + INTERVAL 1 DAY as modified "+
        " FROM history.followers "+
        " WHERE loc_id = ? "+
        " AND modified BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) b " +
        " ON date(a.modified)= date(b.modified) and a.network = b.network;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("monthlyStats getConnection : ",err);
            errorLog.log("monthlyStats getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,locId,locId,locId], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("monthlyStats query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsLikes(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = "SELECT sum(likes) AS total_likes" +
        ",  date(created_at) AS `create_date` " +
        ", network " +
        "FROM history.youposts " +
        "WHERE loc_id = ? and network = ? " +
        "AND created_at BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() " +
        "GROUP BY create_date;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsLikes getConnection : ",err);
            errorLog.log("statsLikes getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsLikes query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsComments(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = "SELECT sum(comments) AS total_comments" +
        ",  date(created_at) AS `create_date` " +
        ", network " +
        "FROM history.youposts " +
        "WHERE loc_id = ? and network = ? " +
        "AND created_at BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() " +
        "GROUP BY create_date;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsComments getConnection : ",err);
            errorLog.log("statsComments getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsComments query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsMedia(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = "SELECT count(id) AS total_media" +
        ",  date(created_at) AS `create_date` " +
        ", network " +
        "FROM history.youposts " +
        "WHERE loc_id = ? and network = ? " +
        "AND created_at BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() " +
        "GROUP BY create_date;";
    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsMedia getConnection : ",err);
            errorLog.log("statsMedia getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsMedia query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsFollowers(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = "SELECT  followers_count "+
        ", network, modified "+
        "FROM history.followers "+
        "WHERE loc_id = ? and network = ? "+
        "AND modified BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() "+
        "ORDER BY network asc, modified asc; ";

    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsFollowers getConnection : ",err);
            errorLog.log("statsFollowers getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsFollowers query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsFollowing(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = "SELECT  following_count "+
        ", network, modified "+
        "FROM history.followers "+
        "WHERE loc_id = ? and network = ? "+
        "AND modified BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() "+
        "ORDER BY network asc, modified asc; ";

    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsFollowing getConnection : ",err);
            errorLog.log("statsFollowing getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsFollowing query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

function statsGrowth(locId,network,duration,done){
    var days = (duration=="w"?7:30);
    var query = " SELECT a.network, a.id, b.id, a.followers_count-b.followers_count as diff, a.modified FROM "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified "+
        "  FROM history.followers "+
        " WHERE loc_id = ? and network = ?  "+
        " AND modified BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) a join "+
        " (SELECT  id, followers_count, following_count "+
        " , network, modified + INTERVAL 1 DAY as modified "+
        " FROM history.followers "+
        " WHERE loc_id = ? and network = ?  "+
        " AND modified BETWEEN CURDATE() - INTERVAL " + days + " DAY AND CURDATE() "+
        " ORDER BY network asc, modified asc) b " +
        " ON date(a.modified)= date(b.modified) and a.network = b.network;";

    pool.getConnection(function(err, connection) {
        if(err){
            console.log("statsGrowth getConnection : ",err);
            errorLog.log("statsGrowth getConnection",err);
            return done(err);
        }
        connection.query(query,[locId,network,locId,network], function(err, rows) {
            connection.release();
            if (err) {
                console.log(err, this.sql,this.values);
                errorLog.log("statsGrowth query",err,this.sql,this.values);
                return done(err);
            }
            return done(null,rows);
        });
    });
}

exports.moduleString    = moduleString;
exports.dailyStats      = dailyStats;
exports.monthlyStats    = monthlyStats;
exports.statsLikes      = statsLikes;
exports.statsComments   = statsComments;
exports.statsMedia      = statsMedia;
exports.statsFollowers  = statsFollowers;
exports.statsFollowing  = statsFollowing;
exports.statsGrowth     = statsGrowth;
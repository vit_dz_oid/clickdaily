# Monthly Statistics

SELECT count(id) AS total_media, sum(likes) AS total_likes, sum(comments) AS total_comments,  DATE_FORMAT(created_at, '%m/%Y') AS `month_created`, network
FROM history.youposts 
WHERE loc_id = 'a77520c0c14a11e5862b7f5fe1721d02' 
		AND created_at BETWEEN CURDATE() - INTERVAL 365 DAY AND CURDATE() 
GROUP BY month_created, network;

# Daily Statistics

SELECT count(id) AS total_media, sum(likes) AS total_likes, sum(comments) AS total_comments,  date(created_at) AS `create_date` , network
FROM history.youposts 
WHERE loc_id = 'a77520c0c14a11e5862b7f5fe1721d02' 
		AND created_at BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() 
GROUP BY create_date, network;
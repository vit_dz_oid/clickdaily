/**
 * Created by Sundeep on 9/22/2015.
 */

var locations = require("../../models/user").LocationName;
var helper = require("./Helpers");
var async = require("async");
var utils = require("util");
var moment = require("moment");
var fs = require("fs");

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'rootPass@',
    database : 'twitterinsights'
});
connection.connect();

function insertToDb(locId,results,callback){

    async.parallel({
        tweets : function(CB){
            if(results.youPosts.length){
                var query = "INSERT INTO `twitterinsights`.`tweets` "+
                "(tweetid, userid, inreply, retweet,date,time)\nVALUES\n";
                results.youPosts.forEach(function(d){
                    if(!d.in_reply){
                        d.in_reply={};
                    }
                    if(!d.retweet){
                        d.retweets = {};
                    }
                    query = query + "(\"" +
                        d.id + "\", \"" +
                        d.user_id + "\", " +
                        (d.in_reply.tweet?"\""+d.in_reply.tweet+"\"":"NULL") + ", " +
                        (d.retweet.tweet?"\""+d.retweet.tweet+"\"":"NULL") + ", \"" +
                        d.created_at.format("YYYY-MM-DD") + "\", \"" +
                        d.created_at.format("HH:mm:ss.SSS") + "\")\n,";
                });
                query=query.substr(0,query.length-1);
                connection.query(query, function(err, rows, fields) {
                    if (err) {
                        return CB(null,{error:err});
                    }
                    return CB(null,true);
                });
            }else {
                return CB(null,{error:new Error("No You posts")});
            }

        },
        mentions : function(CB){
            if((results.mentions.length)&&(results.userinfo.id)){
                var query = "INSERT INTO `twitterinsights`.`mentions` "+
                    "(tweetid, userid, inreplytweetid, mentioneduserid,date,time)\nVALUES\n";
                results.mentions.forEach(function(d){
                    if(!d.in_reply){
                        d.in_reply={};
                    }
                    query = query + "(\"" +
                        d.id + "\", \"" +
                        d.user_id + "\", " +
                        (d.in_reply.tweet?"\""+d.in_reply.tweet+"\"":"NULL") + ", \"" +
                        results.userinfo.id + "\", \"" +
                        d.created_at.format("YYYY-MM-DD") + "\",\" " +
                        d.created_at.format("HH:mm:ss.SSS") + "\")\n,";
                });
                query=query.substr(0,query.length-1);
                connection.query(query, function(err, rows, fields) {
                    if (err) {
                        return CB(null,{error:err});
                    }
                    return CB(null,true);
                });
            }else {
                return CB(null,{error:new Error("No mentions or userinfo")});
            }
        },
        /*retweets : function(CB){
            if(results.youPosts.length){
                var query = "INSERT INTO `twitterinsights`.`retweets` "+
                    "(tweetid, userid, retweetid, retweetuserid,date,time)\nVALUES\n";
                var flag = 0;
                results.youPosts.forEach(function(d){
                    if(d.retweets.length) {
                        flag = 1;
                        d.retweets.forEach(function (e) {
                            query = query + "(\"" +
                                d.id + "\", \"" +
                                d.user_id + "\", \"" +
                                e.id + "\", \"" +
                                e.user_id + "\", \"" +
                                e.created_at.format("YYYY-MM-DD") + "\", \"" +
                                e.created_at.format("HH:mm:ss.SSS") + "\")\n,";
                        });
                    }
                });
                if(flag){
                    query=query.substr(0,query.length-1);
                    connection.query(query, function(err, rows, fields) {
                        if (err) {
                            return CB(null,{error:err});
                        }
                        return CB(null,true);
                    });
                } else {
                    return CB(null,{error:new Error("No retweet posts")});
                }
            }else {
                return CB(null,{error:new Error("No You posts")});
            }
        },*/
        followerCounts : function(CB){
            var query = "INSERT INTO `twitterinsights`.`followercount` "+
                "(userid, followercount, date)\nVALUES\n";
            var flag = 0;
            var todaysDate = "\"" + moment().format("YYYY-MM-DD") + "\"";
            if(results.userinfo.id){
                flag=1;
                query = query + "(\"" +
                    results.userinfo.id + "\", " +
                    results.userinfo.follwers_count + ", " +
                    todaysDate + ")\n,";
            }
            if(results.mentions.length){
                flag=1;
                results.mentions.forEach(function(d){
                    query = query + "(\"" +
                        d.user_id + "\", " +
                        d.user_followers_count + ", " +
                        todaysDate + ")\n,";
                });
            }
            if(results.youPosts.length){
                results.youPosts.forEach(function(d){
                    if(d.retweets.length) {
                        flag=1;
                        d.retweets.forEach(function (e) {
                            query = query + "(\"" +
                                e.user_id + "\", " +
                                e.user_followers_count + ", " +
                                todaysDate + ")\n,";
                        });
                    }
                });
            }
            if(flag){
                query=query.substr(0,query.length-1);
                connection.query(query, function(err, rows, fields) {
                    if (err) {
                        return CB(null,{error:err});
                    }
                    return CB(null,true);
                });
            }else {
                return CB(null,new Error("No followers to gather"));
            }
        }
    }, function(err,results) {
        callback(null, results)
    });
}

function wrapperYouPost(locId,done){
    console.log("--------- Started Gathering YouPost for location("+locId+") ---------");
    helper.getYesterdaysPosts(locId,false,[],function(err,youposts){
        if(err){
            return done(null,{error:err});
        }
        done(null,youposts)
    });
}

function wrapperUserinfo(locId,done){
    console.log("--------- Started Gathering User Info for location("+locId+") ---------");
    helper.getUserInfo(locId,function(err,userinfo){
        if(err){
            return done(null,{error:err});
        }
        done(null,userinfo)
    });
}

function wrapperMentions(locId,done){
    console.log("--------- Started Gathering Mentions for location("+locId+") ---------");
    helper.getYesterdaysMentions(locId,false,[],function(err,youposts){
        if(err){
            return done(null,{error:err});
        }
        done(null,youposts)
    });
}

function getInsightsForALocation(locId,done){
    console.log("--------- Started Gathering Insights for location("+locId+") ---------");
    function getAllRetweets(tweet,cb){
        helper.getAllRetweetsOfATweet(locId,tweet.id,false,[],function(err,retweets){
            if(err){
                tweet.retweets = {error:err};
            }else {
                tweet.retweets = retweets;
            }
            cb(null,tweet)
        })
    }
    async.series({
            youPosts: function (callback) {
                wrapperYouPost(locId, callback);
            },
            userinfo : function(callback){
                wrapperUserinfo(locId,callback)
            },
            mentions: function (callback) {
                wrapperMentions(locId, callback);
            }
        },
    function(err,results){
        if(err){
            console.log("--------- Error Gathering Insights for location("+locId+") ---------",err);
            return done(err)
        }
        console.log("--------- Gathered Insights for location("+locId+") ---------");
        console.log("--- Mentions("+locId+") are as follows ---");
        console.log(utils.inspect(results.mentions));
        console.log("--- Mentions("+locId+") ---");
        console.log("--- You Posts("+locId+") are as follows ---");
        console.log(utils.inspect(results.youPosts));
        console.log("--- YouPosts("+locId+") ---");
        console.log("--------- Started Gathering Retweets for location("+locId+") ---------");
        done(null,results);
        /*return insertToDb(locId,results,function(err,response){
            return done(null,response);
        });*/
    });
}

locations.find(function(err,results){
    var locs = results.map(function(d){return d.SQLid});
    console.log("--------- Started Gathering Insights ---------");
    async.map(locs,getInsightsForALocation,function(err, results){
        connection.end();
        if(err){
            return console.log("--------- Error Gathering Insights for locations ---------",err);
        }
        console.log("--------- Gathered Insights for locations ---------",err);
        var mentions = results.filter(function(d){return d.mentions.length}).map(function(d){var a ={}; return a[d.userinfo.id] = d.mentions;});
        fs.writeFile("./Mentions.txt",utils.inspect(mentions, { depth: null }),function(){
            console.log("Written")
        });
    });
});

/*
helper.getYesterdaysPosts("1c108b93161442f8a1fd0d43e60d68bc",false,[],function(err,feed){
    console.log(feed);
});
helper.getAllFollowerIds("1c108b93161442f8a1fd0d43e60d68bc",[],false,function(err,followers){
    console.log(followers);
});

helper.getAllRetweetsOfATweet("1c108b93161442f8a1fd0d43e60d68bc","648682687415369728",false,[],function(err,retweets){
    console.log(retweets);
});*/


/*locations.find(function(err,results){
    var locs = results.map(function(d){return d.SQLid});
    locs.forEach(function(d){
        helper.getYesterdaysPosts("1c108b93161442f8a1fd0d43e60d68bc",[],function(err,feed){
            console.log(feed);
        });
        helper.getAllFollowerIds(d,[],false,function(err,followers){
            console.log(followers);
        });
    });
});*/
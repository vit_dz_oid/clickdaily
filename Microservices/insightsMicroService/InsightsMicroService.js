/**
 * Created by Sundeep on 3/13/2016.
 */

var helpers = require("./Helpers");

var moduleString = helpers.moduleString;

var redisRPCLib         = require("redis-rpc");


var redisRPC = new redisRPCLib({
    subModule   : moduleString + "socialDispatcher",
    pubModule   : "socialDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var mobileRPC = new redisRPCLib({
    subModule   : moduleString + "mobileDispatcher",
    pubModule   : "mobileDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

console.log("Insights Service Started");
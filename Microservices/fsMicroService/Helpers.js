/**
 * Created by Sundeep on 6/12/2015.
 */

var moduleString = "fs";
var path = require('path');
var vars = require('./vars');

var model = require('./Model')[moduleString];

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function FS_delNetwork(locId,done){
    model.findByIdAndRemove(locId,function(err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        return done(null);
    });
}

function FS_status(locId,done){
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        if(!document){
            if(!document){
                document = new model();
                document._id = locId;
            }
        }
        done(null,!!(document.token));
    });
}

function FS_getUserInfo(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if (document.updateTime.user_info) {
            done(null, document.cache.user_info, document.cache.venue_info, document.selected_venue);
            var diff = (new Date()) - document.updateTime.user_info;
            if (diff > vars.updateTimes.FS.userInfo) {
                flag = 1;
            }
        } else {
            flag = 1;
        }


        if (flag) {
            var token = document.token;
            document.helper.Users.getUser('self', token, function (err, result) {
                if ((err) || (!result)) {
                    errorLog.log( 'Users.getUser.self', err);
                    //document.remove();
                    /*document.save(function (err, doc) {
                     if (err) console.log(err)
                     });*/
                    return done(err);
                }

                result = result.user;
                if (result.type != "user") {
                    document.remove();
                    document.save(function (err, doc) {
                        if (err)
                            errorLog.log("Document save Error",err)
                    });
                    return (done(null, {type: "User type venue"}))

                }

                document.cache.user_info.id = result.id;
                document.cache.user_info.name = result.firstName + " " + result.lastName;
                document.cache.user_info.profile_pic = result.photo.prefix + "48x48" + result.photo.suffix;
                document.private_user_info = result;
                document.updateTime.user_info = new Date();
                var token = document.token;
                document.helper.Venues.getManaged(token, function (err, result) {
                    if (err) {
                        errorLog.log('Venues.getManaged', err);
                        //document.remove();
                        document.save(function (err, doc) {
                            if (err){
                                errorLog.log("Document save Error",err)
                            }
                        });
                        return done(Err);
                    }
                    document.FB[0].updateTime.venue_info = new Date();
                    if (result.venues.count) {
                        result.venues.items.forEach(function (d, i) {
                            d._id = d.id;
                            document.AddVenue(d);
                            document.helper.Venues.getVenue(d.id, token, function (err, results) {
                                if (err) {
                                    errorLog.log('Venues.getVenue', err);
                                    document.remove();
                                    document.save(function (err, doc) {
                                        if (err){
                                            errorLog.log("Document save Error",err)
                                        }
                                    });
                                    return;
                                }
                                document.cache.venue_info.id(results.venue.id).profile_pic = results.venue.page.user.photo.prefix + "50x50" + results.venue.page.user.photo.suffix;
                                document.save(function (err, doc) {
                                    if (err){
                                        errorLog.log("Document save Error",err)
                                    }
                                });
                                done(null, document.cache.user_info, document.cache.venue_info, document.selected_venue.length);
                            });
                        });
                    }

                    document.save(function (err, doc) {
                        if (err){
                            errorLog.log("Document save Error",err)
                        }
                    });
                });
                //done(document.cache.user_info,document.cache.venue_info);

                document.save(function (err, doc) {
                    if (err){
                        errorLog.log("Document save Error",err)
                    }
                });

            });
        }
    });
}

function FS_getVenueFeedFromId(locId,id,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var token = document.token;
        document.helper.Venues.getTips(id,{},token, function (err,result) {
            if (err) {
                errorLog.log('Venues.getTips',err);
                /*document.remove();*/
                document.save(function (err, doc) {
                    if (err){
                        errorLog.log("Document save Error",err)
                    }
                });
                return done(err);
            }
            if(!document.updateTime.feeds){
                document.updateTime.feeds = {};
            }
            if(!document.cache.feeds){
                document.cache.feeds = {};
            }
            if(!document.paging.feeds){
                document.paging.feeds = {};
            }
            document.updateTime.feeds[id] = new Date();
            document.cache.feeds[id] = result.tips.items;
            done(null, document.cache.venue_info.id(id),document.cache.feeds[id]);
            document.save(function (err, doc) {
                if (err){
                    errorLog.log("Document save Error",err)
                }
            });
        });
    });
}

function FS_getVenuesFeed(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        if (document.selected_venue.length) {
            document.selected_venue.forEach(function (d, i) {
                var flag = 0;
                var diff;
                if ((document.cache.feeds) && (document.cache.feeds[d])) {
                    done(null,document.cache.venue_info.id(d), document.cache.feeds[d]);
                    diff = (new Date()) - document.updateTime.feeds[d];
                    if (diff > vars.updateTimes.FS.feed) {
                        flag = 1;
                    }
                } else {
                    flag = 1;
                }

                if (flag) {
                    FS_getVenueFeedFromId( locId, d, done);
                }
            });
        }
    });
}

function  FS_addVenues(locId,data,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        if (typeof(data) === "string") {
            data = [data];
        }
        if (data instanceof Array) {
            data.forEach(function (d, i) {
                if (document.selected_venue.indexOf(d) === -1) {
                    document.selected_venue.push(d);
                }
                FS_getVenueFeedFromId( locId, d, done);
            });
        }
        console.log("Selected Pages", data);
    });
}

function  FS_addAllVenues(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var data = document.venue;
        FS_addVenues( locId, data, done);
    });
}

function FS_clearFeed(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        if(!document.cache.feeds){
            document.cache.feeds = {};
        }
        document.updateTime.feeds = {};
        document.cache.feeds = {};
        document.markModified('cache.feeds');
        document.save(function (err, doc) {
            done(document.cache.feeds.me);
            if(err) return console.log(err);
            console.log("Doc Saved");
        });
    });
}


exports.status           = FS_status;
exports.delNetwork       = FS_delNetwork;
exports.getUserInfo      = FS_getUserInfo;
exports.getVenuesFeed    = FS_getVenuesFeed;
exports.addVenues        = FS_addVenues;
exports.addAllVenues     = FS_addAllVenues;
exports.clearFeed        = FS_clearFeed;
exports.moduleString     = moduleString;
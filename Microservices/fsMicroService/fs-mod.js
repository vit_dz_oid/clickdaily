/**
 * Created by Sundeep on 6/16/2015.
 */

var nodeFoursquare = require('node-foursquare');

var foursquare = function(config){
    var thisVal = nodeFoursquare(config);
    this.Users = thisVal.Users;
    this.Venues = thisVal.Venues;
    this.Checkins = thisVal.Checkins;
    this.Tips = thisVal.Tips;
    this.Lists = thisVal.Lists;
    this.Photos = thisVal.Photos;
    this.Settings = thisVal.Settings;
    this.Specials = thisVal.Specials;
    this.Updates = thisVal.Updates;
    this.Events = thisVal.Events;
    this.getAccessToken = thisVal.getAccessToken;
    this.getAuthClientRedirectUrl = thisVal.getAuthClientRedirectUrl;
};

module.exports = foursquare;
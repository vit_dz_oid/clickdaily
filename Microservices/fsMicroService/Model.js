/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');

var Foursquare = require('./fs-mod');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var FS_VenueInfoSchema = mongoose.Schema({
    name            : String
    , _id           : String
    , profile_pic   : String
    , location      : Object
    , stats         : Object
    , contact       : Object
    , venuePage     : Object
},schemaOptions);

var FS_Schema = mongoose.Schema({
    _id                     : String,  // loc Id
    userId                  : String,  // FB user Id
    token                   : String,
    expires                 : String,
    venue                   : [String],
    selected_venue          : [String],
    feeds                   : Object, //{me:feed,pageId:feed}
    private_user_info       : Object,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        venue_info          : Date,
        feeds               : Object //{me:Date,pageId:Date}
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache                   : {
        user_info           : {
            firstName       : String
            ,lastName       : String
            ,profile_pic    : String
        }
        , venue_info        : [FS_VenueInfoSchema]
        , feeds             : Object
        , messages          : Object
        , friends           : Object
    }

},schemaOptions);


FS_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
}).set(function (id) {
    this.set('userId', id);
});

FS_Schema.virtual('cache.user_info.name').get(function () {
    return this.cache.user_info.firstName + ' ' + this.cache.user_info.lastName;
}).set(function (setFullNameTo) {
    var split = setFullNameTo.split(' ')
        , firstName = split[0]
        , lastName = split[1];

    this.set('cache.user_info.firstName', firstName);
    this.set('cache.user_info.lastName', lastName);
});

FS_Schema.virtual('config').get(function () {
    return {
        secrets:{
            clientId : "LIKUTRY2RNA1I4CZUKKQ0JO4MTLUTFVWVMAEJVDCWZBCVQFR"
            , clientSecret : "CZK11Z0FGSMPY5OMFZX0AVP0H23MW1GZCBBHEKQ3VVOJUEOM"
            , redirectUrl : "https://merchant.clickdaily.com/Social/Foursquare"
        }

    };
});

FS_Schema.virtual('helper').get(function () {
    return new Foursquare(this.config);
});

FS_Schema.methods.AddVenue = function(venue_info) {
    if(this.venue.indexOf(venue_info._id)===-1){
        this.cache.venue_info.push(venue_info);
        this.venue.push(venue_info._id);
    }
};

FS_Schema.methods.AddVenuePic = function(id,url) {
    this.cache.venue_info.id(id).profile_pic=url;
};

exports.fs = mongoose.model('FS', FS_Schema);
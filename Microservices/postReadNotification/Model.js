/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');


var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "readNotifications";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/readNotifications');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var IT_Schema = mongoose.Schema({
    _id                     : String,   //Location Id
    posts                   : {type:Object,default:{}}
},schemaOptions);

var TW_Schema = mongoose.Schema({
    _id                     : String,   //Location Id
    posts                   : {type:Object,default:{}}
},schemaOptions);

var FB_Schema = mongoose.Schema({
    _id                     : String,   //Location Id
    posts                   : {type:Object,default:{}}
},schemaOptions);

var FS_Schema = mongoose.Schema({
    _id                     : String,   //Location Id
    posts                   : {type:Object,default:{}}
},schemaOptions);


exports.it = mongoose.model('IT', IT_Schema);
exports.tw = mongoose.model('TW', TW_Schema);
exports.fb = mongoose.model('FB', FB_Schema);
exports.fs = mongoose.model('FS', FS_Schema);
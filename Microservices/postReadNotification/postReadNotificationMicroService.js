/**
 * Created by Sundeep on 6/12/2015.
 */

var helpers = require("./Helpers");

var moduleString = helpers.moduleString;

var redisRPCLib         = require("redis-rpc");


var redisRPC = new redisRPCLib({
    subModule   : moduleString + "socialDispatcher",
    pubModule   : "socialDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var routesRPC = new redisRPCLib({
    subModule   : moduleString + "routesDispatcher",
    pubModule   : "routesDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var mobileRPC = new redisRPCLib({
    subModule   : moduleString + "mobileDispatcher",
    pubModule   : "mobileDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});
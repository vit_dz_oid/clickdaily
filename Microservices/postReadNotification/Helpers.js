/**
 * Created by Sundeep on 9/6/2015.
 */

var moduleString = "postReadNotification";
exports.moduleString = moduleString;
var path = require('path');

var fbModel = require('./Model').fb;
var fsModel = require('./Model').fs;
var itModel = require('./Model').it;
var twModel = require('./Model').tw;

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var docCache = {TW:{},FB:{},IT:{},FS:{}};

var getDocument = {
    TW : function (id, callback) {
            if (docCache.TW[id]) {
                return callback(null, docCache.TW[id]);
            }
            twModel.findById(id, function (err, document) {
                if (err) {
                    return callback(err);
                }
                if (!document) {
                    document = new twModel();
                    document._id = id;
                    document.save(function(err,doc){
                        if(err){
                            return callback(new Error("Document null"))
                        }
                        docCache.TW[id] = doc;
                        return callback(null, doc);
                    });
                    return;
                }
                if (docCache.TW[id]) {
                    return callback(null, document);
                }
                docCache.TW[id] = document;
                callback(null, document);
            });
        },
    FB : function (id, callback) {
        if (docCache.FB[id]) {
            return callback(null, docCache.FB[id]);
        }
        fbModel.findById(id, function (err, document) {
            if (err) {
                return callback(err);
            }
            if (!document) {
                document = new fbModel();
                document._id = id;
                document.save(function(err,doc){
                    if(err){
                        return callback(new Error("Document null"))
                    }
                    docCache.FB[id] = doc;
                    return callback(null, doc);
                });
                return;
            }
            if (docCache.FB[id]) {
                return callback(null, document);
            }
            docCache.TW[id] = document;
            callback(null, document);
        });
    },
    FS : function (id, callback) {
        if (docCache.FS[id]) {
            return callback(null, docCache.FS[id]);
        }
        fsModel.findById(id, function (err, document) {
            if (err) {
                return callback(err);
            }
            if (!document) {
                document = new fsModel();
                document._id = id;
                document.save(function(err,doc){
                    if(err){
                        return callback(new Error("Document null"))
                    }
                    docCache.FS[id] = doc;
                    return callback(null, doc);
                });
                return;
            }
            if (docCache.FS[id]) {
                return callback(null, document);
            }
            docCache.FS[id] = document;
            callback(null, document);
        });
    },
    IT : function (id, callback) {
        if (docCache.IT[id]) {
            return callback(null, docCache.IT[id]);
        }
        itModel.findById(id, function (err, document) {
            if (err) {
                return callback(err);
            }
            if (!document) {
                document = new itModel();
                document._id = id;
                document.save(function(err,doc){
                    if(err){
                        return callback(new Error("Document null"))
                    }
                    docCache.IT[id] = doc;
                    return callback(null, doc);
                });
                return;
            }
            if (docCache.IT[id]) {
                return callback(null, document);
            }
            docCache.IT[id] = document;
            callback(null, document);
        });
    }
};

exports.save = function(locId,id,network,readUnread,done){
    if(Object.keys(getDocument).indexOf(network)!=-1){
        getDocument[network](locId,function(err,document){
            if(err){
                errorLog.log("Document Fetch Error",err,{locId:locId,id:id,network:network});
                return done(err,id,network,null);
            }
            if(typeof readUnread != "boolean"){
                readUnread = true;
            }
            document.posts[id] = readUnread;
            document.save(function(err){
                if(err){
                    errorLog.log("Document save Error",err,{locId:locId,id:id,network:network});
                    return done(err,id,network,null);
                }
                return done(null,id,network,true);
            });
        });
    }else{
        var err = new Error("wrong network type");
        errorLog.log("Document Fetch Error",err,{locId:locId,id:id,network:network});
        return done(err,id,network,null);
    }
};

exports.getN = function(locId,id,network,done){
    if(Object.keys(getDocument).indexOf(network)!=-1){
        getDocument[network](locId,function(err,document){
            if(err){
                errorLog.log("Document Fetch Error",err,{locId:locId,id:id,network:network});
                return done(err,id,network,null);
            }
            if(!document.posts[id]){
                return done(null,id,network,false);
            }
            return done(null,id,network,true);
        });
    }else{
        var err = new Error("wrong network type");
        errorLog.log("Document Fetch Error",err,{locId:locId,id:id,network:network});
        return done(err,id,network,null);
    }
};
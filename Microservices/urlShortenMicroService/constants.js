exports.moduleString = "urlShorten";
exports.root_url = "cd0.io/";
exports.min_vanity_length = 4;
exports.num_of_urls_per_hour = 50;
exports.max_retries = 50;
exports.segment_length = 6;

exports.get_query = 'SELECT * FROM urls WHERE segment = {SEGMENT} and locId = {LOC}';
exports.get_all_query = 'SELECT * FROM urls WHERE locId = {LOC}';
exports.add_query = 'INSERT INTO urls SET url = {URL}, segment = {SEGMENT}, ip = {IP}, locId = {LOC}';
exports.check_url_query = 'SELECT * FROM urls WHERE url = {URL}';
exports.update_views_query = 'UPDATE urls SET num_of_clicks = {VIEWS} WHERE id = {ID}';
exports.insert_view = 'INSERT INTO stats SET ip = {IP}, url_id = {URL_ID}, referer = {REFERER}';
exports.check_ip_query = 'SELECT COUNT(id) as counted FROM urls WHERE datetime_added >= now() - INTERVAL 1 HOUR AND ip = {IP}';

exports.host = 'localhost';
exports.user = 'root';
exports.password = 'rootPass@';
exports.database = 'urlsh';

exports.port = 3500;
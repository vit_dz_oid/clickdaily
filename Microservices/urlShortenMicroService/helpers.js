/*
* Messages:
* 100: action executed successfully
* 400: couldn't add URL, most likely the vanity url is already taken or locId not provided
* 401: the URL isn't reachable
* 402: the "URL" parameter isn't set
* 403: the vanity string contains invalid characters
* 404: the short link doesn't exist
* 405: the vanity string can't be longer than 15 characters
* 406: the URL can't be longer than 1000 characters
* 407: the vanity string has to contain more characters
* 408: maximum number of URL's per hour exceeded
*/
var mysql = require("mysql");
var req = require("request");
var cons = require("./constants");
var crypto = require('crypto');
var pool = mysql.createPool({
		host:cons.host,
		user:cons.user,
		password:cons.password,
		database:cons.database
	});
var path = require('path');
var errorLogOptions = {
	path : path.join(__dirname, 'error_logs'),
	module : cons.moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

//onSuccess: the method which should be executed if the hash has been generated successfully
//onError: if there was an error, this function will be executed
//retryCount: how many times the function should check if a certain hash already exists in the database
//url: the url which should be shortened
//request / response: the request and response objects
//con: the MySQL connection
//vanity: this should be a string which represents a custom URL (e.g. "url" corresponds to d.co/url)
function generateHash(retryCount, url, con, locId, userId,done) {
	var hash = "";

	//This section creates a string for a short URL on basis of an SHA1 hash
	var shasum = crypto.createHash('sha1');
	shasum.update((new Date).getTime()+"");
	hash = shasum.digest('hex').substring(0, cons.segment_length);
	//This section query's (with a query defined in "constants.js") and looks if the short URL with the specific segment already exists
	//If the segment already exists, it will repeat the generateHash function until a segment is generated which does not exist in the database
    con.query(cons.get_query.replace("{SEGMENT}", con.escape(hash)).replace("{LOC}", con.escape(locId)), function(err, rows){
		if(err){
			errorLog.log("Document Fetch Error",err,locId);
			return done(err,"",url);
		}
        if (rows != undefined && rows.length == 0) {
			handleHash(hash, url, locId, con, userId,done);
        } else {
            if (retryCount > 1) {
                generateHash(retryCount - 1, url, con, locId, userId, done);
            } else {
				err = new Error("Max retires exceeded");
				errorLog.log("Document Fetch Error",err,locId);
				return done(err),"",url;
            }
        }
    });
}

//The function that is executed when the short URL has been created successfully.
function handleHash(hash, url, locId, con,userId,done){
	var add_query = cons.add_query
		.replace("{URL}", con.escape(url))
		.replace("{SEGMENT}", con.escape(hash))
		.replace("{IP}", con.escape(userId))
		.replace("{LOC}", con.escape(locId));
	con.query(add_query
	, function(err, rows){
		if(err){
			errorLog.log("Document Fetch Error",err,locId);
			return done(err,"",url);
		}
		done(null,cons.root_url+hash,url);
	});
}

//This method looks handles a short URL and redirects to that URL if it exists
//If the short URL exists, some statistics are saved to the database
var getUrl = function(locId,segment, done){
	pool.getConnection(function(err, con){
		con.query(cons.get_query
			.replace("{SEGMENT}", con.escape(segment))
			.replace("{LOC}", con.escape(locId))
		, function(err, rows){
			if(err){
				errorLog.log("Document Fetch Error",err,locId);
				return done(err);
			}
			if(rows.length <= 0){
				err = new Error("No Record Found");
				errorLog.log("Document Fetch Error",err,locId);
				return done(err);
			}
			done(null,rows[0].url);
		});
		con.release();
	});
};

function addHTTP(url) {
	if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
		url = "http://" + url;
	}
	return url;
}

//This function adds attempts to add an URL to the database.
// If the URL returns a 404 or if there is another error,
// this method returns an error to the client,
// else an object with the newly shortened URL is sent back to the client.



var addUrl = function(locId,url,userId, done){
	pool.getConnection(function(err, con){
		if(url){
			url = addHTTP(url);
			url = decodeURIComponent(url).toLowerCase();
			con.query(cons.check_ip_query.replace("{IP}", con.escape(userId)), function(err, rows){
				if(err){
					errorLog.log("Document Fetch Error",err,locId);
					return done(err,"",url);
				}
				if(rows[0].counted != undefined && rows[0].counted < cons.num_of_urls_per_hour){
					con.query(cons.check_url_query.replace("{URL}", con.escape(url)), function(err, rows){
						if(err){
							errorLog.log("Document Fetch Error",err,locId);
							return done(err,"",url);
						}
						if(url.indexOf("http://localhost") > -1 || url.indexOf("https://localhost") > -1){
							err = new Error("Adding localhost");
							errorLog.log("Document Fetch Error",err,locId);
							return done(err,"",url);
						}
						if(url.length > 1000){
							err = new Error("Url greater than 1000");
							errorLog.log("Document Fetch Error",err,locId);
							return done(err,"",url);
						}
						if(!err && rows.length > 0){
							done(null,cons.root_url+rows[0].segment, url);
						}
						else{
							req(url, function(err, res, body){
								if(res != undefined && res.statusCode == 200){
									generateHash( cons.max_retries, url, con, locId,userId,done);
								}
								else{
									err = new Error("Url not reachable");
									errorLog.log("Document Fetch Error",err,locId);
									return done(err,"",url);
								}
							});
						}
					});
				}
				else{
					err = new Error("Added more than " +cons.num_of_urls_per_hour + " urls in last hours");
					errorLog.log("Document Fetch Error",err,locId);
					return done(err,"",url);
				}
			});
		}
		else{
			err = new Error("No URL given");
			errorLog.log("Document Fetch Error",err,locId);
			return done(err,"",url);
		}
		con.release();
	});
};

//This method looks up stats of a specific short URL and sends it to the client
var getAll = function(locId, done){
	pool.getConnection(function(err, con){
		if(err){
			errorLog.log("Document Fetch Error",err,locId);
			return done(err);
		}
		con.query(cons.get_all_query.replace("{LOC}", con.escape(locId)), function(err, rows){
			if(err || rows.length == 0){
				errorLog.log("Document Fetch Error",err,locId);
				return done(err);
			}
			else{
				rows = rows.map(function(d){
					return {url: d.url,hash: addHTTP(cons.root_url+ d.segment),clicks: d.num_of_clicks};
				});
				done(null,rows);
			}
		});
		con.release();
	});
};

exports.getUrl = getUrl;
exports.addUrl = addUrl;
exports.getAll = getAll;
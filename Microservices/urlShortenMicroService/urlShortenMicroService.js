/**
 * Created by Sundeep on 6/12/2015.
 */

var helpers = require("./helpers");
var cons = require("./constants");

var moduleString = cons.moduleString;

var redisRPCLib         = require("redis-rpc");


var socialRPC = new redisRPCLib({
    subModule   : moduleString + "socialDispatcher",
    pubModule   : "socialDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var routesRPC = new redisRPCLib({
    subModule   : moduleString + "routesDispatcher",
    pubModule   : "routesDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var mobileRPC = new redisRPCLib({
    subModule   : moduleString + "mobileDispatcher",
    pubModule   : "mobileDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});
/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var schemaOptionsID = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
};

var post_Schema = mongoose.Schema({
    network          : Number //0:Facebook 1:Twitter
    , create_time    : Date
    , post_time      : Date
    , type           : Number //0:Link, 1:Text, 2:Attachment
    , approvingUser  : [Number]
    , needApproval   : {type: Number, default : 0}
    , approved       : {type: Number, default : 1}
    , postingUserType: Number
    , creatingUserID : String
    , network_data   : {
        reached      : Number
        , likes      : Number
        , comments   : Number
        , post_id    : String
        , pic_id     : mongoose.Schema.Types.Mixed
        , pic_url    : {type : String,default : ""}
    }
    , post_data           : {
        stream       : String
        , text       : String
        , name       : String
        , caption    : String
        , picture    : String
        , link       : String
        , oldId      : String
    }

},schemaOptionsID);

var posts_Schema = mongoose.Schema({
    _id         : String
    ,posts      : [post_Schema]
},schemaOptions);


exports.post = mongoose.model('post', posts_Schema);
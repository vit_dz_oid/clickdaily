/**
 * Created by Sundeep on 6/26/2015.
 */

var model = require('../fbMicroService/Model').fb;
var moment = require('moment');
var path  = require('path');

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "fbPost"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var request         = require('request');
var redis           = require('redis');
var redisRStream    = require('redis-rstream'); // factory

var client          = redis.createClient(null, null, {detect_buffers: true}); // create client using your options and auth

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function getPromotablePosts(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if (id) {
            var access_token = document.selected_page_token;
            document.helper.api(id, {
                fields: "promotable_posts{id,scheduled_publish_time,object_id,created_time,full_picture}"
                , access_token: access_token
            }, function (res) {
                if (!res || res.error) {
                    errorLog.log(id + "/promotable_posts", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }
                done(null,res);
            });
        }
    });
}

function FB_postStream(locId,text,stream,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var attachment = redisRStream(client, "FB"+stream);
        if (!attachment.path)
            attachment.path = true;
        if (!attachment.mode)
            attachment.mode = true;
        var fburl = 'https://graph.facebook.com/v2.2/'
            + document.selected_page
            + '/photos?access_token='
            + access_token;
        var formData = {
            'source'  : attachment,
            'message' : text
        };
        request.post({url: fburl, formData: formData}, function (err, res, body) {
            client.del("FB" + stream,function (err, res) {
                console.log("Deleted FB Schedule Stream [FB" , stream,"] : ",err,res);
            });
            if (err) {
                errorLog.log('post.me/feed', err);
                return done(err, body);
            } else {
                body = JSON.parse(body);
                done(null,body);
                /*getPromotablePosts( locId, function (list) {
                    if ((list)
                        && (list.promotable_posts)
                        && (list.promotable_posts.data)
                        && (list.promotable_posts.data.length)
                    ) {
                        list.promotable_posts.data.forEach(function (d) {
                            if ((d.object_id == body.id) && (d.scheduled_publish_time == timestamp)) {
                                return body.id = d.id;
                            }
                        });
                    }
                    getPromotablePosts(locId, function (list) {
                        if ((list)
                            && (list.promotable_posts)
                            && (list.promotable_posts.data)
                            && (list.promotable_posts.data.length)
                        ) {
                            list.promotable_posts.data.forEach(function (d) {
                                if ((d.object_id == body.id) && (d.scheduled_publish_time == timestamp)) {
                                    return body.id = d.id;
                                }
                            });
                        }


                    });
                });*/
            }
        });
    });
}

function FB_schStream(locId,timestamp,text,stream,done) {
    //
    // //timestamp = moment(timestamp).format("X");
    if(parseInt(timestamp)-parseInt(moment().format("X"))<600){
        return done(new Error("time less tha 10 minutes"));
    }
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var attachment;
        if(typeof stream == "string") {
            attachment = redisRStream(client, "FB" + stream);
        } else if (typeof stream == "object" && stream.readable){
            attachment = stream;
        }else {
            var error = new Error("Stream not readable");
            errorLog.log("Stream not readable", error,stream);
            return done(error);
        }
        if (!attachment.path)
            attachment.path = true;
        if (!attachment.mode)
            attachment.mode = true;
        var fburl = 'https://graph.facebook.com/v2.2/'
            + document.selected_page
            + '/photos?access_token='
            + access_token;
        var formData = {
            'images'  : attachment,
            'message' : text,
            scheduled_publish_time: timestamp,
            published: 'false'
        };
        request.post({url: fburl, formData: formData}, function (err, res, body) {
            client.del("FB" + stream,function (err, res) {
                console.log("Deleted FB Schedule Stream [FB" , stream,"] : ",err,res);
            });
            if (err) {
                errorLog.log('post.me/feed', err);
                return done(err, body);
            } else {
                body = JSON.parse(body);
                if(body.error){
                    errorLog.log('post.me/feed', body.error);
                    return done(err, body);
                }
                getPromotablePosts(locId, function (err,list) {
                    if ((list)
                        && (list.promotable_posts)
                        && (list.promotable_posts.data)
                        && (list.promotable_posts.data.length)
                    ) {
                        list.promotable_posts.data.forEach(function (d) {
                            if ((d.object_id == body.id) && (d.scheduled_publish_time == timestamp)) {
                                body.pic_url = d.full_picture;
                                return body.post_id = d.id;
                            }
                        });
                    }

                    done(null,body);
                });
            }
        });
    });
}

function FB_postText(locId,text,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var fburl = 'https://graph.facebook.com/v2.2/'
            + document.selected_page
            + '/feed?access_token='
            + access_token;
        var formData = {
            'message' : text
        };
        request.post({url: fburl, formData: formData}, function (err, res, body) {
            if (err) {
                errorLog.log('post.me/feed', err);
                return done(err, body);
            } else {
                try{
                    body = JSON.parse(body);
                } catch(e){
                    errorLog.log('post.me/feed', e);
                    return done(e);
                }
                done(null, body);
            }
        });
    });
}

function FB_schText(locId,timestamp,text,done) {

    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var fburl = 'https://graph.facebook.com/v2.2/'
            + document.selected_page
            + '/feed?access_token='
            + access_token;
        var formData = {
            'message' : text,
            scheduled_publish_time: timestamp,
            published: 'false'
        };
        request.post({url: fburl, formData: formData}, function (err, res, body) {
            if (err) {
                errorLog.log('post.me/feed', err);
                return done(err, body);
            } else {
                body = JSON.parse(body);
                done(null, body);
            }
        });
    });
}

function FB_getPost(locId,post_id,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        document.helper.api(post_id ,'GET',
            {fields : "id,message,actions,admin_creator,application,from,icon,is_hidden" +
                    ",is_expired,link,message_tags,object_id,picture,privacy,scheduled_publish_time" +
                    ",status_type,subscribed,type,updated_time,full_picture,attachments"
                ,access_token: access_token},function (res) {
            if(!res || res.error) {
                errorLog.log(id + "/GET", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
            }else{
                console.log("Post : ",res);
                done(null,res)
            }
        });
    });
}

function FB_rechdulePost(locId,timestamp,post_id,done) {
    getDocument(locId, function (err, document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var formData = {
            scheduled_publish_time: timestamp
        };
        var fburl = 'https://graph.facebook.com/v2.2/'
            + post_id
            + '?access_token='
            + access_token;
        request.post({url: fburl, formData: formData}, function (err, res, body) {
            if (err) {
                errorLog.log('post.me/feed', err);
                return done(err, body);
            } else {
                body = JSON.parse(body);
                done(null, body)
            }
        });
    });
}

function FB_delPost(locId,id,done) {
    if(id){
        getDocument(locId, function (err,document) {
            if (err) {
                errorLog.log("Document Fetch Error", err);
                return done(err);
            }
            var access_token = document.selected_page_token;
            document.helper.api(id ,'delete',{access_token: access_token},function (res) {
                if(!res || res.error) {
                    errorLog.log(id + "/delete", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }else{
                    console.log("Post : ",res);
                    done(null,res)
                }
            });
        });
    }else{
        errorLog.log(id + " No post", new Error("No post"));
        done(new Error("No post"));
    }
}

exports.postStream  = FB_postStream;
exports.schStream   = FB_schStream;
exports.postText    = FB_postText;
exports.schText     = FB_schText;
exports.getPost     = FB_getPost;
exports.delPost     = FB_delPost;
exports.rechdulePost  = FB_rechdulePost;
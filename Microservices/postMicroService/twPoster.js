/**
 * Created by Sundeep on 6/29/2015.
 */

var model = require('../twMicroService/Model').tw;
var path  = require('path');
var fs    = require('fs');

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "twPost"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var request         = require('request');
var redis           = require('redis');
var redisRStream    = require('redis-rstream'); // factory

var client          = redis.createClient(null, null, {detect_buffers: true});


var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function TW_postStream(locId,text,stream,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.updateTime.you = new Date(1427988307103);
        document.markModified('updateTime.you');
        function sendTwitter(attachment){
            document.helper.postMedia({media: attachment, text: text}, function (err, response, body) {
                errorLog.log("postMedia", err, body);
                return done(err);
            }, function (res) {
                if ((!res ) || (res.error)) {
                    errorLog.log("postMedia", !res ? new Error('error occurred') : res.error);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                done(null, res);
            });
        }
        if(typeof stream == "string") {
            var attachmentStream = redisRStream(client, "TW" + stream);
            var bufs = [];
            attachmentStream.on('data', function (chunck) {
                bufs.push(chunck);
            });
            attachmentStream.on('end', function () {
                //console.log('buffer',buff);
                client.del("TW" + stream,function (err, res) {
                    console.log("Deleted TW Schedule Stream [TW" , stream,"] : ",err,res);
                });
                var attachment = Buffer.concat(bufs);
                sendTwitter(attachment);
            });
        } else if(typeof stream == "object"){
            var attachment = stream.path;
            fs.readFile(attachment,{encoding:"base64"},function(err,data){
                if(err){
                    errorLog.log("Could not read picture", new Error("Could not read picture"), locId);
                }
                sendTwitter(data);
            });

        } else {
            errorLog.log("Stream type wrong", new Error("Stream type wrong"), stream);
            return done(err);
        }
    });
}

function TW_postText(locId,postData,done) {
    getDocument(locId, function (err, document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.updateTime.you = new Date(1427988307103);
        document.markModified('updateTime.you');
        //var data = {status: text};
        document.helper.tweet(postData, function (err, response, body) {
            errorLog.log("return", err);
            return done(err,body);
        }, function (res) {
            if ((!res ) || (res.error)) {
                errorLog.log("tweet",!res ? new Error('error occurred') : res.error);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null, res);
        });
    });
}

function TW_schText(locId,postData,done) {
    getDocument(locId, function (err, document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.updateTime.you = new Date(1427988307103);
        document.markModified('updateTime.you');
        document.helper.tweet(postData, function (err, response, body) {
            errorLog.log("return", err);
            return done(err,body);
        }, function (res) {
            if ((!res ) || (res.error)) {
                errorLog.log("tweet",!res ? new Error('error occurred') : res.error);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null, res);
        });
    });
}

function TW_getPost(locId,post_id,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.getTweet({id:post_id},function(err){
            errorLog.log("return", err);
            return done(err);
        },function(res){
            if((!res )||(res.error)) {
                errorLog.log("tweet",!res ? new Error('error occurred') : res.error);
                return done(!res ? new Error('error occurred') : res.error);
            }
            res = JSON.parse(res);
            done(null,res);
        });
    });
}


function TW_delPost(locId,id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.delTweet({id:id},function(err,resp,body){
            errorLog.log("return", err);
            return done(err);
        },function(res){
            console.log("deleted res : ",res);
            if((!res )||(res.error)) {
                errorLog.log("tweet",!res ? new Error('error occurred') : res.error);
                return done(!res ? new Error('error occurred') : res.error);
            }
            document.updateTime.you = new Date(1427988307103);
            document.markModified('updateTime.you');
            document.save(function(err){
                if(err){
                    return errorLog.log("Document save Error",err)
                }
                done(null,res);
            });
        });
    });
}


exports.postStream  = TW_postStream;
exports.postText    = TW_postText;
exports.schText     = TW_schText;
exports.getPost     = TW_getPost;
exports.delPost     = TW_delPost;
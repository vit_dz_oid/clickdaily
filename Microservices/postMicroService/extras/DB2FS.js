/**
 * Created by Sundeep on 1/10/2016.
 */

var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');

var model = require('../Model')["post"];

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "db2fs"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocuments = function(callback){
    model.find({},function(err,document){
        if(err){
            return callback(err);
        }
        if(!document) {
            if (!document) {
                document = new model();
                document._id = id;
            }
        }
        callback(null,document);
    });
};

getDocuments(function(err,docs){
    if(err){
        console.log("Error Getting Documents",err);
        return errorLog.log("Error Getting Documents",err);
    }
    console.log("Converting a total of locations : ",docs.length);
    docs.forEach(function(d){
        console.log("Starting Location : ", d._id );
        console.log("Creating Directory Location : ", d._id );
        var dirPath = path.normalize(path.join(__dirname + '/../pics/' + d._id));
        mkdirp(dirPath, function (err) {
            if(err){
                console.error("Error Creating Directory",err,dirPath,d._id);
                return errorLog.log("Error Creating Directory",err,dirPath,d._id);
            }
            console.log("Created Directory : ", d._id );
            console.log("Converting ["+ d.posts.length +"] Posts for locations : ", d._id);
            d.posts.forEach(function(e,i){
                var fileName = e._id.toString()+".png";
                console.log("Writing post [" + i + "] with id [",fileName, "] for location [" + d._id + "]");
                if(e.post_data && e.post_data.stream ){
                    console.log("Writing Picture for Post [" + fileName + "]");
                    fs.writeFile(dirPath + "/" + fileName, e.post_data.stream,{encoding:"base64"},function(err){
                        if(err){
                            console.error("Error Writing Picture",err,dirPath,d._id, fileName);
                            return errorLog.log("Error Writing Picture",err,dirPath,d._id, fileName);
                        }
                        console.log("Wrote Picture for Post [" + fileName + "]");
                    });
                }
            })
        });
    })
});
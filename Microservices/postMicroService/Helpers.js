/**
 * Created by Sundeep on 6/12/2015.
 */

var moduleString    = "post";
var streamLib       = require('stream');
var path            = require('path');
var fs              = require('fs');
var scheduler       = require('node-schedule');
var moment          = require('moment');
var redis           = require('redis');
var redisRStream    = require('redis-rstream'); // factory
var mkdirp          = require('mkdirp');

var client          = redis.createClient(null, null, {detect_buffers: true});

var model = require('./Model')[moduleString];

var fbPost = require('./fbPoster');
var twPost = require('./twPoster');
var itPost = require('./itPoster');

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib   = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var dbconfig = require("./dbconfig");

var mysql      = require('mysql');
var pool       = mysql.createPool(dbconfig.dbConfig);

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document) {
            if (!document) {
                document = new model();
                document._id = id;
            }
        }
        callback(null,document);
    });
};

var restart = function(){
    model.find({},function(err,documents){
        var now = parseInt(moment().format('x'));
        documents.forEach(function(document){
            document = document.toObject();
            var locId = document.id;
            document.posts.forEach(function(post){
                if(post.network==1 && post.approved==1){
                    if(post.post_time) {
                        var post_time = parseInt(moment(post.post_time).format("x"))
                        if ((post_time - now) > 0) {
                            schedule(locId, 1, post.type, post.post_data, function () {
                                console.log("Scheduled following ", JSON.stringify(arguments));
                            });
                        }
                    }
                }
            });
        });


    });
};

restart();

function create(locId,network,type,data,done){
    if(network==1){
        return done(new Error("Twitter Shut Down"),null,network,data);
    }
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err,null,network);
        }

        //var data = {};
        var post = {
            network         : network,
            type            : type,
            post_data       : data,
            postingUserType : data.postingUserType,
            creatingUserID  : data.creatingUserID,
            create_time     : new Date(),
            post_time       : new Date()
        };
        switch (type) {
            case 0 :  //Link

                break;
            case 1 : //Text
                if(network==0){
                    fbPost.postText(locId,data.text,function(err,body){
                        if((err)||(body.error)){
                            errorLog.log('Error Posting Fb postText',err,body);
                            return done({err:err,body:body},null,network);
                        }
                        post.network_data = {
                            post_id : body.id
                        };
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function(err){
                            if (err) {
                                return errorLog.log("Document save Error",err)
                            }
                            done(null,body,network,post.post_data);
                        });
                    });
                } else{
                    var postData = {status : data.text};
                    if (data.inReplyto){
                        postData.in_reply_to_status_id = data.inReplyto;
                    }
                    twPost.postText(locId,postData,function(err,body) {
                        if((err)||((body)&&((body.error)||(body.errors)))){
                            errorLog.log('Error Posting TW postText',err,body);
                            return done({err:err,body:body},null,network);
                        }
                        post.network_data = {
                            post_id: body.id,
                            pic_id: (body && body.entities && body.entities.media && body.entities.media.length ? body.entities.media
                                .filter(function (d) {
                                    return d.type == "photo";
                                })
                                .map(function (d) {
                                    return d.id_str
                                }) : false)
                        };
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function (err) {
                            if (err) {
                                return errorLog.log("Document save Error", err)
                            }
                            done(null, body,network,post.post_data);
                        });
                    });
                }
                break;
            case 2 : //Attachment Stream
                if(network==0){
                    fbPost.postStream(locId,data.text,data.stream,function(err,body){
                        if((err)||(body.error)){
                            errorLog.log('Error Posting Fb postStream',err,body);
                            return done({err:err,body:body},null,network);
                        }
                        post.network_data = {
                            post_id : body.post_id,
                            pic_id : body.id
                        };
                        post.post_data.stream = body.id;
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function(err){
                            if (err) {
                                return errorLog.log("Document save Error",err)
                            }
                            done(null,body,network,post.post_data);
                        });
                    });
                } else{
                    twPost.postStream(locId,data.text,data.stream,function(err,body){
                        if((err)||(body.error)){
                            errorLog.log('Error Posting TW postStream',err,body);
                            return done({err:err,body:body},null,network);
                        }
                        post.network_data = {
                            post_id : body.id,
                            pic_id : (body && body.entities && body.entities.media && body.entities.media.length?body.entities.media
                                .filter(function (d) {
                                    return d.type == "photo";
                                })
                                .map(function (d) {
                                    return d.id_str
                                }):false)
                        };
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function(err){
                            if (err) {
                                return errorLog.log("Document save Error",err)
                            }
                            done(null,body,network,post.post_data);
                        });
                    });
                }
                break;
            case 3 : //Attachment Object

                break;
        }


    });
}

function schedule(locId,network,type,data,done){

    var timestamp = moment(data.dateTime).format("X");
    if(parseInt(timestamp)-parseInt(moment().format("X"))<600){
        return done(new Error("time less tha 10 minutes"),null,network,data);
    }
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err,null,network);
        }

        //var data = {};
        var post = {
            approvingUser   : data.approvingUser,
            postingUserType : data.postingUserType,
            creatingUserID  : data.creatingUserID,
            network         : network,
            type            : type,
            post_data       : data,
            create_time     : new Date(),
            post_time       : data.dateTime
        };

        switch (type) {
            case 0 :  //Link

                break;
            case 1 : //Text
                if(network==0){
                    fbPost.schText(locId,timestamp,data.text,function(err,body){
                        if(err){
                            errorLog.log('Error Posting Fb postStream',err);
                            return done(err,null,network);
                        }
                        if(body.error){
                            errorLog.log('Error Posting Fb postStream',body);
                            return done(body.error,null,network);
                        }
                        post.network_data = {
                            post_id : body.id
                        };
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function(err){
                            if (err) {
                                return errorLog.log("Document save Error",err)
                            }
                            done(null,body,network,post.post_data);
                        });
                    });
                }
                else if(network==1){
                    return done(new Error("Twitter Shut Down"),null,network,data);
                    var newPost = document.posts.create(post);
                    var cb = function(document,newPostId) {
                        return function(err,body){
                            document.posts.id(newPostId).network_data.post_id = body.id;
                            document.markModified('posts');
                            document.save(function (err) {
                                if (err) {
                                    return errorLog.log("Document save Error", err)
                                }
                            });
                        };
                    }(document,newPost.id);
                    var dateTime = new Date(data.dateTime);
                    var postData = {status : data.text};
                    if (data.inReplyto){
                        postData.in_reply_to_status_id = data.inReplyto;
                    }
                    var method = twPost.schText.bind(null,locId,postData,cb);
                    var j = scheduler.scheduleJob(newPost.id,dateTime,method);
                    post.post_data.id = newPost.id;
                    document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err) {
                        if (err) {
                            return errorLog.log("Document save Error", err)
                        }
                        done(null,null,network,post.post_data);
                    });
                }
                else {
                    newPost = document.posts.create(post);
                    post.post_data.id = newPost.id;
                    document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err) {
                        if (err) {
                            return errorLog.log("Document save Error", err)
                        }
                        done(null, null,network,post.post_data);
                    });
                }
                break;
            case 2 : //Attachment Stream
                if(network==0){
                    fbPost.schStream(locId,timestamp,data.text,data.stream,function(err,body){
                        if((err)||(body.error)){
                            errorLog.log('Error Posting Fb postStream',err,body);
                            return done(err,null,network,post.post_data);
                        }
                        post.network_data = {
                            post_id : body.post_id,
                            pic_id : body.id,
                            pic_url : body.pic_url
                        };
                        post.post_data.stream = body.id;
                        var newPost = document.posts.create(post);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function(err){
                            if (err) {
                                return errorLog.log("Document save Error",err)
                            }
                            done(null,body,network,post.post_data);
                        });
                    });
                }
                else if (network==1){
                    return done(new Error("Twitter Shut Down"),null,network,data);
                    var attachmentStream = redisRStream(client, "TW"+data.stream);

                    var bufs = [];
                    attachmentStream.on('data',function(chunck){
                        bufs.push(chunck);
                    });
                    attachmentStream.on('end',function() {
                        //console.log('buffer',buff);
                        var attachment = Buffer.concat(bufs);
                        post.post_data.stream = "true";
                        client.del("TW" + data.stream,function (err, res) {
                            console.log("Deleted TW Schedule Stream [TW" , data.stream,"] : ",err,res);
                        });
                        newPost = document.posts.create(post);

                        var filePath= __dirname+"/pics/"+locId+"/"+newPost.id + ".png";
                        mkdirp(__dirname+"/pics/"+locId+"/", function (err) {
                            if(err){
                                console.log("Error Creating directory", err);
                                return errorLog.log("Error Creating directory", err);
                            }
                            fs.writeFile(filePath,attachment.toString('base64'),{"encoding":"base64"},function(err){
                                if(err){
                                    console.log("Error writing file", err);
                                    return errorLog.log("Error writing file", err);
                                }
                                console.log("Wrote Picture");
                            });
                        });
                        cb = function(document,newPostId) {
                            return function(err,body){
                                document.posts.id(newPostId).network_data.post_id = body.id;
                                document.posts.id(newPostId).network_data.pic_id =
                                    (((body)&&(body.entities)&&(body.entities.media)&&(body.entities.media.length))?
                                        (body.entities.media
                                                .filter(function(d){return (d.type=="photo")})
                                                .map(function(d){return d.id_str})
                                        )
                                        :false);
                                document.markModified('posts');
                                document.save(function (err) {
                                    console.log(err);
                                });
                            };
                        }(document,newPost.id);
                        var dateTime = new Date(data.dateTime);
                        var method = twPost.postStream.bind(null,locId,data.text,{path:filePath},cb);
                        var j = scheduler.scheduleJob(newPost.id,dateTime,method);
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function (err) {
                            if (err) {
                                return errorLog.log("Document save Error", err)
                            }
                            done(null, null, network, post.post_data);
                        });
                    });
                }
                else {
                    attachmentStream = redisRStream(client, "IT"+post.post_data.stream);
                    bufs = [];
                    attachmentStream.on('data',function(chunck){
                        bufs.push(chunck);
                    });
                    attachmentStream.on('end',function(){
                        //console.log('buffer',buff);
                        client.del("IT" + post.post_data.stream,function (err, res) {
                            console.log("Deleted IT Schedule Stream [IT" , post.post_data.stream,"] : ",err,res);
                        });
                        var attachment = Buffer.concat(bufs);
                        post.post_data.stream = "true";
                        newPost = document.posts.create(post);
                        var filePath= __dirname+"/pics/"+locId+"/"+newPost.id + ".png";
                        mkdirp(__dirname+"/pics/"+locId+"/", function (err) {
                            if(err){
                                console.log("Error Creating directory", err);
                                return errorLog.log("Error Creating directory", err);
                            }
                            fs.writeFile(filePath,attachment.toString('base64'),{"encoding":"base64"},function(err){
                                if(err){
                                    console.log("Error writing file", err);
                                    return errorLog.log("Error writing file", err);
                                }
                                console.log("Wrote Picture");
                            });
                        });
                        post.post_data.id = newPost.id;
                        document.posts.push(newPost);
                        document.markModified('posts');
                        document.save(function (err) {
                            if (err) {
                                return errorLog.log("Document save Error", err)
                            }
                            done(null, null,network,post.post_data);
                        });
                    });
                }
                break;
            case 3 : //For Approval with stream
                if(network==0) {
                    attachmentStream = redisRStream(client, "FB" + post.post_data.stream);

                } else if(network==1){
                    attachmentStream = redisRStream(client, "TW" + post.post_data.stream);

                } else {
                    attachmentStream = redisRStream(client, "IT" + post.post_data.stream);

                }
                bufs = [];
                attachmentStream.on('data',function(chunck){
                    bufs.push(chunck);
                });
                attachmentStream.on('end',function(){
                    //console.log('buffer',buff);
                    var attachment = Buffer.concat(bufs);
                    if(network==0) {
                        var keyName = "FB" + post.post_data.stream;
                    } else if(network==1) {
                        var keyName = "TW" + post.post_data.stream;
                    } else {
                        var keyName = "IT" + post.post_data.stream;
                    }
                    client.del(keyName,function (err, res) {
                        console.log("Deleted FB Schedule Stream [" ,keyName,"] : ",err,res);
                    });
                    post.post_data.stream = "true";
                    post.needApproval = 1;
                    post.approved = 2;
                    newPost = document.posts.create(post);
                    var filePath= __dirname+"/pics/"+locId+"/"+newPost.id + ".png";
                    mkdirp(__dirname+"/pics/"+locId+"/", function (err) {
                        if(err){
                            console.log("Error Creating directory", err);
                            return errorLog.log("Error Creating directory", err);
                        }
                        fs.writeFile(filePath,attachment.toString('base64'),{"encoding":"base64"},function(err){
                            if(err){
                                console.log("Error writing file", err);
                                return errorLog.log("Error writing file", err);
                            }
                            console.log("Wrote Picture");
                        });
                    });
                    post.post_data.id = newPost.id;
                    document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err) {
                        if (err) {
                            return errorLog.log("Document save Error", err)
                        }
                        done(null, null,network,post.post_data);
                    });
                });
                break;
            case 4 : //For Approval without stream
                post.needApproval = 1;
                post.approved = 2;
                newPost = document.posts.create(post);
                post.post_data.id = newPost.id;
                document.posts.push(newPost);
                document.markModified('posts');
                document.save(function (err) {
                    if (err) {
                        return errorLog.log("Document save Error", err)
                    }
                    done(null, null,network,post.post_data);
                });

                break;
        }
    });
}

function delPost(locId,post_id,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post  = document.posts.id(post_id);
        if(!post){
            return done ("No Post");
        }
        var networkPostId = post.network_data.post_id;
        if(post.approved==2){
            if(document.posts.id(post_id)){
                document.posts.id(post_id).remove();
                document.markModified('posts');

            }
            document.save(function (err) {
                console.log(err);
                return  done(null,post_id);
            });
            return;
        }
        switch(post.network){
            case 0 :
                fbPost.delPost(locId,networkPostId,function(err,res){
                    if(err){
                        return done (err);
                    }
                    if(document.posts.id(post_id)){
                        document.posts.id(post_id).remove();
                        document.markModified('posts');

                    }
                    document.save(function (err) {
                        console.log(err);
                        return  done(null,post_id);
                    });

                });
                break;
            case 1 :
                if(!networkPostId){
                    var old = scheduler.scheduledJobs[post_id];
                    if(old){
                        old.cancel();
                    }
                    if(document.posts.id(post_id)){
                        document.posts.id(post_id).remove();
                        document.markModified('posts');

                    }
                    return document.save(function (err) {
                        console.log(err);
                        return  done(null,post_id);
                    });
                }
                twPost.delPost(locId,networkPostId,function(err,res){
                    if(err){
                        return done (err);
                    }
                    if(document.posts.id(post_id)){
                        document.posts.id(post_id).remove();
                        document.markModified('posts');

                    }
                    document.save(function (err) {
                        console.log(err);
                        return  done(null,post_id);
                    });
                });
                break;
            case 2 :
                if(document.posts.id(post_id)){
                    document.posts.id(post_id).remove();
                    document.markModified('posts');

                }
                document.save(function (err) {
                    console.log(err);
                    return  done(null,post_id);
                });
                break;
        }
    });
}

function reschedulePost(locId,post_id,dateTime,done){
    var timestamp = moment(dateTime).format("X");
    if(parseInt(timestamp)-parseInt(moment().format("X"))<600){
        return done(new Error("time less tha 10 minutes"));
    }
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post  = document.posts.id(post_id);
        if(!post){
            return done ("No Post");
        }
        var networkPostId = post.network_data.post_id;
        switch(post.network){
            case 0 :
                console.log("Reschduling");
                fbPost.rechdulePost(locId,timestamp,networkPostId,function(err,res){
                    console.log("Reschduled",arguments);
                    if(err){
                        console.log("Reschduled",arguments);
                        return done (err);
                    }
                    post.post_time = dateTime;
                    document.markModified('posts');
                    document.save(function (err) {
                        console.log(err);
                        return  done(null,post_id);
                    });

                });
                break;
            case 1 :
                if(!networkPostId){
                    var old = scheduler.scheduledJobs[post_id];
                    if(old){
                        old.cancel();
                    }
                    post.post_time = timestamp;
                    document.markModified('posts');
                    var data = post.post_data.toObject();
                    data.dateTime = timestamp;
                    schedule(locId,post.network,post.type,data,function(err,body,network,post_data){
                        if(err){
                            errorLog.log("post Duplicate Error", err);
                            return done(err);
                        }
                        document.save(function (err) {
                            console.log(err);
                            return  done(null,post_id);
                        });
                    });
                }
        }
    });
}

function getPosts(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var posts = document.posts.toObject();
        posts = posts.map(function(d){
           if((d.network)&&(d.post_data.stream)){
               d.post_data.stream = false;
           }
            return d;
        });
        done(null,posts);
    });
}

function getPost(locId,post_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post  = document.posts.id(post_id);
        if(!post){
            return done ("No Post");
        }
        var networkPostId = post.network_data.post_id;
        switch(post.network){
            case 0 :
                if(!networkPostId){
                    if(post.post_data.stream){
                        var filePath = path.join(__dirname + "/pics/" + locId + "/" + post_id + ".png" );
                        return fs.readFile(filePath,{encoding:"base64"},function(err,data){
                            if(err) {
                                console.log("Picture Fetch Error", err);
                                errorLog.log("Picture Fetch Error", err);
                                return done(err);
                            }
                            return client.set("FB"+post_id,data,function(err){
                                post.post_data.stream = "FB"+post_id;
                                return  done(null,post);
                            });
                        });

                    }
                    return  done(null,post);
                }
                fbPost.getPost(locId,networkPostId,function(err,data){
                    if(err){
                        errorLog.log('Error getting FB post',err);
                        return done(err);
                    }
                    return  done(null,data);
                });
                break;
            case 1 :
                if(!networkPostId){
                    if(post.post_data.stream){
                        var filePath = path.join(__dirname + "/pics/" + locId + "/" + post_id + ".png" );
                        return fs.readFile(filePath,{encoding:"base64"},function(err,data){
                            if(err) {
                                console.log("Picture Fetch Error", err);
                                errorLog.log("Picture Fetch Error", err);
                                return done(err);
                            }
                            return client.set("TW"+post_id,data,function(err){
                                post.post_data.stream = "TW"+post_id;
                                return  done(null,post);
                            });
                        });
                    }
                    return  done(null,post);
                }
                twPost.getPost(locId,networkPostId,function(err,data){
                    if(err){
                        errorLog.log('Error getting TW post',err);
                        return done(err);
                    }
                    return  done(null,data);
                });
                break;
            case 2 :
                if(post.post_data.stream){
                    var filePath = path.join(__dirname + "/pics/" + locId + "/" + post_id + ".png" );
                    return fs.readFile(filePath,{encoding:"base64"},function(err,data){
                        if(err) {
                            console.log("Picture Fetch Error", err);
                            errorLog.log("Picture Fetch Error", err);
                            return done(err);
                        }
                        return client.set("IT"+post_id,data ,function(err){
                            post.post_data.stream = "IT"+post_id;
                            return done(null,post);
                        });
                    });
                }
                return done(null,post);
        }
    });
}

function getHistoryPost(locId,post_id,network,done) {
    switch(network){
        case 0 :
            fbPost.getPost(locId,post_id,function(err,data){
                if(err){
                    errorLog.log('Error getting FB post',err);
                    return done(err);
                }
                return  done(null,data);
            });
            break;
        case 1 :
            twPost.getPost(locId,post_id,function(err,data){
                if(err){
                    errorLog.log('Error getting TW post',err);
                    return done(err);
                }
                return  done(null,data);
            });
            break;
        case 2 :
            itPost.getPost(locId,post_id,function(err,data){
                if(err){
                    errorLog.log('Error getting TW post',err);
                    return done(err);
                }
                data.clickdailyhistory = true;
                data.network = 2;
                return  done(null,data);
            });
            break;
    }
}

function getPostPicture(locId,post_id,done) {
    getDocument(locId, function (err,document) {
        console.log("Getting Picture : ",locId,post_id);
        if (err) {
            console.log("Document Fetch Error", err);
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post  = document.posts.id(post_id);
        if(!post){
            console.log("No Post", err);
            return done ("No Post",null);
        }
        if(post.post_data.stream){
            //var binString = new Buffer(post.post_data.stream,'base64').toString('utf8');
            var filePath = path.join(__dirname + "/pics/" + locId + "/" + post_id + ".png" );
            return fs.readFile(filePath,{encoding:"base64"},function(err,data){
                if(err) {
                    console.log("Picture Fetch Error", err);
                    errorLog.log("Picture Fetch Error", err);
                    return done(err);
                }
                return client.set("pic_"+post_id,data,function(err){
                    if(err) {
                        console.log("Picture Fetch Error", err);
                        errorLog.log("Picture Fetch Error", err);
                        return done(err);
                    }
                    post.post_data.stream = "pic_"+post_id;
                    return  done(null,"pic_"+post_id);
                });
            });

        } else {
            return done("No Picture", null);
        }
    });
}

function duplicatePost(locId,id,datetime,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post  = document.posts.id(id);
        if(!post){
            return done ("No Post");
        }
        var data = post.post_data.toObject();
        data.dateTime = datetime;
        schedule(locId,post.network,post.type,data,function(err,body,network,post_data){
            if(err){
                errorLog.log("post Duplicate Error", err);
                return done(err);
            }
            done(null,body,network,post_data);
        });
    });
}

function getPendingApprovalPosts(locId,userType,done){
    console.log("getPendingApprovalPosts : " ,locId,userType);
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        console.log("getPendingApprovalPosts  getDocument");
        var now = moment();
        var posts = document.posts.toObject();
        posts = posts.filter(function(d){
            return d.needApproval==1
                && d.postingUserType==userType
                && d.approved==2
                && now.isBefore(d.post_time);
        });

        posts = posts.map(function(d){
            if(d.type ==2 || d.type== 3) {
                d.post_data.stream = "https://mobile.clickdaily.com/social/posts/getPostPicture/" + d._id + "?locId=" + locId;
            } else {
                d.post_data.stream = false;
            }
            return d;
        });

        console.log("getPendingApprovalPosts  posts");
        done(null,posts);
    });
}

function getNeedingApprovalPosts(locId,userType,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var now = moment();
        var posts = document.posts.toObject();
        posts = posts.filter(function(d){
            return d.needApproval == 1
                && d.postingUserType != userType
                && d.approved == 2
                && d.approvingUser.indexOf(userType) != -1
                && now.isBefore(d.post_time);
        });

        posts = posts.map(function(d){
            if(d.type ==2 || d.type== 3) {
                d.post_data.stream = "https://mobile.clickdaily.com/social/posts/getPostPicture/" + d._id + "?locId=" + locId;
            } else {
                d.post_data.stream = false;
            }
            return d;
        });

        done(null,posts);
    });
}

function getScheduledPosts(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var now = moment();
        var posts = document.posts.toObject();
        posts = posts.filter(function(d){
            return (d.needApproval == 0
                || d.approved == 1) && now.isBefore(d.post_time);
        });

        posts = posts.map(function(d){
            if(d.type ==2 || d.type== 3) {
                if(d.network==0){
                    d.post_data.stream = d.network_data.pic_url;
                }else {
                    d.post_data.stream = "https://mobile.clickdaily.com/social/posts/getPostPicture/" + d._id + "?locId=" + locId;
                }
            } else {
                d.post_data.stream = false;
            }
            return d;
        });

        done(null,posts);
    });
}

function getRejectedPosts(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var now = moment();
        var posts = document.posts.toObject();
        posts = posts.filter(function(d){
            return d.needApproval == 1
                && ( d.approved == 0
                    || (d.approved == 2 && now.isAfter(d.post_time)));
        });

        posts = posts.map(function(d){
            if(d.type ==2 || d.type== 3) {
                d.post_data.stream = "https://mobile.clickdaily.com/social/posts/getPostPicture/" + d._id + "?locId=" + locId;
            } else {
                d.post_data.stream = false;
            }
            return d;
        });

        done(null,posts);
    });
}

function getHistoryPosts(locId,done) {
    var funcName = "getHistoryPosts";
    var query = "SELECT `id`, `text`, `created_at`, `network` " +
        " FROM `youposts` WHERE `loc_id`=?;";
    console.log("Started Fetching History");
    pool.getConnection(function(err, connection) {
        if (err) {
            errorLog.log(funcName + " getConnection", err);
            return done(err,null);
        }
        console.log("Connection Established");
        connection.query(query, [locId], function(err, results) {
            console.log("Results for History : ",results);
            connection.release();
            console.log(this.sql);
            if(err){
                return done(err);
            }
            done(null,results);
        });
    });
}

function getExpiredPosts(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var now = moment();
        var posts = document.posts.toObject();
        posts = posts.filter(function(d){
            return ( d.approved == 1 && now.isAfter(d.post_time));
        });

        posts = posts.map(function(d){
            if (d.type ==2 || d.type== 3) {
                d.post_data.stream = "https://mobile.clickdaily.com/posts/getPostPicture/" + d._id + "?locId=" + locId;
            } else {
                d.post_data.stream = false;
            }
            return d;
        });

        done(null,posts);
    });
}

function approvePost(locId,postId,userId,done){
    console.log("Got approvePost");
    console.log("locId : ",locId);
    console.log("postId : ",postId);
    console.log("userId : ",userId);
    getDocument(locId, function (err,document) {
        if (err) {
            console.log("Document Fetch Error", err);
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post = document.posts.id(postId);
        if (!post) {
            console.log("No Post");
            return done(new Error("No Post"));
        }
        if(post.approvingUser.indexOf(userId)==-1){
            console.log("Forbidden");
            return done(new Error("Forbidden"));
        }
        var now = moment();
        if(now.isAfter(post.post_time)){
            console.log("Rejected");
            return done(new Error("Rejected"));
        }
        post.approved = 1;
        switch(post.network){
            case 0 :
                document.markModified('posts');
                switch (post.type){
                    case 4 :
                        var timestamp = post.post_time.getTime()/1000;
                        console.log("case 4 Scheduling to FB text");
                        fbPost.schText(locId,timestamp,post.post_data.text,function(err,body){
                            if(err){
                                console.log('case 4 Error Posting Fb schText',err);
                                errorLog.log('Error Posting Fb schText',err);
                                return done(err,null,network);
                            }
                            if(body.error){
                                console.log('case 4 Error Posting Fb schText',body);
                                errorLog.log('Error Posting Fb schText',body);
                                return done(body.error,null,post.network);
                            }
                            post.network_data = {
                                post_id : body.id
                            };
                            document.markModified('posts');
                            document.save(function(err){
                                if (err) {
                                    console.log("Document save Error",err)
                                    return errorLog.log("Document save Error",err)
                                }
                                done(null,postId);
                            });
                        });
                        break;
                    case 3 :
                        console.log("case 3 Scheduling to FB Stream");
                        var timestamp = post.post_time.getTime()/1000;
                        var upStream = fs.createReadStream(__dirname+"/pics/"+locId+"/"+postId + ".png");
                        fbPost.schStream(locId,timestamp,post.post_data.text,upStream,function(err,body){
                            if((err)||(body.error)){
                                console.log('case 3 Error Posting Fb postStream',err,body);
                                errorLog.log('Error Posting Fb postStream',err,body);
                                return done(err,null,post.network,post.post_data);
                            }
                            post.network_data = {
                                post_id : body.post_id,
                                pic_id : body.id
                            };
                            post.post_data.stream = body.id;
                            document.markModified('posts');
                            document.save(function(err){
                                if (err) {
                                    console.log("case 3 Document save Error",err)
                                    return errorLog.log("Document save Error",err)
                                }
                                done(null,postId);
                            });
                        });

                }
                break;
            case 1 :
                document.markModified('posts');
                document.save(function (err) {
                    console.log("case 1 error", err);
                    return  done(null,postId);
                });
                break;
            case 2 :
                document.markModified('posts');
                document.save(function (err) {
                    console.log("case 2 error", err);
                    return  done(null,postId);
                });
                break;
        }
    });
}

function rejectPost(locId,postId,userId,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post = document.posts.id(postId);
        if (!post) {
            return done(new Error("No Post"));
        }
        if(post.approvingUser.indexOf(userId)==-1){
            return done(new Error("Forbidden"));
        }
        post.approved = 0;
        document.markModified('posts');
        document.save(function (err) {
            console.log(err);
            return  done(null,postId);
        });
    });
}

function editPostText(locId,postId,text,done){
    console.log("Got editPostText");
    console.log("locId : ",locId);
    console.log("postId : ",postId);
    getDocument(locId, function (err,document) {
        if (err) {
            console.log("Document Fetch Error", err);
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }

        var post = document.posts.id(postId);
        if (!post) {
            console.log("No Post");
            return done(new Error("No Post"));
        }
        if(post.approved==2) {
            var logData = moment().format("YYYY/MM/DD hh:mm:ss a ZZ") + ", " + locId + ", " + postId + ", "
                + post.post_data.text + ", " + text + ", SAME";
            fs.appendFile("edit.csv", logData, {encoding:"utf8"}, function(err){
                if(err){
                    console.log("Could not Log");
                    return done(new Error("Could not Log"));
                }
                post.post_data.text = text;
                document.markModified('posts');
                document.save(function (err) {
                    return  done(null,postId);
                });
            });

        } else{
            console.log("Not Allowed");
            return done(new Error("Not Allowed"));
        }
    });
}


exports.create                      = create;
exports.schedule                    = schedule;
exports.getPosts                    = getPosts;
exports.getPost                     = getPost;
exports.getHistoryPost              = getHistoryPost;
exports.getPendingApprovalPosts     = getPendingApprovalPosts;
exports.getNeedingApprovalPosts     = getNeedingApprovalPosts;
exports.getExpiredPosts             = getExpiredPosts;
exports.getScheduledPosts           = getScheduledPosts;
exports.getRejectedPosts            = getRejectedPosts;
exports.getHistoryPosts             = getHistoryPosts;
exports.delPost                     = delPost;
exports.duplicatePost               = duplicatePost;
exports.reschedulePost              = reschedulePost;
exports.approvePost                 = approvePost;
exports.rejectPost                  = rejectPost;
exports.getPostPicture              = getPostPicture;
exports.editPostText                = editPostText;
exports.moduleString                = moduleString;
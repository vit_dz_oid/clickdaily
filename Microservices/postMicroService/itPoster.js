/**
 * Created by Sundeep on 6/29/2015.
 */

var model = require('../itMicroService/Model').it;
var path  = require('path');
var fs    = require('fs');

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : "itPost"
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function IT_getPost(locId,post_id,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.helper.media.info({
            media_id: post_id,
            complete: function(data,pagination) {
                done(null,data);
            },
            error : function(e, value, caller){
                errorLog.log("Instagram Error", e,{value :value,caller : caller});
                return done(e);
            }
        });
    });
}

exports.getPost     = IT_getPost;
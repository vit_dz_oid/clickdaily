/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');

var Instagram = require('./it-mod');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var IT_Schema = mongoose.Schema({
    _id                     : String,   //Location Id
    token                   : String,
    userId                  : String,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        feeds               : Date,
        friends             : Date,
        followers           : Date
    },
    paging                  : {
        feeds               : Object,
        messages            : Object,
        friends             : Object,
        followers           : Object
    },
    cache:{
        user_info           : {
            username        : String
            , profile_pic   : String
            , full_name     : String
            , bio           : String
            , website       : String
        }
        , feeds             : Object
        , messages          : Object
        , friends           : Object
        , followers         : Object
    }
},schemaOptions);

IT_Schema.virtual('config').get(function () {
    return {
        client_id : "3c1eaf6ee5404799a7db2fba3d5f93f2"
        , client_secret : "c58478dc4e5f4e3eae3cb7ef290dad2a"
        , accessToken : this.token
        , callBackUrl : "https://merchant.clickdaily.com/social/Instagram"
    };
});

IT_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
});

IT_Schema.virtual('helper').get(function () {
    var It = new Instagram();
    It.set('client_id', this.config.client_id);
    It.set('client_secret', this.config.client_secret);
    It.set('callback_url', this.config.callBackUrl);
    It.set('redirect_uri', this.config.callBackUrl);
    It.set('maxSockets', 10);
    if(this.config.accessToken){
        It.set('access_token', this.config.accessToken);
    }
    return It;
});


exports.it = mongoose.model('IT', IT_Schema);
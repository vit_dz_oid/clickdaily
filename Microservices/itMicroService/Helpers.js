/**
 * Created by Sundeep on 6/12/2015.
 */

var moduleString = "it";

var path    = require('path');
var request = require('request');
var qs      = require('qs');

var vars = require('./vars');

var model = require('./Model')[moduleString];

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var docCache = {};

var getDocument = function(id,callback){
    if(docCache[id]){
        return callback(null,docCache[id]);
    }
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
         if(docCache[id]){
            return callback(null,document);
        }
        docCache[id] = document;
        callback(null,document);
    });
};

function IT_create(locId,requrl,code,done){
    model.findById(locId,function(err,document){
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        if(!document){
            if(!document){
                document = new model();
                document._id = locId;
            }
        }
        if (!code) {
            var url = document.helper.oauth.authorization_url({
                scope: 'basic'// comments likes relationships' // use a space when specifying a scope; it will be encoded into a plus
                //, display: 'touch'
            });
            return done(null,'redirect',url);
        }

        var a = qs.parse(requrl.split("?")[1]);

        var post_data =  {
            "client_id": document.config.client_id,
            "client_secret": document.config.client_secret,
            "grant_type": "authorization_code",
            "redirect_uri": document.config.callBackUrl,
            "code": a.code
        };

        var queryStr = qs.stringify(post_data);
        var url = "https://api.instagram.com/oauth/access_token";//?"+queryStr;
        request.post({url:url, form:post_data},function (err,body,res) {
            if(err) {
                errorLog.log("ask_for_access_token", err,data,arguments);
                return done(err);
            }
            var params;
            try {
                params = JSON.parse(res);
            } catch (e) {
                errorLog.log("JSON.parse", err,data,arguments);
                return done(2);
            }
            document.token = params['access_token'];
            document.cache.user_info = params['user'];
            document.userId = params['user'].id;
            document.updateTime.user_info = new Date();
            document.save(function(err,doc){
                if(err) errorLog.log("error : ",err);
                done(null,"",(!!(document.token)?1:0));
            });
        });
        /*document.helper.oauth.ask_for_access_token({
            request: {url:requrl},
            complete:function(params){
                console.log(arguments);
                document.token = params['access_token'];
                document.cache.user_info = params['user'];
                document.userId = params['user'].id;
                document.updateTime.user_info = new Date();
                document.save(function(err,doc){
                    if(err) errorLog.log("error : ",err);
                    done(null,"",(!!(document.token)?1:0));
                });
            },
            error:  function (err,data) {
                errorLog.log("ask_for_access_token", err,data,arguments);
                return done(err);
            }
        });*/
    });
}

function IT_status(locId,done){
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        if(!document){
            if(!document){
                document = new model();
                document._id = locId;
            }
        }
        done(null,!!(document.token));
    });
}

function IT_delNetwork(locId,done){
    delete docCache[locId];
    model.findByIdAndRemove(locId,function(err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        return done(null);
    });
}

function IT_getUserInfo(locId,mobile,done){
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        var flag = 0;
        if ((document.cache.user_info) && (document.cache.user_info.profile_pic)) {

            var diff = (new Date()) - document.updateTime.user_info;
            if (diff > vars.updateTimes.IT.user_info) {
                flag = 1;
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.user_info.toObject());
                }
            } else {
                done(null,document.cache.user_info.toObject());
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.users.info({
                user_id : "self",
                complete: function (data, pagination) {
                    if (!document.cache.user_info) {
                        document.cache.user_info = {};
                    }
                    document.updateTime.user_info = new Date();
                    document.cache.user_info.profile_pic = data.profile_picture;
                    done(null,document.cache.user_info.toObject());
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log("IT_user_info document.saves",err);
                        }
                        console.log("Doc Saved");
                    });
                },
                error : function(e, value, caller){
                    errorLog.log("Instagram Error", e,{value :value,caller : caller});
                    return done(e);
                }
            });
        }
    });
}

function IT_getMeFeed(locId,mobile,done) {
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        var flag = 0;
        if ((document.cache.feeds) && (document.cache.feeds.me)) {

            var diff = (new Date()) - document.updateTime.feeds;
            if (diff > vars.updateTimes.IT.feed) {
                flag = 1;
            }
            var next = (document.paging.feeds.me ? (document.paging.feeds.me.next_max_id ? document.paging.feeds.me.next_max_id : null) : null);
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.feeds.me, next);
                }
            } else {
                done(null,document.cache.feeds.me, next);
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.users.self({
                complete: function (data, pagination) {
                    if (!document.cache.feeds) {
                        document.cache.feeds = {};
                    }
                    if (!document.paging.feeds) {
                        document.paging.feeds = {};
                    }
                    document.updateTime.feeds = new Date();
                    document.cache.feeds.me = data;
                    document.paging.feeds.me = pagination;
                    done(null,document.cache.feeds.me, document.paging.feeds.me.next_max_id);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log("IT_getMeFeed document.saves",err);
                        }
                        console.log("Doc Saved");
                    });
                },
                error : function(e, value, caller){
                    errorLog.log("Instagram Error", e,{value :value,caller : caller});
                    return done(e);
                }
            });
        }
    });
}

function IT_getYouFeed(locId,mobile,done) {
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        var flag = 0;
        if ((document.cache.feeds) && (document.cache.feeds.you)) {

            var diff = (new Date()) - document.updateTime.feeds;
            if (diff > vars.updateTimes.IT.feed) {
                flag = 1;
            }
            var next = (document.paging.feeds.you ? (document.paging.feeds.you.next_max_id ? document.paging.feeds.you.next_max_id : null) : null);
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.feeds.you, next);
                }
            } else {
                done(null,document.cache.feeds.you, next);
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.users.recent({
                user_id: document.cache.user_info.id,
                complete: function (data, pagination) {
                    if (!document.cache.feeds) {
                        document.cache.feeds = {};
                    }
                    if (!document.paging.feeds) {
                        document.paging.feeds = {};
                    }
                    document.updateTime.feeds = new Date();
                    document.cache.feeds.you = data;
                    document.paging.feeds.you = pagination;
                    done(null,document.cache.feeds.you, document.paging.feeds.you.next_max_id);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log("IT_getYouFeed document.saves",err);
                        }
                        console.log("Doc Saved");
                    });
                },
                error : function(e, value, caller){
                    errorLog.log("Instagram Error", e,{value :value,caller : caller});
                    return done(e);
                }
            });
        }
    });
}

function IT_loadMoreFeed(locId,next,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.helper.users.self({
            max_id: next,
            complete: function (data, pagination) {
                done(null,data, pagination.next_max_id);
            },
            error : function(e, value, caller){
                errorLog.log("Instagram Error", e,{value :value,caller : caller});
                return done(e);
            }
        });
    });
}

function IT_likePost(locId,id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.helper.media.like({
            media_id: id,
            complete: function(data,pagination) {
                done(null,true,id);
            },
            error : function(e, value, caller){
                errorLog.log("Instagram Like Error", e, {value:value, caller : caller, id:id});
                return done(e);
            }
        });
    });
}

function IT_commentPost(locId,id,text,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.helper.media.comment({
            media_id: id,
            text : text,
            complete: function(data,pagination) {
                done(null,true,id);
            },
            error : function(e, value, caller){
                errorLog.log("Instagram Error", e,{value :value,caller : caller});
                return done(e,false,id);
            }
        });
    });
}

function IT_getFriends(locId,mobile,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        var userId = document.userId;
        if (document.cache.friends) {
            var diff = (new Date()) - document.updateTime.friends;
            if (diff > vars.updateTimes.IT.friends) {
                flag = 1;
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.friends,document.paging.friends);
                }
            } else {
                done(null,document.cache.friends,document.paging.friends);
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.users.follows({
                user_id: userId,
                complete: function (data, pagination) {
                    if (!document.cache.friends) {
                        document.cache.friends = {};
                    }
                    if (!document.paging.friends) {
                        document.paging.friends = {};
                    }
                    document.updateTime.friends = new Date();
                    document.cache.friends = data;
                    document.paging.friends = pagination;
                    done(null,document.cache.friends, document.paging.friends);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log("IT_getFriends document.saves",err);
                        }
                        console.log("Doc Saved");
                    });
                },
                error : function(e, value, caller){
                    errorLog.log("Instagram Error", e,{value :value,caller : caller});
                    return done(e);
                }
            });
        }
    });
}

function IT_getFollwers(locId,mobile,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        var userId = document.userId;
        if (document.cache.followers) {
            var diff = (new Date()) - document.updateTime.followers;
            if (diff > vars.updateTimes.IT.followers) {
                flag = 1;
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.followers, document.paging.followers);
                }
            } else {
                done(null,document.cache.followers, document.paging.followers);
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.users.followed_by({
                user_id: userId,
                complete: function (data, pagination) {
                    if (!document.cache.followers) {
                        document.cache.followers = {};
                    }
                    if (!document.paging.followers) {
                        document.paging.followers = {};
                    }
                    document.updateTime.followers = new Date();
                    document.cache.followers = data;
                    document.paging.followers = pagination;
                    done(null,document.cache.followers, document.paging.followers);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log("IT_getFollwers document.saves",err);
                        }
                        console.log("Doc Saved");
                    });
                },
                error : function(e, value, caller){
                    errorLog.log("Instagram Error", e,{value :value,caller : caller});
                    return done(e);
                }
            });
        }
    });
}

function IT_Follow(userId,locId,done){
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var userId = document.userId;
        document.helper.users.relationship({
            user_id: userId,
            action: 'follow',
            complete: function (data, pagination) {
                done(null);
            },
            error : function(e, value, caller){
                errorLog.log("Instagram Error", e,{value :value,caller : caller});
                return done(e);
            }
        });
    });

}

function IT_clearFeed(userId,locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        if(!document.cache.feeds){
            document.cache.feeds = {};
        }
        document.updateTime.feeds = new Date(1427988307103);
        document.cache.feeds.me = [];
        document.markModified('cache.feeds');
        done(document.cache.feeds.me);
        document.save(function (err, doc) {
            if(err){
                return errorLog.log("IT_clearFeed document.saves",err);
            }
            console.log("Doc Saved");
        });
    });
}

exports.status           = IT_status;
exports.create           = IT_create;
exports.delNetwork       = IT_delNetwork;
exports.getUserInfo      = IT_getUserInfo;
exports.getMeFeed        = IT_getMeFeed;
exports.clearFeed        = IT_clearFeed;
exports.likePost         = IT_likePost;
exports.commentPost      = IT_commentPost;
exports.loadMoreFeed     = IT_loadMoreFeed;
exports.getFriends       = IT_getFriends;
exports.getFollwers      = IT_getFollwers;
exports.getYouFeed       = IT_getYouFeed;
exports.moduleString     = moduleString;
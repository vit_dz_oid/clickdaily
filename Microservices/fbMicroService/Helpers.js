/**
 * Created by Sundeep on 6/12/2015.
 */

var moduleString = "fb";
var path = require('path');
var vars = require('./vars');
var ProfilePicsCache = {};
var model = require('./Model')[moduleString];
var _ = require('lodash');
var request = require('request');

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocument = function(id,callback){
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        callback(null,document);
    });
};

function FB_status(locId,done){
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        if(!document){
            if(!document){
                document = new model();
                document._id = locId;
            }
        }
        done(null,!!(document.token));
    });
}

function FB_create(locId,code,error,done){
    if(error) {
        var err;
        if(error.error_description) {
            err= new Error(error.error_description);

        } else {
            err= new Error("error in fb auth callback");
        }
        errorLog.log("Fb create",err,error);
        return done(err);
    } else if (!code) {
        err = new Error("no code in fb auth callback");
        errorLog.log("Fb create",err);
        return done(err);
    }
    model.findById(locId,function(err,document){
        if(err){
            errorLog.log("Document Fetch Error",err);
            return done(err);
        }
        if(!document){
                document = new model();
                document._id = locId;
        }
        document.helper.api('oauth/access_token', {
            client_id: "752078144824356",
            client_secret: "e628edfb0049e06a8276520d19356285",
            redirect_uri: "https://merchant.clickdaily.com/Social/FBAuth",
            code: code
        }, function (res) {
            if(!res || res.error) {
                console.log(!res ? 'error occurred' : JSON.stringify(res.error));
                return;
            }

            var accessToken = res.access_token;
            var expires = res.expires ? res.expires : 0;

            document.token = accessToken;
            document.expires = expires;
            //vars.users[locId].social.FB[0].helper.setAccessToken(accessToken);
            document.save(function(err,doc){
                if(err) errorLog.log("error : ",err);
                done(null,(!!(document.token)?1:0));
            });
        });


    });
}

function FB_delNetwork(locId,done){
    model.findByIdAndRemove(locId,function(err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        return done(null);
    });
}

function FB_getUserInfo(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if (document.updateTime.user_info) {
            done(null,document.cache.user_info, document.cache.page_info, document.selected_page);
            var diff = (new Date()) - document.updateTime.user_info;
            if (diff > vars.updateTimes.FB.userInfo) {
                flag = 1;
            }
        } else {
            flag = 1;
        }
        if (flag) {
            document.helper.api('me', {
                    fields: 'accounts.limit(100){id,' +
                    'name,access_token,picture.type(large).redirect(0)},picture.type(large).redirect(0),id,name'
                }
                , function (result) {
                    if (!result || result.error) {
                        errorLog.log("batch[me, me/picture, me/accounts]",!result ? new Error('error occurred') : JSON.stringify(result.error));
                        return done(!result ? new Error('error occurred') : JSON.stringify(result.error));
                    }

                    var res0 = result;
                    var res1 = result.picture;
                    var res2 = result.accounts;
                    console.log('pages from fb ', res2);
                    if (res0.error) {
                        //errLogger.otherErr(__filename,res0.error,{position:'me userInfo',user:document});
                        errorLog.log('me', res0.error);
                    } else {
                        document.cache.user_info.id = res0.id;
                        document.cache.user_info.name = res0.name;
                        document.updateTime.user_info = new Date();
                    }

                    if (res1.error) {
                        //errLogger.otherErr(__filename,res1.error,{position:'me userPic',user:document});
                        errorLog.log('me/picture', res1.error);
                    } else {
                        document.cache.user_info.profile_pic = res1.data.url;
                        ProfilePicsCache[res0.id] = res1.data.url;
                    }

                    if (res2.error) {
                        //errLogger.otherErr(__filename,res2.error,{position:'me userAccounts',user:document});
                        errorLog.log('me/accounts', res2.error);
                    } else {
                        res2.data.forEach(function (d) {
                            d._id = d.id;
                            d.profile_pic = d.picture.data.url;
                            document.AddPages(d);
                        });
                        document.updateTime.pages_info = new Date();
                    }
                    document.markModified('pages');
                    document.markModified('cache.page_info');
                    document.markModified('page_access_token');
                    document.save(function (err) {
                        if (err) {
                            errorLog.log("Document save Error",err);
                        }
                        done(null,document.cache.user_info, document.cache.page_info, document.selected_page);
                    });
                }
            );
        }
    });
}

function FB_getPageFeed(locId,mobile,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if (id) {
            var flag = 0;
            var diff;
            if ((document.selected_page_feed) && (document.updateTime.feeds)) {
                diff = (new Date()) - document.updateTime.feeds[id];
                if (diff > vars.updateTimes.FB.feed) {
                    flag = 1;
                }
                if (mobile) {
                    if (!flag) {
                        return done(null,document.selected_page_info, document.selected_page_feed, next);
                    }
                } else {
                    var next;
                    if (document.selected_page_feed_paging.cursors) {
                        next = document.selected_page_feed_paging.cursors.after;
                    }
                    done(null,document.selected_page_info, document.selected_page_feed, next);
                }
            } else {
                flag = 1;
            }

            if (flag) {
                var access_token = document.selected_page_token;
                document.helper.api(id, {
                    fields: "feed.limit(25){from{id,name,picture.redirect(0).type(large)}," +
                    "id,likes.limit(25).summary(true){pic_large,name,id},actions,application,call_to_action,caption," +
                    "comments.limit(25).summary(true){created_time,id,from{id,name,picture.type(large)}," +
                    "user_likes,like_count,message,message_tags}," +
                    "created_time,description,feed_targeting,icon,is_hidden,link,message,message_tags,name," +
                    "object_id,picture,full_picture,attachments,place,privacy,properties,shares,source,status_type,story" +
                    ",story_tags,to,type,updated_time,with_tags}"
                    , access_token: access_token
                }, function (res) {
                    if (!res || res.error) {
                        errorLog.log(id + "/feed", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                        return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    }
                    res = res.feed;
                    if (!document.updateTime.feeds) {
                        document.updateTime.feeds = {};
                    }
                    if (!document.cache.feeds) {
                        document.cache.feeds = {};
                    }
                    if (!document.paging.feeds) {
                        document.paging.feeds = {};
                    }
                    document.updateTime.feeds[id] = new Date();

                    if (res) {
                        document.cache.feeds[id] = res.data;
                        document.paging.feeds[id] = res.paging;
                    } else {
                        document.cache.feeds[id] = [];
                    }
                    var next;
                    if (document.selected_page_feed_paging.cursors) {
                        next = document.selected_page_feed_paging.cursors.after;
                    }
                    done(null,document.cache.page_info.id(id), document.cache.feeds[id], next);
                    document.save(function (err, doc) {
                        if (err) {
                            return errorLog.log("Document save Error",err)
                        }
                    });
                });

            }
        }
    });
}

function FB_getPageYouFeed(locId,mobile,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if (id) {
            var access_token = document.selected_page_token;
            document.helper.api(id, {
                fields: "posts.limit(25){from{id,name,picture.redirect(0).type(large)}," +
                "id,likes.limit(25).summary(true){pic_large,name,id},actions,application,call_to_action,caption," +
                "comments.limit(25).summary(true){created_time,id,from{id,name,picture.type(large)}," +
                "user_likes,like_count,message,message_tags}," +
                "created_time,description,feed_targeting,icon,is_hidden,link,message,message_tags,name," +
                "object_id,picture,full_picture,attachments,place,privacy,properties,shares,source,status_type,story,story_tags,to," +
                "type,updated_time,with_tags}"
                , access_token: access_token
            }, function (res) {
                if (!res || res.error) {
                    errorLog.log(id + "/posts", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }

                //console.log(res.posts.data);
                if ((res) && (res.posts) && (res.posts.data)) {
                    return done(null,res.posts.data);
                }
                done(null,[]);
            });
        }
    });
}

function FB_getPromotablePosts(locId,mobile,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if (id) {
            var access_token = document.selected_page_token;
            document.helper.api(id, {
                fields: "promotable_posts{id,scheduled_publish_time,object_id,created_time}"
                , access_token: access_token
            }, function (res) {
                if (!res || res.error) {
                    errorLog.log(id + "/promotable_posts", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }
                done(null,res);
            });
        }
    });
}

function FB_loadMoreFeed(locId,next,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if (id) {
            var access_token = document.selected_page_token;
            document.helper.api(id, {
                fields: "feed.limit(25){from{id,name,picture.redirect(0).type(large)}," +
                "id,likes.limit(25).summary(true){pic_large,name,id},actions,application,call_to_action,caption," +
                "comments.limit(25).summary(true){created_time,id,from{id,name,picture.type(large)}," +
                "user_likes,like_count,message,message_tags}," +
                "created_time,description,feed_targeting,icon,is_hidden,link,message,message_tags,name," +
                "object_id,picture,full_picture,attachments,place,privacy,properties,shares,source,status_type,story,story_tags,to," +
                "type,updated_time,with_tags}"
                , access_token: access_token
                , after: next
            }, function (res) {
                if (!res || res.error) {
                    errorLog.log(id + "/feed More", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }
                res = res.feed;
                var next = (res.paging ? (res.paging.cursors ? (res.paging.cursors.after ? res.paging.cursors.after : "") : "") : "");
                done(null,res.data, next);
            });
        }
    });
}

/*
 function FB_getMeFeed(locId,done,mobile) {
 var flag = 0;
 var document = vars.users[locId];
 if((document.cache.feeds)&&(document.cache.feeds.me)){
 var diff = (new Date()) - document.updateTime.feeds.me;
 if(diff>1000*60*10){
 flag=1;
 }
 if(mobile){
 if(!flag){
 return done(document.cache.feeds.me);
 }
 }else{
 done(document.cache.feeds.me);
 }

 }else{
 flag = 1;
 }
 if(flag) {
 document.helper.api("me", {
 fields:"home.limit(25){from{id,name,picture.redirect(0).type(large)},id,likes.limit(25).summary(true),actions,application,call_to_action,caption,created_time,description,feed_targeting,icon,is_hidden,link,message,message_tags,name,object_id,picture,place,privacy,properties,shares,source,status_type,story,story_tags,to,type,updated_time,with_tags}"
 }
 ,function (res) {
 if (!res || res.error) {
 errorLog.log(me/home',!res ? 'error occurred' : JSON.stringify(res.error));
 return;
 }
 res = res.home;
 if (!document.updateTime.feeds) {
 document.updateTime.feeds = {};
 }
 if (!document.cache.feeds) {
 document.cache.feeds = {};
 }
 if (!document.paging.feeds) {
 document.paging.feeds = {};
 }
 document.updateTime.feeds.me = new Date();
 document.cache.feeds.me = res.data;
 document.paging.feeds.me = res.paging;
 document.save(function (err, doc) {

 if (err) return errorLog.log("me/home",err);
 console.log("Doc Saved");
 });
 done(document.cache.feeds.me);

 });
 }

 }
 */

function FB_getProfilePic( locId,id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        if(ProfilePicsCache[id]){
            return done(null,id,ProfilePicsCache[id].url);
        }

        document.helper.api(id + "/picture", { redirect: 0, type:'large', access_token: access_token}, function (res) {
            if(!res || res.error) {
                errorLog.log(id + "/picture", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
            }
            if(!ProfilePicsCache[id]){
                ProfilePicsCache[id]={};
            }
            ProfilePicsCache[id].url = res.data.url;
            ProfilePicsCache[id].lastUpdate = new Date();
            done(null,id,ProfilePicsCache[id].url);
        });
    });
}

function FB_addPage(locId,id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        document.SelectPage(id);
        document.save(function (err) {
            if (err) {
                errorLog.log("Document save Error",err)
            }
            FB_getPageFeed(locId, false, done);
        });
    });
}

function  FB_unselectedPages(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var unselectedPages = {};
        document.cache.page_info.toObject().forEach(function (d) {
            unselectedPages[d.id] = d;
        });
        delete unselectedPages[document.selected_page];
        unselectedPages = _.values(unselectedPages);
        done(null,document.cache.user_info, unselectedPages)
    });
}

function FB_likePost(locId,id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err,null,id);
        }
        var access_token = document.selected_page_token;
        document.helper.api(id + "/likes", 'post', {access_token: access_token}, function (res) {
            if (!res || res.error) {
               errorLog.log('post.likes', !res ?  new Error('error occurred'): JSON.stringify(res.error));
                return done(!res ?  new Error('error occurred'): JSON.stringify(res.error),null,id);
            } else {
                console.log("Result of like", res);
                done(null, res,id);
            }
        });
    });
}

function FB_commentPost(locId,id,text,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err,id);
        }
        var access_token = document.selected_page_token;
        document.helper.api(id + "/comments", 'post',{message:text, access_token: access_token},function (res) {
            if(!res || res.error) {
                errorLog.log(id + "/comments", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)),id);
            } else {
                console.log("Result of comment",res);
                done(null,id)
            }
        });
    });
}

function FB_getPost(locId,id,done) {
    if(typeof(id)=="string"){
        getDocument(locId, function (err,document) {
            if (err) {
                errorLog.log("Document Fetch Error", err);
                return done(err);
            }
            var access_token = document.selected_page_token;
            document.helper.api(id ,'GET',{access_token: access_token},function (res) {
                if(!res || res.error) {
                    errorLog.log(id + "/comments", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }else{
                    console.log("Post : ",res);
                    done(null,res)
                }
            });
        });

    } else {
        errorLog.log(id + " No post", new Error("No post"));
        done(new Error("No post"));
    }

}

function FB_delPost(locId,id,done) {
    if(id){
        getDocument(locId, function (err,document) {
            if (err) {
                errorLog.log("Document Fetch Error", err);
                return done(err);
            }
            var access_token = document.selected_page_token;
            document.helper.api(id ,'delete',{access_token: access_token},function (res) {
                if(!res || res.error) {
                    errorLog.log(id + "/delete", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }else{
                    console.log("Post : ",res);
                    done(null,res)
                }
            });
        });
    }else{
        errorLog.log(id + " No post", new Error("No post"));
        done(new Error("No post"));
    }
}

function FB_post(locId,text,stream,oldId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var newPost = {
            network: 0
            , create_time: new Date()
            , post_time: new Date()
            , num_likes: 0
            , num_comments: 0
            , num_reached: 0
            , data: {
                text: text,
                stream: (stream ? true : false)
            },
            locId: locId
            , oldId: oldId
        };
        var fburl;
        var formData;
        if (!stream.path)
            stream.path = true;
        if (!stream.mode)
            stream.mode = true;
        if ((typeof(stream) == "object") && (stream.readable)) {
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/photos?access_token='
                + access_token;
            formData = {
                'source': stream,
                message: text
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    newPost.post_id = body.id;
                    //document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err) {
                        if (err){
                            errorLog.log("Document save Error",err)
                        }
                        done(null, body);
                    });
                }
            });
        } else if (typeof(stream) == "string") {
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/feed?access_token='
                + access_token;
            formData = {
                message: text,
                object_attachment: stream
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    newPost.post_id = body.id;
                    //document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err, doc) {
                        if (err){
                            errorLog.log("Document save Error",err)
                        }
                        done(null, body);
                    });

                }
            });
        } else {
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/feed?access_token='
                + access_token;
            formData = {
                message: text
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    newPost.post_id = body.id;
                    //document.posts.push(newPost);
                    document.markModified('posts');
                    document.save(function (err, doc) {
                        if (err){
                            errorLog.log("Document save Error",err)
                        }
                        done(null, body);
                    });
                }
            });
        }
    });
}

function FB_postLink(locId,url,name,caption,picture,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var newPost = {
            network          : 0
            , create_time    : new Date()
            , post_time      : new Date()
            , num_likes      : 0
            , num_comments   : 0
            , num_reached    : 0
            , data           : {
                text         : name,
                stream       : false
            },
            locId            : locId
        };
        console.log(access_token);
        document.helper.api(document.selected_page+"/feed", 'post',{
            link   : url
            , caption : caption
            , name : name
            , access_token: access_token
            , picture : picture
        },function (res) {
            console.log('Upload successful! Server responded with:', res);
            newPost.post_id = res.id;
            //document.posts.push(newPost);
            document.markModified('posts');
            document.save(function(err){
                if (err){
                    errorLog.log("Document save Error",err)
                }
                done(null, res);
            });
        });
    });
}

function FB_postSchedule(locId,text,stream,timestamp,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var fburl;
        var formData;
        if ((typeof(stream) == "object") && (stream.readable)) {
            if (!stream.path)
                stream.path = true;
            if (!stream.mode)
                stream.mode = true;
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/photos?access_token='
                + access_token;
            formData = {
                'source': stream,
                message: text,
                scheduled_publish_time: timestamp,
                published: 'false'
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    console.log('Image Id:', body.id);
                    FB_getPromotablePosts( locId, function (list) {
                        if ((list)
                            && (list.promotable_posts)
                            && (list.promotable_posts.data)
                            && (list.promotable_posts.data.length)
                        ) {
                            list.promotable_posts.data.forEach(function (d) {
                                if ((d.object_id == body.id) && (d.scheduled_publish_time == timestamp)) {
                                    return body.id = d.id;
                                }
                            });
                        }

                        done(null,body);
                    });
                }
            });
        } else if (typeof(stream) == "string") {
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/feed?access_token='
                + access_token;
            formData = {
                message: text,
                scheduled_publish_time: timestamp,
                published: 'false',
                object_attachment: stream
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    done(null,body)
                }
            });

        } else {
            fburl = 'https://graph.facebook.com/v2.2/'
                + document.selected_page
                + '/feed?access_token='
                + access_token;
            formData = {
                message: text,
                scheduled_publish_time: timestamp,
                published: 'false'
            };
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    done(null,body)
                }
            });

        }
    });
}

function FB_postReSchedule(locId,text,stream,timestamp,post_id,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        var formData = {
            scheduled_publish_time: timestamp
        };
        if (text) {
            formData.message = text;
        }
        var fburl = 'https://graph.facebook.com/v2.2/'
            + post_id
            + '?access_token='
            + access_token;
        if ((typeof(stream) == "object") && (stream.readable)) {
            if (!stream.path)
                stream.path = true;
            if (!stream.mode)
                stream.mode = true;
            formData.source = stream;
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    console.log('Image Id:', body.id);
                    done(null,body)
                }
            });
        } else if (typeof(stream) == "string") {
            formData.object_attachment = stream;
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                } else {
                    body = JSON.parse(body);
                    done(null,body)
                }
            });

        } else {
            request.post({url: fburl, formData: formData}, function (err, res, body) {
                if (err) {
                    errorLog.log('post.me/feed', err);
                    return done(err, body);
                }
                body = JSON.parse(body);
                done(null,body)
            });

        }
    });
}

function FB_postLinkSchedule(locId,url,name,caption,picture,timestamp,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var access_token = document.selected_page_token;
        console.log(access_token);
        document.helper.api(document.selected_page+"/feed", 'post',{
            link   : url
            , caption : caption
            , name : name
            , access_token: access_token
            , picture : picture
            , scheduled_publish_time : timestamp
            , published : 'false'
        },function (res) {
            console.log('Upload successful! Server responded with:', res);
            done(null,res);
        });
    });
}

function FB_getInsights(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if (document.updateTime.insights) {
            var diff = (new Date()) - document.updateTime.insights;
            if (diff > vars.updateTimes.FB.insights) {
                flag = 1;
            } else {
                done(null,document.cache.insights);
            }
        } else {
            flag = 1;
        }
        if (flag) {
            var access_token = document.selected_page_token;
            console.log(access_token);
            var id = document.selected_page;
            document.helper.api(id+"/insights", {
                access_token: access_token
            },function (res) {
                if(!res || res.error) {
                    errorLog.log(id + "/insights", new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                    return done( new Error(!res ? 'error occurred' : JSON.stringify(res.error)));
                }
                document.updateTime.insights = new Date();
                document.cache.insights = res;
                document.save(function(err){
                    if (err) {
                        errorLog.log("Document save Error",err);
                    }
                    done(null,res);
                });
            });
        }

    });
}

var FB_GetPostViews = function(locId,done){
    FB_getInsights(locId,function(err,result){
        if(err) {
            errorLog.log("/FB_GetPostViews", err);
            return done(err);
        }
        if((result)&&(result.data)){
            var daily = {};
            var weekly = {};
            var monthly = {};
            var rest = {};
            result.data.forEach(function (d, i) {
                if (d.title.substr(0, 5) == 'Daily') {
                    daily[d.title] = d;
                } else if (d.title.substr(0, 6) == 'Weekly') {
                    weekly[d.title] = d;
                } else if (d.title.substr(0, 7) == '28 Days') {
                    monthly[d.title] = d;
                } else {
                    rest[d.title] = d;
                }
            });

            var Impressions        = {
                weekly              : {
                    val             : weekly["Weekly Total Impressions of your posts"].values[2].value,
                    ch              : weekly["Weekly Total Impressions of your posts"].values[2].value
                                        - weekly["Weekly Total Impressions of your posts"].values[0].value
                },
                daily               : {
                    val             : daily["Daily Total Impressions of your posts"].values[2].value,
                    ch              : daily["Daily Total Impressions of your posts"].values[2].value
                                        - daily["Daily Total Impressions of your posts"].values[0].value
                },
                monthly             : {
                    val             : monthly["28 Days Total Impressions of your posts"].values[2].value,
                    ch              : monthly["28 Days Total Impressions of your posts"].values[2].value
                                        - monthly["28 Days Total Impressions of your posts"].values[0].value
                }
            };
            done(null,Impressions)
        } else {
            var error = new Error("Not data in insights");
            errorLog.log("/FB_GetPostViews", error);
            return done(error);
        }

    });
};

var FB_GetPostInteractions = function(locId,done){
    FB_getInsights(locId,function(err,result){
        if(err) {
            errorLog.log("/FB_GetPostInteractions", err);
            return done(err);
        }
        if((result)&&(result.data)){
            var daily = {};
            var weekly = {};
            var monthly = {};
            var rest = {};
            result.data.forEach(function (d, i) {
                if (d.title.substr(0, 5) == 'Daily') {
                    daily[d.title] = d;
                } else if (d.title.substr(0, 6) == 'Weekly') {
                    weekly[d.title] = d;
                } else if (d.title.substr(0, 7) == '28 Days') {
                    monthly[d.title] = d;
                } else {
                    rest[d.title] = d;
                }
            });

            var Reach        = {
                weekly              : {
                    val             : weekly["Weekly Reach of page posts"].values[2].value,
                    ch              : weekly["Weekly Reach of page posts"].values[2].value
                    - weekly["Weekly Reach of page posts"].values[0].value
                },
                daily               : {
                    val             : daily["Daily Reach of page posts"].values[2].value,
                    ch              : daily["Daily Reach of page posts"].values[2].value
                    - daily["Daily Reach of page posts"].values[0].value
                },
                monthly             : {
                    val             : monthly["28 Days Reach of page posts"].values[2].value,
                    ch              : monthly["28 Days Reach of page posts"].values[2].value
                    - monthly["28 Days Reach of page posts"].values[0].value
                }
            };
            done(null,Reach)
        } else {
            var error = new Error("Not data in insights");
            errorLog.log("/FB_GetPostInteractions", error);
            return done(error);
        }

    });
};

var FB_GetUniquePostViews = function(locId,done){
    FB_getInsights(locId,function(err,result){
        if(err) {
            errorLog.log("/FB_GetUniquePostViews", err);
            return done(err);
        }
        if((result)&&(result.data)){
            var daily = {};
            var weekly = {};
            var monthly = {};
            var rest = {};
            result.data.forEach(function (d, i) {
                if (d.title.substr(0, 5) == 'Daily') {
                    daily[d.title] = d;
                } else if (d.title.substr(0, 6) == 'Weekly') {
                    weekly[d.title] = d;
                } else if (d.title.substr(0, 7) == '28 Days') {
                    monthly[d.title] = d;
                } else {
                    rest[d.title] = d;
                }
            });

            var OrganicImpressions  = {
                weekly              : {
                    val             : weekly["Weekly Organic impressions"].values[2].value,
                    ch              : weekly["Weekly Organic impressions"].values[2].value
                    - weekly["Weekly Organic impressions"].values[0].value
                },
                daily               : {
                    val             : daily["Daily Organic impressions"].values[2].value,
                    ch              : daily["Daily Organic impressions"].values[2].value
                    - daily["Daily Organic impressions"].values[0].value
                },
                monthly             : {
                    val             : monthly["28 Days Organic impressions"].values[2].value,
                    ch              : monthly["28 Days Organic impressions"].values[2].value
                    - monthly["28 Days Organic impressions"].values[0].value
                }
            };

            done(null,OrganicImpressions)
        } else {
            var error = new Error("Not data in insights");
            errorLog.log("/FB_GetUniquePostViews", error);
            return done(error);
        }

    });
};

var FB_GetUniquePostInteractions = function(locId,done){
    FB_getInsights(locId,function(err,result){
        if(err) {
            errorLog.log("/FB_GetUniquePostInteractions", err);
            return done(err);
        }
        if((result)&&(result.data)){
            var daily = {};
            var weekly = {};
            var monthly = {};
            var rest = {};
            result.data.forEach(function (d, i) {
                if (d.title.substr(0, 5) == 'Daily') {
                    daily[d.title] = d;
                } else if (d.title.substr(0, 6) == 'Weekly') {
                    weekly[d.title] = d;
                } else if (d.title.substr(0, 7) == '28 Days') {
                    monthly[d.title] = d;
                } else {
                    rest[d.title] = d;
                }
            });
            var OrganicReach  = {
                weekly              : {
                    val             : weekly["Weekly Organic Reach"].values[2].value,
                    ch              : weekly["Weekly Organic Reach"].values[2].value
                    - weekly["Weekly Organic Reach"].values[0].value
                },
                daily               : {
                    val             : daily["Daily Organic Reach"].values[2].value,
                    ch              : daily["Daily Organic Reach"].values[2].value
                    - daily["Daily Organic Reach"].values[0].value
                },
                monthly             : {
                    val             : monthly["28 Days Organic Reach"].values[2].value,
                    ch              : monthly["28 Days Organic Reach"].values[2].value
                    - monthly["28 Days Organic Reach"].values[0].value
                }
            };

            done(null,OrganicReach);
        } else {
            var error = new Error("Not data in insights");
            errorLog.log("/FB_GetUniquePostInteractions", error);
            return done(error);
        }

    });
};

var FB_clearFeed = function(locId,done) {
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log("Document Fetch Error", err);
            return done(err);
        }
        var id = document.selected_page;
        if(!document.updateTime.feeds){
            document.updateTime.feeds = {};
        }
        if(!document.cache.feeds){
            document.cache.feeds = {};
        }
        if(!document.paging.feeds){
            document.paging.feeds = {};
        }
        document.updateTime.feeds[id] = new Date(1427988307103);
        document.cache.feeds[id] = [];
        document.markModified('cache.feeds');
        document.save(function (err, doc) {
            if(err){
                errorLog.log(id + " FB_clearFeed",err);
                return done(err);
            }
            done(null,document.cache.page_info.id(id).profile_pic, document.cache.feeds[id]);
        });
    });
};

exports.clearFeed                   = FB_clearFeed;
exports.status                      = FB_status;
exports.create                      = FB_create;
exports.delNetwork                  = FB_delNetwork;
exports.getUserInfo                 = FB_getUserInfo;
exports.getPageFeed                 = FB_getPageFeed;
exports.getProfilePic               = FB_getProfilePic;
exports.unselectedPages             = FB_unselectedPages;
exports.likePost                    = FB_likePost;
exports.commentPost                 = FB_commentPost;
exports.post                        = FB_post;
exports.getPost                     = FB_getPost;
exports.delPost                     = FB_delPost;
exports.postSchedule                = FB_postSchedule;
exports.postReSchedule              = FB_postReSchedule;
exports.postLinkSchedule            = FB_postLinkSchedule;
exports.postLink                    = FB_postLink;
exports.addPage                     = FB_addPage;
exports.loadMoreFeed                = FB_loadMoreFeed;
exports.getInsights                 = FB_getInsights;
exports.getPageYouFeed              = FB_getPageYouFeed;
exports.getPostViews                = FB_GetPostViews;
exports.getPostInteractions         = FB_GetPostInteractions;
exports.getUniquePostViews          = FB_GetUniquePostViews;
exports.getUniquePostInteractions   = FB_GetUniquePostInteractions;
exports.moduleString                = moduleString;
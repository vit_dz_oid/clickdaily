/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');

var Facebook = require('./fb-mod');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var FB_PageInfoSchema = mongoose.Schema({
    name            : String
    , _id           : String
    , profile_pic   : String
},schemaOptions);

FB_PageInfoSchema.virtual('link').get(function () {
    return "https://www.facebook.com/"+this.id;
});

var FB_Schema = mongoose.Schema({
    _id                     : String,  // loc Id
    userId                  : String,  // FB user Id
    token                   : String,
    expires                 : String,
    private_user_info       : String,
    pages                   : [String],
    page_access_token       : [String],
    selected_page           : {type : String, default : ""},
    feeds                   : Object, //{me:feed,pageId:feed}
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        pages_info          : Date,
        feeds               : Object, //{me:Date,pageId:Date}
        insights            : Date
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache                   : {
        user_info           : {
            name            : String
            , id            : String
            , profile_pic   : String
        }
        , page_info         : [FB_PageInfoSchema]
        , feeds             : Object
        , messages          : Object
        , friends           : Object
        , insights          : Object
    }
},schemaOptions);

FB_Schema.virtual('helper').get(function () {
    var fb = new Facebook();
    fb.setAccessToken(this.token);
    return fb;
});

FB_Schema.virtual('selected_page_info').get(function () {
    return this.cache.page_info.id(this.selected_page);
});

FB_Schema.virtual('selected_page_feed').get(function () {
    if(this.cache.feeds) return this.cache.feeds[this.selected_page];
    return [];
});

FB_Schema.virtual('selected_page_feed_paging').get(function () {
    if(this.paging.feeds[this.selected_page]) return this.paging.feeds[this.selected_page];
    return {};
});

FB_Schema.virtual('selected_page_pic').get(function () {
    if (this.cache.page_info.id(this.selected_page))  return this.cache.page_info.id(this.selected_page).profile_pic;
    return "";
});

FB_Schema.virtual('selected_page_token').get(function () {
    var index = this.pages.indexOf(this.selected_page);
    if((index>-1)&&(index<this.page_access_token.length))  return this.page_access_token[index];
    return "";
});

FB_Schema.methods.AddPages = function(page_info) {
    if(this.pages.indexOf(page_info._id)==-1){
        this.cache.page_info.push(page_info);
        this.pages.push(page_info._id);
        this.page_access_token.push(page_info.access_token);

    }
};

FB_Schema.methods.SelectPage = function(page_id) {
    if(this.pages.indexOf(page_id)!==-1){
        this.selected_page = page_id;
    }
};

FB_Schema.methods.AddPagePic = function(id,url) {
    this.cache.page_info.id(id).profile_pic=url;
};

exports.fb = mongoose.model('FB', FB_Schema);
/**
 * Created by Sundeep on 6/18/2015.
 */

var moduleString = "status";
var async   = require("async");
var path    = require("path");
var FbModel = require('../fbMicroService/Model').fb;
var TwModel = require('../twMicroService/Model').tw;
var FsModel = require('../fsMicroService/Model').fs;
var ItModel = require('../itMicroService/Model').it;

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocument = function(model,id,callback){
    switch (model){
        case 'fb' : FbModel.findById(id).exec(callback);
            break;
        case 'tw' : TwModel.findById(id).exec(callback);
            break;
        case 'fs' : FsModel.findById(id).exec(callback);
            break;
        case 'it' : ItModel.findById(id).exec(callback);
            break;
    }
};

function status(locId,info,done){
    var statusCaller = function(model){
        return function(callback){
            getDocument(model,locId, function (err,document) {
                if(err){
                    errorLog.log("Document Fetch Error",err);
                    return done(err);
                }
                if(!document){
                    if(!document){
                        switch (model){
                            case 'fb' :
                                document = new FbModel();
                                break;
                            case 'tw' :
                                document = new TwModel();
                                break;
                            case 'fs' :
                                document = new FsModel();
                                break;
                            case 'it' :
                                document = new ItModel();
                                break;
                        }
                        document._id = locId;
                    }
                }
                document.save(function (err) {
                    if(err) {
                        errorLog.log("Document creation Error",err);
                    }
                });
                callback(null,!!(document.token));
            });
        };
    };
    var cb = function(result){
        return done(null,locId,info,result);
    };
    async.parallel({
        fb: statusCaller('fb'),
        tw: statusCaller('tw'),
        it: statusCaller('it'),
        fs: statusCaller('fs')
    },
        function(err, results) {
            if(err){
                errorLog.log("Document Fetch Error",err);
                return done(err);
            }
            cb(results);
        }
    );

}

function update_feeds(locId,done){
    var statusCaller = function(model){
        return function(callback){
            getDocument(model,locId, function (err,document) {
                if(err){
                    errorLog.log("Document Fetch Error",err);
                    return done(err);
                }
                if(!document){
                    if(!document){
                        switch (model){
                            case 'fb' :
                                document = new FbModel();
                                break;
                            case 'tw' :
                                document = new TwModel();
                                break;
                            case 'fs' :
                                document = new FsModel();
                                break;
                            case 'it' :
                                document = new ItModel();
                                break;
                        }
                        document._id = locId;
                    }
                }
                document.save(function(err){
                    if(err) {
                        errorLog.log("Document creation Error",err);
                    }
                });
                callback(null,!!(document.token));
            });
        };
    };
    var cb = function(result){
        return done(null,locId,result);
    };
    async.parallel({
            fb: statusCaller('fb'),
            tw: statusCaller('tw'),
            it: statusCaller('it'),
            fs: statusCaller('fs')
        },
        function(err, results) {
            if(err){
                errorLog.log("Document Fetch Error",err);
                return done(err);
            }
            cb(results);
        }
    );

}

exports.status                  = status;
exports.update_feeds            = update_feeds;
exports.moduleString            = moduleString;
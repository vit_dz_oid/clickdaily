/**
 * Created by Sundeep on 6/12/2015.
 */

var mongoose = require('mongoose');

var Twitter = require('./tw-mod');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var TW_Schema = mongoose.Schema({
    _id                     : String,  // loc Id
    token                   : String,
    token_secret            : String,
    userId                  : String,
    request_token           : String,
    request_token_secret    : String,
    private_user_info       : Object,
    updateTime              : {
        messages            : Date,
        user_info           : Date,
        feeds               : Date,
        friends             : Date,
        followers           : Date,
        mentions            : Date,
        you                 : Date
    },
    paging                  : {
        feeds               : Object,
        messages            : Object
    },
    cache:{
        user_info           : {
            screen_name     : String
            , profile_pic   : String
            , name          : String
            , friends       : Number
            , followers     : Number
        }
        , feeds             : mongoose.Schema.Types.Mixed
        , messages          : Object
        , friends           : Object
        , followers         : Object
    }
},schemaOptions);

TW_Schema.virtual('config').get(function () {
    return {
        consumerKey : "RNoSRCk5LgqNjacdZD2zebDvf"
        , consumerSecret : "DRFXWbSDZpVxPc6e1TniNEI4TrxqC0TON94PKXWXbHnp6p0x0R"
        , accessToken : this.token
        , accessTokenSecret : this.token_secret
        , callBackUrl : "https://merchant.clickdaily.com/social/Twitter"
    };
});

TW_Schema.virtual('cache.user_info.id').get(function () {
    return this.userId;
});

TW_Schema.virtual('helper').get(function () {
    return new Twitter(this.config);
});
exports.tw = mongoose.model('TW', TW_Schema);
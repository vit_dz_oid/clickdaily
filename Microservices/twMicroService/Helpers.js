/**
 * Created by Sundeep on 6/12/2015.
 */

var moduleString = "tw";
var path = require('path');
var vars = require('./vars');

var model = require('./Model')[moduleString];

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var docCache = {};

var getDocument = function(id,callback){
    if(docCache[id]){
        return callback(null,docCache[id]);
    }
    model.findById(id,function(err,document){
        if(err){
            return callback(err);
        }
        if(!document){
            return callback(new Error("Document null"))
        }
        if((document)&&(!document.token)){
            return callback(new Error("Document does not have token"))
        }
        if(docCache[id]){
            return callback(null,document);
        }
        docCache[id] = document;
        callback(null,document);
    });
};

function TW_status(locId,done){
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if(err){
            errorLog.log(func_name +" | Document Fetch Error",err);
            return done(err);
        }
        if(!document){
            if(!document){
                document = new model();
                document._id = locId;
            }
        }
        done(null,!!(document.token));
    });
}

function TW_Create (locId,code,done){
    var func_name = arguments.callee.name;
    model.findById(locId,function(err,document) {
        if (err) {
            errorLog.log(func_name + " | Document Fetch Error", err);
            return done(err);
        }
        if (!document) {
            if (!document) {
                document = new model();
                document._id = locId;
            }
        }
        if ((typeof(document.request_token) === "undefined")|| (
                (typeof(code) === "undefined")&& (typeof(document.request_token) !== "undefined"))) {
            document.helper.oauth.getOAuthRequestToken(function (err, oauth_token, oauth_token_secret) {
                if (err) {
                    errorLog.log(func_name+" | Twitter Reply Error",err,arguments);
                    return done(err);
                }
                document.request_token = oauth_token;
                document.request_token_secret = oauth_token_secret;
                document.save(function (err, doc) {
                    if (err) errorLog.log(func_name+" | Document Save Error", err);
                        done(null, "redirect", oauth_token);
                });
            });
        } else if (typeof(code) !== "undefined") {
            document.helper.oauth.getOAuthAccessToken(
                document.request_token
                , document.request_token_secret
                , code
                , function (err, oauth_access_token, oauth_access_token_secret, results) {

                    if (err) {
                        errorLog.log(func_name+"OAuthAccessToken Error",err);
                        return done(err);
                    }
                    document.request_token = undefined;
                    document.requestTokensecret = undefined;
                    document.token = oauth_access_token;
                    document.token_secret = oauth_access_token_secret;

                    document.cache.user_info.screen_name = results.screen_name;
                    document.userId = results.user_id;
                    document.updateTime.user_info = new Date();
                    document.save(function (err, doc) {
                        if (err) errorLog.log(func_name+"Document Save Error",err);
                        done(null, "", "");
                    });
                }
            );
        }
    });
}

function TW_delNetwork(locId,done){
    var func_name = arguments.callee.name;
    delete docCache[locId];
    model.findByIdAndRemove(locId,function(err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        return done(null);
    });
}

function TW_getUserInfo(locId,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        if (document.cache.user_info.id) {
            var flag = 0;
            if (!document.cache.user_info.profile_pic) {
                flag = 1;
            }
            if (document.updateTime.user_info) {
                var diff = (new Date()) - document.updateTime.user_info;
                if (diff > vars.updateTimes.TW.userInfo) {
                    flag = 1;
                }
            } else {
                flag = 1;
            }
            if (flag) {
                document.helper.getUser({user_id: document.cache.user_info.id}
                    , function (err) {
                        errorLog.log(func_name+" | Twitter Reply Error",err,arguments);
                        return done(err);
                    },
                    function (res) {
                        if(!res ) {
                            errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                            return done(!res ? new Error('error occurred') : res.error);
                        }
                        try{
                            res = JSON.parse(res);
                        }catch(e){
                            errorLog.log(func_name+" | JSON parse error",e,res);
                            return done(e);
                        }

                        document.cache.user_info.screen_name = res.screen_name;
                        document.cache.user_info.name = res.name;
                        document.cache.user_info.friends = res.friends_count;
                        document.cache.user_info.followers = res.followers_count;
                        document.userId = res.id_str;
                        document.cache.user_info.profile_pic = res.profile_image_url_https;
                        document.private_user_info = res;
                        document.updateTime.user_info = new Date();
                        done(null,document.cache.user_info);
                        document.save(function (err, doc) {
                            if (err){
                                return errorLog.log(func_name+" | Document save Error",err)
                            }
                        });
                    });
            } else {
                done(null,document.cache.user_info);
            }
        }
    });
}

function TW_getMeFeed(locId,mobile,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if((document.cache.feeds)&&(document.cache.feeds.me)){

            var diff = (new Date()) - document.updateTime.feeds;
            if(diff>vars.updateTimes.TW.feed){
                flag = 1
            }
            if(mobile){
                if(!flag) {
                    return done(null,document.cache.feeds.me);
                }
            }else{
                done(null,document.cache.feeds.me);
            }
        } else {
            flag = 1
        }
        if(flag) {
            document.helper.getHomeTimeline({ count: '25'}
                ,function(err){
                    errorLog.log(func_name+" | Twitter Reply Error",err,arguments);
                    return done(err);
                },
                function(res){
                    if(!res ) {
                        errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                        return done(!res ? new Error('error occurred') : res.error);
                    }
                    if(!document.cache.feeds){
                        document.cache.feeds = {};
                    }
                    document.updateTime.feeds = new Date();
                    try {
                        document.cache.feeds.me = JSON.parse(res);
                    } catch(e) {
                        errorLog.log(func_name+" | JSON parse error",e,res);
                        return done(e);
                    }
                    done(null,document.cache.feeds.me);
                    document.save(function (err, doc) {
                        if (err){
                            return errorLog.log(func_name+" | Document save Error",err)
                        }
                    });
                }
            );
        }
    });
}

function TW_getMentionsFeed(locId,mobile,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if ((document.cache.feeds) && (document.cache.feeds.mentions)) {

            var diff = (new Date()) - document.updateTime.mentions;
            if (diff > vars.updateTimes.TW.feed) {
                flag = 1
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.feeds.mentions);
                }
            } else {
                done(null,document.cache.feeds.mentions);
            }
        } else {
            flag = 1
        }
        if (flag) {
            document.helper.getMentionsTimeline({count: '25',include_rts:'true'}, function (err) {
                    errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                    return done(err);
                },
                function (res) {
                    if(!res ) {
                        errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error, res,arguments);
                        return done(!res ? new Error('error occurred') : res.error);
                    }

                    if (!document.cache.feeds) {
                        document.cache.feeds = {};
                    }
                    document.updateTime.mentions = new Date();
                    try{
                        document.cache.feeds.mentions = JSON.parse(res);
                    } catch(e) {
                        errorLog.log(func_name+" | JSON parse error",e,res);
                        return done(e);
                    }
                    done(null,JSON.parse(res));
                    document.save(function (err, doc) {
                        if (err){
                            return errorLog.log(func_name+" | Document save Error",err)
                        }
                    });
                }
            );
        }
    });
}

function TW_getYouFeed(locId,mobile,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if ((document.cache.feeds) && (document.cache.feeds.you)) {
            var diff = (new Date()) - document.updateTime.you;
            if (diff > vars.updateTimes.TW.feed) {
                flag = 1
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.feeds.you);
                }
            } else {
                done(null,document.cache.feeds.you);
            }
        } else {
            flag = 1
        }
        if (flag) {
            document.helper.getUserTimeline({count: '50'}, function (err) {
                    errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                    return done(err);
                },
                function (res) {
                    if(!res ) {
                        errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                        return done(!res ? new Error('error occurred') : res.error);
                    }

                    if (!document.cache.feeds) {
                        document.cache.feeds = {};
                    }
                    document.updateTime.you = new Date();
                    try{
                        var feed = JSON.parse(res);
                    } catch(e) {
                        errorLog.log(func_name+" | JSON parse error",e,res);
                        return done(e);
                    }
                    /*if(feed.length){
                        feed = feed.filter(function(d){
                            return d.in_reply_to_status_id_str == null;
                        });
                    }*/
                    document.cache.feeds.you = feed;
                    done(null,document.cache.feeds.you);
                    document.save(function (err, doc) {
                        if (err){
                            return errorLog.log(func_name+" | Document save Error",err);
                        }
                    });
                }
            );
        }
    });
}

function TW_getInReplyTweets(locId,id,count,tweetObject,done){
    var func_name = arguments.callee.name;
    if(tweetObject==null){
        tweetObject = [];
    }
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        document.helper.getTweet({id:id}, function (err) {
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err);
        },function (res) {
            if (!res) {
                errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            var tweet;
            try {
                tweet = JSON.parse(res);
            } catch(e) {
                errorLog.log(func_name+" | JSON parse error",e,res);
                return done(e);
            }
            tweetObject.push(tweet);
            if((tweet.in_reply_to_status_id_str != null)&&(count)){
                TW_getInReplyTweets(locId,tweet.in_reply_to_status_id_str,count-1,tweetObject,done);
            }else{
                done(null,tweetObject);
            }

        });
    });
}

function TW_loadMoreFeed(locId,next,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        document.helper.getHomeTimeline({count: '25', max_id: next}, function (err) {
                errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                return done(err);
            },
            function (res) {
                if (!res) {
                    errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                var feed = [];
                try {
                    feed = JSON.parse(res);
                } catch(e) {
                    errorLog.log(func_name+" | JSON parse error",e,res);
                    return done(e);
                }
                done(null,feed);
            });
    });
}

function TW_retweetPost(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        document.helper.retweet({id:id},function(err){
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err)
        },function(res){
            if((!res )||(res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null, res.retweeted, id);
        });
    });
}

function TW_getPost(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.getTweet({id:id},function(err){
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err);
        },function(res){
            if((!res )||(res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null,res.retweeted);
        });
    });
}

function TW_delPost(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.delTweet({id:id},function(err,resp,body){
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err);
        },function(res){
            console.log("deleted res : ",res);
            if((!res )||(res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error",!res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            document.updateTime.you = new Date(1427988307103);
            document.markModified('updateTime.you');
            document.save(function(err){
                if(err){
                    return errorLog.log(func_name+"Document save Error",err)
                }
                done(null,res);
            });
        });
    });
}

function TW_favoritePost(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.favorite({id: id}, function (err) {
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err);
        }, function (res) {
            if ((!res ) || (res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null, res.favorited,   id);
        });
    });
}

function TW_unfavoritePost(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        //console.log("stream - ",stream);
        document.helper.unfavorite({id:id},function(err){
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err);
        },function(res){
            if((!res )||(res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                return done(!res ? new Error('error occurred') : res.error);
            }
            done(null, res.favorited, id);
        });
    });

}

function TW_replyPost(locId,id,text,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err,null,id);
        }
        //console.log("stream - ",stream);
        document.helper.tweet({in_reply_to_status_id:id,status:text},function(err){
            errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
            return done(err,null,id);
        },function(res){
            if((!res )||(res.error)) {
                errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,null,arguments);
                return done(!res ? new Error('error occurred') : res.error,null,id);
            }
            done(null,res,id);
        });
    });
}

function TW_getFriends(locId,mobile,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if (document.cache.friends) {
            var diff = (new Date()) - document.updateTime.friends;
            if (diff > vars.updateTimes.TW.friends) {
                flag = 1
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.friends);
                }
            } else {
                done(null,document.cache.friends);
            }
        } else {
            flag = 1
        }
        if (flag) {
            document.helper.getFriends(
                {
                    user_id: document.userId,
                    count: (document.private_user_info.friends_count > 200 ? 200 : document.private_user_info.friends_count)
                },
                function (err) {
                    errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                    return done(err);
                },
                function (res) {
                    if (!res) {
                        errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                        return done(!res ? new Error('error occurred') : res.error);
                    }

                    if (!document.cache.friends) {
                        document.cache.friends = {};
                    }
                    try{
                        res = JSON.parse(res);
                    } catch(e) {
                        errorLog.log(func_name+" | JSON parse error",e,res);
                        return done(e);
                    }
                    document.updateTime.friends = new Date();
                    res.users.forEach(function (d, i) {
                        document.cache.friends[d.id_str] = d;
                    });
                    done(null,document.cache.friends);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log(func_name+" | Document save Error",err)
                        }
                    });
                }
            );
        }
    });
}

function TW_getFollwers(locId,mobile,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var flag = 0;
        if (document.cache.follwers) {
            var diff = (new Date()) - document.updateTime.follwers;
            if (diff > vars.updateTimes.TW.follwers) {
                flag = 1
            }
            if (mobile) {
                if (!flag) {
                    return done(null,document.cache.follwers);
                }
            } else {
                done(null,document.cache.follwers);
            }
        } else {
            flag = 1
        }
        if (flag) {
            document.helper.getFollowers(
                {
                    user_id: document.userId,
                    count: (document.private_user_info.followers_count > 200 ? 200 : document.private_user_info.followers_count)
                },
                function (err) {
                    errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                    return done(err);
                },
                function (res) {
                    if (!res) {
                        errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                        return done(!res ? new Error('error occurred') : res.error);
                    }

                    if (!document.cache.follwers) {
                        document.cache.follwers = {};
                    }
                    try{
                        res = JSON.parse(res);
                    } catch(e) {
                        errorLog.log(func_name+" | JSON parse error",e,res);
                        return done(e);
                    }
                    document.updateTime.follwers = new Date();
                    res.users.forEach(function (d, i) {
                        document.cache.follwers[d.id_str] = d;
                    });
                    done(null,document.cache.follwers);
                    document.save(function (err, doc) {
                        if(err){
                            return errorLog.log(func_name+" | Document save Error",err)
                        }
                    });
                }
            );
        }
    });
}

function TW_getRetweets(locId,id,maxID,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var params = {id:id, count:'100'};
        if(typeof (maxID)=='string'){
            params.max_id = maxID;
        }
        document.helper.getReTweets(params,
            function(err){
                errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                return done(err);
            }, function(res){
                if (!res) {
                    errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                try{
                    res = JSON.parse(res);
                } catch(e) {
                    errorLog.log(func_name+" | JSON parse error",e,res);
                    return done(e);
                }
                done(null,id,res);
            }
        );
    });
}

function TW_Follow(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        document.helper.follow({user_id:id},
            function(err){
                errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                return done(err);
            },
            function(res){
                if (!res) {
                    errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }

                //res = JSON.parse(res);
                done(null,true,id);
            }
        );
    });
}

function TW_UnFollow(locId,id,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        document.helper.unfollow({user_id:id},
            function(err){
                errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                return done(err);
            },
            function(res){
                if (!res) {
                    errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }

                //res = JSON.parse(res);
                done(null,true,id);
            }
        );
    });
}

function TW_Search(locId,query,geocode,next,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        var params = {
            q               : query
            ,geocode        : geocode
            ,result_type    : 'recent'
            ,lang           : 'en'
            ,count          : '100'
        };
        if(next){
            params.max_id = next;
            params.count  = '25';
        }
        document.helper.search(params,
            function(err){
                errorLog.log(func_name+" | Twitter Reply Error", err,arguments);
                return done(err);
            },
            function(res){
                if (!res) {
                    errorLog.log(func_name+" | Twitter Reply Error", !res ? new Error('error occurred') : res.error,arguments);
                    return done(!res ? new Error('error occurred') : res.error);
                }
                try{
                    res = JSON.parse(res);
                } catch(e) {
                    errorLog.log(func_name+" | JSON parse error",e,res);
                    return done(e);
                }
                done(null,res,query);
            }
        );
    });
}

function TW_clearFeed(locId,done) {
    var func_name = arguments.callee.name;
    getDocument(locId, function (err,document) {
        if (err) {
            errorLog.log(func_name+" | Document Fetch Error", err);
            return done(err);
        }
        if(!document.cache.feeds){
            document.cache.feeds = {};
        }
        document.updateTime.mentions = new Date(1427988307103);
        document.updateTime.you = new Date(1427988307103);
        document.updateTime.feeds = new Date(1427988307103);
        document.cache.feeds.me = [];
        document.markModified('cache.feeds');
        document.save(function (err, doc) {
            done(null,document.cache.feeds.me);
            if(err){
                return errorLog.log(func_name+" | Document save Error",err)
            }
        });
    });

}

exports.status           = TW_status;
exports.create           = TW_Create;
exports.delNetwork       = TW_delNetwork;
exports.getUserInfo      = TW_getUserInfo;
exports.getMeFeed        = TW_getMeFeed;
exports.getPost          = TW_getPost;
exports.delPost          = TW_delPost;
exports.getFriends       = TW_getFriends;
exports.getFollwers      = TW_getFollwers;
exports.clearFeed        = TW_clearFeed;
exports.retweetPost      = TW_retweetPost;
exports.favoritePost     = TW_favoritePost;
exports.unfavoritePost   = TW_unfavoritePost;
exports.replyPost        = TW_replyPost;
exports.loadMoreFeed     = TW_loadMoreFeed;
exports.getMentionsFeed  = TW_getMentionsFeed;
exports.getYouFeed       = TW_getYouFeed;
exports.getRetweets      = TW_getRetweets;
exports.follow           = TW_Follow;
exports.unfollow         = TW_UnFollow;
exports.Search           = TW_Search;
exports.getInReplyTweets = TW_getInReplyTweets;
exports.moduleString     = moduleString;
/**
 * Created by Sundeep on 6/8/2015.
 */

//var vars = require('./Vars/socialVars.js');

var helpers = require("./Helpers");

var moduleString = helpers.moduleString;

var redisRPCLib         = require("redis-rpc");


var redisRPC = new redisRPCLib({
    subModule   : moduleString + "socialDispatcher",
    pubModule   : "socialDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});

var mobileRPC = new redisRPCLib({
    subModule   : moduleString + "mobileDispatcher",
    pubModule   : "mobileDispatcher" + moduleString,
    tasks       : helpers,
    dispatcher  : false
});
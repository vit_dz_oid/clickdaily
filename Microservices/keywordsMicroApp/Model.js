/**
 * Created by Sundeep on 6/8/2015.
 */

var mongoose = require('mongoose');

var connectFlag = mongoose.connections.filter(function(d){
    return d.name == "merchants";
}).length;
if(!connectFlag){
    mongoose.connect('mongodb://localhost:27017/merchants');
}

var schemaOptions = {
    toObject     : {
        virtuals : true
    }
    ,toJSON      : {
        virtuals : true
    }
    , _id: false
};

var keywordSchema = mongoose.Schema({
   inPostBar    : {type : Boolean, default : true}
    , value     : String
    , _id       : String
},schemaOptions);

var keywordsSchema = mongoose.Schema({
    _id             : String
    , list          : [keywordSchema]
},schemaOptions);

exports.keywords = mongoose.model('keyword', keywordsSchema);
/**
 * Created by Sundeep on 6/9/2015.
 */

var moduleString = "keywords";
var path = require('path');

var model = require('./Model')[moduleString];

var errorLogOptions = {
    path : path.join(__dirname, 'error_logs'),
    module : moduleString
};
var errLib = require("error-logger");
var errorLog = new errLib(errorLogOptions);

var getDocument = function(id,callback){
    model.findById(id).exec(callback);
};

var getKeyWords = function (locId,done) {
    getDocument(locId, function (err,document) {
        if((document)&&(document.list)&&(document.list.length)){
            return done(null,document.list.toObject());
        } else {
            if(!document){
                document = new model();
                document._id = locId;
            }
            initial.forEach(function (d) {
                document.list.push({_id : d, value : d});
            });

            document.markModified('list');
            document.save(function(err,doc){
                if(err){
                    errorLog.log("getKeyWords document.saves",err);
                    return done(err);
                }
                return done(null,doc.list.toObject());
            });
        }
    });
};

var addKeywords = function (locId,list,done) {
    getDocument(locId, function (err,document) {
        if (typeof(list) == "string") {
            list = [list];
        }

        if ((typeof(list) != "object" ) || !(list instanceof Array)) {
            return done("Only array or strings can be added to keywords.")
        }

        if(!document){
            document = new model();
            document._id = locId;
        }

        list.forEach(function (d) {
            if ((typeof(d) == "string") && !(document.list.id(d))) {
                var keyword = d.replace(/[\W_]+/g, "");
                document.list.push({_id : keyword, value : keyword});
            }
        });
        document.markModified('list');
        document.save(function (err, doc) {
            if (err) {
                errorLog.log("addKeywords",err);
                return done(err);
            }
            return done(null, doc.list.toObject());
        });
    });
};

var delKeyword = function (locId,keyword,done) {
    getDocument(locId, function (err,document) {
        if (typeof(keyword) != "string") {
            return done("Only string keywords.")
        }

        if(!document){
            document = new model();
            document._id = locId;
        }
        if(document.list.id(keyword)) {
            document.list.id(keyword).remove()
        }
        document.markModified('list');
        document.save(function (err, doc) {
            if (err) {
                errorLog.log("delKeyword",err);
                return done(err);
            }
            return done(null, doc.list.toObject());
        });
    });
};

var initial = [
    "FoodForThought"
    ,"tbt"
    ,"FreeFood"
    ,"Spicy"
    ,"Yummy"
    ,"Zesty"
];

exports.moduleString    = moduleString;
exports.getKeyWords     = getKeyWords;
exports.addKeywords     = addKeywords;
exports.delKeyword      = delKeyword;

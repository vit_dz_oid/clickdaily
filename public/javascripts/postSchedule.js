/**
 * Created by Sundeep on 4/8/2015.
 */

eventCache = {};
var postBar;
var env = "Schedule";
var currentView = "all";
function makeTempContainer(ref,id,network){
    var networkIcon = "";
    switch(network){
        case "IT" :
            networkIcon = "<img class='img-responsive' src='/images/insta.png'>";
            break;
        case "FB" :
            networkIcon = "<i class='fa fa-facebook'></i>";
            break;
        case "FS" :
            networkIcon = "<i class='fa fa-foursquare'></i>";
            break;
        case "TW" :
            networkIcon = "<i class='fa fa-twitter'></i>";
            break;
        default : break;
    }
    $("#" + ref).append(
        "<div class='flip-container'>" +
        "<div class='row rowPost flipper'>" +
        "<div id=" + id + " class='col-md-12 post front "+network +"'>" +
        "<div class='network-type'>" +
        networkIcon +
        "&nbsp;&nbsp;" +
        //"<a onclick='zoom("+network+"_|_"+id+")'><i class='fa fa-expand'></i></a>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-12 post back "+network +"'>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>");
}

$(function () {
    postBar = $('.ClonePostBar');
    postBar.remove();
    postBar.css("margin","15px");
    postBar.css("display","block");
    postBar.find(".postBarPostButton").remove();
    postBar.find("#datetimepicker1").remove();
    postBar.find("input#it").parent().show();
    social.emit("Ready",{feed:0,userInfo:1,schedule:1});

    // page is now ready, initialize the calendar...
    $('#calendar').fullCalendar({
        customButtons: {
            allButton: {
                text: 'All',
                click: function() {
                    if(currentView != "all") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getScheduledPosts");
                        social.emit("getNeedingApprovalPosts");
                        social.emit("getPendingApprovalPosts");
                        social.emit("getHistoryPosts");
                        currentView = "all";
                    }
                }
            },
            hisButton: {
                text: 'History',
                click: function() {
                    if(currentView != "his") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getHistoryPosts");
                        currentView = "his";
                    }
                }
            },
            schButton: {
                text: 'Scheduled',
                click: function() {
                    if(currentView != "sch") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getScheduledPosts");
                        currentView = "sch";
                    }
                }
            },
            needButton: {
                text: 'Need Your Approval',
                click: function() {
                    if(currentView != "need") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getNeedingApprovalPosts");
                        currentView = "need";
                    }
                }
            },
            pendingButton: {
                text: 'Pending Approval',
                click: function() {
                    if(currentView != "pending") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getPendingApprovalPosts");
                        currentView = "pending";
                    }
                }
            },
            expiredButton: {
                text: 'Expired',
                click: function() {
                    if(currentView != "expired") {
                        $('#calendar').fullCalendar('removeEvents');
                        social.emit("getExpiredPosts");
                        currentView = "expired";
                    }
                }
            }
        },
        header: {
            left: 'prev,today,next',
            center: 'title',
            right: 'allButton' +
            //',hisButton' +
            ',schButton,needButton,pendingButton,expiredButton   agendaWeek,month'
        },
        defaultTimedEventDuration : moment.duration(5,'minutes'),
        defaultView:'month',
        selectable: true,
        selectHelper: true,
        eventClick : function( event, jsEvent, view ) {
            if(event.history){
                social.emit("getHistoryPost", event.id, event.network);
            } else {
                social.emit("getPost", event.id);
            }
            showOverlay();
            var refDivId;
            if(event.network==0){
                refDivId = "fbView";
            } else if(event.network==1){
                refDivId = "twView";
            } else{
                refDivId = "itView";
            }
            $(".viewer").append(
                "<br/>"+
                "<div class='row'>" +
                "<div class='col-md-12'>" +
                "<div id='"+refDivId+"' class='"+refDivId+" col-md-8 col-md-offset-2'>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<hr/>"+
                "<div class='row'>" +
                "<div class='text-center' style='margin-bottom: 30px;'>" +
                "<button id='delPost' type='button' class='btn btn-primary'>Delete</button>" +
                "<br/>" +
                "</div>"+
                "</div>"
            );
            $("button#delPost").click(function(){
                if(event.history){
                    social.emit("delHistoryPost", event.id, event.network);
                } else {
                    social.emit("delPost", event.id, event.network);
                }
            });

        },
        eventDragStop : function( event, jsEvent, ui, view ) {
            var now = moment();
            var dateTime = event.start.format("X");
            if(parseInt(dateTime)-parseInt(now.format("X"))<=0){
                event.action = "duplicate";
            }else{
                event.action = "reschedule";
            }
        },
        eventDrop : function( event, delta, revertFunc, jsEvent, ui, view ) {
            if(event.network==2) {
                return revertFunc();
            }
            var eventData;
            var now = moment();
            var dateTime = event.start.format("X");
            if(parseInt(dateTime)-parseInt(now.format("X"))<=0) {
                alert("Sorry, I have not learned time travel yet. I cannot schedule something in past.");
                return revertFunc();
            } else if(parseInt(dateTime)-parseInt(now.format("X"))<=600){
                var r = confirm("Cannot schedule for less than 10 mins from now. I will be posted right away. Are you sure you cant to continue?");
                if(r){
                    eventData = {
                        id      : uuid.v1().replace(/\-/g,''),
                        title   : "Processing",
                        start   : now,
                        network : 2
                    };
                    revertFunc();
                    $('#calendar').fullCalendar('renderEvent', eventData, true);

                    return postAll($("#mergedPost.mergedPost"));
                }
                return revertFunc();
            }

            eventData = {
                id      : uuid.v1().replace(/\-/g,''),
                title   : "Processing",
                start   : event.start,
                end     : event.end,
                network : 2
            };

            switch(event.network){
                case 1 :
                    if(event.action == "duplicate"){
                        social.emit("TW_RePost",event.id,event.start);
                        $('#calendar').fullCalendar('renderEvent', eventData, true);
                    } else{
                        social.emit("TW_ChangePost",event.id,event.start);
                    }
                    break;
                case 0 :
                    if(event.action == "duplicate"){
                        social.emit("FB_RePost",event.id,event.start);
                        $('#calendar').fullCalendar('renderEvent', eventData, true);
                    } else{
                        social.emit("FB_ChangePost",event.id,event.start);
                    }
                    break;
                default : return revertFunc();
                    break;
            }
        },
        select: function(start, end) {
            var QS = queryString.parse(window.location.search);
            if(QS.id){
                var eventData;
                eventData = {
                    id      : QS.id,
                    title   : QS.title,
                    start   : start,
                    end     : end
                };
                $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                $('#calendar').fullCalendar('unselect');
            }else {
                showPostConfirm(start, end,function(confirm){
                    if(confirm){
                        eventData = {
                            id      : confirm.id,
                            title   : confirm.title,
                            start   : start,
                            end     : end,
                            network : -1,
                            oldId   : confirm.id
                        };
                        $('#calendar').fullCalendar('renderEvent', eventData, true);
                        postAll(start,confirm.id,confirm.approve);
                        $(".overlay").hide();
                        $(".viewer").hide();
                    }
                    $('#calendar').fullCalendar('unselect');
                });
            }
        },
        editable: true,
        eventDurationEditable : false,
        eventLimit: true, // allow "more" link when too many events
        timezone : "local",
        views : {
            agendaWeek  :{
                scrollTime : new Date(),
                slotDuration : "00:05:00"
            },
            agendaDay   :{
                scrollTime : new Date(),
                slotDuration : "00:05:00"
            }
        },
        allDaySlot : false,
        firstDay   : function(){
            var a = new Date();
            return a.getDay() - 3;
        }(),
        eventRender : function( event, element ) {
            var title = "";
            if(event.network==1){
                title = "Twitter";
            }else if(event.network==0){
                title = "Facebook";
            } else if(event.network==2){
                title = "Instagram";
            }else{
                title = "Processing..."
            }
            element.popover({
                title: title,
                content : event.title,
                placement : 'auto top',
                trigger   : 'hover'
            });
        },
        height : window.innerHeight-100 ,
        eventBorderColor : "#f0ad4e",
        eventBackgroundColor : "#f0ad4e"
    });

    $(window).resize(function () {
        var h = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
        $("#calendar").fullCalendar('option','height',h - 100);
    });

});

social.on("status",function(data){
    if(data.redirectUrl){
        window.location.href=data.redirectUrl;
    }
    postDisable(data);
});

social.on("scheduledPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.needApproval){
            console.log(d);
        }
        if(d.post_time) {
            var eventData = {
                id: d.id
                , title: d.post_data.text
                , start: d.post_time
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network,
                oldId: d.post_data.oldId
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("needingApprovalPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.needApproval){
            console.log(d);
        }
        if(d.post_time) {
            var eventData = {
                id: d.id
                , title: d.post_data.text
                , start: d.post_time
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network,
                oldId: d.post_data.oldId
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("pendingApprovalPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.needApproval){
            console.log(d);
        }
        if(d.post_time) {
            var eventData = {
                id: d.id
                , title: d.post_data.text
                , start: d.post_time
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network,
                oldId: d.post_data.oldId
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("rejectedPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.needApproval){
            console.log(d);
        }
        if(d.post_time) {
            var eventData = {
                id: d.id
                , title: d.post_data.text
                , start: d.post_time
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network,
                oldId: d.post_data.oldId
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("historyPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.created_at) {
            var eventData = {
                id: d.id
                , title: d.text
                , start: d.created_at
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network
                , history : true
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("expiredPosts",function(posts){
    console.log(posts);
    posts.forEach(function(d){
        var bgColor = "";
        var borderColor = "";
        switch(d.network){
            case 0 :
                bgColor = "rgb(68, 97, 157)";
                borderColor = "rgb(68, 97, 157)";
                break;
            case 1 :
                bgColor = "rgb(85, 172, 238)";
                borderColor = "rgb(85, 172, 238)";
                break;
            case 2 :
                bgColor = "rgb(108, 68, 57)";
                borderColor = "rgb(108, 68, 57)";
                break;
            default :
                bgColor = "#f0ad4e";
                borderColor = "#f0ad4e";
                break;
        }
        if(d.needApproval){
            console.log(d);
        }
        if(d.post_time) {
            var eventData = {
                id: d.id
                , title: d.post_data.text
                , start: d.post_time
                , backgroundColor: bgColor
                , borderColor: borderColor
                , network: d.network,
                oldId: d.post_data.oldId
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true);
        }
    });
});

social.on("TW_PostSuccessful",function(newID,oldID){
    if(eventCache[oldID]){
        var eventData = {
            id      : newID,
            title   : eventCache[oldID].title,
            start   : eventCache[oldID].start,
            end     : eventCache[oldID].end,
            backgroundColor : 'rgb(85, 172, 238)',
            borderColor     : 'rgb(85, 172, 238)',
            network : 1,
            oldId   : oldID
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
        //delete eventCache[oldID]
    }else{
        var evenDataOld = $('#calendar').fullCalendar( 'clientEvents' , oldID  )[0];
        $('#calendar').fullCalendar('removeEvents' , oldID );
        eventCache[oldID] = evenDataOld;
        eventData = {
            id      : newID,
            title   : evenDataOld.title,
            start   : evenDataOld.start,
            end     : evenDataOld.end,
            backgroundColor : 'rgb(85, 172, 238)',
            borderColor     : 'rgb(85, 172, 238)',
            network : 1,
            oldId   : oldID
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
    }
    console.log(oldID,newID);
});

social.on("IT_PostSuccessful",function(newID,oldID){
    if(eventCache[oldID]){
        var eventData = {
            id      : newID,
            title   : eventCache[oldID].title,
            start   : eventCache[oldID].start,
            end     : eventCache[oldID].end,
            backgroundColor : 'rgb(108, 68, 57)',
            borderColor     : 'rgb(108, 68, 57)',
            network : 2,
            oldId   : oldID
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
        //delete eventCache[oldID]
    }else{
        var evenDataOld = $('#calendar').fullCalendar( 'clientEvents' , oldID  )[0];
        $('#calendar').fullCalendar('removeEvents' , oldID );
        eventCache[oldID] = evenDataOld;
        eventData = {
            id      : newID,
            title   : evenDataOld.title,
            start   : evenDataOld.start,
            end     : evenDataOld.end,
            backgroundColor : 'rgb(108, 68, 57)',
            borderColor     : 'rgb(108, 68, 57)',
            network : 2,
            oldId   : oldID
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
    }
    console.log(oldID,newID);
});

social.on("FB_PostSuccessful",function(newID,oldID){
    if(eventCache[oldID]){
        var eventData = {
            id      : newID,
            title   : eventCache[oldID].title,
            start   : eventCache[oldID].start,
            end     : eventCache[oldID].end,
            backgroundColor : 'rgb(68, 97, 157)',
            borderColor     : 'rgb(68, 97, 157)',
            network : 0,
            oldId   : oldID
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
        //delete eventCache[oldID]
    }else{
        var event = $('#calendar').fullCalendar( 'clientEvents' , oldID  )[0];
        var eventData = {
            id      : newID,
            title   : event.title,
            start   : event.start,
            end     : event.end,
            backgroundColor : 'rgb(68, 97, 157)',
            borderColor     : 'rgb(68, 97, 157)',
            network : 0
        };
        $('#calendar').fullCalendar('removeEvents' , oldID );
        eventCache[oldID] = event;
        $('#calendar').fullCalendar('renderEvent', eventData, true);
        $('#calendar').fullCalendar('unselect');
    }
    console.log(oldID,newID);
});

function makeEditable(refDiv, id){
    var textH4 = $("#" + refDiv + " #" + id).find("h4.content");
    var text = textH4.text();
    textH4.parent().append("<div class='col-md-10'>" +
        "<textarea class='form-control'>"+text+"</textarea></div>" +
        "<div class='col-md-2'>" +
        "<button class='btn btn-primary' onclick='editPost(\"" + id + "\")'>Save</button></div>");
    textH4.remove();
}

social.on("postData",function(d,userType){
    console.log(d);
    var refDiv = "itView";
    if(d.needApproval) {
        switch (d.approved){
            case 2 :
                if(d.approvingUser.indexOf(userType)!==-1) {
                    var buttonBar = $("button#delPost").parent().empty();
                    buttonBar.append("<button id='approvePost' type='button' class='btn btn-success'>Approve</button>" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<button id='rejectPost' type='button' class='btn btn-danger'>Reject</button>");
                    $("button#approvePost").click(function () {
                        social.emit("approvePost", d.id);
                    });
                    $("button#rejectPost").click(function () {
                        social.emit("rejectPost", d.id);
                    });
                }
                break;
            case 0 :
                $("button#delPost").text("Permanentaly Delete");
                break;
        }
    }
    if(d.network==2){
        if(d.clickdailyhistory){
            d.CommonCreateDate = new Date(parseInt(d.created_time)*1000);
            makeTempContainer(refDiv, d.id, "IT",moment(d.CommonCreateDate));
            makeHead(refDiv, d.id, "https://merchant.clickdaily.com/ssl_image?url="+d.caption.from.profile_picture, d.caption.from.username
                , d.caption.from.full_name, moment(d.CommonCreateDate), "IT");
            if (typeof (d.caption.text) != "undefined") {
                addText(refDiv, d.id, d.caption.text, "IT");
            }
            if(typeof (d.videos) != "undefined") {
                var image = (d.images?(d.images.standard_resolution?(d.images.standard_resolution.url?d.images.standard_resolution.url:""):""):"");
                addVideo(refDiv, d.id,image, d.videos.standard_resolution.url, "IT");
            }
            else if (typeof (d.images.standard_resolution) != "undefined") {
                addPic(refDiv, d.id, d.images.standard_resolution.url, "IT");
            }
            makeFooter(refDiv, d.id, d.likes.count, d.comments.count, "IT");
            colorLike(refDiv, d.id, d.user_has_liked, "fa-heart", "red", "IT");
            makeBack(refDiv, d.id, "IT");
            d.comments.data.forEach(function (e, j) {
                addComment(refDiv, d.id, "https://merchant.clickdaily.com/ssl_image?url="+e.from.profile_picture.replace(/\s/g,"%20")
                    , e.from.username, moment.unix(parseInt(e.created_time)), e.text, "IT");
            });
        } else {
            makeTempContainer(refDiv, d.id, "IT", moment(d.post_time));
            makeHead(refDiv, d.id, userInfo.IT.profile_pic, userInfo.IT.name, false, moment(d.post_time), "IT");
            if (d.post_data.stream && d.post_data.stream != "false") {
                addPic(refDiv, d.id, "data:image/png;base64," + d.post_data.stream, "IT");
            }
            if (typeof (d.post_data.text) != "undefined") {
                addText(refDiv, d.id, d.post_data.text, "IT");
            }

            makeFooter(refDiv, d.id, 0, 0, "IT");
        }
    } else if(d.id_str){
        refDiv = "twView";
        makeTempContainer(refDiv, d.id_str, "TW", moment(d.CommonCreateDate));
        makeHead(refDiv, d.id_str, d.user.profile_image_url_https
            , d.user.name, '&#64;' + d.user.screen_name, moment(d.CommonCreateDate), "TW");
        if (typeof (d.text) != "undefined") {
            addText(refDiv, d.id_str, d.text, "TW");
        }
        var doneEntities = [];
        if ((typeof (d.extended_entities) != "undefined") &&(typeof (d.extended_entities.media) != "undefined") && (d.extended_entities.media.length)) {
            d.extended_entities.media.forEach(function (e, j) {
                if(doneEntities.indexOf(e.id_str)==-1) {
                    if (e.type == "animated_gif") {
                        var video =
                            (e.video_info ?
                                ( e.video_info.variants ?
                                    ( e.video_info.variants.length ?
                                        (e.video_info.variants[0].url ?
                                            e.video_info.variants[0].url
                                            : false)
                                        : false)
                                    : false)
                                : false);
                        if (video) {
                            doneEntities.push(e.id_str);
                            addVideo(refDiv, d.id_str, e.media_url_https, video, "TW");
                        }
                    }
                }
            });
        }
        if ((typeof (d.entities.media) != "undefined") && (d.entities.media.length)) {

            d.entities.media.forEach(function (e, j) {
                if(doneEntities.indexOf(e.id_str)==-1){
                    if (e.type == "photo") {
                        doneEntities.push(e.id_str);
                        addPic(refDiv, d.id_str, e.media_url_https, "TW");
                    }
                }
            });
        }
        makeFooter(refDiv, d.id_str, d.retweet_count, d.favorite_count, "TW");
        colorLike(refDiv, d.id_str, d.retweeted, "fa-retweet", "goldenrod", "TW");
        colorLike(refDiv, d.id_str, d.favorited, "fa-star", "goldenrod", "TW");
        makeBack(refDiv, d.id_str, "TW");
        if (typeof (d.text) != "undefined") {
            addComment(refDiv, d.id_str, null, null, moment(d.CommonCreateDate), d.text, "TW");
        }
    } else if(d.network){
        refDiv = "twView";
        makeTempContainer(refDiv, d.id, "TW",moment(d.post_time));
        makeHead(refDiv, d.id, userInfo.TW.profile_pic, userInfo.TW.name, false,moment(d.post_time), "TW");
        if (typeof (d.post_data.text) != "undefined") {
            addText(refDiv, d.id, d.post_data.text, "TW");
        }
        if (d.post_data.stream) {
            addPic(refDiv, d.id, "data:image/png;base64,"+d.post_data.stream, "TW");
        }
        makeFooter(refDiv, d.id, 0, 0, "TW");
        colorLike(refDiv, d.id, 0, "fa-retweet", "goldenrod", "TW");
        colorLike(refDiv, d.id, 0, "fa-star", "goldenrod", "TW");
    } else{
        refDiv = "fbView";
        if(d.post_data){
            makeTempContainer(refDiv, d.id, "FB",moment(d.post_time));
            makeHead(refDiv, d.id, userInfo.FB.profile_pic, userInfo.FB.name, false,moment(d.post_time), "FB");
            if (d.post_data.stream && d.post_data.stream!="false") {
                addPic(refDiv, d.id, "data:image/png;base64,"+d.post_data.stream, "FB");
            }
            if (typeof (d.post_data.text) != "undefined") {
                addText(refDiv, d.id, d.post_data.text, "FB");
            }

            makeFooter(refDiv, d.id, 0, 0, "FB");
            return;
        }
        function fetchImageAttachment(attachement) {
            if ((attachement) && (attachement.data) && (attachement.data.length)) {
                for (var i = 0; i < attachement.data.length; i++) {
                    if ((attachement.data[i])
                        && (attachement.data[i].media)
                        && (attachement.data[i].media.image)
                        && (attachement.data[i].media.image.src)) {
                        return attachement.data[i].media.image.src;
                    }
                }
            }
            return false;
        }

        var image = fetchImageAttachment(d.attachments);
        makeTempContainer(refDiv, d.id, "FB", moment(d.scheduled_publish_time,"X"));
        makeHead(refDiv, d.id, userInfo.FB.profile_pic, userInfo.FB.name, false, moment(d.scheduled_publish_time,"X"), "FB");
        if (typeof (d.message) != "undefined") {
            addText(refDiv, d.id, d.message, "FB");
        }
        if (typeof (d.name) != "undefined") {
            if (!d.caption) {
                d.caption = "";
            }
            if (image) {
                addLink(refDiv, d.id, image, d.caption, d.name, d.link, "FB")
            }
            else if (typeof (d.full_picture) != "undefined") {
                addLink(refDiv, d.id, d.full_picture, d.caption, d.name, d.link, "FB")
            }else if (typeof (d.source) != "undefined") {
                addLink(refDiv, d.id, d.source, d.caption, d.name, d.link, "FB");
            } else if((typeof (d.images) != "undefined")
                &&(d.images.length)
                &&(d.images[0].source)
            ){
                addLink(refDiv, d.id, d.images[0].source, d.caption, d.name, d.link, "FB");
            }else if (typeof (d.picture) != "undefined") {
                addLink(refDiv, d.id, d.picture, d.caption, d.name, d.link, "FB");
            } else {
                addLink(refDiv, d.id, false, d.caption, d.name, d.link, "FB");
            }

        } else if (image) {
            addPic(refDiv, d.id, image, "FB");
        } else if (typeof (d.full_picture) != "undefined") {
            addPic(refDiv, d.id, d.full_picture, "FB");
        }else if (typeof (d.source) != "undefined") {
            addPic(refDiv, d.id, d.source, "FB");
        } else if((typeof (d.images) != "undefined")
            &&(d.images.length)
            &&(d.images[0].source)
        ){
            addPic(refDiv, d.id, d.images[0].source, "FB");
        }else if (typeof (d.picture) != "undefined") {
            addPic(refDiv, d.id, d.picture, "FB");
        }
        makeFooter(refDiv, d.id
            , ((d.likes) ? ((d.likes.summary) ? d.likes.summary.total_count : ((d.likes.data) ? d.likes.data.length : 0)) : 0)
            , ((d.comments) ? ((d.comments.summary) ? d.comments.summary.total_count : ((d.comments.data) ? d.comments.data.length : 0)) : 0), "FB");
        makeBack(refDiv, d.id, "FB");
        if (d.comments) {
            d.comments.data.forEach(function (e, j) {
                addComment(refDiv, d.id, e.from.picture.data.url
                    , e.from.name, moment(e.created_time), e.message, "FB");
            });
        }
    }
    if( d.needApproval && d.approved == 2){
        //makeEditable(refDiv, d.id);
    }
});

social.on("postDeleted",function(id){
    $(".overlay").hide();
    $(".viewer").hide().empty();
    $('#calendar').fullCalendar('removeEvents' , id );
});

social.on('approvedPost',function(id){
    $(".overlay").hide();
    $(".viewer").hide().empty();
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Approved Successfully'
    });
});

social.on('rejectedPost',function(id){
    $(".overlay").hide();
    $(".viewer").hide().empty();
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Rejected Successfully'
    });
});
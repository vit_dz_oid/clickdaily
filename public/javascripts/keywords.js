/**
 * Created by Sundeep on 6/3/2015.
 */


var i = 0;

$(function(){
   social.emit("Ready",{userInfo:1});
});

social.on("keywords",function(list){
    console.log(list);

    list.forEach(function(e){
        d = e.value;
        var html = "<li id='" + d + "' class='list-group-item active'>" +
            "<span class='badge' style='cursor:pointer;' onclick=\"removeKeyword('"+d+"')\">&times;</span>" +
            "#"+d+"</li>";
        switch(i%4){
            case 0 : $("#first ul").append(html);
                break;
            case 1 : $("#second ul").append(html);
                break;
            case 2 : $("#third ul").append(html);
                break;
            case 3 : $("#fourth ul").append(html);
                break;
        }
        i = i+1;
        i = i%4;
    });
});
var keywords;
function addKeyWords(){
    keywords = $("textarea#keyWordsAdd")
        .val()
        .split(/\,|\s/)
        .map(function(s){return s.replace(/[\W_]+/g,"");})
        .filter(function(s){return s != ""});
    showOverlay();
    $(".viewer").append(
    "<br/>" +
    "<h3 class='text-center'>Following Keywords will be added</h3>" +
    "<br/>" +
    "<div class='row'>" +
    "<div class='col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-10'>" +
    "<ul id='previewFirst' class='list-group'></ul>" +
    "</div>" +
    "<div class=' col-md-5 col-sm-10'>" +
    "<ul id='previewSecond' class='list-group'></ul>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-md-12 text-center'>" +
    "<button id='previewConfirm' type='button' class='btn btn-primary' onclick='sendKeywords()'>" +
    "Confirm Add</button>" +
    "&nbsp;&nbsp;" +
    "<button type='button' class='btn btn-default' onclick='hideOverlay()'>" +
    "Cancel</button>" +
    "</div>" +
    "</div>");
    var removePreviews = [];
    keywords.forEach(function(d,j){
        var html;
        if($("#"+d).length){
            html = "<li id='preview' class='list-group-item list-group-item-warning'>" +
                "#"+d+" Already Added</li>";
            removePreviews.push(d);
        }else {
            html = "<li id='preview_" + d + "' class='list-group-item active'>" +
                "<span class='badge' style='cursor:pointer;' " +
                "onclick=\"removePreview('"+d+"')\">&times;</span>" +
                "#"+d+"</li>";
        }

        switch(j%2){
            case 0 : $("#previewFirst").append(html);
                break;
            case 1 : $("#previewSecond").append(html);
                break;
        }
    });

    removePreviews.forEach(function(d){
        keywords.splice(keywords.indexOf(d),1);
    });
    if(!keywords.length){
        $("button#previewConfirm").remove();
        $(".viewer").find("h3").text("No Keywords to add!!");
    }
}

function sendKeywords(){
    hideOverlay();
    social.emit('addKeywords',keywords);
}

function removePreview(d){
    $("#preview_"+d).remove();
    keywords.splice(keywords.indexOf(d),1);
}

function removeKeyword(d){
    $("#"+d).remove();
    social.emit('delKeyword',d);
}

social.on("updateKeywords",function(newList){
    $("textarea#keyWordsAdd").val('');
    newList.forEach(function(e){
        d = e.value;
        if($("#"+ d).length){
            return;
        }
        var html = "<li id='" + d + "' class='list-group-item active'>" +
            "<span class='badge' style='cursor:pointer;' onclick=\"removeKeyword('"+d+"')\">&times;</span>" +
            "#"+d+"</li>";
        switch(i%4){
            case 0 : $("#first ul").append(html);
                break;
            case 1 : $("#second ul").append(html);
                break;
            case 2 : $("#third ul").append(html);
                break;
            case 3 : $("#fourth ul").append(html);
                break;
        }
        i = i+1;
        i = i%4;
    });
});
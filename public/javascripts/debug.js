/**
 * Created by Sundeep on 4/2/2015.
 */

var debug = io.connect('/debug');

function clearFbFeed(){
    debug.emit("FB_ClearFeed");
}

function clearTwFeed(){
    debug.emit("TW_ClearFeed");
}

function clearFsFeed(){
    debug.emit("FS_ClearFeed");
}

function clearItFeed(){
    debug.emit("IT_ClearFeed");
}

﻿var FBFeed = {};
var TWFeed = {};
var ITFeed = {};
var FSFeed = {};

var counts = {
    FB : 1,
    TW : 2,
    IT : 3,
    FS : 4
};

function showDate(comment_date){
    var today = moment();
    return comment_date.from(today);
}

function twReplaceEntity(text,link,start,end){
    return text.substr(0,start) + link + text.substr(end);
}

function updatePostTimes() {
    $('.flip-container').each(function (index, el) {
        $(el).find('small.time').text(showDate(moment($(el).data('date-created'), "YYYY-MM-DD-HH-mm-ss")));
    });
}

function sortTypeDate(date){
    return date.format("YYYY-MM-DD-HH-mm-ss");
}
function makeLoadMore (ref,network,next){
    if(!$('#loadMore' + network).length){
        $("#" + ref).append(
            "<div class='row rowPost'>" +
            "<div id='loadMore" + network + "' class='col-md-4 col-lg-3 col-sm-6 col-xs-12 post'>" +
            "<div class='text-center'>" +
            "<span><a onclick=\"loadmore('" + network + "','" + next + "')\">Load More</a></span>" +
            "</div>" +
            "</div>" +
            "</div>");
    }

}

function makeContainer(ref,id,network,date){
    var networkIcon = "";
    switch(network){
        case "IT" :
            networkIcon = "<img class='img-responsive' src='/images/insta.png'>";
            break;
        case "FB" :
            networkIcon = "<i class='fa fa-facebook'></i>";
            break;
        case "FS" :
            networkIcon = "<i class='fa fa-foursquare'></i>";
            break;
        case "TW" :
            networkIcon = "<i class='fa fa-twitter'></i>";
            break;
        default : break;
    }
    var container = $(
        //
        "<div  id=contain_" + id + " class='flip-container col-md-4 col-lg-3 col-sm-6 col-xs-12'" +
        " data-date-created='"+sortTypeDate(date)+"'" +
        " data-network='"+counts[network]+"'>" +
        "<div class='row rowPost flipper'>" +
        "<div id=" + id + " class='post front "+network +"'>" +
        "<div class='network-type'>" +
        networkIcon +
        "&nbsp;&nbsp;" +
        //"<a onclick='zoom("+network+"_|_"+id+")'><i class='fa fa-expand'></i></a>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-12 post back "+network +"'>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>");
    $("#" + ref).append(container);
    $('#group').shuffle('appended',container);
    //counts[network]= counts[network]+4;
}

function makeHead(ref,id,picture,from,handle,date,network){
    var name;
    var pp;
    var ts;
    if(network=="TW" && handle){
        name = '<strong  class="name">'
            + "<a href=\"https://twitter.com/" + handle.substr(5) + "\" style=\"color: black;\">"+ from +"</a>"
            + '</strong>'
            + '<br/>'
            + '<small class="handle">'
            + "<a href=\"https://twitter.com/" + handle.substr(5) + "\" style=\"color: black;\">"+ handle +"</a>"
            + '</small>'
            + '<br/>';
        pp = '<a href="https://twitter.com/' + handle.substr(5) + '" class="profile-pic">'
            + '<img class="media-object'
            + (network=="IT"?' img-circle':'')
            + '" src="'
            + picture
            + '" alt="ProfilePic">'
            + '</a>';

        ts = '<a href="https://twitter.com/' + handle.substr(5) + '/status/' + id + '">'
            + '<small class="time">'
            + showDate(date)
            + '</small>'
            + '</a>';
    } else if (network=="TW") {
        name = '<strong  class="name">'
            + "<a href=\"https://twitter.com/" + userInfo.TW.screen_name.substr(5) + "\" style=\"color: black;\">"+ userInfo.TW.name +"</a>"
            + '</strong>'
            + '<br/>'
            + '<small class="handle">'
            + "<a href=\"https://twitter.com/" + userInfo.TW.screen_name.substr(5) + "\" style=\"color: black;\">"+ userInfo.TW.screen_name +"</a>"
            + '</small>'
            + '<br/>';
        pp = '<a href="https://twitter.com/' + userInfo.TW.screen_name.substr(5) + '" class="profile-pic">'
            + '<img class="media-object'
            + (network=="IT"?' img-circle':'')
            + '" src="'
            + picture
            + '" alt="ProfilePic">'
            + '</a>';

        ts = '<small class="time">'
            + showDate(date)
            + '</small>';
    }else {
        name = '<strong  class="name">'
            + from
            + '</strong>'
            + '<br/>';
        pp = '<a href="#" class="profile-pic">'
            + '<img class="media-object'
            + (network=="IT"?' img-circle':'')
            + '" src="'
            + picture
            + '" alt="ProfilePic">'
            + '</a>';
        ts = '<small class="time">'
            +showDate(date)
            +'</small>';
    }
    var mediaHeading = name
            + ts

        ;
    $("#" + ref + " #" + id)
        .append('<div class="row"></div>')
        .find(".row")
        .append('<div class="media header"></div>')
        .find(".media.header")
        .append('<div class="media-left"></div>')
        .find(".media-left")
        .append(pp)
        .end()
        .append(
            '<div class="media-body">'
            + '<h4 class="media-heading">'
            + mediaHeading
            + '</h4>'
            + '</div>'
        );
}

function addText(ref,id,text,network,entities){
    if(entities) {
        var replacements = [];
        if(entities.urls.length){
            entities.urls.forEach(function(d,i){
                replacements.push({
                    start : d.indices[0],
                    end : d.indices[1],
                    link : "<a href='"+ d.url + "'>" + d.display_url + "</a>"
                });
            })
        }
        if(entities.user_mentions.length){
            entities.user_mentions.forEach(function(d,i){
                replacements.push({
                    start : d.indices[0],
                    end : d.indices[1],
                    link : "<a href='https://twitter.com/"+ d.screen_name + "'>@" + d.screen_name + "</a>"
                });
            })
        }
        if(entities.hashtags.length){
            entities.hashtags.forEach(function(d,i){
                replacements.push({
                    start : d.indices[0],
                    end : d.indices[1],
                    link : "<a href='https://twitter.com/hashtag/"+ d.text + "'>#" + d.text + "</a>"
                });
            })
        }
        replacements.sort(function(a,b){
            return b.start - a.start;
        });
        replacements.forEach(function(d,i){
            text = twReplaceEntity(text, d.link, d.start, d.end);
        });
    }

    $("#" + ref + " #" + id).append(
        '<div class="row">'
        +'<div class="col-md-12">'
        +'<h4 class="content" ' +
        'title="'+text+'">'
        +text
        +"</h4>"
        +'</div>'
        +'</div>'
    );

    if(network!='TW') {
        $("#" + ref + " #" + id + " .row .col-md-12 .content").linkify();
        $("#" + ref + " #" + id + " .row .col-md-12 .content").shorten({
            "showChars": 50,
            "moreText": "See More",
            "lessText": "Less"
        });
    }
    twemoji.parse($("#" + ref + " #" + id + " .row .col-md-12 .content")[0])
}

function addPic(ref,id,pic,network,expanded_url){
    $("#" + ref + " #" + id).append(
        '<div class="row post-contents">'
        +'<div class="col-md-12">'
        +'<a href="'+ (expanded_url?expanded_url:"#") +'">'
        +'<img src="'
        + pic
        +'" class="img-rounded img-responsive post-pic">'
        +'</a>'
        +'</div>'
        +'</div>'
    )
}

function addVideo(ref,id,pic,video,network){
    $("#" + ref + " #" + id).append(
        '<div class="row post-contents">'
        +'<div class="col-md-12">'
        +'<a href="#">'
        +'<video class="' +
        'video-js vjs-default-skin'+
        // 'vjs-paused vjs-controls-enabled vjs-user-inactive ' +
        ' img-rounded img-responsive post-pic" controls preload="auto" ' +
        (pic?('poster="'+pic+'"'):'') +
        //' data-setup=\'{ "controls": true, "autoplay": false, "preload": "auto" }\'' +
        '>'
        +'<source src="'
        + video
        +'"></video>'
        +'</a>'
        +'</div>'
        +'</div>'
    )
}

function addLink(ref,id,pic,caption,name,link,network){
    if(!caption){
        caption = "";
    }
    $("#" + ref + " #" + id).append(
        '<div class="row post-contents">'
        +'<div class="col-md-12">'
        +'<a href="' + link + '" class="thumbnail">'
        +(pic?'<img src="'
        + pic
        +'" class="img-rounded img-responsive post-pic" width="100%">':'')
        +'<div class="caption">'
        +'<h4 class="link-name">'
        + name
        +'</h4>'
        +'<p class="link-caption">'
        + caption
        +'</p>'
        +'</div>'
        +'</a>'
        +'</div>'
        +'</div>'
    )
}

function makeFooter(ref,id,likes,comments,network){
    var begin = ' <hr>'
        +'<div class="row">'
        +'<div class="col-md-12 footer">';

    var left = '<div class="pull-left ">'
        +'<span class="like-info">';

    switch(network){
        case 'FB' :
        case 'IT' :
            left = left
            +'<strong>'
            +'<span class="like-number '
            + (likes?'text-success':'') + '">'
            +(likes?likes:0)
            +'</span>'
            +' LIKES '
            +'<span class="comment-number '
            + (comments?'text-success':'') +'" >'
            +(comments?comments:0)
            +'</span> '
            +'COMMENTS'
            +'</strong>';
            break;
        case 'FS' :
            left = left
            +'<strong>'
            +'<span class="like-number '
            + (likes?'text-success':'') + '">'
            +(likes?likes:0)
            +'</span>'
            +' LIKES '
            +'</strong>';
            break;
        case 'TW' :
            left = left
            +'<strong>'
            +'<a onclick="getTWRetweetsPost(\''
            + id
            + '\')" class="like-link '
            + (likes?'text-success':'') +'">'
            +'<span class="like-number '
            + (likes?'text-success':'') +'">'
            +(likes?likes:0)
            +'</span>'
            +' RETWEETS  '
            +'</a>'
            +'<a onclick="getTWFavsPost(\''
            + id
            + '\')" class="comment-link '
            + (comments?'text-success':'') +'">'
            +'<span class="comment-number '
            + (comments?'text-success':'') +'">'
            +(comments?comments:0)
            +'</span> '
            +'FAVORITES'
            +'</a>'
            +'</strong>';
            break;
    }

    left = left+'</span>'
        +'</div>';

    var right = '<div class="pull-right icons">';

    switch(network) {
        case 'FB' :
            right = right
            +'<a onclick="likeFbPost(\''
            + id
            + '\')" data-toggle="tooltip" title="Like">'
            +'<i class="fa fa-thumbs-up "></i>'
            +'</a>'
            +'&nbsp;'
            +'<a ' +
            'onclick="$(this).parent().parent().parent().parent().parent().parent()[0].classList.toggle(\'flip\')"' +
            ' data-toggle="tooltip" title="Comment">'
            +'<i class="fa fa-comment"></i>'
            +'</a>';
            /*+'&nbsp;'
            +'<a  data-toggle="tooltip" title="Share">'
            +'<i class="fa fa-share"></i>'
            +'</a>';*/
            break;
        case 'IT' :
            right = right
            + '<a onclick="FavInsta(\''
            + id
            + '\')"  data-toggle="tooltip" title="Like">'
            + '<i class="fa fa-heart"></i>'
            + '</a>'
            + '&nbsp;'
            + '<a ' +
            'onclick="$(this).parent().parent().parent().parent().parent().parent()[0].classList.toggle(\'flip\')"' +
            ' data-toggle="tooltip" title="Comment">'
            + '<i class="fa fa-comment"></i>'
            + '</a>';
            break;
        case 'FS' :
            right = right
            +'<a onclick="ReTweet(\''
            + id
            + '\')" data-toggle="tooltip" title="Save">'
            +'<i class="fa fa-bookmark "></i>'
            +'</a>'
            +'&nbsp;'
            +'<a  data-toggle="tooltip" title="Like">'
            +'<i class="fa fa-heart"></i>'
            +'</a>';
            break;
        case 'TW' :
            right = right
                +'<a ' +
                'onclick="$(this).parent().parent().parent().parent().parent().parent()[0].classList.toggle(\'flip\')"' +
                ' data-toggle="tooltip" title="Reply">'
                +'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 72" width="16px" height="17px">'
                +'<path style="fill: #AAB8C2;" d="M41 31h-9V19c0-1.14-.647-2.183-1.668-2.688-1.022-.507-2.243-.39-3.15.302l-21 16C5.438 33.18 5 34.064 5 35s.437 1.82 1.182 2.387l21 16c.533.405 1.174.613 1.82.613.453 0 .908-.103 1.33-.312C31.354 53.183 32 52.14 32 51V39h9c5.514 0 10 4.486 10 10 0 2.21 1.79 4 4 4s4-1.79 4-4c0-9.925-8.075-18-18-18z"/>'
                +'</svg>'
                +'</a>'
                +'&nbsp;'
                +'<a onclick="ReTweet(\''
                + id
                + '\')"  data-toggle="tooltip" title="Retweet">'
                +'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72" width="16px" height="17px">'
                +'<path  class="tw-retweet" style="fill: #AAB8C2;" d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/>'
                +'</svg>'
                +'</a>'
                +'&nbsp;'
                +'<a onclick="FavTweet(\''
                + id
                + '\')"' +
                ' data-toggle="tooltip" title="Favorite">'
                +'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 72"  width="16px" height="17px">'
                +'<path class="tw-like" style="fill: #AAB8C2;" d="M38.723,12c-7.187,0-11.16,7.306-11.723,8.131C26.437,19.306,22.504,12,15.277,12C8.791,12,3.533,18.163,3.533,24.647 C3.533,39.964,21.891,55.907,27,56c5.109-0.093,23.467-16.036,23.467-31.353C50.467,18.163,45.209,12,38.723,12z"/>'
                +'</svg>'
                +'</a>';
            break;
            break;
    }

    right = right + '</div>';

    var end = '</div>'
        +'</div>';

    $("#" + ref + " #" + id).append(begin+left+right+end);
}

function colorLike(ref,id,parameter,className,color,network){
    if(parameter){
        if(network=="TW"){
            $("#" + ref + " #" + id).find("." + className).css("fill", color);
        }else {
            $("#" + ref + " #" + id).find("." + className).css("color", color);
        }
    }
}

function makeBack(ref,id,network,screen_name){
    $("#" + ref + " #" + id).parent().find(".back").append(
        "<div class='comment "+
        (network!="TW"?"well":"")
        +"'></div>"+
        "<div class='form-group'>" +
        "<textarea class='comment-area' rows='"
        +(network=="TW"?2:4)
        +"' placeholder='Your Comment here...' style='width: 100%'></textarea>" +
        "</div>" +
        "<div class='pull-right'>" +
        "<button type='button' class='btn' onClick='$(this).parent().parent().parent().parent()[0].classList.toggle(\"flip\")'>Cancel</button>&nbsp;&nbsp;&nbsp;" +
        "<button type='button' class='btn btn-primary' " +
        (network=="FB"?"onClick='commentFbPost(\""+id+"\",$(this).parent().parent().find(\".comment-area\").val())'":"") +
        (network=="IT"?"onClick='commentInsta(\""+id+"\",$(this).parent().parent().find(\".comment-area\").val())'":"") +
        (network=="TW"?"onClick='commentTwitter(\""+id+"\",$(this).parent().parent().find(\".comment-area\").val())'":"") +
        ">Send</button>" +
        "</div>" +
        "<div class='pull-left'>" +
        "<span class='comment-msg-success'><i class='fa fa-check-square'></i> Success</span>" +
        "<span class='comment-msg-fail'><i class='fa fa-times-circle'></i> Failed</span>" +
        "</div>"
    );
    if((network=="TW")&&(screen_name)){
        $("#" + ref + " #" + id)
            .parent()
            .find(".back")
            .find(".comment-area")
            .val("@"+screen_name+" ");
    }
    var resizeFactor = 160;
    if(network=="TW"){
        resizeFactor = 113;
    }
    $("#" + ref + " #" + id).parent().find(".back").find(".comment").css(
        "height",($("#" + ref + " #" + id).parent().find(".front").height()-resizeFactor)+"px"
    );

    $("#" + ref + " #" + id).parent().find(".front").find("img").load(function(){
        $(this).closest(".flipper").find(".back").find(".comment").css(
            "height",($(this).closest(".flipper").find(".front").height()-resizeFactor)+"px"
        );
    });
}

function addComment(ref,id,pic,name,date,text,network){
    if(network=="TW"){
        $("#" + ref + " #" + id).parent().find(".back").find(".comment").prepend(
            "<h4 class='content'>"
            +twemoji.parse(text)
            +"</h4>"
        );
    }else{
        $("#" + ref + " #" + id).parent().find(".back").find(".comment").prepend(
            '<div class="media header">'
            + '<div class="media-left">'
            + '<a href="#" class="comment-profile-pic">'
            + '<img class="media-object'
            + (network=="IT"?' img-circle':'')
            + '" src="'
            + pic
            + '" alt="ProfilePic">'
            + '</a>'
            + '</div>'
            + '<div class="media-body">'
            + '<h4 class="media-heading">'
            + '<strong  class="comment-name">' + name + ''
            + '<small class="comment-time pull-right">'
            + showDate(date)
            + '&nbsp;&nbsp;</small>'
            + '</strong>'
            + '<br/>'
            + '<small class="comment-text">' + twemoji.parse(text) + '</small>'
            + '</h4>'
            + '</div>'
            + '</div>'
        );
    }
}

function Create_FB_Post(d,refDiv) {
    if(!FBFeed[d.id]) {
        FBFeed[d.id] = d;

        function fetchImageAttachment(attachement) {
            if ((attachement) && (attachement.data) && (attachement.data.length)) {
                for (var i = 0; i < attachement.data.length; i++) {
                    if ((attachement.data[i])
                        && (attachement.data[i].media)
                        && (attachement.data[i].media.image)
                        && (attachement.data[i].media.image.src)) {
                        return attachement.data[i].media.image.src;
                    }
                }
            }
            return false;
        }

        var image = fetchImageAttachment(d.attachments);
        makeContainer(refDiv, d.id, "FB",moment(d.CommonCreateDate));
        makeHead(refDiv, d.id, d.from.picture.data.url, d.from.name, false, moment(d.CommonCreateDate), "FB");
        if (typeof (d.message) != "undefined") {
            addText(refDiv, d.id, d.message, "FB");
        }
        if (typeof (d.name) != "undefined") {
            if (!d.caption) {
                d.caption = "";
            }
            if (image) {
                addLink(refDiv, d.id, image, d.caption, d.name, d.link, "FB")
            }
            else if (typeof (d.full_picture) != "undefined") {
                addLink(refDiv, d.id, d.full_picture, d.caption, d.name, d.link, "FB")
            } else {
                addLink(refDiv, d.id, false, d.caption, d.name, d.link, "FB");
            }

        } else if((d.type=="video")&&(typeof (d.source) != "undefined")) {
            if (image) {
                addVideo(refDiv, d.id, image, d.source,"FB");
            }
            else if (typeof (d.full_picture) != "undefined") {
                addVideo(refDiv, d.id, d.full_picture, d.source,"FB");
            } else {
                addVideo(refDiv, d.id, false, d.source,"FB");
            }
        } else if (image) {
            addPic(refDiv, d.id, image, "FB");
        } else if (typeof (d.full_picture) != "undefined") {
            addPic(refDiv, d.id, d.full_picture, "FB");
        } else if (typeof (d.picture) != "undefined") {
            addPic(refDiv, d.id, d.picture, "FB");
        }
        makeFooter(refDiv, d.id
            , ((d.likes) ? ((d.likes.summary) ? d.likes.summary.total_count : ((d.likes.data) ? d.likes.data.length : 0)) : 0)
            , ((d.comments) ? ((d.comments.summary) ? d.comments.summary.total_count : ((d.comments.data) ? d.comments.data.length : 0)) : 0), "FB");
        makeBack(refDiv, d.id, "FB");
        if (d.comments) {
            d.comments.data.forEach(function (e, j) {
                addComment(refDiv, d.id, e.from.picture.data.url
                    , e.from.name, moment(e.created_time), e.message, "FB");
            });
        }
        return true
    }
    return false
}

function Create_Insta_Post(d, refDiv) {
    if(!ITFeed[d.id]) {
        ITFeed[d.id] = d;
        if (d.caption != null) {
            makeContainer(refDiv, d.id, "IT",moment(d.CommonCreateDate));
            makeHead(refDiv, d.id, "https://merchant.clickdaily.com/ssl_image?url="+d.caption.from.profile_picture, d.caption.from.username
                , d.caption.from.full_name, moment(d.CommonCreateDate), "IT");
            if (typeof (d.caption.text) != "undefined") {
                addText(refDiv, d.id, d.caption.text, "IT");
            }
            if(typeof (d.videos) != "undefined") {
                var image = (d.images?(d.images.standard_resolution?(d.images.standard_resolution.url?d.images.standard_resolution.url:""):""):"");
                addVideo(refDiv, d.id,image, d.videos.standard_resolution.url, "IT");
            }
            else if (typeof (d.images.standard_resolution) != "undefined") {
                addPic(refDiv, d.id, d.images.standard_resolution.url, "IT");
            }
            makeFooter(refDiv, d.id, d.likes.count, d.comments.count, "IT");
            colorLike(refDiv, d.id, d.user_has_liked, "fa-heart", "red", "IT");
            makeBack(refDiv, d.id, "IT");
            d.comments.data.forEach(function (e, j) {
                addComment(refDiv, d.id, "https://merchant.clickdaily.com/ssl_image?url="+e.from.profile_picture.replace(/\s/g,"%20")
                    , e.from.username, moment.unix(parseInt(e.created_time)), e.text, "IT");
            });
        }
        return true;
    }
    return false;
}

function Create_Fs_Post(d, refDiv) {
    if(!FSFeed[d.id]) {
        FSFeed[d.id] = d;
        makeContainer(refDiv, d.id, "FS",moment(d.CommonCreateDate));
        makeHead(refDiv, d.id, d.user.photo.prefix + "original" + d.user.photo.suffix
            , d.user.firstName, false, moment(d.CommonCreateDate), "FS");
        if (typeof (d.text) != "undefined") {
            addText(refDiv, d.id, d.text, "FS");
        }
        if (typeof (d.photo) != "undefined") {
            addPic(refDiv, d.id, d.photo.prefix + "original" + d.photo.suffix, "FS");
        }
        makeFooter(refDiv, d.id, d.likes.count, false, "FS");
        colorLike(refDiv, d.id, d.like, "fa-heart", "red", "FS");
        makeBack(refDiv, d.id, "FS");
        return true
    }
    return false
}

function Create_Tw_Post(d,refDiv) {
    if(!TWFeed[d.id_str]) {
        TWFeed[d.id_str] = d;
        var Retweet = null;
        /*if (d.retweeted_status) {
            Retweet = {
                screen_name: d.user.screen_name,
                name: d.user.name,
                id: d.user.id, type: "retweet",
                picture: d.user.profile_image_url_https,
                id_str: d.id_str
            };
            d = d.retweeted_status;
            //d.id_str = Retweet.id_str;
            d.CommonCreateDate = new Date(d.created_at);
            d.text = "<span class='retweetHead'><i class='fa fa-retweet'></i> " +
            "<a data-toggle='tooltip' title='" + Retweet.name +
            "' " +
            " data-tooltip-img='" + Retweet.picture + "' >@" +
            Retweet.screen_name + "</a> retweeted</span><br />" + d.text;
        }*/
        if (d.retweeted_status) {
            d = d.retweeted_status;
        }
        makeContainer(refDiv, d.id_str, "TW",moment(d.CommonCreateDate));
        makeHead(refDiv, d.id_str, d.user.profile_image_url_https
            , d.user.name, '&#64;' + d.user.screen_name, moment(d.CommonCreateDate), "TW");
        if (typeof (d.text) != "undefined") {
            addText(refDiv, d.id_str, d.text, "TW", d.entities);
        }
        var doneEntities = [];
        if ((typeof (d.extended_entities) != "undefined") &&(typeof (d.extended_entities.media) != "undefined") && (d.extended_entities.media.length)) {
            d.extended_entities.media.forEach(function (e, j) {
                if(doneEntities.indexOf(e.id_str)==-1) {
                    if (e.type == "animated_gif") {
                        var video =
                            (e.video_info ?
                                ( e.video_info.variants ?
                                    ( e.video_info.variants.length ?
                                        (e.video_info.variants[0].url ?
                                            e.video_info.variants[0].url
                                            : false)
                                        : false)
                                    : false)
                                : false);
                        if (video) {
                            doneEntities.push(e.id_str);
                            addVideo(refDiv, d.id_str, e.media_url_https, video, "TW");
                        }
                    }
                }
            });
        }
        if ((typeof (d.entities.media) != "undefined") && (d.entities.media.length)) {

            d.entities.media.forEach(function (e, j) {
                if(doneEntities.indexOf(e.id_str)==-1){
                    if (e.type == "photo") {
                        doneEntities.push(e.id_str);
                        addPic(refDiv, d.id_str, e.media_url_https, "TW", e.expanded_url);
                    }
                }
            });
        }
        makeFooter(refDiv, d.id_str, d.retweet_count, d.favorite_count, "TW");
        colorLike(refDiv, d.id_str, d.retweeted,  "tw-retweet", "#19CF86", "TW");
        colorLike(refDiv, d.id_str, d.favorited, "tw-like", "#E81C4F", "TW");
        makeBack(refDiv, d.id_str, "TW", d.user.screen_name);
        if (typeof (d.text) != "undefined") {
            addComment(refDiv, d.id_str, null, null, moment(d.CommonCreateDate), d.text, "TW");
        }
        return true;
    }
    return false;
    /*$("#" + refDiv + " #" + d.id_str + " .comment input").keyup(function (e) {
        if (e.keyCode == 13) {
            PostReply(d.id_str, $(this).val());
        }
    });*/
}
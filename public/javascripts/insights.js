/**
 * Created by Sundeep on 5/7/2015.
 */





social.on("FB_Insights",function(err,data){
    console.log(err,data);
    Gdata = data;
    Gerr = err;
    var daily = {};
    var weekly = {};
    var monthly = {};
    var rest = {};
    if((data)&&(data.data)){
        data.data.forEach(function (d, i) {
            if (d.title.substr(0, 5) == 'Daily') {
                daily[d.title] = d;
            } else if (d.title.substr(0, 6) == 'Weekly') {
                weekly[d.title] = d;
            } else if (d.title.substr(0, 7) == '28 Days') {
                monthly[d.title] = d;
            } else {
                rest[d.title] = d;
            }
        });

        var color = "#d9534f";

        var TotalImpressions   = weekly["Weekly Total Impressions of your posts"].values[2].value;
        var TotalImpressionsCh = TotalImpressions - weekly["Weekly Total Impressions of your posts"].values[0].value;
        if(TotalImpressionsCh>0) {
            TotalImpressionsCh = "&uArr;" + TotalImpressionsCh;
            color = "#5cb85c";
        } else if(TotalImpressionsCh==0){
            color = "#7B7B7B";
        }else {
            TotalImpressionsCh = "&dArr;"+Math.abs(TotalImpressionsCh);
        }
        $(".ioyp").text(TotalImpressions + " ")
            .append("<small>" + TotalImpressionsCh + "</small>")
            .css("color",color);


        color = "#d9534f";
        var ReachPosts   = weekly["Weekly Reach of page posts"].values[2].value;
        var ReachPostsCh = ReachPosts - weekly["Weekly Reach of page posts"].values[0].value;
        if(ReachPostsCh>0) {
            ReachPostsCh = "&uArr;" + ReachPostsCh;
            color = "#5cb85c";
        } else if(ReachPostsCh==0){
            color = "#7B7B7B";
        }else {
            ReachPostsCh = "&dArr;"+Math.abs(ReachPostsCh);
        }
        $(".ropp").text(ReachPosts + " ")
            .append("<small>" + ReachPostsCh + "</small>")
            .css("color",color);

        color = "#d9534f";
        var OrganicImpressions   = weekly["Weekly Organic impressions"].values[2].value;
        var OrganicImpressionsCh = OrganicImpressions - weekly["Weekly Organic impressions"].values[0].value;
        if(OrganicImpressionsCh>0) {
            OrganicImpressionsCh = "&uArr;" + OrganicImpressionsCh;
            color = "#5cb85c";
        } else if(OrganicImpressionsCh==0){
            color = "#7B7B7B";
        }else {
            OrganicImpressionsCh = "&dArr;"+Math.abs(OrganicImpressionsCh);
        }
        $(".oi").text(OrganicImpressions + " ")
            .append("<small>" + OrganicImpressionsCh + "</small>")
            .css("color", color);

        color = "#d9534f";
        var NumberOfPosts   = weekly["Weekly Number of posts made by the admin"].values[2].value;
        var NumberOfPostsCh = NumberOfPosts - weekly["Weekly Number of posts made by the admin"].values[0].value;
        if(NumberOfPostsCh>0) {
            NumberOfPostsCh = "&uArr;" + NumberOfPostsCh;
            color = "#5cb85c";
        } else if(NumberOfPostsCh==0){
            color = "#7B7B7B";
        }else {
            NumberOfPostsCh = "&dArr;"+Math.abs(NumberOfPostsCh);
        }
        $(".nopm").text(NumberOfPosts + " ")
            .append("<small>" + NumberOfPostsCh + "</small>")
            .css("color", color);

        color = "#d9534f";
        var OrganicReach   = weekly["Weekly Organic Reach"].values[2].value;
        var OrganicReachCh = OrganicReach - weekly["Weekly Organic Reach"].values[0].value;
        if(OrganicReachCh>0) {
            OrganicReachCh = "&uArr;" + OrganicReachCh;
            color = "#5cb85c";
        } else if(OrganicReachCh==0){
            color = "#7B7B7B";
        }else {
            OrganicReachCh = "&dArr;"+Math.abs(OrganicReachCh);
        }
        $(".or").text(OrganicReach + " ")
            .append("<small>" + OrganicReachCh + "</small>")
            .css("color", color);

    }

});

$(function(){
    social.emit("Ready",{userInfo:1});
    social.emit("FB_Insights");
});

function getReplies(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_PRIMARY,
        cssClass:"repliesDialog",
        title: 'Conversation',
        message: '<div id="convoPromt_'+ id +'" style="height:70vh;overflow:auto;padding:20px" ></div>',
        size :  'size-wide',
        onhide: function(dialogRef){
            $('#convoPromt_'+ id).remove();
            return true;
        },
        onshown: function(dialogRef){
            social.emit("TW_getreplies",id);
        }
    });

}

social.on("status", function (data) {
    if (data.redirectUrl) {
        return window.location.href = data.redirectUrl;
    }
    if(data.indexOf("Twitter")!=-1){
        social.emit("TW_GetMentions");
        social.emit("TW_Posted");
    }
    if(data.indexOf("Facebook")!=-1){
        social.emit("FB_Posted");
    }
    if(data.indexOf("Instagram")!=-1){
        social.emit("IT_Posted");
    }
    postDisable(data);
    social.emit("getPendingApprovalPosts");
    social.emit("getNeedingApprovalPosts");
    social.emit("getScheduledPosts");
    social.emit("getRejectedPosts");
});

social.on("TW_Mentions", function (data) {
    var counts = {r:0,un:0,ur:0};
    data.forEach(function(d){
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d,'mentions');
        counts = addNotification('mentions', d.id_str,d, counts,"TW");
    });
    $(".urgentNum .num").text(counts.ur);
    $(".readNum .num").text(counts.r);
    $(".unreadNum .num").text(counts.un);
});

social.on("notificationType",function(id,network,res){
    var translation = {
        FB : 1,
        TW : 2,
        IT : 3,
        FS : 4
    };
    var el = $("#contain_"+id+"[data-network="+translation[network]+"]").find(".Notification");
    el
        .removeClass("unreadNotification")
        .removeClass("urgentNotification")
        .find('.fa')
        .remove();

    el
        .find('svg')
        .remove();
    if(res){
        el
            .addClass("readNotification")
            .append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"'
            + 'width="21.41756px" height="18px" fill="white" viewBox="0 0 495.086 416.086" enable-background="new 0 0 495.086 416.086"'
            + 'xml:space="preserve">'
            + '<g id="Layer_1">'
            + '</g>'
            + '<g id="Layer_2">'
            + '<polygon fill="inherit" points="193.806,284.721 104.574,340.123 15.341,395.523 15.341,284.721 15.341,173.917 104.574,229.318 '
            + '"/>'
            + '<polygon fill="inherit" points="295.341,285.054 384.573,340.456 473.806,395.856 473.806,285.054 473.806,174.25 384.573,229.651 '
            + '"/>'
            + '<path fill="none" stroke="inherit" stroke-miterlimit="10" d="M-198.435-6.913"/>'
            + '<path fill="inherit" d="M32,395.488"/>'
            + '<polygon fill="inherit" points="450.532,396.523 34,396.523 203.243,288 281.494,288 	"/>'

            + '<rect x="121.981" y="-33.298" transform="matrix(0.5839 0.8118 -0.8118 0.5839 142.7655 -66.5833)" fill="inherit" width="28.707" height="278.547"/>'
            + '<polygon fill="inherit" points="456.898,197.664 474.624,173.017 240.651,13.271 222.924,37.918 	"/>'
            + '</g>'
            + '</svg>');
        $(".readNum .num").text(parseInt($(".readNum .num").text())+1);
    }else{
        el
            .addClass("urgentNotification")
            .append("<i class='fa fa-exclamation-circle'></i>")
            .on('click',function() {
                //$(this).off('click');
                var id = $(this).parent().find(".post").attr('id');
                social.emit("markRead", id, network);
            });

        $(".urgentNum .num").text(parseInt($(".urgentNum .num").text())+1);
    }
});

social.on("savedNotification",function(id,network,res){
    var translation = {
        FB : 1,
        TW : 2,
        IT : 3,
        FS : 4
    };
    if(res){
        var el = $("#contain_"+id+"[data-network="+translation[network]+"]").find(".Notification");
        var type=0;
        if(el.hasClass("unreadNotification")){
            type = 1;
        }
        el
            .removeClass("unreadNotification")
            .removeClass("urgentNotification")
            .find('.fa')
            .remove();

        el
            .find('svg')
            .remove();
        el
            .off('click')
            .addClass("readNotification")
            .append(
                '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"'
                + 'width="21.41756px" height="18px" fill="white" viewBox="0 0 495.086 416.086" enable-background="new 0 0 495.086 416.086"'
                + 'xml:space="preserve">'
                + '<g id="Layer_1">'
                + '</g>'
                + '<g id="Layer_2">'
                + '<polygon fill="inherit" points="193.806,284.721 104.574,340.123 15.341,395.523 15.341,284.721 15.341,173.917 104.574,229.318 '
                + '"/>'
                + '<polygon fill="inherit" points="295.341,285.054 384.573,340.456 473.806,395.856 473.806,285.054 473.806,174.25 384.573,229.651 '
                + '"/>'
                + '<path fill="none" stroke="inherit" stroke-miterlimit="10" d="M-198.435-6.913"/>'
                + '<path fill="inherit" d="M32,395.488"/>'
                + '<polygon fill="inherit" points="450.532,396.523 34,396.523 203.243,288 281.494,288 	"/>'

                + '<rect x="121.981" y="-33.298" transform="matrix(0.5839 0.8118 -0.8118 0.5839 142.7655 -66.5833)" fill="inherit" width="28.707" height="278.547"/>'
                + '<polygon fill="inherit" points="456.898,197.664 474.624,173.017 240.651,13.271 222.924,37.918 	"/>'
                + '</g>'
                + '</svg>'
            );

        $(".readNum .num").text(parseInt($(".readNum .num").text())+1);
        if(type){
            $(".unreadNum .num").text(parseInt($(".unreadNum .num").text())-1);
        }else {
            $(".urgentNum .num").text(parseInt($(".urgentNum .num").text())-1);
        }
    }
});

social.on("TW_Posted", function (data) {
    var counts = {r:0,un:0,ur:0};
    data.forEach(function(d){
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d,'you #TW ');

    });
    $("#you #TW .network-type").css("right","0")
});

social.on("IT_YouFeed", function (data) {
    console.log("IT Posted : ",data);
    data.forEach(function(d){
        d.CommonCreateDate = new Date(moment(d.created_time, "X").format("YYYY-MM-DD HH:mm:ss"))
        Create_Insta_Post(d,'you #IT');
    });
});


social.on("FB_Posted", function (data) {
    data.forEach(function(d){
        d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"))
        Create_FB_Post(d,'you #FB');
    });
});
/*
social.on("Retweets", function (id,data) {
    console.log("id - ",id);
    console.log("data - ",data);
    data.forEach(function(d) {
        $("#" + id)
            .find(".retweets")
            .parent()
            .show()
            .find(".retweets")
            .append(

                "<div class='col-md-12 marginBottom'>" +
                "<div class='media'>" +
                "<div class='media-left'>" +
                "<a>" +
                "<img class='media-object' src='"
                + d.user.profile_image_url_https
                + "' alt='...'>" +
                "</a>" +
                "</div>" +
                "<div class='media-body'>" +
                "<h4 class='media-heading'>"
                + d.user.name + "</h4>" +
                "@" + d.user.screen_name +
                "<button id='" + d.user.id_str+ "' class='pull-right btn follow' " +
                "onclick='follow(\""+ d.user.id_str+"\",\"TW\")'>" +
                "<i class='fa fa-user-plus'></i>" +
                " Follow</button> </div>" +
                "</div>" +
                "</div>"
            );
        if(d.user.following){
            $("#"+d.user.id_str+".btn.follow")
                .addClass("following")
                .removeClass("follow")
                .text("Following");
        }
    });
});*/

social.on("rejectedPosts", function (data) {
    console.log("rejectedPosts : ",data);
    if(data.length){
        $("#tab5 .nopost").hide();
    } else {
        $("#tab5 .nopost").show();
    }
    data.forEach(function(d){
        switch(d.network) {
            case 0 :
                d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"))
                Create_FB_CD_Post(d, 'FB_rejected');
                break;
            case 1 :
                d.CommonCreateDate = new Date(d.created_at);
                Create_Tw_CD_Post(d, 'TW_rejected');
                break;
            case 2 :
                d.CommonCreateDate = new Date(moment(d.created_time, "X").format("YYYY-MM-DD HH:mm:ss"))
                Create_Insta_CD_Post(d, 'IT_rejected');
                break;
        }
    });
});

social.on("scheduledPosts", function (data) {
    console.log("scheduledPosts : ",data);
    $(".schcount").text(data.length);
    if(data.length){
        $("#tab2 .nopost").hide();
    } else {
        $("#tab2 .nopost").show();
    }
    data.forEach(function(d){
        switch(d.network) {
            case 0 :
                d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"))
                Create_FB_CD_Post(d, 'FB_sch');
                break;
            case 1 :
                d.CommonCreateDate = new Date(d.created_at);
                Create_Tw_CD_Post(d, 'TW_sch');
                break;
            case 2 :
                d.CommonCreateDate = new Date(moment(d.created_time, "X").format("YYYY-MM-DD HH:mm:ss"))
                Create_Insta_CD_Post(d, 'IT_sch');
                break;
        }
    });
});

social.on("needingApprovalPosts", function (data) {
    console.log("needingApprovalPosts : ",data);
    $(".needapprovalcount").text(data.length);
    if(data.length){
        $("#tab3 .nopost").hide();
    } else {
        $("#tab3 .nopost").show();
    }
    data.forEach(function(d){
        switch(d.network) {
            case 0 :
                d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"))
                Create_FB_CD_Post(d, 'FB_need',true);
                break;
            case 1 :
                d.CommonCreateDate = new Date(d.created_at);
                Create_Tw_CD_Post(d, 'TW_need',true);
                break;
            case 2 :
                d.CommonCreateDate = new Date(moment(d.created_time, "X").format("YYYY-MM-DD HH:mm:ss"))
                Create_Insta_CD_Post(d, 'IT_need',true);
                break;
        }
    });

});

social.on("pendingApprovalPosts", function (data) {
    console.log("pendingApprovalPosts : ",data);
    $(".pendingapprovalcount").text(data.length);
    if(data.length){
        $("#tab4 .nopost").hide();
    } else {
        $("#tab4 .nopost").show();
    }
    data.forEach(function(d){
        switch(d.network) {
            case 0 :
                d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"))
                Create_FB_CD_Post(d, 'FB_pending');
                break;
            case 1 :
                d.CommonCreateDate = new Date(d.created_at);
                Create_Tw_CD_Post(d, 'TW_pending');
                break;
            case 2 :
                d.CommonCreateDate = new Date(moment(d.created_time, "X").format("YYYY-MM-DD HH:mm:ss"))
                Create_Insta_CD_Post(d, 'IT_pending');
                break;
        }
    });
});


social.on("InReplyTweets", function (data) {
    console.log(arguments);
    for(var i = data.length-1;i>-1;i--){
        var d = data[i];
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d,'convoPromt_'+ data[0].id_str,true)
    }
});

social.on('approvedPost',function(id){
    $("#TW_need").empty();
    $("#FB_need").empty();
    $("#IT_need").empty();
    $("#TW_sch").empty();
    $("#FB_sch").empty();
    $("#IT_sch").empty();
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Approved Successfully'
    });
    social.emit("getNeedingApprovalPosts");
    social.emit("getScheduledPosts");
});

social.on('rejectedPost',function(id){
    $("#TW_need").empty();
    $("#FB_need").empty();
    $("#IT_need").empty();
    $("#TW_rejected").empty();
    $("#FB_rejected").empty();
    $("#IT_rejected").empty();
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Rejected Successfully'
    });
    social.emit("getNeedingApprovalPosts");
    social.emit("getRejectedPosts");
});

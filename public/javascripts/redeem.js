/**
 * Created by Sundeep on 3/9/2015.
 */

function validateEmail(email) {
    var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return re.test(email);
}

var email = function(code){
    var id = $('input#email').val();
    var flag = 0;
    if(id==''){
        return alert('Please Enter a email');
    }
    if(!validateEmail(id)){
        r = confirm("Email entered doesn't appear valid. Are you sure you want to continue?");
        if(!r){
            return;
        }
    }
    $("#sending").remove();
    $("#qr").append('<h2 id="sending">Sending Email...</h2>')
    $.post('/redeem/email',{email:id,code:code},function(data,status){
        $("#sending").empty();
        if(data){
            $("#sending").text("Email Sent");
            $('input#email').val("");
        }else{
            $("#sending").text("Could not send Email. Verify email and try again");
        }
    });
};
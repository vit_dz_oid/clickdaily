﻿social.on('FS_VenueFeed',function(tips){
    $("#group4 .AddButton").remove();
    console.log("Tips - ", tips);
    tips.forEach(function (d, i) {
        d.CommonCreateDate = new Date(moment(moment.unix(parseInt(d.createdAt)).format("YYYY MM DD HH:mm:ss +ZZ").split(" +")[0] + " +0000", "YYYY MM DD HH:mm:ss ZZ").format("YYYY-MM-DD HH:mm:ss"));
        Create_Fs_Post(d, "group4");
    });
    //makeLoadMore("group4","FS");
    $('.icon').find('a').tooltip({
        'placement': 'top'
    });
});
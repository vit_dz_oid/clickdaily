﻿var FBFeed = {};
var TWFriends = {};
var ITFriends = {};
var FSFeed = {};
var counts = {
    FB : 1,
    TW : 2,
    IT : 3,
    FS : 4
};
function makeContainer(ref,id,network,name,following,follower){
    var container = $(
        "<div " +
        "class='col-md-4 col-lg-3 col-sm-6 col-xs-12 friend-container' " +
        " data-name='"+name +"' " +
        " data-network='"+counts[network]+
        "' data-following='"+(following?true:false) +
        "' data-follower='"+follower +"'>" +
        "<div class='row nomargin'>" +
        "<div id='" + id + "' class ='post "+network +"'>" +
        "</div>"+
        "</div>"+
        "</div>");
    $("#" + ref).append(container);
    return container;
    //$('#group').shuffle('appended',container);
}

function makeHead(ref,id,picture,from,handle,location,network,profile_url){
    var networkIcon = "";
    switch(network){
        case "IT" :
            networkIcon = "<img class='img-responsive' src='/images/insta.png'>";
            break;
        case "FB" :
            networkIcon = "<i class='fa fa-facebook'></i>";
            break;
        case "FS" :
            networkIcon = "<i class='fa fa-foursquare'></i>";
            break;
        case "TW" :
            networkIcon = "<i class='fa fa-twitter'></i>";
            break;
        default : break;
    }
    $("#" + ref + " #" + id).append(
        "<div class='network-type'>" +
        networkIcon +
        "</div>"
        +'<div class="row">'
        +'<div class="media header">'
        +'<div class="media-left">'
        +'<a href="'
        +(network=="TW"?profile_url:'#')
        +'" class="profile-pic">'
        +'<img class="media-object' +
        (network=="IT"?' img-circle':'') +
        '" src="'
        +picture
        +'" alt="ProfilePic">'
        +'</a>'
        +'</div>'
        +'<div class="media-body">'
        +'<h4 class="media-heading">'
        +'<strong  class="name"><a href="'
        +(network=="TW"?profile_url:'#')
        +'" >'+from+'</a></strong>'
        +'<br/>'
        +(handle?
        '<small class="handle"><a href="'
        +(network=="TW"?profile_url:'#')
        +'" >'+handle+'</a></small>'
        + '<br/>':'')
        +(location?'<small class="time">'
        + location
        +'</small>':'')
        +'</h4>'
        +'</div>'
        +'</div>'
        +'</div>'
    );
}

function addDescription(ref,id,text,network){
    $("#" + ref + " #" + id).append(
        '<div class="row">'
        +'<div class="col-md-12">'
        +'<h4 class="content">'
        +twemoji.parse(text)
        +'</div>'
        +'</div>'
    );
    $("#" + ref + " #" + id + " .row .col-md-12 .content").linkify();
}

function addFollow(ref,id,network,following){
    if(!following){
        $("#" + ref + " #" + id).append(
            '<div class="row follow">'
            +'<div class="pull-right">'
            +'<button class="btn follow" type="button" onclick="follow(\''+id+'\',\''+network+'\')"><i class="fa fa-user-plus"></i> Follow</button>'
            + '</div>'
            +'</div>'
        );
    }else{
        $("#" + ref + " #" + id).append(
            '<div class="row follow">'
            +'<div class="pull-right">'
            +'<button class="btn btn-success following" type="button" onclick="unfollow(\''+id+'\',\''+network+'\')"><i class="fa fa-check"></i> Following</button>'
            + '</div>'
            +'</div>'
        );
    }
}

function changeFollow(ref,id,network){
    $("#" + ref + " #" + id)
        .find(".row.following")
        .find("button")
        .removeClass("follow")
        .addClass("following")
        .attr("onclick","unfollow('"+id+"','"+network+"')")
        .find("i.fa")
        .removeClass("fa-user-plus")
        .addClass("fa-check");
    $("#" + ref + " #" + id).parent().parent().data("following",true);
}

function changeFollower(ref,id,network){
    $("#" + ref + " #" + id).parent().parent().data("follower",true);
}

function addBanner(ref,id,pic,network,profile_url){
    if(pic){
        $("#" + ref + " #" + id).append(
            '<div class="row banner">'
            +'<div class="col-md-12">'
            +'<a href="'
            +(network=="TW"?profile_url:'#')
            +'">'
            +'<img src="'
            + pic
            +'" class="img-rounded img-responsive post-pic">'
            +'</a>'
            +'</div>'
            +'</div>'
        )
    }else{
        $("#" + ref + " #" + id).append(
            '<div class="row banner">'
            +'<div class="col-md-12">'
            +'<a href="'
            +(network=="TW"?profile_url:'#')
            +'" class="'+network+' post-pic img-responsive">'
            +'</a>'
            +'</div>'
            +'</div>'
        )
    }

}

function makeFooter(ref,id,friends,followers,network){
    var begin = ' <hr>'
        +'<div class="row">'
        +'<div class="col-md-12 footer">';

    var left = '<div class="pull-left ">'
        +'<span class="like-info">';

    switch(network){
        case 'TW' :
        /*case 'IT' :*/
            left = left
            +'<strong>'
            +'<span class="like-number">'
            +(friends?friends:0)
            +'</span>'
            +' FOLLOWING '
            +'<span class="comment-number">'
            +(followers?followers:0)
            +'</span> '
            +'FOLLOWERS'
            +'</strong>';
            break;
    }

    left = left+'</span>'
        +'</div>';

    var right = '<div class="pull-right follows ">';

    right = right + '</div>';

    var end = '</div>'
        +'</div>';

    $("#" + ref + " #" + id).append(begin+left+right+end);
}

function Create_Tw_Friend(d,type,refDiv) {
    if(d.name){
        if(!TWFriends[d.id_str]){
            TWFriends[d.id_str] = d;
            var container = makeContainer(refDiv, d.id_str,"TW",d.name,d.following,type == "follower");
            addBanner(refDiv,d.id_str,d.profile_banner_url,"TW","https://twitter.com/" + d.screen_name);
            makeHead(refDiv,d.id_str,d.profile_image_url_https
                ,d.name,'&#64;' + d.screen_name, d.location,"TW","https://twitter.com/" + d.screen_name);


            if (typeof (d.description) != "undefined"){
                addDescription(refDiv,d.id_str,d.description,"TW");
            }
            if(!d.following){
                addFollow(refDiv,d.id_str,"TW");
            }else{
                addFollow(refDiv,d.id_str,"TW",true);
            }
            makeFooter(refDiv,d.id_str,d.friends_count,d.followers_count,"TW");
            if (type == "follower") {
                $("#" + d.id).find('.follows').empty();
                $("#" + d.id).find('.follows').append("<i class='fa fa-check-square'></i>&nbsp;" +
                "<strong>Follows You</strong>")
            }
            return container;
        }else{
            if(d.following){
                changeFollow(refDiv,d.id_str,"TW");
            }
            if(type=="follower"){
                $("#"+ d.id_str).find('.follows').empty();
                $("#"+ d.id_str).find('.follows').append("<i class='fa fa-check-square'></i>&nbsp;" +
                "<strong>Follows You</strong>");
                changeFollower(refDiv,d.id_str,"TW");
            }
        }
    }

}

function Create_It_Friend(d,type,refDiv) {
    if(d.full_name) {
        if (!ITFriends[d.id]) {
            ITFriends[d.id] = d;
            var container = makeContainer(refDiv, d.id, "IT", d.full_name,d.following,type == "follower");
            addBanner(refDiv, d.id, false, "IT");
            makeHead(refDiv, d.id, d.profile_picture
                , d.full_name, '&#64;' + d.username, false, "IT");


            /* if (typeof (d.description) != "undefined"){
             addDescription(refDiv,d.id,d.description,"IT");
             }*/
            if (!d.following) {
                addFollow(refDiv, d.id, "IT");
            }else{
                addFollow(refDiv, d.id, "IT",true);
            }
            makeFooter(refDiv, d.id, false, false, "IT");
            if (type == "follower") {
                $("#" + d.id).find('.follows').empty();
                $("#" + d.id).find('.follows').append("<i class='fa fa-check-square'></i>&nbsp;" +
                "<strong>Follows You</strong>")
            }
            return container;
        } else {
            if(d.following){
                changeFollow(refDiv,d.id,"IT");
            }
            if (type == "follower") {
                $("#" + d.id).find('.follows').empty();
                $("#" + d.id).find('.follows').append("<i class='fa fa-check-square'></i>&nbsp;" +
                "<strong>Follows You</strong>")
            }
        }
    }
}
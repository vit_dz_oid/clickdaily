/**
 * Created by Sundeep on 2/20/2015.
 */

var social = io.connect('/social');

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#previewImage').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$(function() {
    $("#imgInp").change(function () {
        readURL(this);
    });

    $("select#discount").change(function () {
        $("#preview").val("First "+ $("select#userCount").val() +" Users to like/retweet this post get "+ $("select#discount").val() +"% off. Click the link to claim");
    });
    $("select#userCount").change(function () {
        $("#preview").val("First "+ $("select#userCount").val() +" Users to like/retweet this post get "+ $("select#discount").val() +"% off. Click the link to claim");
    });
});

function post(obj) {
    var file     = obj.find("#imgInp")[0].files[0];
    var formData = obj.serializeArray();
    var formObject = {};
    formData.forEach(function(d,i){
       formObject[d.name] = d.value;
    });
    var stream = ss.createStream();
    if(!file){
        return alert('Please Attach a Picture');
    }
    ss(social).emit('loyalty_post',stream, {size: file.size,form:formObject,type:file.type});
    ss.createBlobReadStream(file).pipe(stream);
    $("#FB_post span.post-msg-processing").show();
    if($("#FB_post span.post-msg-fail")){
        $("#FB_post span.post-msg-fail").hide();
    }
    $("#TW_post span.post-msg-processing").show();
    if($("#TW_post span.post-msg-fail")){
        $("#TW_post span.post-msg-fail").hide();
    }
}

social.on("TW_PostSuccessful",function(){
    console.log("TW_PostSuccessful");
    if($("#TW_post span.post-msg-processing")) {
        $("#TW_post span.post-msg-processing").hide();
    }
    if($("#TW_post span.post-msg-fail")){
        $("#TW_post span.post-msg-fail").hide();
    }
    if($("#TW_post span.post-msg-success")){
        $("#TW_post span.post-msg-success").show();
        setTimeout(function(){
            $("#TW_post span.post-msg-success").hide();
        },2*1000)
    }
});

social.on("FB_PostSuccessful",function(){
    console.log("FB_PostSuccessful");
    if($("#FB_post span.post-msg-processing")){
        $("#FB_post span.post-msg-processing").hide();
    }
    if($("#FB_post span.post-msg-fail")){
        $("#FB_post span.post-msg-fail").hide();
    }
    if($("#FB_post span.post-msg-success")){
        $("#FB_post span.post-msg-success").show();
        setTimeout(function(){
            $("#FB_post span.post-msg-success").hide();
        },2*1000)
    }
});

social.on("TW_PostUnsuccessful",function(){
    console.log("TW_PostUnsuccessful");
    if($("#TW_post span.post-msg-processing")) {
        $("#TW_post span.post-msg-processing").hide();
    }
    if($("#TW_post span.post-msg-fail")) {
        $("#TW_post span.post-msg-fail").show();
    }
});

social.on("FB_PostUnsuccessful",function(){
    console.log("FB_PostUnsuccessful");
    if($("#FB_post span.post-msg-processing")){
        $("#FB_post span.post-msg-processing").hide();
    }
    if($("#FB_post span.post-msg-fail")){
        $("#FB_post span.post-msg-fail").show();
    }
});

/**
 * Created by Sundeep on 4/1/2015.
 */
var status = 8;

function allDone(){
    if(status>0){
        console.log("Still Loading");
    }else{
        $("#loading").remove();
        console.log("All Done");
    }
}

var social = io.connect('/social');
$(function () {
    social.emit("GetFriends");
    var $grid = $('#group');

    $grid.shuffle({
        itemSelector: '.friend-container',
        speed: 750,
        initialSort : {
            by: function($el) {
                return $el.data('name');
            }
        },
        easing: ''
    });
    var overPos = $grid.position();
    $("body").append("<div id='loading'></div>")
    $("#loading")
        .css("top",(overPos.top +45 )+"px")
        .css("left",overPos.left+"px")
        .css("height",(window.innerHeight - overPos.top)+"px")
        .css("width",window.innerWidth-60 + "px")
        .append(loader);
    $grid.on('layout.shuffle', function() {
        allDone();
    });

    $("input[name=sort-options]:radio").on('change', function() {
        var sort = this.value,
            opts = {};
        if(this.checked) {
            $(this).parent().siblings().each(function (index, el) {
                $(el).removeClass('btn-success');
                $(el).addClass('btn-danger');
            });
            $(this).parent().addClass('btn-success');
            $(this).parent().removeClass('btn-danger');
        }
        // We're given the element wrapped in jQuery
        if ( sort === 'dsc' ) {
            opts = {
                reverse: true,
                by: function($el) {
                    return $el.data('name');
                }
            };
        } else if ( sort === 'asc' ) {
            opts = {
                by: function($el) {
                    return $el.data('name');
                }
            };
        }

        // Filter elements
        $('#group').shuffle('sort', opts);
    });
    $("input[name=filter-options]:radio").on('change', function() {
        if(this.checked) {
            $(this).parent().siblings().each(function (index, el) {
                $(el).removeClass('btn-success');
                $(el).addClass('btn-danger');
            });
            $(this).parent().addClass('btn-success');
            $(this).parent().removeClass('btn-danger');
        }
        $('#group').shuffle('shuffle', function($el, shuffle) {
            if(($el.data('network')==1)&&$("input.network.fb").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==2)&&$("input.network.tw").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==3)&&$("input.network.it").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==4)&&$("input.network.fs").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            return false;
        });
    });
    $("input.network").on('change', function() {
        if(this.checked){
            $(this).parent().removeClass('btn-danger');
            $(this).parent().addClass('btn-success');
        }else {
            $(this).parent().removeClass('btn-success');
            $(this).parent().addClass('btn-danger');
        }
        $('#group').shuffle('shuffle', function($el, shuffle) {
            if(($el.data('network')==1)&&$("input.network.fb").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==2)&&$("input.network.tw").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==3)&&$("input.network.it").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            if(($el.data('network')==4)&&$("input.network.fs").prop("checked")){
                return followFilter($el.data('following'),$el.data('follower'))
            }
            return false;
        });
    });
    social.emit("Ready",{userInfo:1});
});

function followFilter(following,follow){
    var filterOpt = $("input[name=filter-options]:checked").val();
    switch(filterOpt){
        case "eve" :
            return true;
            break;
        case "yfl" :
            return following;
            break;
        case "fly" :
            return follow;
            break;
        case "ydf" :
            return !following;
            break;
        case "nfy" :
            return !follow;
            break;
        case "feo" :
            return follow&following;
            break;
        default :
            return false;
    }
    return false;
}

var follow = function (id,network) {
  console.log("id", id, " network",network);
  switch(network){
      case 'IT': social.emit("it_follow",id);
          break;
      case 'TW': social.emit("TW_follow",id);
          break;
  }
};


social.on("tw_friends",function(list){
    console.log("tw_friends",list);
    var added = false;
    for(key in list){
        if(!added) {
            added = $(Create_Tw_Friend(list[key], "friend","group"));
        }else {
            var selector = Create_Tw_Friend(list[key], "friend","group");
            added = added.add(selector);
        }
    }
    $('#group').shuffle('appended',added);
    status = status -1;
    imagesLoaded( document.querySelector('#group'), function( instance ) {
        status = status -1;
        $('#group').shuffle('update');
    });
    $(".row.banner").find('img').each(function(i,el){
        $(el).error(function(){
            console.log("error : ",$(this));
            $(this).parent().addClass('post-pic');
            $(this).parent().addClass('img-responsive');
            $(this).remove();
        }) ;
    });
});


social.on("it_friends",function(list){
    console.log("it_friends",list);
    var added = false;
    for(key in list){
        list[key].following = true;
        if(!added) {
            added = $(Create_It_Friend(list[key], "friend","group"));
        }else{
            var selector = Create_It_Friend(list[key], "friend","group");
            added = added.add(selector);
        }
    }
    status = status -1;
    $('#group').shuffle('appended',added);
    imagesLoaded( document.querySelector('#group'), function( instance ) {
        status = status -1;
        $('#group').shuffle('update');
    });
    $(".row.banner").find('img').each(function(i,el){
        $(el).error(function(){
            console.log("error : ",$(this));
            $(this).parent().addClass('post-pic');
            $(this).parent().addClass('img-responsive');
            $(this).remove();
        }) ;
    });
});

social.on("tw_follwers",function(list){
    console.log("tw_follwers",list);
    var added = false;
    for(key in list){
        if(!added){
            added = $(Create_Tw_Friend(list[key],"follower", "group"));
        }else{
            var selector = Create_Tw_Friend(list[key],"follower", "group");
            added = added.add(selector);
        }
    }
    $('#group').shuffle('appended',added);
    status = status -1;
    imagesLoaded( document.querySelector('#group'), function( instance ) {
        status = status -1;
        $('#group').shuffle('update');
    });
    $(".row.banner").find('img').each(function(i,el){
        $(el).error(function(){
            console.log("error : ",$(this));
            $(this).parent().addClass('post-pic');
            $(this).parent().addClass('img-responsive');
            $(this).remove();
        }) ;
    });
});

social.on("it_follwers",function(list){
    console.log("it_follwers",list);
    var added = false;
    for(key in list){
        if(!added){
            added = $(Create_It_Friend(list[key],"follower", "group"));
        }else{
            var selector = Create_It_Friend(list[key],"follower", "group");
            added = added.add(selector);
        }
    }
    status = status -1;
    $('#group').shuffle('appended',added);
    imagesLoaded( document.querySelector('#group'), function( instance ) {
        status = status -1;
        $('#group').shuffle('update');
    });
    $(".row.banner").find('img').each(function(i,el){
        $(el).error(function(){
            console.log("error : ",$(this));
            $(this).parent().addClass('post-pic');
            $(this).parent().addClass('img-responsive');
            $(this).remove();
        }) ;
    });
});


social.on("status",function(data){
    if(data.redirectUrl){
        return window.location.href=data.redirectUrl;
    }
    //postDisable(data);
    if(data.indexOf("Twitter")==-1){
        status = status - 4;
    }
    if(data.indexOf("Instagram")==-1){
        status = status - 4;
    }

    allDone();
});
﻿// -----------------------------------------------------------------
//                              Helpers
// -----------------------------------------------------------------

function showSelectPages(){
    showOverlay();
    $(".viewer")
        .append("<div id='FbOverlay'></div>")
        .find("#FbOverlay")
        .append("<img id='profilePic'>")
        .find("img")
        .attr("src", userInfo.FB.profile_pic);
    social.emit('FB_SendUnselectedPages');
}

function showFbUsersPages(data) {
    $("#PagesInfo").remove();
    data.forEach(function(d,i){
        if ($("#PagesInfo").length == 0) {
            $("#FbOverlay").append("<br/><div id='PagesInfo'><br / ></div>");
        }
        $("#PagesInfo").append("<div class='pageSelector' id='" +d.id + "'></div>")
            .find("#" + d.id).append("<img class='pageSelectorPic'>")
            .append("<a href='" + d.link + "'><H4>" + d.name + "</H4></a>")
            .append("<button onclick='SelectPage($(this).parent().attr(\"id\"))'>" +
            "Add Account</button><br/>")
            .find("img")
            .attr("src", d.profile_pic);
    });
}


function likeFbPost( id) {
    social.emit('FB_Like',id);
}

function showFbComment(id) {
    $("#" + id + ".fbpost .comment").toggle();
}

function commentFbPost(id, text) {
    social.emit('FB_Comment',id,text);
}

function postFB(obj,start,id,approve) {
    var file = obj.find("#AttachFile")[0].files[0];
    var text = obj.find("textarea").val();
    var stream = ss.createStream();
    var data = {text:text};

    if(start){
        data.dateTime = start.toISOString();
    }
    if(id){
        data.oldId       = id;
    }
    if(file){
        data.size = file.size;
        ss(social).emit('FB_Post',stream, data,approve);
        ss.createBlobReadStream(file).pipe(stream);
    }else{
        data.size = 0;
        ss(social).emit('FB_Post',0,data,approve);
    }
}

function SelectPage(id) {
    hideOverlay();
    social.emit('FB_AddPage',id);
}

// -----------------------------------------------------------------
//                              Socket Ons
// -----------------------------------------------------------------

// Select Page

social.on("FB_SelectPage",function(user_info,page_info){
    console.log('FB_SelectPage',arguments);
    $(".overlay").show();
    $(".viewer").show();
    //$(".viewer").empty();
    if($(".viewer .close" ).length==0){
        $(".viewer").append("<button type='button' class='close' onclick='$(\".viewer\").toggle();$(\".overlay\").toggle();' aria-label='Close' style='top: 0px;right: 0px;width: 20px;height: 20px;padding: 2px;margin: 10px;'><span aria-hidden='true' style='top: 0;'>&times;</span></button>");
    }
    if($(".viewer #FbOverlay").length==0) {
        $(".viewer").append("<div id='FbOverlay'></div>")
    }else{
        $(".viewer #FbOverlay").empty();
    }
    $("#FbOverlay")
        .append("<img id='profilePic'>")
        .append("<a href=\"https://www.facebook.com\\" + user_info.id + "\">" +
        "<span class=\"username\">" + user_info.name + "</span>")
        .find("img")
        .attr("src", userInfo.FB.profile_pic);
    showFbUsersPages(page_info);
});

//TimeLine

social.on('FB_ProfilePic',function(id,url){
    console.log('FB_ProfilePic',arguments);
    console.log('fb_user_PP',arguments);
    if(typeof(picCache[id])==="undefined"){
        picCache[id] = {url:url};
    }else{
        picCache[id].url = url;
    }
});

social.on('FB_CommentSuccesful',function(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Commented Successfully'
    });
    $("#contain_"+id)
        .find(".FB")
        .find(".comment-area")
        .val("");
    /*$("#contain_"+id)
        .find(".FB")
        .find(".comment-msg-success")
        .show("slow");
    setTimeout(function(){
        $("#contain_"+id)
            .find(".FB")
            .find(".comment-msg-success")
            .hide("slow");
    },2000);*/
});
social.on('FB_CommentUnsuccesful',function(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Error!',
        message: 'Comment Failed'
    });
    /*$("#contain_"+id)
        .find(".FB")
        .find(".comment-area")
        .val("");
    $("#contain_"+id)
        .find(".FB")
        .find(".comment-msg-fail")
        .show("slow");*/
});

social.on('FB_LikeSuccesful',function(id){
    $("#contain_"+id)
        .find(".FB")
        .find(".icons")
        .find(".fa-thumbs-up")
        .css("color", "blue");
});


social.on('FB_PageFeed',function(feed){
    var added = [];
    var i = 0;
    feed.forEach(function (d, j) {
        d.sn_type = "Fb";
        d.CommonCreateDate = new Date(moment(d.created_time, "YYYY-MM-DDTHH:mm:ss+ZZ").format("YYYY-MM-DD HH:mm:ss"));
        var created = Create_FB_Post(d, "group");
        if(created){
            if(!added[i]) {
                added[i] = $("#contain_"+ d.id);
            } else{
                var selector = $("#contain_"+ d.id);
                added[i] = added[i].add(selector);
            }
            if(added[i].length >= 10){
                i = i + 1;
            }
        }
    });
    //$('#group').shuffle('appended',added);
    console.log("Facebook Added");
    status = status - 1;
    imagesLoaded( document.querySelector('#group'), function( instance ) {
        status = status - 1;
        $('#group').shuffle('layout');
        console.log("Facebook Images");
    });
    $('.icons').find('a').tooltip({
        'placement': 'top'
    });
    updateFeeds();
});

social.on('FB_ProfilePic',function(id,url){
    console.log('FB_ProfilePic',arguments);
    console.log('fb_user_PP',arguments);
    if(typeof(picCache[id])==="undefined"){
        picCache[id] = {url:url};
    }else{
        picCache[id].url = url;
    }
});
﻿var venueID = ""

function postFS(obj) {
    var file = obj.find("AttachFile")[0].files[0];
    var text = obj.find("textarea").val();
    var fd = new FormData;
    fd.append('photo1', file);
    fd.append('text', text);
    fd.append('venueID', venueID);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (4 == this.readyState) {
            obj.find("textarea").val("");
            obj.find("input").val("");
            console.log(['xhr upload complete', arguments]);
        }
    };
    xhr.open('post', '/FourSquare/CreateTip', true);
    xhr.send(fd);
}

function SelectVenue(id) {
    $(".overlay").hide();
    $(".viewer").hide();
    $(".viewer #FsOverlay").empty();
    social.emit('FS_AddVenue',id);
}
/*
function SelectSelectedVenues(el) {
    $(".overlay").hide();
    $(".viewer").hide();
    $(".viewer #FsOverlay").empty();
    var pages = el.find('input');
    var checked = pages.filter(function(d,i){return i.checked});
    var names = checked.map(function(){return $(this).attr("name")}).get();
    social.emit('FS_AddVenue',names);
}


function SelectAllVenues() {
    $(".overlay").hide();
    $(".viewer").hide();
    $(".viewer #FsOverlay").empty();
    social.emit('FS_AddAllVenues');
}*/


function showFsUsersPages(data) {
    $("#VenueInfo").remove();
    data.forEach(function(d,i){
        if ($("#VenueInfo").length == 0) {
            $("#FsOverlay").append("<br/><div id='VenueInfo'><br / ></div>");
        }
        $("#VenueInfo").append("<div class='venueSelector' id='" +d.id + "'></div>");
        //$("#VenueInfo #" + d.id).append("<input type='checkbox' name='"+d.id+"' style='position: absolute;top: 49px;left: 0px;margin: 10px;' />");
        $("#VenueInfo #" + d.id).append("<img class='venueSelectorPic'>")
        $("#VenueInfo #" + d.id).append("<a href='#'><H4>" + d.name + "</H4></a>");
        $("#VenueInfo #" + d.id).append("<button onclick='SelectVenue($(this).parent().attr(\"id\"))'>Add Venue</button><br/>")
        $("#VenueInfo #" + d.id + " img").attr("src", d.profile_pic);

    });
//$("#VenueInfo").append("<button onclick='SelectAllVenues()' style='margin: 20px;'>Add All Venues</button><button onclick='SelectSelectedVenues($(this).parent())' style='margin: 20px;'>Add Selected Venues</button><br/>")
}

social.on("FS_SelectVenue",function(user_info,page_info){
    console.log('FS_SelectVenue',arguments);
    $(".overlay").show();
    $(".viewer").show();
    //$(".viewer").empty();
    if(($(".viewer .close" )).length==0){
        $(".viewer").append("<button type='button' class='close' onclick='$(\".viewer\").toggle();$(\".overlay\").toggle();' aria-label='Close' style='top: 0px;right: 0px;width: 20px;height: 20px;padding: 2px;margin: 10px;'><span aria-hidden='true' style='top: 0;'>&times;</span></button>");
    }
    if(($(".viewer #FsOverlay" )).length==0) {
        $(".viewer").append("<div id='FsOverlay'></div>")
    }else{
        $(".viewer #FsOverlay").empty();
    }
    $("#FsOverlay").append("<img id='profilePic'>");
    $("#FsOverlay").append("<span class=\"username\">" + user_info.firstName + " " + user_info.lastName + "</span>");
    $("#FsOverlay img").attr("src", user_info.profile_pic);
    showFsUsersPages(page_info);
});

social.on("FS_UserTypeWrong",function(){
    alert('The user type selected is not a personal account. Please Switch to a personal account to use foursquare with clickdaily');
});
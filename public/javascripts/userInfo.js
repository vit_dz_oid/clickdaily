/**
 * Created by Sundeep on 4/14/2015.
 */
var userInfo = {
    FB : {},
    TW : {},
    IT : {},
    FS : {}
};

var social = io.connect('/social');

social.on('FB_PageInfo',function(data){
    if(data) {
        userInfo.FB.profile_pic = data.profile_pic;
        userInfo.FB.name = data.name;
    }
});

social.on('TW_UserInfo',function(data){
    if(data) {
        userInfo.TW.profile_pic = data.profile_pic;
        userInfo.TW.name = data.name;
        userInfo.TW.screen_name = data.screen_name;
        userInfo.TW.followers = data.followers;
        userInfo.TW.friends = data.friends;
    }
});

social.on('IT_UserInfo',function(data){
    if(data) {
        userInfo.IT.profile_pic = data.profile_pic;
        userInfo.IT.name = data.full_name;
        userInfo.IT.username = data.username;
    }
});

social.on('FS_VenueInfo',function(data){
    if(data) {
        userInfo.FS.profile_pic = data.profile_pic;
        userInfo.FS.name = data.name;
    }
});

social.on("locationChange", function (result) {
    if((result)&&(result!=="false")) {
        $.get("changeLocation?id="+result.id, function (data, status) {
            if ((data) && (data !== "false")) {
                location.reload();
            }
        });
    }
});
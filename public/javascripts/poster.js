﻿var posted = [];
var original;

function initPostBar() {
    original = $("input#tw").prop( "checked");
    $("input#tw").click(function(){
        original = $(this).prop( "checked");
    });
    $(".mergedPost").find("textarea").on('change input propertychange paste', function() {
        var count = $(this).val().length;
        $(".postCount.postCountNumber").text(count);
        if(count>140){
            $(".postCount").css("color","red");
            $("input#tw").attr("disabled","disabled");
            $("input#tw").prop( "checked", false );
        }else{
            $(".postCount").css("color","#337ab7");
            $("input#tw").removeAttr("disabled");
            $("input#tw").prop( "checked", original );

        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#mergedPost").find("#AttachFile").change(function () {
        $('#previewContainer').removeClass("hidden");
        readURL(this);
    });
    $('#datetimepicker1').datetimepicker({
        sideBySide: true});
}

$(initPostBar);

function removeImage(){
    $("#mergedPost").find("#AttachFile").val("");
    $("#mergedPost").find("#AttachFile").change();
    $('#previewContainer').addClass("hidden");
}

function shortUrl(el){
    var url = el.val();
    el.css("background-color","");
    social.emit("create_url",url);
    el.attr("disabled","disabled");
    el.val("Processing....");
}

social.on("URL_Created",function(hash,url){
    var el = $("#urlShorten").find("input");
    el.val("");
    el.css("background-color","");
    el.removeAttr("disabled");
    var msg = $("#mergedPost").find("#postText").val();
    if(msg.length){
        msg = msg + " ";
    }
    msg = msg + hash;
    $("#mergedPost").find("#postText").val(msg);
});

social.on("URL_CreateFailed",function(url) {
    var el = $("#urlShorten").find("input");
    el.val(url);
    el.css("background-color","rgba(255, 0, 0, 0.3)");
    el.removeAttr("disabled");
});

social.on("keywords",function(list){
   populateKeywords(list);
});

social.on('FB_PostSuccessful',function(id){
    if(posted.indexOf("FB")!=-1){
        posted.splice(posted.indexOf("FB"),1);
    }
    if(!posted.length){
        $("#mergedPost").find("#AttachFile").val("");
        $("#mergedPost").find("textarea").val("");
        removeImage();
    }
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Posted to Facebook Successfully'
    });
    if($("#FB_post span.post-msg-processing")){
        $("#FB_post span.post-msg-processing").hide();
    }
   /* if($("#FB_post span.post-msg-fail")){
        $("#FB_post span.post-msg-fail").hide();
    }
    if($("#FB_post span.post-msg-success")){
        $("#FB_post span.post-msg-success").show();
        setTimeout(function(){
            $("#FB_post span.post-msg-success").hide();
        },2*1000)
    }*/
});

social.on('FB_PostUnsuccessful',function(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Error!',
        message: 'Posted to Facebook Failed'
    });
    if($("#FB_post span.post-msg-processing")){
        $("#FB_post span.post-msg-processing").hide();
    }
    /*if($("#FB_post span.post-msg-fail")){
        $("#FB_post span.post-msg-fail").show();
    }*/
});

social.on('TW_PostSuccessful',function(id){
    if(posted.indexOf("TW")!=-1){
        posted.splice(posted.indexOf("TW"),1);
    }
    if(!posted.length){
        $("#mergedPost").find("#AttachFile").val("");
        $("#mergedPost").find("textarea").val("");
        removeImage();
    }
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Posted to Twitter Successfully'
    });
    if($("#TW_post span.post-msg-processing")){
        $("#TW_post span.post-msg-processing").hide();
    }
   /* if($("#TW_post span.post-msg-fail")){
        $("#TW_post span.post-msg-fail").hide();
    }
    if($("#TW_post span.post-msg-success")){
        $("#TW_post span.post-msg-success").show();
        setTimeout(function(){
            $("#TW_post span.post-msg-success").hide();
        },2*1000)
    }*/
});

social.on('TW_PostUnsuccessful',function(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Error!',
        message: 'Posted to Twitter Failed'
    });
    if($("#TW_post span.post-msg-processing")){
        $("#TW_post span.post-msg-processing").hide();
    }
    /*if($("#TW_post span.post-msg-fail")){
        $("#TW_post span.post-msg-fail").show();
    }*/
});

social.on('IT_PostSuccessful',function(id){
    if(posted.indexOf("IT")!=-1){
        posted.splice(posted.indexOf("IT"),1);
    }
    if(!posted.length){
        $("#mergedPost").find("#AttachFile").val("");
        $("#mergedPost").find("textarea").val("");
        removeImage();
    }
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Posted to Instagram Successfully'
    });
});

social.on('IT_PostUnsuccessful',function(id){
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Error!',
        message: 'Posted to Instagram Failed'
    });
});

function postDisable(networks) {
    if (networks.indexOf("Twitter") == -1) {
        $("input#tw")
            .attr("disabled","disabled")
            .removeAttr("checked");
        if(typeof(postBar)!= "undefined") {
            postBar
                .find("input#tw")
                .attr("disabled", "disabled")
                .removeAttr("checked");
        }
        $("input.network.tw").parent().hide();
        $(".Add-Networks")
            .removeClass("hidden")
            .show()
            .find(".tw")
            .removeClass("hidden")
            .show();
    } else {
        $("input#tw")
            .attr("checked", "checked")
            .removeAttr("disabled");
        if(typeof(postBar)!= "undefined") {
            postBar
                .find("input#tw")
                .attr("checked", "checked")
                .removeAttr("disabled");
        }
    }

    if (networks.indexOf("Facebook") == -1) {
        $("input#fb")
            .attr("disabled", "disabled")
            .removeAttr("checked");
        if(typeof(postBar)!= "undefined"){
            postBar
                .find("input#fb")
                .attr("disabled", "disabled")
                .removeAttr("checked");
        }

        $("input.network.fb").parent().hide();
        $(".Add-Networks")
            .removeClass("hidden")
            .show()
            .find(".fb")
            .removeClass("hidden")
            .show();
    } else {
        $("input#fb")
            .attr("checked", "checked")
            .removeAttr("disabled");
        if(typeof(postBar)!= "undefined") {
            postBar
                .find("input#fb")
                .attr("checked", "checked")
                .removeAttr("disabled");
        }
    }

    if (networks.indexOf("FourSquare") == -1) {
        $("input.network.fs").parent().hide();
        $(".Add-Networks")
            .removeClass("hidden")
            .show()
            .find(".fs")
            .removeClass("hidden")
            .show();
    }

    if (networks.indexOf("Instagram") == -1) {
        $("input.network.it").parent().hide();
        $(".Add-Networks")
            .removeClass("hidden")
            .show()
            .find(".it")
            .removeClass("hidden")
            .show();
        $("input#it")
            .attr("disabled", "disabled")
            .removeAttr("checked");
        if(typeof(postBar)!= "undefined"){
            postBar
                .find("input#it")
                .attr("disabled", "disabled")
                .removeAttr("checked");
        }
    } else {
        $("input#it")
            .attr("checked", "checked")
            .removeAttr("disabled");
        if(typeof(postBar)!= "undefined") {
            postBar
                .find("input#it")
                .attr("checked", "checked")
                .removeAttr("disabled");
        }
    }

    original = $("input#tw").prop( "checked");
}

/* if (networks.indexOf("FourSquare") == -1) {
 $("input#fs").attr("disabled", "disabled");
 } else {
 $("input#fs").attr("checked", "checked");
 }*/

function populateKeywords(list){
    if((list)&&(list.length)){
        list.forEach(function(d,i){
            if(d.inPostBar){
                d = d.value;
            }else{
                return;
            }
           $("#mergedPost.mergedPost")
                .find(".keywords")
                .append(
                    "<button class='btn btn-small btn-danger " +
                    "type='button' onclick='addKeywords($(this))'>#" +
                    d + "</button><span>&nbsp;</span>"
                );
            if(typeof(postBar)!= "undefined"){
                postBar.find(".keywords")
                    .append(
                    "<button class='btn btn-small btn-danger " +
                    "type='button' onclick='addKeywords($(this))'>#" +
                    d + "</button><span>&nbsp;</span>"
                );
            }

        });
    }
}

function addKeywords(el){
    $("#mergedPost.mergedPost")
        .find("textarea.message")
        .val(
            $("#mergedPost.mergedPost")
                .find("textarea.message")
                .val()
            + " "
            + el.text()
            + " "
        )
        .change()
        .focus();
}

function readpostImage(input,done) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            done(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function createPostObject(twname,twpp,fbname,fbpp,itname,itpp,time,text,input,done){
    var fb = $("input#fb").prop( "checked");
    var tw = $("input#tw").prop( "checked");
    var it = $("input#it").prop( "checked");
    function makePostContainer(ref,id,network,date){
        var networkIcon = "";
        switch(network){
            case "IT" :
                networkIcon = "<img class='img-responsive' src='/images/insta.png'>";
                break;
            case "FB" :
                networkIcon = "<i class='fa fa-facebook'></i>";
                break;
            case "FS" :
                networkIcon = "<i class='fa fa-foursquare'></i>";
                break;
            case "TW" :
                networkIcon = "<i class='fa fa-twitter'></i>";
                break;
            default : break;
        }
        var container = $(
            //" +
            "<div class='row rowPost flipper'>" +
            "<div id=" + id + " class='post front "+network +"'>" +
            "<div class='network-type'>" +
            networkIcon +
            "&nbsp;&nbsp;<a onclick='zoom("+network+"_|_"+id+")'><i class='fa fa-expand'></i>" +
            "</div>" +
            "</div>" +
            "<div class='col-md-12 post back "+network +"'>" +
            "</div>" +
            "</div>" +
            "</div>");
        $("#" + ref).append(container);
        //counts[network]= counts[network]+4;
    }
    if(fb){
        makePostContainer('fbView', "dummyFb", "FB");
        makeHead('fbView', "dummyFb", fbpp, fbname, false, moment(time), "FB");
        addText('fbView', "dummyFb", text, "FB");
    }
    if(tw){
        makePostContainer('twView', "dummyTw", "TW");
        makeHead('twView', "dummyTw", twpp, twname, '&#64;' + userInfo.TW.screen_name, moment(time), "TW");
        addText('twView', "dummyTw", text, "TW");
    }
    if(it){
        makePostContainer('itView', "dummyIt", "IT");
        makeHead('itView', "dummyIt", itpp, itname, false, moment(time), "IT");
    }
    if(($(".mergedPost").find("textarea").val().length > 140)&&(original)){
        makeContainer('twView', "dummyTw", "TW");
        $("#twView").find("#dummyTw").append(
            "<h1>You cannot post more than 140 Chars to twitter</h1>"
        );
    }

    if (input.files && input.files[0]) {
        readpostImage(input, function (url) {
            if(fb) {
                addPic('fbView', "dummyFb", url, "FB");
            }
            if(tw) {
                addPic('twView', "dummyTw", url, "TW");
            }
            if(it) {
                addPic('itView', "dummyIt", url, "IT");
                addText('itView', "dummyIt", text, "IT");
            }
            done()
        });
    }else{
        if(fb) {
            addPic('fbView', "dummyFb", '', "FB");
        }
        if(tw) {
            addPic('twView', "dummyTw", '', "TW");
        }
        if(it) {
            addPic('itView', "dummyIt", '', "IT");
            addText('itView', "dummyIt", text, "IT");
        }
        done();
    }
}

function postAll(start,id,approve) {
    var el = $("#mergedPost.mergedPost");
    if(!start) {
        var postDate = el.find("#datetimepicker1").find("input").val();
        if(postDate!=""){
            start = (new Date(postDate));
        }
    }
    if (el.find("input#fb").is(':checked')) {
        if(posted.indexOf("FB")==-1) {
            posted.push("FB");
        }
        postFB(el,start,id,approve);
        $("#FB_post span.post-msg-processing").show();
        if($("#FB_post span.post-msg-fail")){
            $("#FB_post span.post-msg-fail").hide();
        }
    }
    if (el.find("input#tw").is(':checked')) {
        if(posted.indexOf("TW")==-1){
            posted.push("TW");
        }
        postTW(el,start,id,approve);
        $("#TW_post span.post-msg-processing").show();
        if($("#TW_post span.post-msg-fail")){
            $("#TW_post span.post-msg-fail").hide();
        }
    }
    if (el.find("input#it").is(':checked') && env=="Schedule") {
        postIT(el,start,id,approve);
    }
    if (el.find("input#fs").is(':checked')) {
        postFS(el,start);
    }
}

function showPostConfirm(start, end,done){
    showOverlay();
    //$(".viewer").append($("#mergedPost").parent().detach());
    $(".viewer").append("<br/><H1 class='text-center'>"+ start.format("ddd MMMM Do YYYY, h:mm a") +"</H1><br/>");
    $(".viewer").append(postBar);
    initPostBar();
    $(".viewer").append(
        "<br/>"+
        "<div class='row'>" +
        "<div class='col-md-12'>" +
        "<div id='fbView' class='fbView col-md-4'>" +
        "</div>" +
        "<div id='twView' class='twView col-md-4'>" +
        "</div>" +
        "<div id='itView' class='twView col-md-4'>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "<hr/>"+
        "<div class='row'>" +
        "<div class='text-center'>" +
        "<button id='dummyPost' type='button' class='btn btn-primary'>Post</button>&nbsp;&nbsp;&nbsp;&nbsp;" +
        "<button id='dummyPostApprove' type='button' class='btn btn-warning'>Post for Approval</button>" +
        "<br/>" +
        "</div>"+
        "</div>"
    );
    createPostObject(
        userInfo.TW.name,userInfo.TW.profile_pic
        ,userInfo.FB.name,userInfo.FB.profile_pic
        ,userInfo.IT.name,userInfo.IT.profile_pic
        ,start,$("#postText").val(),$("#AttachFile")[0],function(){
        postBar
            .find("textarea")
            .on('change input propertychange paste', function() {
                var text = $(this).val();
                var fb = $("input#fb").prop( "checked");
                var tw = $("input#tw").prop( "checked");
                if(fb){
                    $('#fbView').find("#dummyFb").find(".content").text(text);
                }
                if(it){
                    $('#itView').find("#dummyIt").find(".content").text(text);
                }
                if(tw) {
                    if (text.length < 140) {
                        $('#twView').find("#dummyTw").show();
                        $('#twView').find(".CharWarning").hide();
                        $('#twView').find("#dummyTw").find(".content").text(text);
                    } else {
                        $('#twView').find("#dummyTw").hide();
                        if (!$('#twView').find(".CharWarning").length) {
                            $('#twView').append("<div class='CharWarning'><H1>You cannot post more than 140 Chars to twitter</H1></div>")
                        }
                        $('#twView').find("#dummyTw").find(".CharWarning").show();

                    }
                }
            });

        postBar.find("#AttachFile").change(function () {
            if (this.files && this.files[0]) {
                readpostImage(this, function (url) {
                    $('#fbView').find("#dummyFb").find(".post-pic").attr("src",url);
                    $('#twView').find("#dummyTw").find(".post-pic").attr("src",url);
                    $('#itView').find("#dummyIt").find(".post-pic").attr("src",url);
                });
            }else{
                $('#fbView').find("#dummyFb").find(".post-pic").attr("src","");
                $('#twView').find("#dummyTw").find(".post-pic").attr("src","");
                $('#itView').find("#dummyIt").find(".post-pic").attr("src","");
            }
        });

        $("input#tw").click(function(){
            $('#twView').toggle();
        });
        $("input#fb").click(function(){
            $('#fbView').toggle();
        });
        $("input#it").click(function(){
            $('#itView').toggle();
        });
        $("button#dummyPost").click(function(){
           var confirm = {
               id       : uuid.v1().replace(/\-/g,''),
               title    : $("#postText").val(),
               file     : $("#AttachFile")[0],
               approve  : false
           };
           done(confirm);
        });
        $("button#dummyPostApprove").click(function(){
            var confirm = {
                id      : uuid.v1().replace(/\-/g,''),
                title   : $("#postText").val(),
                file    : $("#AttachFile")[0],
                approve : true
            };
            done(confirm);
        });
    });
}

function approvePost(id,network){
    $("#"+network+"_need #"+id+" .footer").empty().append("<button class='btn btn-info'>Approving</button>");
    social.emit("approvePost",id);
}

function rejectPost(id,network){
    $("#"+network+"_need #"+id+" .footer").empty().append("<button class='btn btn-info'>Rejecting</button>");
    social.emit("rejectPost",id);
}
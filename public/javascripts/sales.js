/**
 * Created by Sundeep on 1/9/2015.
 */

var socket = io.connect();
socket.on('update', function (data) {
    $("#reports #sr").remove();
    $("#reports #payment").remove();
    $("#reports #taxes").remove();
    $("#reports").append(data);
    $("table").tablesorter();
    $("#update").css("display","none");
});

socket.on("locationChanged",function(){
    var filter = $("#period").value;
    var startDate = new Date();
    var endDate = new Date();
    if(this.value === "0"){
        startDate = new Date($("#period").parent().find("#two input").val());
        endDate = new Date($("#period").parent().find("#three input").val());
    }
   socket.emit("update",filter,startDate,endDate);
});

$(function(){

    $("table").tablesorter();
    $("#period").on('change',function(){
       if(this.value === "0"){
           $(".filterItem input").removeAttr("disabled");
           $("#button-one").css("display","inline-block");
       }else{
           $(".filterItem input").attr("disabled","disabled");
           $("#button-one").css("display","none");
           socket.emit('update', this.value,new Date(),new Date());
           $("#update").css("display","block");
       }
    });

    $("#right-information #user").on('change',function(){
        socket.emit('changeLocation',this.value);
        $("#update").css("display","block");
    });

    $("#button-one").click(function(){
        var startDate = new Date($(this).parent().find("#two input").val());
        var endDate = new Date($(this).parent().find("#three input").val());
        var filterType = $("#period").val();
        console.log(startDate,endDate,filterType);
        if((startDate.getDate())&&(endDate.getDate())){
            socket.emit('update', filterType,startDate,endDate);
        }

    });
});


/**
 * Created by Sundeep on 2/10/2015.
 */
var menu;


var loader = '<div class="board">' +
    '<img src="/images/loading.gif">' +
    '<br/>' +
    '<br/>' +
    '<p><strong>Loading ... </strong></p>' +
    '</div>';

$(function(){
    var w = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    var h = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    $("#container").css("width",w-70+"px");
    $(".scroller").css("height",h-70+"px");
    menu = new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );

    $("#trigger").click(function(){
        if($(this).find(".glyphicon-chevron-right").length) {
            $(this)
                .find(".glyphicon-chevron-right")
                .removeClass("glyphicon-chevron-right")
                .addClass("glyphicon-chevron-left");
        }

    });

    $("#user").change(function () {
       var newLoc = $(this).val();
        $.get("changeLocation?id="+newLoc, function (data, status) {
            if ((data) && (data !== "false")) {
                location.reload();
            }
        });
    });

    $(window).resize(function () {
        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
        var h = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
        $("#container").css("width",w-70+"px");
        $(".scroller").css("height",h-70+"px");
        /*$('#menu').multilevelpushmenu('option', 'menuHeight', $(document).height());
        $('#menu').multilevelpushmenu('redraw');*/
        $(".overlay")
            .css('width',window.innerWidth+"px")
            .css('height',window.innerHeight+"px");
        $(".viewer")
            .css('width',Math.floor(window.innerWidth/2)+"px")
            .css('height',Math.floor(window.innerHeight *0.8)+"px");
        $("#alert-box")
            .css('top', ((window.innerWidth-175)/2) + "px");

    });
    /*$('#menu').multilevelpushmenu({
        collapsed : true

    });
    $('#menu').multilevelpushmenu('option', 'menuHeight', $(document).height());
    $('#menu').multilevelpushmenu('redraw');*/

    console.log('%cStop', "color: red; border:1px black;font-size:64px;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;");
    console.log('%cDo not paste or write anything here. You could be hacked', "color: red; border:1px black;font-size:64px;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;");
});

var secondlevel = function(el){
  $(".second-level-list").addClass('hidden');
  $("#"+el).removeClass('hidden');
};

function showOverlay(){
    $(".overlay")
        .show()
        .css('width',window.innerWidth+"px")
        .css('height',window.innerHeight+"px");
    $(".viewer")
        .show()
        .empty()
        .css('width',Math.floor(window.innerWidth/2)+"px")
        .css('height',Math.floor(window.innerHeight *0.8)+"px")
        .append(
        "<button type='button' class='close' " +
        "onclick='hideOverlay()' " +
        "aria-label='Close' " +
        "style='top: 0px;right: 0px;width: 20px;height: 20px;padding: 2px;margin: 10px;'>" +
        "<span aria-hidden='true' style='top: 0;'>&times;</span></button>"
    );
}

function hideOverlay() {
    $(".overlay").hide();
    $(".viewer").hide().empty();
}

function showAlert(msg,type) {
    $(".overlay")
        .show()
        .css('width', window.innerWidth + "px")
        .css('height', window.innerHeight + "px");
    if(type=="error"){
        $("#alert-box")
            .find('.bar')
            .css('background:red;');
    }
    $("#alert-box")
        .show()
        .css('top', ((window.innerWidth-175)/2) + "px")
        .find(".alert-message")
        .text(msg);
}

function hideAlert(){
    $(".overlay").hide();
    $("#alert-box").hide();
}

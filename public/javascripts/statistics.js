/**
 * Created by Sundeep on 3/13/2016.
 */
var color = d3.scale.category10();
color.domain([1,2]);

var dailyStats = function () {};
var monthlyStats = function () {};
var tickDays = 4;

$(function(){
    $("[name='network-select']").bootstrapSwitch({
        size : "large",
        onText : "Twitter",
        offText : "Instagram",
        onColor : "info",
        offColor: "warning"
    });
    $("[name='time-select']").bootstrapSwitch({
        size : "large",
        onText : "Last 30 Days",
        offText : "Last 07 Days",
        onColor : "info",
        offColor: "warning"
    }).on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            monthlyStats();
        } else {
            dailyStats();
        }
    });
    social.emit("dailyStats");
    social.emit("monthlyStats");
});

function updateNumberSum(data,id,prop,activeNetwork){
    if(data.length){
        data = data.filter(function (d) {
            return d.network==activeNetwork;
        });
        if(data.length) {
            var total = data.reduce(function(a,b){
                var op = {};
                op[prop] = a[prop]+b[prop];
                return op;
            });
            $(id)
                .text(total[prop])
                .css("color",color(activeNetwork));
        } else {
            $(id)
                .text("No Data")
                .css("color",color(activeNetwork));
        }
    } else {
        $(id)
            .text("No Data")
            .css("color",color(activeNetwork));
    }
}

function updateNumberAve(data,id,prop,activeNetwork){
    if(data.length){
        data = data.filter(function (d) {
            return d.network==activeNetwork;
        });
        if(data.length) {
            var total = data.reduce(function(a,b){
                var op = {};
                op[prop] = a[prop]+b[prop];
                return op;
            });
            $(id)
                .text(total[prop]/data.length)
                .css("color",color(activeNetwork));
        } else {
            $(id)
                .text("No Data")
                .css("color",color(activeNetwork));
        }
    } else {
        $(id)
            .text("No Data")
            .css("color",color(activeNetwork));
    }
}

function updateNumberMax(data,id,prop,activeNetwork){
    if(data.length){
        data = data.filter(function (d) {
            return d.network==activeNetwork;
        });
        if(data.length) {
            var max = d3.max(data, function(v) { return v[prop]; });
            $(id)
                .text(max)
                .css("color",color(activeNetwork));
        } else {
            $(id)
                .text("No Data")
                .css("color",color(activeNetwork));
        }
    } else {
        $(id)
            .text("No Data")
            .css("color",color(activeNetwork));
    }
}

function updateNumberLast(data,id,prop,activeNetwork){
    if(data.length){
        data = data.filter(function (d) {
            return d.network==activeNetwork;
        });
        if(data.length) {
            var total = data.sort(function (a, b) {
                return (new Date(b.date)).getTime() - (new Date(a.date)).getTime();
            });
            $(id)
                .text(total[total.length-1][prop])
                .css("color",color(activeNetwork));
        } else {
            $(id)
                .text("No Data")
                .css("color",color(activeNetwork));
        }
    } else {
        $(id)
            .text("No Data")
            .css("color",color(activeNetwork));
    }
}

function activeNetworkFinder(stats) {
    var totalTwitter = stats.map(function (d) {
        return d.filter(function (e) {
            return e.network == 1;
        }).length;
    }).reduce(function (a,b) {
        return a+b;
    });
    var totalInsta = stats.map(function (d) {
        return d.filter(function (e) {
            return e.network == 2;
        }).length;
    }).reduce(function (a,b) {
        return a+b;
    });
    return (totalTwitter>totalInsta?1:2);
}

social.on("dailyStats",function(stats){
    dailyStats = function () {
        tickDays = 1;
        $("span.StatCount").text("7");
        var activeNetwork = ($('input[name="network-select"]').bootstrapSwitch('state')?1:2);
        $('input[name="network-select"]')
            .on('switchChange.bootstrapSwitch', function (event, state) {
                if (state) {
                    activeNetwork = 1;
                } else {
                    activeNetwork = 2;
                }
                updateNumberSum(stats[0], "#likes-number", "total_likes", activeNetwork);
                updateNumberSum(stats[0], "#comments-number", "total_comments", activeNetwork);
                updateNumberSum(stats[0], "#posts-number", "total_media", activeNetwork);
                updateNumberSum(stats[2], "#growth-number", "diff", activeNetwork);
                updateNumberMax(stats[1], "#followers-number", "followers_count", activeNetwork);
                updateNumberMax(stats[1], "#following-number", "following_count", activeNetwork);
                makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
                makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
                makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
                makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
                makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
                makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
            });
        console.log("Daily Stats : ", stats);
        updateNumberSum(stats[0], "#likes-number", "total_likes", activeNetwork);
        updateNumberSum(stats[0], "#comments-number", "total_comments", activeNetwork);
        updateNumberSum(stats[0], "#posts-number", "total_media", activeNetwork);
        updateNumberSum(stats[2], "#growth-number", "diff", activeNetwork);
        updateNumberMax(stats[1], "#followers-number", "followers_count", activeNetwork);
        updateNumberMax(stats[1], "#following-number", "following_count", activeNetwork);
        makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
        makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
        makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
        makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
        makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
        makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
        function dailyResize() {
            makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
            makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
            makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
            makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
            makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
            makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
        }
        $(window).off("resize",dailyResize).resize(dailyResize);
    };
});

social.on("monthlyStats",function(stats){
    $('input[name="network-select"]').bootstrapSwitch('state', activeNetworkFinder(stats)==1, true)
    monthlyStats = function () {
        tickDays = 4;
        $("span.StatCount").text("30");
        var activeNetwork = ($('input[name="network-select"]').bootstrapSwitch('state')?1:2);
        $('input[name="network-select"]')
            .on('switchChange.bootstrapSwitch', function (event, state) {
                if (state) {
                    activeNetwork = 1;
                } else {
                    activeNetwork = 2;
                }
                updateNumberSum(stats[0], "#likes-number", "total_likes", activeNetwork);
                updateNumberSum(stats[0], "#comments-number", "total_comments", activeNetwork);
                updateNumberSum(stats[0], "#posts-number", "total_media", activeNetwork);
                updateNumberSum(stats[2], "#growth-number", "diff", activeNetwork);
                updateNumberMax(stats[1], "#followers-number", "followers_count", activeNetwork);
                updateNumberMax(stats[1], "#following-number", "following_count", activeNetwork);
                makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
                makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
                makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
                makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
                makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
                makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
            });
        console.log("Daily Stats : ", stats);
        updateNumberSum(stats[0], "#likes-number", "total_likes", activeNetwork);
        updateNumberSum(stats[0], "#comments-number", "total_comments", activeNetwork);
        updateNumberSum(stats[0], "#posts-number", "total_media", activeNetwork);
        updateNumberSum(stats[2], "#growth-number", "diff", activeNetwork);
        updateNumberMax(stats[1], "#followers-number", "followers_count", activeNetwork);
        updateNumberMax(stats[1], "#following-number", "following_count", activeNetwork);
        makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
        makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
        makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
        makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
        makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
        makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
        function monthlyResize(){
            makeLineChart("Likes", "total_likes", "create_date", "#likes-graph", activeNetwork, stats[0]);
            makeLineChart("Comments", "total_comments", "create_date", "#comments-graph", activeNetwork, stats[0]);
            makeLineChart("Posts", "total_media", "create_date", "#posts-graph", activeNetwork, stats[0]);
            makeLineChart("Followers", "followers_count", "modified", "#followers-graph", activeNetwork, stats[1]);
            makeLineChart("Following", "following_count", "modified", "#following-graph", activeNetwork, stats[1]);
            makeLineChart("Growth", "diff", "modified", "#growth-graph", activeNetwork, stats[2]);
        }
        $(window).off("resize",monthlyResize).resize(monthlyResize);
    };
    monthlyStats();
});


function makeLineChart(yLabel,propName,dateProp,chartDivId,activeNetwork,data){
    $(chartDivId).empty();
    var margin = {top: 20, right: 20, bottom: 30, left: 50},
        width = $(chartDivId).width() - margin.left - margin.right,
        height = 200 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%Y-%m-%dT%H:%M:%S.%LZ").parse;

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(d3.time.days,tickDays)
        .tickFormat(d3.time.format("%m/%d"));

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var startvalueline2 = d3.svg.line()
        .x(function(d) { return x(0); })
        .y(function(d) { return y(0); });


    var line = d3.svg.line()
       // .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.activeProp); });

    var svg = d3.select(chartDivId).append("svg")
        .attr("width", "100%")
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    data = data.filter(function (d) {
        return d.network==activeNetwork;
    }).map(function(d) {
        d.date = parseDate(d[dateProp]);
        d.activeProp = d[propName];
        return d;
    });

    x.domain(d3.extent(data, function(d) { return d.date; }));

    var min = d3.min(data, function(v) { return v.activeProp; });
    var max = d3.max(data, function(v) { return v.activeProp; });

    if(min==max){
        min = min-1;
        max = max+1;
    }

    y.domain([min, max]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(yLabel);

    function getSmoothInterpolation() {
        var interpolate = d3.scale.linear()
            .domain([0, 1])
            .range([1, data.length + 1]);

        return function(t) {
            var flooredX = Math.floor(interpolate(t));
            var interpolatedLine = data.slice(0, flooredX);

            if(flooredX > 0 && flooredX < data.length) {
                var weight = interpolate(t) - flooredX;
                var weightedLineAverage = data[flooredX].activeProp * weight + data[flooredX-1].activeProp * (1-weight);
                interpolatedLine.push( {"x":interpolate(t)-1, "y":weightedLineAverage} );
            }

            return line(interpolatedLine);
        }
    }

    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(5)
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5)
    }

    svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .call(make_x_axis()
            .tickSize(-height, 0, 0)
            .tickFormat("")
        );

    svg.append("g")
        .attr("class", "grid")
        .call(make_y_axis()
            .tickSize(-width, 0, 0)
            .tickFormat("")
        );

    svg.append("path")
        .datum(data)
        //.transition()
        //.duration(500)
        //.attrTween('d', getSmoothInterpolation)
        .attr("class", "line")
        .attr("d", line)
        .style('stroke', function(d) { return color(activeNetwork); })
        .style('stroke-width', 2);

    var div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    svg.selectAll("dot")
        .data(data)
        .enter().append("circle")
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.date); })
        .attr("cy", function(d) { return y(d.activeProp); })
        .on("mouseover", function(d) {
            div.transition()
                .duration(200)
                .style("opacity", 1);
            div.text(d[propName])
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function(d) {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        });
}
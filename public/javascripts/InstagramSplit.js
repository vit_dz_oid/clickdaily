﻿social.on("IT_MeFeed",function (feed,next) {
    $("#group3 .AddButton").remove();
    if (feed.length == 0) {
        $("#group3").append("<div class='AddButton'></div>");
        $("#group3 .AddButton").append("<p>You have no Feeds</p>")
    } else {
        feed.forEach(function (d, i) {
            if (d.caption != null) {
                // date = eval("new " + d.CreatedTime.split('/')[1]);
                d.CommonCreateDate = new Date(parseInt(d.created_time)*1000);
                Create_Insta_Post(d, "group3");
            }

        });
        makeLoadMore("group3","IT",next);
    }
    $('.icon').find('a').tooltip({
        'placement': 'top'
    });
    updateFeeds();
});

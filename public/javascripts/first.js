/**
 * Created by Sundeep on 8/8/2015.
 */

var userInfo = {
    FB : {},
    TW : {},
    IT : {},
    FS : {}
};

var networks;

var social = io.connect('/social');

social.on("status", function (data) {
    networks = data;
    if(data.indexOf("Facebook")==-1){
        $(".fb.add").show();
    } else {
        $(".fb.del").show();
    }
    if(data.indexOf("Twitter")==-1){
        $(".tw.add").show();
    } else {
        $(".tw.del").show();
    }
    if(data.indexOf("Instagram")==-1){
        $(".it.add").show();
    } else {
        $(".it.del").show();
    }
    if(data.indexOf("FourSquare")==-1){
        $(".fs.add").show();
    } else {
        $(".fs.del").show();
    }
});
social.on('FB_PageInfo',function(data){
    if(data) {
        userInfo.FB.profile_pic = data.profile_pic;
        userInfo.FB.name = data.name;
        $(".fb.name").text(data.name);
        $(".fb.pic").attr("src",data.profile_pic);
    }
});

social.on('TW_UserInfo',function(data){
    if(data) {
        userInfo.TW.profile_pic = data.profile_pic;
        userInfo.TW.name = data.screen_name;
        $(".tw.name").text(data.screen_name);
        $(".tw.pic").attr("src",data.profile_pic);
    }
});

social.on('IT_UserInfo',function(data){
    if(data) {
        userInfo.IT.profile_pic = data.profile_pic;
        userInfo.IT.name = data.full_name;
        $(".it.name").text(data.full_name);
        $(".it.pic").attr("src",data.profile_pic);
    }
});

social.on('FS_VenueInfo',function(data){
    if(data) {
        userInfo.FS.profile_pic = data.profile_pic;
        userInfo.FS.name = data.name;
        $(".fs.name").text(data.name);
        $(".fs.pic").attr("src",data.profile_pic);
    }
});

$(function(){
    $("#container").css("height","100%");
    social.emit("Ready",{userInfo:1});
});
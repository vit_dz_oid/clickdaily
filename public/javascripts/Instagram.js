﻿function FavInsta(id) {
    social.emit('IT_Like',id);
}

function commentInsta(id,text) {
    social.emit('IT_Comment',id,text);
}

social.on("IT_LikeSuccesful",function(id){
    $("#"+id+".IT").find(".icons").find(".fa-heart").css("color", "red");
});


function postIT(obj,start,id,approve) {
    var file = obj.find("#AttachFile")[0].files[0];
    var text = obj.find("textarea").val();
    var stream = ss.createStream();
    var data = {text:text};
    if(start) {
        data.dateTime = start.toISOString();
    }
    if(id){
        data.oldId       = id;
    }
    // upload a file to the server.
    if(file){
        data.size = file.size;
        ss(social).emit('IT_Post',stream, data,approve);
        ss.createBlobReadStream(file).pipe(stream);
    }else{
        data.size = 0;
        ss(social).emit('IT_Post',0, data,approve);
    }
}

/*

function (data, statustext) {
    if (data.status != 2) {
        $("#" + id + ".instaPost .footer .footertext a .Likes").text(data.count + " Likes");
        if (data.status) {
            $("#" + id + ".instaPost .footer .footertext a .Likes").css("color", "red");
        } else {
            $("#" + id + ".instaPost .footer .footertext a .Likes").css("color", "grey");
        }
    }
});

function PostInsta(id, text) {
    $.post('/Instagram/Comment?id=' + id + "&text=" + text,
        function (data, statustext) {
            $("#" + id + ".instaPost .footer .footertext .Comments").text(data.count + " Comments");
            $("#" + id + ".instaPost .comment input").val("");
        });
}

*/


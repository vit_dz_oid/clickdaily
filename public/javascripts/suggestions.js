/**
 * Created by Sundeep on 5/18/2015.
 */

var lat = null;
var lon = null;
var miles = null;
var qs = [];
var queries = {};

var globalReplyIds = {
    pass : 0,
    fail : 0,
    processing : 0
};
var env = "Suggesstion";
var map, circle, circleFeature ;
loader = "<div id='loader'><div class='wrapper'><div class='dot'></div><div class='dot'></div><div class='dot'></div>";
loader = loader + "<div class='dot'></div><div class='dot'></div><div class='dot'></div>";
loader = loader + "<div class='dot'></div><div class='dot'></div><div class='dot'></div>";
loader = loader + "<div class='dot'></div><div class='dot'></div><div class='dot'></div>";
loader = loader + "<div class='dot'></div><div class='dot'></div></div></div>";
$(function(){

    /**
     * Define a namespace for the application.
     */
    window.app = {};
    var app = window.app;



    /**
     * @constructor
     * @extends {ol.interaction.Pointer}
     */
    app.Drag = function() {

        ol.interaction.Pointer.call(this, {
            handleDownEvent: app.Drag.prototype.handleDownEvent,
            handleDragEvent: app.Drag.prototype.handleDragEvent,
            handleMoveEvent: app.Drag.prototype.handleMoveEvent,
            handleUpEvent: app.Drag.prototype.handleUpEvent
        });

        /**
         * @type {ol.Pixel}
         * @private
         */
        this.coordinate_ = null;

        /**
         * @type {string|undefined}
         * @private
         */
        this.cursor_ = 'move';

        /**
         * @type {ol.Feature}
         * @private
         */
        this.feature_ = null;

        /**
         * @type {string|undefined}
         * @private
         */
        this.previousCursor_ = undefined;

    };
    ol.inherits(app.Drag, ol.interaction.Pointer);


    /**
     * @param {ol.MapBrowserEvent} evt Map browser event.
     * @return {boolean} `true` to start the drag sequence.
     */
    app.Drag.prototype.handleDownEvent = function(evt) {
        var map = evt.map;

        var feature = map.forEachFeatureAtPixel(evt.pixel,
            function(feature, layer) {
                return feature;
            });

        if (feature) {
            this.coordinate_ = evt.coordinate;
            this.feature_ = feature;
        }
        return !!feature;
    };


    /**
     * @param {ol.MapBrowserEvent} evt Map browser event.
     */
    app.Drag.prototype.handleDragEvent = function(evt) {
        var map = evt.map;

        var feature = map.forEachFeatureAtPixel(evt.pixel,
            function(feature, layer) {
                return feature;
            });

        var deltaX = evt.coordinate[0] - this.coordinate_[0];
        var deltaY = evt.coordinate[1] - this.coordinate_[1];

        var geometry = /** @type {ol.geom.SimpleGeometry} */
            (this.feature_.getGeometry());
        geometry.translate(deltaX, deltaY);

        this.coordinate_[0] = evt.coordinate[0];
        this.coordinate_[1] = evt.coordinate[1];

    };


    /**
     * @param {ol.MapBrowserEvent} evt Event.
     */
    app.Drag.prototype.handleMoveEvent = function(evt) {
        if (this.cursor_) {
            var map = evt.map;
            var feature = map.forEachFeatureAtPixel(evt.pixel,
                function(feature, layer) {
                    return feature;
                });
            var element = evt.map.getTargetElement();
            if (feature) {
                if (element.style.cursor != this.cursor_) {
                    this.previousCursor_ = element.style.cursor;
                    element.style.cursor = this.cursor_;
                }
            } else if (this.previousCursor_ !== undefined) {
                element.style.cursor = this.previousCursor_;
                this.previousCursor_ = undefined;
            }
        }

    };


    /**
     * @param {ol.MapBrowserEvent} evt Map browser event.
     * @return {boolean} `false` to stop the drag sequence.
     */
    app.Drag.prototype.handleUpEvent = function(evt) {
        var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
        lon = lonlat[0];
        lat = lonlat[1];
        refreshKeys();
        this.coordinate_ = null;
        this.feature_ = null;

        return false;

    };

    map = new ol.Map({
        interactions: ol.interaction.defaults().extend([new app.Drag()]),
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center: ol.proj.transform([-73.979723,40.758680], 'EPSG:4326', 'EPSG:3857'),
            zoom: 9.5
        })
    });


    var view = map.getView();
    var projection = view.getProjection();
    var resolutionAtEquator = view.getResolution();
    var center = map.getView().getCenter();
    var pointResolution = projection.getPointResolution(resolutionAtEquator, center);
    var resolutionFactor = resolutionAtEquator/pointResolution;
    var radius = (16093.4 / ol.proj.METERS_PER_UNIT.m) * resolutionFactor;


    circle = new ol.geom.Circle(center, radius);
    circleFeature = new ol.Feature(circle);

    // Source and vector layer
    var vectorSource = new ol.source.Vector({
        projection: 'EPSG:4326'
    });
    vectorSource.addFeature(circleFeature);
    var vectorLayer = new ol.layer.Vector({
        source: vectorSource
        , style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                width: 3,
                color: [255, 0, 0, 1]
            }),
            fill: new ol.style.Fill({
                color: [0, 0, 255, 0.3]
            })
        })
    });

    map.addLayer(vectorLayer);
    social.emit("Ready",{userInfo:1});
    var h = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    $(".outerContain").css("height",h-420+"px");
    $(window).resize(function () {
        var h = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
        $(".outerContain").css("height",h-420+"px");
    });

    $("input.miles").on('change input propertychange paste', function() {
        var view = map.getView();
        var projection = view.getProjection();
        var resolutionAtEquator = view.getResolution();
        var center = map.getView().getCenter();
        var pointResolution = projection.getPointResolution(resolutionAtEquator, center);
        var resolutionFactor = resolutionAtEquator/pointResolution;

        var mile = parseFloat($("input.miles").val());
        var meters = mile * 1609.34;
        var radius = (meters / ol.proj.METERS_PER_UNIT.m) * resolutionFactor;
        miles = mile + "mi";
        refreshKeys();
        circle.setRadius(radius);
    });
});

social.on("status", function (data) {
    if (data.redirectUrl) {
        return window.location.href = data.redirectUrl;
    }
    /*if(data.indexOf("Twitter")!=-1){
        social.emit("searchTweets","food");
    }*/
    postDisable(data);
});

social.on("TweetsNearYou", function (data,q) {
    $("#loading").remove();
    var title = q;
    q = q.toLowerCase().replace(/\s/g,'');
    if($("#loadMore"+q)){
        $("#loadMore"+q).parent().parent().remove();
    }
    var now         = moment();
    var lastHour    = {time : moment(now).subtract(1,'hours')   , count : 0};
    var lastDay     = {time : moment(now).subtract(1,'days')    , count : 0};
    var lastWeek    = {time : moment(now).subtract(1,'weeks')   , count : 0};
    var lastMonth   = {time : moment(now).subtract(1,'months')  , count : 0};
    var lastYear    = {time : moment(now).subtract(1,'years')   , count : 0};
    var remaning  = 0;
    title = title.charAt(0).toUpperCase() + title.substring(1);
    if(!($("#"+q).length)) {
        TWFeed[q] = {};
        var top =
            "<div class='row'>" +
                "<div class='col-md-10'>" +
                    "<h1 class='pull-left' style='margin: 10px 0px 10px 0px;'>"+ title +"</h1>" +
                "</div>" +

                "<div class='col-md-2'>" +
                    "<button type='button' class='close' aria-label='Close' " +
                    "data-keyword='"+q+"' onclick='removeKeyword($(this))'>" +
                        "<span aria-hidden='true'>&times;</span>" +
                    "</button>"+
                "<div/>" +
            "</div>" +
            '<div class="row text-center">' +
                '<div class="col-md-12 stats">' +
                    '<div class="col-md-2 hour">' +
                        '<strong>Last Hour</strong>' +
                    '</div>' +
                    '<div class="col-md-2 day">' +
                        '<strong>Last Day</strong>' +
                    '</div>' +
                    '<div class="col-md-2 week">' +
                        '<strong>Last Week</strong>' +
                    '</div>' +
                    '<div class="col-md-2 month">' +
                        '<strong>Last Month</strong>' +
                    '</div>' +
                    '<div class="col-md-2 year">' +
                        '<strong>Last Year</strong>' +
                    '</div>' +
                    '<div class="col-md-2 century">' +
                        '<strong>Last Century</strong>' +
                    '</div>' +
                '</div>' +
                '<br />' +
            '</div>' +
            '<div class="row text-center">' +
                '<div class="col-md-12 stats">' +
                    '<div class="col-md-2 hour number">00</div>' +
                    '<div class="col-md-2 day number">00</div>' +
                    '<div class="col-md-2 week number">00</div>' +
                    '<div class="col-md-2 month number">00</div>' +
                    '<div class="col-md-2 year number">00</div>' +
                    '<div class="col-md-2 century number">00</div>' +
                '</div>' +
                '<br />' +
            '</div>';
        var rest =
            "<div class='row'>" +
                "<div class='col-md-12 text-center' >" +
                    "<button class='btn btn-success' type='button' onclick=\"followSelected('"+ q +"')\">Follow Selected</button>&nbsp;&nbsp;" +
                    "<button class='btn btn-info' type='button' " +
                        "onclick=\"$(this).parent().parent().find('#replyBox').show()\" >" +
                        "Reply Selected" +
                    "</button>&nbsp;&nbsp;" +
                    "<button class='btn btn-info selectAll' id='sel_"+ q +"'>Select All" +
                        "<div class='pull-right specialAll squaredTwo'>" +
                            "<input type='checkbox'><label for='squaredTwo'></label> " +
                        "</div>" +
                    "</button>" +
                "</div>" +
                "<div id='replyBox' class='col-md-12' style='display:none;'>" +
                    "<br/>" +
                    "<div class='col-md-8 form-group'>" +
                        "<input class='form-control' type='text' placeholder='Your Reply Here...'>" +
                    "</div>" +
                    "<button class='btn btn-success' type='button' onclick=\"replySelected('"+ q +"')\">Send</button>&nbsp;&nbsp;" +
                    "<button class='btn btn-default' type='button' onclick=\"$(this).parent().hide()\">Cancel</button>" +
                    "<br/>" +
                "</div>"+
            "</div>" +
            "<div class='row'>" +
                "<div class='col-md-12' >" +
                    "<div id='"+ q +"' style='width: 95%;'></div> " +
                "</div>"+
            "</div>";
        $('#suggestions')
            .append(
                "<div id='contain_"+q+"' class='col-4'>" +
                "</div>"
            )
            .find("#contain_"+q)
            .append(top)
            .append(rest);
    }

    $('#suggestions').css("width",($('.col-4').length * 530) + "px");
    data.statuses.forEach(function(d){
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d,q);
    });

    Object.keys(TWFeed[q]).forEach(function(d){
        if(lastHour.time.isBefore(TWFeed[q][d].CommonCreateDate)){
            lastHour.count = lastHour.count +1;
        } else if(lastDay.time.isBefore(TWFeed[q][d].CommonCreateDate)){
            lastDay.count = lastDay.count +1;
        } else if(lastWeek.time.isBefore(TWFeed[q][d].CommonCreateDate)){
            lastWeek.count = lastWeek.count +1;
        } else if(lastMonth.time.isBefore(TWFeed[q][d].CommonCreateDate)){
            lastMonth.count = lastMonth.count +1;
        } else if(lastYear.time.isBefore(TWFeed[q][d].CommonCreateDate)){
            lastYear.count = v.count +1;
        } else {
            remaning = remaning + 1;
        }
    });

    if(lastHour.count){
        $('#suggestions')
            .find("#contain_"+q)
            .find(".stats")
            .find(".hour")
            .show()
            .filter(".number")
            .text(lastHour.count);
    }
    if(lastDay.count){
        $('#suggestions')
            .find("#contain_"+q)
            .find(".stats")
            .find(".day")
            .show()
            .filter(".number")
            .text(lastDay.count);
    }
    if(lastWeek.count){
        $('#suggestions')
            .find("#contain_"+q)
            .find(".stats")
            .find(".week")
            .show()
            .filter(".number")
            .text(lastWeek.count);
    }
    if(lastMonth.count){
        $('#suggestions')
            .find("#contain_"+q)
            .find(".stats")
            .find(".month")
            .show()
            .filter(".number")
            .text(lastMonth.count);
    }
    if(lastYear.count){
        $('#suggestions')
            .find("#contain_"+q)
            .find(".stats")
            .find(".year")
            .show()
            .filter(".number")
            .text(lastYear.count);
    }


    $('#suggestions')
        .find("#"+q)
        .find(".stats")
        .append();

    console.log("lastHour - ", lastHour.count);
    console.log("lastDay - ", lastDay.count);
    console.log("lastWeek - ", lastWeek.count);
    console.log("lastMonth - ", lastMonth.count);
    console.log("lastYear - ", lastYear.count);
    console.log("remaning - ", remaning);

    $('#suggestions').find("#"+q).find(".special label").click(function(){
       var $check = $(this).parent().find("input");
       $check.prop("checked", !$check.prop("checked"));
    });
    $('#suggestions').find("#sel_"+q).click(function(){
        var $check = $(this).find("input");
        $check.prop("checked", !$check.prop("checked"));
        var value = $check.prop("checked");
       $(this).parent().parent().parent().find("#"+q).find(".special input").prop("checked", value);
    });

    var next = data.search_metadata.max_id_str;
    if(data.search_metadata.next_results){
        next = data
            .search_metadata
            .next_results
            .split("&")
            .filter(function(d){
                return d.indexOf("max_id")!= -1
            })[0]
            .split("=")[1]
    }
    makeLoadMore(q,next);
});

function loadmore(query,next){
    social.emit("searchTweets",query,lat,lon,miles,next);
}

function addkeyword(el){
    var q = el.parent().find("input").val();
    q = q.toLowerCase();
    q = q.replace(/\s/g,'');
    if((q!="")&&qs.indexOf(q)==-1){
        $("#"+q).parent().remove();
        if(qs.indexOf(q)==-1) {
            qs.push(q);
            queries[q] = el.parent().find("input").val();
        }
        social.emit("searchTweets",el.parent().find("input").val(),lat,lon,miles);
    }
    var overPos = $(".outerContain").position();
    $("body").append("<div id='loading'></div>")
    $("#loading")
        .css("top",(overPos.top +25 )+"px")
        .css("left",overPos.left+"px")
        .css("height",(window.innerHeight - overPos.top)+"px")
        .css("width",window.innerWidth-60 + "px")
        .append(loader);
}

function removeKeyword(el) {
    el.parent().parent().parent().remove();
    var keyword = el.data('keyword');
    qs.splice(qs.indexOf(keyword),1);
    delete queries[keyword];
    delete TWFeed[keyword];
}

function refreshKeys(){
    TWFeed = {};
    qs.forEach(function(q){
        if(q!=""){
            $("#"+q).parent().parent().parent().remove();
            TWFeed[q] = {};
            social.emit("searchTweets",queries[q],lat,lon,miles);
        }
    });
}


function getSelected(q){
    return $("#"+q)
        .find("input:checked")
        .map(function(i,d){
            return TWFeed[q][$(d).val()]
        }) ;
}

function unselectThis(el,q,id){
    $("#"+q)
        .find("#contain_"+id)
        .find(".special input")
        .prop("checked",false);
    el.parent().parent().remove();
    cache.delItem(id);
    var count = cache.getSize();
    if(count<1){
        $('.viewer').find('.exceedCount').parent().parent().parent().parent().remove();
    }else{
        $('.viewer').find('.exceedCount').text(count);
    }
}

function removeThis(el,q,id){
    users[id].tweets.forEach(function(d){
        $("#"+q)
            .find("#contain_"+d)
            .find(".special input")
            .prop("checked",false);
    });
    delete users[id];
    el.parent().parent().remove();
}

function ignoreThis(el,id){
    el
        .removeClass('btn-warning')
        .addClass('btn-default')
        .text('Ignored')
        .attr("onclick",'')
        .parent()
        .find("textarea")
        .removeClass('border-danger')
        .removeClass('text-danger')
        .addClass('border-warning')
        .addClass('text-warning');

    cache.ignoreItem(id);

    var count = cache.getSize();
    if(count<1){
        $('.viewer').find('.exceedCount').parent().parent().parent().parent().remove();
    }else{
        $('.viewer').find('.exceedCount').text(count);
    }
}

function ignoreAll(){
    var overflow = cache.getItems();
    var n = overflow.length;
    for(var i =0 ;i<n; i++){
       $("#preview_"+overflow[i])
           .find(".ignoreThis")
           .removeClass('btn-warning')
           .addClass('btn-default')
           .text('Ignored')
           .attr("onclick",'')
           .parent()
           .find("textarea")
           .removeClass('border-danger')
           .removeClass('text-danger')
           .addClass('border-warning')
           .addClass('text-warning');
       $('.viewer').find('.exceedCount').parent().parent().parent().parent().remove();
    }
    cache.ignoreAll();
}

function removeAll(q){
    var overflow = cache.getItems();
    var n = overflow.length;
    for(var i =0 ;i<n; i++){
        $("#"+q)
            .find("#contain_"+overflow[i])
            .find(".special input")
            .prop("checked",false);
        $("#preview_"+overflow[i])
            .find(".unselectThis")
            .parent()
            .parent()
            .remove();
    }
    cache.empty();
}

function testCache(){
    var overflowed = cache.getSize();
    var ignored  = cache.getIgnoredItemsCount();
    if(overflowed||ignored){
        var msg = (overflowed? overflowed + " Items will not be posted":"")
            + (overflowed&&ignored? " and ":"")
            + (ignored? ignored + " Items will be posted with partial text":"")
            + ". Are you sure you want to continue?";
        return confirm(msg);
    }else {
        return true
    }

}

function postReplies(q) {
    if(testCache()){
        var tweets = getSelected(q);
        //var text =  $("#contain_"+q).find("#replyBox").find("input").val();
        var replies = [];
        globalReplyIds = {
            pass : 0,
            fail : 0,
            processing : 0
        };
        var promptHtml =
            "<H1 class='text-center geoPromptHead'></H1>" +
            "<p class='text-center geoPromptSummary'></p>" +
            "<table class='table'>" +
            "<thead>" +
            "<tr>" +
            "<th>#</th>" +
            "<th>Time</th>" +
            "<th>Name</th>" +
            "<th>Handle</th>" +
            "<th style='display: none'>Tweet Text</th>" +
            "<th style='display: none'>Reply Text</th>" +
            "<th>Follower Count</th>" +
            "<th>Following Count</th>" +
            "<th>Favorite Count</th>" +
            "<th>Status Count</th>" +
            "<th>You Follow</th>" +
            "<th>Status</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody id='geoPrompt'>";

        for(var i =0;i<tweets.length;i++){
            var d = tweets[i];
            var reply =  $("#preview_"+d.id_str).find("textarea").val();
            replies.push({
                reply       : reply,
                id          : d.id_str
            });
            promptHtml = promptHtml +"<tr id='prompt_"+ d.id_str +"'>" +
                "<td>" + (i+1) +"</td>" +
                "<td>" + moment().format("HH:mm MM/DD/YY") +"</td>" +
                "<td>" + d.user.name + "</td>" +
                "<td>&#64;" + d.user.screen_name + "</td>" +
                "<td style='display: none'>" + d.text + "</td>" +
                "<td style='display: none'>" + reply + "</td>" +
                "<td>" + d.user.followers_count + "</td>" +
                "<td>" + d.user.friends_count + "</td>" +
                "<td>" + d.user.favourites_count + "</td>" +
                "<td>" + d.user.statuses_count + "</td>" +
                "<td>" + (d.user.following?"Yes":"No") + "</td>" +
                "<td class='geoReplyStatus'>Processing</td>" +
                "</tr>";
        }
        promptHtml = promptHtml +"</tbody>" +
            "</table>";
        dialogInstance = BootstrapDialog.show({
            type: BootstrapDialog.TYPE_PRIMARY,
            title: 'Sending....',
            message: promptHtml,
            closable: true,
            closeByBackdrop: false,
            buttons: [{
                label: 'Close',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }],
            size :  'size-wide'
        });
        globalReplyIds.processing = replies.length;
        replies.forEach(function(d){
            social.emit("TW_Reply", d.id, d.reply);
        });
        setTimeout(function(){
            if(globalReplyIds.processing == replies.length){
                dialogInstance.setType(BootstrapDialog.TYPE_DANGER);
                dialogInstance.setTitle("Failed");
                $("h1.geoPromptHead").text("Timed Out!!!");
                if(!globalReplyIds.fail){
                    dialogInstance.setType(BootstrapDialog.TYPE_SUCCESS);
                    dialogInstance.setTitle("Sent");
                    message =  globalReplyIds.pass + ' Customers Reached Out Successfully.';
                } else if(!globalReplyIds.pass) {
                    dialogInstance.setType(BootstrapDialog.TYPE_DANGER);
                    dialogInstance.setTitle("All Failed");
                    message = globalReplyIds.fail + ' Customers could not be reached!';
                } else {
                    dialogInstance.setType(BootstrapDialog.TYPE_WARNING);
                    dialogInstance.setTitle("Some Failed");
                    message = globalReplyIds.pass + ' Customers Reached Out Successfully.' +
                        '\n' + globalReplyIds.fail + ' Customers could not be reached!';
                }
                $("p.geoPromptSummary").text(message);
            }
        },30*1000);
        hideOverlay();
        cache.empty();
    } else {
        hideOverlay();
    }
}

function followUsers(q) {
    if(users){
        Object.keys(users).forEach(function(d){
            social.emit("TW_follow", d);
        });
    } else {
        hideOverlay();
    }
}

var cache = function(){
    var oveflowedItems = [];
    var ignoredItems = [];
    return {
        addItem : function(id){
            oveflowedItems.push(id);
        },
        delItem : function(id){
            oveflowedItems.splice(oveflowedItems.indexOf(id),1);
            ignoredItems.splice(ignoredItems.indexOf(id),1);
        },
        getIndex : function(id){
            return oveflowedItems.indexOf(id);
        },
        empty : function(){
            oveflowedItems = [];
            ignoredItems = [];
        },
        getSize : function(){
            return oveflowedItems.length;
        },
        getItems : function(){
            return oveflowedItems;
        },
        ignoreItem : function(id){
            oveflowedItems.splice(oveflowedItems.indexOf(id),1);
            ignoredItems.push(id);
        },
        getIgnoredItems : function(){
            return ignoredItems;
        },
        getIgnoredItemsCount : function(){
            return ignoredItems.length;
        },
        ignoreAll : function(){
            ignoredItems = oveflowedItems;
            oveflowedItems = [];
        }
    }
}();
var users = {};
function followSelected(q) {
    var tweets = getSelected(q);
    if(!tweets.length){
        return alert("Nothing Selected to Reply to.");
    }
    users = {};
    tweets.each(function(i,d){
        if(!users[d.user.id_str]) {
            users[d.user.id_str] = {};
        }
        users[d.user.id_str].user = d.user;
        if(!users[d.user.id_str].tweets){
            users[d.user.id_str].tweets = []
        }
        users[d.user.id_str].tweets.push(d.id_str);
    });
    showOverlay();
    $(".viewer").append(
        "<br/>" +
        "<br/>" +
        "<div class='row' style='  max-height: 70%;overflow: auto;margin-right: 10px;margin-left: 10px;'>" +
        "<div id='previewFollows' class='col-md-12' >" +
        "</div>" +
        "</div>"
    );
    Object.keys(users).forEach(function(d){
        if(users[d].user.following){
            users[d].tweets.forEach(function(e){
                $("#"+q)
                    .find("#contain_"+e)
                    .find(".special input")
                    .prop("checked",false);
            });
            var before =
                '<div id="follow_'+ d +'" class="media">'+
                '<div class="media-left">' +
                '<a href="#">' +
                '<img class="media-object previewReplyImage" src="'+users[d].user.profile_image_url_https+'" alt="...">'+
                '</a>'+
                '</div>'+
                '<div class="media-body">'+
                '<h4 class="media-heading">'+users[d].user.name +' &nbsp;<small>@' + users[d].user.screen_name + '</small></h4>'+
                '<button type="button" class="btn btn-defaults">' +
                'Already Followed</button>&nbsp;' +
                '</div>' +
                '</div>';
            $(".viewer").find("#previewFollows").append(before);
            delete users[d];
        }else{
            var before =
                '<div id="follow_'+ d +'" class="media">'+
                '<div class="media-left">' +
                '<a href="#">' +
                '<img class="media-object previewReplyImage" src="'+users[d].user.profile_image_url_https+'" alt="...">'+
                '</a>'+
                '</div>'+
                '<div class="media-body">'+
                '<h4 class="media-heading">'+users[d].user.name +' &nbsp;<small>@' + users[d].user.screen_name + '</small></h4>'+
                '<button type="button" class="btn btn-danger removeThis" onclick="removeThis($(this),\''+q+'\',\''+ d +'\')">' +
                'Remove</button>&nbsp;' +
                '</div>' +
                '</div>';
            $(".viewer").find("#previewFollows").append(before);
        }


    });
    var bottom = "<br/>" +
        "<div class='row'>" +
        "<div class='col-md-12 text-center'>" +
        "<button class='btn btn-success' type='button' onclick='followUsers(\"" + q + "\")'>Follow</button>&nbsp;&nbsp;" +
        "<button class='btn btn-default' type='button' onclick='hideOverlay()'>Cancel</button>" +
        "</div>" +
        "</div>" +
        "<br/>";

    $(".viewer").append(bottom);
}

function replySelected(q){
    cache.empty();
    var tweets = getSelected(q);
    if(!tweets.length){
        return alert("Nothing Selected to Follow.");
    }
    var text =  $("#contain_"+q).find("#replyBox").find("input").val();
    var replies = [];
    tweets.each(function(i,d){
        var reply = "@" + d.user.screen_name + " " + text;
        replies.push({
            reply       : reply,
            overflow    : reply.length > 140,
            user        : d.user,
            id          : d.id_str
        });
        if(reply.length > 140){
            cache.addItem(d.id_str);
        }
    });
    showOverlay();
    $(".viewer").append(
        "<br/>" +
        "<br/>" +
        "<div class='row' style='  max-height: 70%;overflow: auto;margin-right: 10px;margin-left: 10px;'>" +
        "<div id='previewReplies' class='col-md-12' >" +
        "</div>" +
        "</div>"
    );
    replies.forEach(function(d){

        var before =   '<div id="preview_'+ d.id +'" class="media">'+
            '<div class="media-left">'+
            '<a href="#">'+
            '<img class="media-object previewReplyImage" src="'+d.user.profile_image_url_https+'" alt="...">'+
            '</a>'+
            '</div>'+
            '<div class="media-body">'+
            '<h4 class="media-heading">'+ d.user.name +'</h4>';

        var middle;



        if(d.overflow){
            middle = '<textarea maxlength="140" rows="2" class="border-danger text-danger">' + d.reply.substr(0,140) +
            '</textarea> ' +
            '<button type="button" class="btn btn-danger unselectThis" onclick="unselectThis($(this),\''+q+'\',\''+ d.id +'\')">' +
            'Remove</button>&nbsp;' +
            '<button type="button" class="btn btn-warning ignoreThis" onclick="ignoreThis($(this),\''+ d.id +'\')">' +
            'Ignore</button>'
        }else{
            middle = '<textarea maxlength="140" rows="2" style="width:80%;">' + d.reply.substr(0,140) +
                '</textarea> ' +
                '<button type="button" class="btn btn-danger unselectThis" onclick="unselectThis($(this),\''+q+'\',\''+ d.id +'\')">' +
                'Remove</button>&nbsp;'
        }

        var after = '</div></div>';
        $(".viewer").find("#previewReplies").append(before+middle+after);

    });

    var bottom = "<br/>" +
        "<div class='row'>" +
        "<div class='col-md-12 text-center'>" +
        "<button class='btn btn-success' type='button' onclick='postReplies(\"" + q + "\")'>Post</button>&nbsp;&nbsp;" +
        "<button class='btn btn-default' type='button' onclick='hideOverlay()'>Cancel</button>" +
        "</div>" +
        "</div>" +
        "<br/>";
    var count = cache.getSize();
    if(count){
        bottom = bottom +
        "<div class='row'>" +
        "<div class='col-md-12 text-center'>" +
        "<p class='text-danger'>" +
        "<strong>" +
        "<span class='exceedCount'>" + count + "</span> Tweets exceed 140 Characters and will not be posted</strong>" +
        "</p>" +
        "</div>" +
        "<div class='col-md-12 text-center'>" +
        "<button class='btn btn-danger' type='button' onclick='removeAll(\""+ q +"\")'>Remove All</button>&nbsp;&nbsp;" +
        "<button class='btn btn-warning' type='button' onclick='ignoreAll()'>Ignore All</button>" +
        "</div>" +
        "</div>"
    }

    $(".viewer").append(bottom);
}
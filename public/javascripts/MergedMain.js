﻿
var picCache ={};
var Networks = null;
var Recieved = {};
var combinedFeed = [];
var interval = 0;
var status = 8;

function allDone(){
    if(status>0){
        console.log("Still Loading");
    }else{
        $("#loading").remove();
        console.log("All Done");
    }
}

var updateFeeds =    function() {
    if (!interval) {
        console.log("update sequence intiatied");
        clearInterval(interval);
        interval = setInterval(function () {
            social.emit("update_feeds");
            updatePostTimes();
        }, 10*60*1000)
    }
};

$(function () {
    //today = moment();
    moment.tz.add('America/New_York|EST EDT|50 40|0101|1Lz50 1zb0 Op0');
    $('[data-toggle="tooltip"]').tooltip({
        'placement': 'top'
    });
    var $grid = $('#group');

    $grid.shuffle({
        itemSelector: '.flip-container',
        speed: 750,
        initialSort : {
            reverse: true,
            by: function($el) {
                return $el.data('date-created');
            }
        },
        easing: ''
    });
    var overPos = $grid.position();
    $("body").append("<div id='loading'></div>")
    $("#loading")
        .css("top",(overPos.top +25 )+"px")
        .css("left",overPos.left+"px")
        .css("height",(window.innerHeight - overPos.top)+"px")
        .css("width",window.innerWidth-60 + "px")
        .append(loader);


    $grid.on('layout.shuffle', function() {
        allDone();
    });


    $("input[name=sort-options]:radio").on('change', function() {
        if(this.checked) {
            $(this).parent().siblings().each(function (index, el) {
                $(el).removeClass('btn-success');
                $(el).addClass('btn-danger');
            });
            $(this).parent().addClass('btn-success');
            $(this).parent().removeClass('btn-danger');
        }
        var sort = this.value,
            opts = {};

        // We're given the element wrapped in jQuery
        if ( sort === 'date-created' ) {
            opts = {
                reverse: true,
                by: function($el) {
                    return $el.data('date-created');
                }
            };
        } else if ( sort === 'network' ) {
            opts = {
                by: function($el) {
                    return $el.data('network');
                }
            };
        }

        // Filter elements
        $('#group').shuffle('sort', opts);
    });
    $("input.network").on('change', function() {
        if(this.checked){
            $(this).parent().removeClass('btn-danger');
            $(this).parent().addClass('btn-success');
        }else {
            $(this).parent().removeClass('btn-success');
            $(this).parent().addClass('btn-danger');
        }
        $('#group').shuffle('shuffle', function($el, shuffle) {
            if(($el.data('network')==1)&&$("input.network.fb").prop("checked")){
                return true
            }
            if(($el.data('network')==2)&&$("input.network.tw").prop("checked")){
                return true
            }
            if(($el.data('network')==3)&&$("input.network.it").prop("checked")){
                return true
            }
            if(($el.data('network')==4)&&$("input.network.fs").prop("checked")){
                return true
            }
            return false;
        });
    });
    social.emit("Ready",{feed:1,userInfo:1});
});

social.on("status",function(data){
    if(data.redirectUrl){
        return window.location.href=data.redirectUrl;
    }
    postDisable(data);
    status = status - 8 + (data.length*2);
    allDone();
});
/*
var created = 0;
function createPosts() {
    if(!created) {
        created = 1;

        //$("#group1").empty();
        //$("#group2").empty();
        //$("#group3").empty();
        //$("#group4").empty();
        console.log("CreatePost Ran");
        console.log("Combined feed at start : ", combinedFeed);
        var i = 0;
        combinedFeed.sort(function (a, b) {
            var keyA = a.CommonCreateDate.getTime(),
                keyB = b.CommonCreateDate.getTime();
            // Compare the 2 dates
            if (keyA > keyB) return -1;
            if (keyA < keyB) return 1;
            return 0;
        });
        var flag;
        while (combinedFeed.length) {
            d = combinedFeed.shift();
            switch (d.sn_type) {
                // + ((i % 4) + 1)
                case "Tw":
                    flag = Create_Tw_Post(d, "group");
                    break;
                case "Fb":
                    flag = Create_FB_Post(d, "group");
                    break;
                case "Insta":
                    flag = Create_Insta_Post(d, "group");
                    break;
                case "Fs":
                    flag = Create_Fs_Post(d, "group");
                    break;
            }
            if(flag) i += 1;
        }
        $('.icon').find('a').tooltip({
            'placement': 'top'
        });
        imagesLoaded( document.querySelector('#group'), function( instance ) {
            $('#group').shuffle('update');


        console.log("Combined feed at end : ", combinedFeed);
    }
}*/
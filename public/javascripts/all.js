﻿function loadmore(network,next){
    social.emit(network+"_LoadMore",next);
}

social.on("IT_LoadMore",function(feed,next){
    $("#loadMoreIT").parent().remove();
    feed.forEach(function (d, i) {
        if (d.caption != null) {
            // date = eval("new " + d.CreatedTime.split('/')[1]);
            d.CommonCreateDate = new Date(parseInt(d.created_time)*1000);
            Create_Insta_Post(d, "group3");
        }

    });
    makeLoadMore("group3","IT",next);
    $('.icon').find('a').tooltip({
        'placement': 'top'
    });
});

social.on("TW_LoadMore",function(timeline){
    $("#loadMoreTW").parent().remove();
    timeline.forEach(function (d, i) {
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d, "group2");
    });
    makeLoadMore("group2","TW",timeline[timeline.length-1].id_str);
    $('.icon').find('a').tooltip({
        'placement': 'top'
    });
});


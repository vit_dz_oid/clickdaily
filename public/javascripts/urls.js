/**
 * Created by Sundeep on 8/30/2015.
 */

$(function(){
   social.emit("getAllUrls");
});

social.on("URL_list",function(list){
    list.forEach(function(d,i){
       var row = "<tr>" +
               "<td>"+ (i+1) +"</td>" +
               "<td>"+ d.url +"</td>" +
               "<td>"+ d.hash +"</td>" +
               "<td>"+ d.clicks +"</td>" +
               "<td><a onclick=\"deleteUrl('"+d.url+"')\">Delete</a> </td>" +
           "</tr>";
        $("tbody").append(row);
    });
});
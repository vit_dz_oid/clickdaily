﻿var Retweets = {};

function FavTweet(id) {
    social.emit('TW_Favorite',id);
}

function UnFavTweet(id) {
    social.emit('TW_UnFavorite',id);
}

social.on("TW_LikeSuccesful",function(id){
    $("#contain_"+id+" .TW")
        .find(".icons")
        .find(".tw-like")
        .css("color", "#E81C4F")
        .parent()
        .attr('onclick',"UnFavTweet('"+ id  + "')");
});

social.on("TW_UnLikeSuccesful",function(id){
    $("#contain_"+id+" .TW")
        .find(".icons")
        .find(".tw-like")
        .css("color", "#AAB8C2")
        .parent()
        .attr('onclick',"FavTweet('"+ id  + "')");
});

function ReTweet(id) {
    social.emit('TW_Retweet',id);
}

social.on("TW_RetweetSuccesful",function(id){
    $("#contain_"+id+" .TW").find(".icons").find(".tw-retweet").css("color", "#19CF86");
});

function commentTwitter(id,text) {
    social.emit('TW_Reply',id,text);
}

function DelTweet(id){
    r = confirm("Are you sure you want to delete this Tweet?");
    if(r){
        social.emit("TW_Delete",id);
    }
}

social.on("TW_Deleted",function(tweet){
    $("#contain_"+tweet.id_str).remove();
   delete TWFeed[tweet.id_str];
});

function postTW(obj,start,id,approve) {
    var file = obj.find("#AttachFile")[0].files[0];
    var text = obj.find("textarea").val();
    var stream = ss.createStream();
    var data = {text:text};
    if(start) {
        data.dateTime = start.toISOString();
    }
    if(id){
        data.oldId       = id;
    }
    // upload a file to the server.
    if(file){
        data.size = file.size;
        ss(social).emit('TW_Post',stream, data,approve);
        ss.createBlobReadStream(file).pipe(stream);
    }else{
        data.size = 0;
        ss(social).emit('TW_Post',0, data,approve);
    }
}

function ShowReply(id) {
    $("div#" + id + ".twpost .comment").toggle();
}

social.on('TW_CommentSuccesful',function(res_id,id){
    if(env == "Suggesstion"){
        globalReplyIds.pass = globalReplyIds.pass + 1;
        globalReplyIds.processing = globalReplyIds.processing -1;
        dialogInstance.getModalBody().find("tbody#geoPrompt tr#prompt_"+id).css("background","#dff0d8")
            .find("td.geoReplyStatus").text("Sent!");
        if(!globalReplyIds.processing){
            dialogInstance.enableButtons(true);
            dialogInstance.setClosable(true);
            $("h1.geoPromptHead").text("All Done!!!");
            var message;
            if(!globalReplyIds.fail){
                dialogInstance.setType(BootstrapDialog.TYPE_SUCCESS);
                dialogInstance.setTitle("Sent");
                message =  globalReplyIds.pass + ' Customers Reached Out Successfully.';
            } else if(!globalReplyIds.pass) {
                dialogInstance.setType(BootstrapDialog.TYPE_DANGER);
                dialogInstance.setTitle("All Failed");
                message = globalReplyIds.fail + ' Customers could not be reached!';
            } else {
                dialogInstance.setType(BootstrapDialog.TYPE_WARNING);
                dialogInstance.setTitle("Some Failed");
                message = globalReplyIds.pass + ' Customers Reached Out Successfully.' +
                    '\n' + globalReplyIds.fail + ' Customers could not be reached!';
            }
            $("p.geoPromptSummary").text(message);
        }
        return ;
    }
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS,
        title: 'Success!',
        message: 'Commented Successfully'
    });
    $("#contain_"+id).find(".TW").find(".comment-area").val("@" + TWFeed[id].user.screen_name + " ");
    /*$("#contain_"+id).find(".TW").find(".comment-msg-success").show("slow").css("opacity",1);
    setTimeout(function(){
        $("#contain_"+id).find(".TW").find(".comment-msg-success").hide("slow");
    },2000);*/
});
social.on('TW_CommentUnsuccesful',function(res_id,id){
    if(env == "Suggesstion"){
        globalReplyIds.fail = globalReplyIds.fail + 1;
        globalReplyIds.processing = globalReplyIds.processing -1;
        dialogInstance.getModalBody().find("tbody#geoPrompt tr#prompt_"+id).css("background","#f2dede")
            .find("td.geoReplyStatus").text("Failed!");
        if(!globalReplyIds.processing){
            $("h1.geoPromptHead").text("All Done!!!");
            var message;
            if(!globalReplyIds.fail){
                dialogInstance.setType(BootstrapDialog.TYPE_SUCCESS);
                dialogInstance.setTitle("Sent");
                message =  globalReplyIds.pass + ' Customers Reached Out Successfully.';
            } else if(!globalReplyIds.pass) {
                dialogInstance.setType(BootstrapDialog.TYPE_DANGER);
                dialogInstance.setTitle("All Failed");
                message = globalReplyIds.fail + ' Customers could not be reached!';
            } else {
                dialogInstance.setType(BootstrapDialog.TYPE_WARNING);
                dialogInstance.setTitle("Some Failed");
                message = globalReplyIds.pass + ' Customers Reached Out Successfully.' +
                    '\n' + globalReplyIds.fail + ' Customers could not be reached!';
            }
            $("p.geoPromptSummary").text(message);
        }
        return;
    }
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Error!',
        message: 'Comment Failed'
    });
    //$("#container_"+id).find(".TW").find(".comment-area").val("@" + TWFeed[id].user.screen_name + " ");
    //$("#contain_"+id).find(".TW").find(".comment-msg-fail").show("slow");
});

social.on("Retweets",makeRetweetOverlay);

function getTWRetweetsPost(id){
    if((!Retweets[id])||(!Object.keys(Retweets[id]).length)) {
        social.emit("getRetweets",id);
    }else {
        delete Retweets[id].max_id;
        delete Retweets[id].minDate;
        var retweets = Object.keys(Retweets[id])
            .map(function (d) {
                if ((d != "max_id") && (d != "minDate")) return Retweets[id][d]
            })
            .filter(function(d){
                return d
            });
        makeRetweetOverlay(id, retweets);
    }
}

function loadmoreRetweets(id){
    social.emit("getRetweets",id,Retweets[id].max_id);
}

function getTWFavsPost(id){
    console.log("getTWlikesPost : ",id);
}

function makeRetweetOverlay(id,retweets){

    console.log("id - ", id);
    console.log("Retweets - ",retweets);
    var LastMaxId = null;
    if(Retweets[id]){
        LastMaxId = Retweets[id].max_id;
    }
    if(! Retweets[id]){
        Retweets[id] = {};
        if(retweets[0]) {
            Retweets[id].minDate = moment(retweets[0].created_at);
            Retweets[id].max_id = retweets[0].id_str;
        }
    }
    if(!$(".viewer").find("#previewRetweets").length){
        showOverlay();
        $(".viewer").append(
            "<br/>" +
            "<br/>" +
            "<div class='row' style='  max-height: 70%;overflow: auto;margin-right: 10px;margin-left: 10px;'>" +
            "<div id='previewRetweets' class='col-md-12' >" +
            "</div>" +
            "</div>"
        );
    }
    if(!retweets[0]) {
        return $(".viewer").append("<br/><h3 class='text-center'>No retweets</h3>")
    }
    $(".viewer").find("#more_"+ id).remove();
    retweets.forEach(function(d){
        var time = moment(d.created_at);
        if(!Retweets[id].minDate) {
            Retweets[id].minDate = moment(d.created_at);
            Retweets[id].max_id = d.id_str;
        }
        if(Retweets[id].minDate > time) {
            Retweets[id].minDate = moment(d.created_at);
            Retweets[id].max_id = d.id_str;
        }
        Retweets[id][d.id_str] = d;
        if(!$("#preview_"+ d.user.id_str).length){
            var before =   '<div id="preview_'+ d.user.id_str +'" class="media">'+
                '<div class="media-left">'+
                '<a href="#">'+
                '<img class="media-object previewRetweetImage" src="'+d.user.profile_image_url_https+'" alt="...">'+
                '</a>'+
                '</div>'+
                '<div class="media-body">'+
                '<h4 class="media-heading">'+ d.user.name +'</h4>';

            var middle = '<p class="previewRetweetText">' +
                '<strong>'+ d.text +'</strong></p>' +
                (!d.user.following?
                "<button id='" + d.user.id_str+ "' class='pull-right btn follow' " +
                "onclick='follow(\""+ d.user.id_str+"\",\"TW\")'>" +
                "<i class='fa fa-user-plus'></i>" +
                " Follow</button>"
                    :
                "<button id='" + d.user.id_str+ "' class='pull-right btn following' " +
                "onclick='unfollow(\""+ d.user.id_str+"\",\"TW\")'>" +
                "Following</button>");

            var after = '</div></div>';
            $(".viewer").find("#previewRetweets").append(before+middle+after);
        }

    });

    if(LastMaxId ==  Retweets[id].max_id) {
        console.log("No More : ",LastMaxId,Retweets[id].max_id);
    }else {
        $(".viewer").append(
            "<br />" +
            "<div id='more_"+ id +"' class='col-md-12 text-center'>" +
            "<a onclick='loadmoreRetweets(\""+id+"\")'>" +
            "Load More" +
            "</a>" +
            "</div>"
        );
    }
}

var follow = function (id,network) {
    console.log("id", id, " network",network);
    switch(network){
        case 'IT': social.emit("it_follow",id);
            break;
        case 'TW': social.emit("TW_follow",id);
            break;
    }
};

var unfollow = function (id,network) {
    console.log("id", id, " network",network);
    switch(network){
        case 'IT': social.emit("it_unfollow",id);
            break;
        case 'TW': social.emit("TW_unfollow",id);
            break;
    }
};

social.on("TW_FollowSuccesful",function(id){
    $("#"+id+".btn.follow")
        .addClass("following")
        .removeClass("follow")
        .text("Following")
        .attr('onclick','unfollow(\'' + id + '\',\'TW\')');
});

social.on("TW_UnFollowSuccesful",function(id){
    $("#"+id+".btn.following")
        .addClass("follow")
        .removeClass("following")
        .empty()
        .append("<i class='fa fa-user-plus'></i>" +
        " Follow")
        .attr('onclick','follow(\'' + id + '\',\'TW\')');;
});
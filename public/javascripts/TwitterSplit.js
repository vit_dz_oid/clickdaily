﻿social.on("TW_MeFeed",function(timeline){
    $("#group2 .AddButton").remove();
    timeline.forEach(function (d, i) {
        d.CommonCreateDate = new Date(d.created_at);
        Create_Tw_Post(d, "group2");
    });
    if(timeline[timeline.length-1])
        makeLoadMore("group2","TW",timeline[timeline.length-1].id_str);
    $('.icon').find('a').tooltip({
        'placement': 'top'
    });
    updateFeeds();
});
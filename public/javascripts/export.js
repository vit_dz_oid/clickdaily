/**
 * Created by Sundeep on 1/12/2015.
 */

$(function(){
   $(".print").click(function(){
       ExportPrint();
   });
    $(".pdfPrint").click(function() {
        ExportPdf();
    });
});

function ExportPrint() {
    ("#container").printArea({extraCss: "", extraHead: "", mode: "iframe", popClose: false, retainAttr: [ "class", "id", "style"]});
   // $("#" + id).printArea({ contentHeader: document.getElementById(contentHeader).innerHTML, contentFooter: document.getElementById(contentFooter).innerHTML, filter: filterHtml });
};
function ExportPdf() {

    var pdf = new jsPDF('p', 'pt', 'letter')

// source can be HTML-formatted string, or a reference
// to an actual DOM element from which the text will be scraped.
        , source = $('#container')[0].innerHTML;

    source = "<img src='/images/Clickdaily-logo_no_background.png', width='130'><br><br>\n"+$("#set H2")[0].outerHTML+"<br>" + source + "<br><div style='text-align: center'><span>&copy; 2015 Clickdaily</span></div>";

// we support special element handlers. Register them with jQuery-style
// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
// There is no support for any other type of selectors
// (class, of compound) at this time.
    var specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#update': function(element, renderer){
                return true;
            },
            '#set': function(element, renderer){
                return true;
            },
            '#box': function(element, renderer){
                return true;
            }
        }

    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source // HTML string or DOM elem ref.
        , margins.left // x coord
        , margins.top // y coord
        , {
            'width': margins.width // max width of content on PDF
            , 'elementHandlers': specialElementHandlers
        },
        function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            pdf.save('x.pdf');
        },
        margins
    )
    /*var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
    var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
    var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
    var is_safari = navigator.userAgent.indexOf("Safari") > -1;
    var is_Opera = navigator.userAgent.indexOf("Presto") > -1;

    if (is_safari) {
        ExportPrint();
        return;
    }
    var iframe = $("#container").getPrintAreaWindow({extraCss: "", extraHead: "", mode: "iframe", popClose: false, retainAttr: [ "class", "id", "style"]});
    var doc = new jsPDF();
    var a4Whids = 210;
    html2canvas($(iframe.doc).find("body"), {
        onrendered: function (canvas) {
            doc.addImage(convertCanvasToImage(canvas), "image/png", 0, 0, a4Whids, 0);
            doc.save(fileName);
        }
    });*/
};

function SendEmail(id, contentHeader, contentFooter, fileName, filterHtml, reportName) {
    var iframe = $("#" + id).getPrintAreaWindow({ contentHeader: document.getElementById(contentHeader).innerHTML, contentFooter: document.getElementById(contentFooter).innerHTML, filter: filterHtml });
    var doc = new jsPDF();
    var a4Whids = 210;
    html2canvas($(iframe.doc).find("body"), {
        onrendered: function (canvas) {
            $("body").spin();
            doc.addImage(convertCanvasToImage(canvas), "image/png", 0, 0, a4Whids, 0);
            var data = doc.output();

            var formData = new FormData();
            formData.append("Subject", reportName);
            formData.append("File", data);
            formData.append("FileName", fileName);
            $.ajax({
                type: "POST",
                url: ClickDaily.SMTPSendEmailMethodUrl,
                contentType: false,
                cache: false,
                processData: false,
                data: formData,
                success: function (response) {
                    ClickDaily.PNotify.ShowSuccessOrError(response.IsSuccess, response.Message);
                    $("body").spin();
                }
            });
        }
    });
};

function convertCanvasToImage(canvas) {
    var image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
}

function ExportTablesToCsv(id, fileName) {
    $.downloadStringAsFile(fileName, GettableResult(id, ","), "UTF-8", "text/plain");
};
function ExportTablesToTxt(id, fileName) {
    var result = GettableResult(id, "|");
    var formatedResult = "";
    result.split("\n").map(function (str) {
        var formatedStr = "";
        str.split("|").map(function (item) {
            while (item.length < 25) {
                item += " ";
            }
            formatedStr += item + "|";
        });
        formatedResult += formatedStr.slice(0, -1) + "\n";
    });
    $.downloadStringAsFile(fileName, formatedResult, "UTF-8", "text/plain");
};
function GettableResult(id, separator) {
    var result;
    if ($("#" + id).is("table")) {
        var $table = $("#" + id);
        if (result) {
            result += $table.table2CSV({ delivery: 'value', separator: separator });
        } else {
            result = $table.table2CSV({ delivery: 'value', separator: separator });
        }
    } else {
        $("#" + id).find("table").each(function () {
            var $table = $(this);
            if (result) {
                result += $table.table2CSV({ delivery: 'value', separator: separator });
            } else {
                result = $table.table2CSV({ delivery: 'value', separator: separator });
            }
            result += "\n";
        });
    }
    return result;
};

function GetFilter(filterContainerId) {
    var filter = $("#" + filterContainerId + " form").serialize();
    var dicrionary = new Object();
    filter.split("&").map(function (keyValue) {
        var keyValueArray = keyValue.split("=");
        if (keyValueArray[0] == "SelectedBUIds") {
            keyValueArray[1] = $("label[for='BusinessUnitsIds_" + keyValueArray[1] + "']").text();
        }
        if (dicrionary[keyValueArray[0]]) {
            dicrionary[keyValueArray[0]] += "; " + decodeURIComponent(keyValueArray[1]);
        } else {
            dicrionary[keyValueArray[0]] = decodeURIComponent(keyValueArray[1]);
        }
    });
    return dicrionary;
}

function FilterToHtml(filterDictionaty) {
    var html = "<div class='filterContainer'>";
    $.each(filterDictionaty, function (key, value) {
        html += "<div><label>" + value + "</label></div>";
    });
    html += "</div>";
    return html;
}


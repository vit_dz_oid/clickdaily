//console.time("ScriptStart");
var Chance = require('chance');
var uuid = require('node-uuid');
var call = require("call-n-times");

// Instantiate Chance so it can be used
var chance = new Chance();


var n = 1000;

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017');

var bcrypt   = require('bcrypt-nodejs');

var schemaOptions = {
    toObject: {
        virtuals: true
    }
    ,toJSON: {
        virtuals: true
    }
};

var FB_pageInfoSchema = mongoose.Schema({
    _id              : String,
    accountIds       : [String]
}, { _id: false });

var ITorTWorFWAccSchema = mongoose.Schema({
    token         : String,
    token_secret : String,
    id           : String
});

var ITorTWorFWSchema = mongoose.Schema({
    _id              : String,
    accountIds       : [ITorTWorFWAccSchema]
}, { _id: false });

var locationSchema = mongoose.Schema({
    _id                  : String,
    name                 : String,
    permissions         : {
        salesReport     : {
            read        : { type: Boolean, default: false}
        },
        Inventory       : {
            read        : { type: Boolean, default: false}
        },
        POSManagement   : {
            read        : { type: Boolean, default: false}
        },
        userManagement  : {
            readYours   : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , create    : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
        },
        schedules       : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , readYours : { type: Boolean, default: false}
        },
        timeClocking    : {
            createYours : { type: Boolean, default: false}
            , createAll : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , readAll   : { type: Boolean, default: false}
            , readYours : { type: Boolean, default: false}
        },
        logbook         : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , read      : { type: Boolean, default: false}
        },
        socialMedia     : {
            read        : { type: Boolean, default: false}
        },
        reservation     : {
            create      : { type: Boolean, default: false}
            , edit      : { type: Boolean, default: false}
            , read      : { type: Boolean, default: false}
        }
    }
}, { _id: false });

// define the schema for our user model
var userSchema = mongoose.Schema({

    clickdaily              : {
        username            : { type: String, required: true},
        email               : String,
        password            : { type: String, required: true},
        phonenumber         : String,
        dob                 : Date,
        firstname           : { type: String, required: true},
        lastname            : { type: String, required: true},
        middlename          :  String,
        gender              : { type: Number
            , min: 0
            , max: 2
            , required: true
        }, //0: Male, 1: Female, 2: Other
        martialstatus       : { type: Number
            , min: 0
            , max: 4
            , required: true
        }, //0: Single, 1: Married, 2:Seperated, 3:Divorced, 4: Widowed
        address             :  {
            line1           : String,
            line2           : String,
            line3           : String,
            city            : String,
            state           : String,
            zip             : Number,
            country         : String
        },
        homephone           : String,
        emergencycontact    : {
            name            : String,
            relationship    : String,
            phone           : String
        },
        creator             : String,
        jobs                : [String],
        posUser             : String,
        lastLocationId      : String,
        locations           : [locationSchema],
        userType            : {
            type: Number
            , min: 1
            , max: 2
            , default : 2
        },
        firstLogin          :  { type: Boolean, default: true}
    }
},schemaOptions);

userSchema.virtual('clickdaily.lastLocation').get(function () {
    return this.clickdaily.locations.id(this.clickdaily.lastLocationId);
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.clickdaily.password);
};

if (!userSchema.options.toObject) userSchema.options.toObject = {};
userSchema.options.toObject.hide = 'password';
userSchema.options.toObject.transform = function (doc, ret, options) {
    if (options.hide) {
        options.hide.split(' ').forEach(function (prop) {
            delete ret[prop];
        });
    }
    if (options.show) {
        for(key in ret.clickdaily){
            if(options.show.indexOf(key)===-1){
                delete ret.clickdaily[key];
            }
        }
    }
};
// create the user

var User  = mongoose.model('User', userSchema);

var Location  = mongoose.model('Location', locationSchema);

var times = [];

var createUser = function(){

    //var start              = (new Date).getTime();
	var newLoc 			   = new Location();

	newLoc._id                             		= "9e527d10e9064aa3a6c2aa8774b43178";
    newLoc.name                                 = "Felix";
    newLoc.permissions.salesReport.read   		= true;
    newLoc.permissions.Inventory.read        	= true;
    newLoc.permissions.POSManagement.read       = true;
    newLoc.permissions.userManagement.readYours = true;
    newLoc.permissions.userManagement.readAll   = true;
    newLoc.permissions.userManagement.create    = true;
    newLoc.permissions.userManagement.edit      = true;
    newLoc.permissions.schedules.create      	= true;
    newLoc.permissions.schedules.edit      		= true;
    newLoc.permissions.schedules.readAll   		= true;
    newLoc.permissions.schedules.readYours      = true;
    newLoc.permissions.timeClocking.createYours = true;
    newLoc.permissions.timeClocking.createAll 	= true;
    newLoc.permissions.timeClocking.edit      	= true;
    newLoc.permissions.timeClocking.readAll   	= true;
    newLoc.permissions.timeClocking.readYours 	= true;
    newLoc.permissions.logbook.create      		= true;
    newLoc.permissions.logbook.edit      		= true;
    newLoc.permissions.logbook.read      		= true;
    newLoc.permissions.socialMedia.read        	= true;
    newLoc.permissions.reservation.create      	= true;
    newLoc.permissions.reservation.edit      	= true;
    newLoc.permissions.reservation.read      	= true;


    var newUser                = new User();

    newUser.clickdaily.username                 = "sundeepnarang";//uuid.v1();
    newUser.clickdaily.email                    = "sundeepnrng@gmail.com";//chance.email();
    newUser.clickdaily.password                 = newUser.generateHash("testpass"/*chance.string()*/);
    newUser.clickdaily.phonenumber              = "3475647945"//chance.phone();
    newUser.clickdaily.dob                      = chance.birthday();
    newUser.clickdaily.firstname                = "Sundeep";//chance.first();
    newUser.clickdaily.lastname                 = "Narang";//chance.last();
    newUser.clickdaily.lastLocationId           = newLoc._id;
    newUser.clickdaily.locations.push(newLoc);
    newLoc._id                                  = "d769129267cd4dd7936e1419334bb81f";
    newLoc.name                                 = "Barbes";
    newUser.clickdaily.locations.push(newLoc);
    newLoc._id                                  = "dc0987c11d6042ecb72d7eae3dd08ff7";
    newLoc.name                                 = "Test8";
    newUser.clickdaily.locations.push(newLoc);
    newUser.clickdaily.userType                 = 1;
    newUser.clickdaily.gender                   = 0;
    newUser.clickdaily.martialstatus            = 0;





    // save the user
    newUser.save(function(err) {
        if (err)
            throw err;
        /*times.push((new Date).getTime() - start);
        console.log(times.length);
        if(times.length == n){
            console.timeEnd("ScriptStart");
            console.log("Username", newUser.clickdaily.username);
        }*/
    });
};

//call(createUser, n);

/*function find(){
    console.time("FindStart")
    User.findOne({ 'clickdaily.username' :  " 07de92d0-9c3e-11e4-a22f-e13f79402b36" }, function(err, user) {
        if(err) console.error(err);
        console.dir(user);
        console.timeEnd("FindStart")
    });

}

find();*/

createUser();